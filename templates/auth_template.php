<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?= BASE_URL ?>">
    <title><?= PROJECT_NAME ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="vendor/components/bootstrap/css/bootstrap.min.css?<?= COMMIT_HASH ?>">
    <link rel="stylesheet" href="vendor/components/bootstrap/css/bootstrap-theme.min.css?<?= COMMIT_HASH ?>">
    <link href="assets/css/main.css?<?= COMMIT_HASH ?>" rel="stylesheet">
    <script src="vendor/components/jquery/jquery.min.js?<?= COMMIT_HASH ?>"></script>
    <link rel="stylesheet" type="text/css"
          href="vendor/components/font-awesome/css/font-awesome.min.css?<?= COMMIT_HASH ?>">
    <script src="vendor/components/bootstrap/js/bootstrap.min.js?<?= COMMIT_HASH ?>"></script>
    <script src="vendor/components/jqueryui/jquery-ui.min.js?<?= COMMIT_HASH ?>"></script>
    <script src="assets/js/app.js?<?= COMMIT_HASH ?>"></script>
    <link rel="stylesheet" href="assets/css/app.css?<?= COMMIT_HASH ?>">
    <script src="assets/js/sweetalert-2.1.0.js"></script>
    <script src="assets/js/main.js?<?= COMMIT_HASH ?>"></script>

    <style>
        body {
            margin: 0;
            padding-top: 50px;
            background-color: #834eb6;
        }

        .auth-background {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            opacity: 0.2;
            background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' width='190' height='100'%3E%3Cimage width='120' height='80' xlink:href='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJIAAABCCAYAAAC8RY+hAAAbUklEQVR42u1dZ3iVxdYlkJBAGikWLkUBKUoVLxdBpCpNikpREQQBEQUVRAUVKzYEREUQpHeUJlV6Cb33UEMCCSG00KUm3LXy7Mk3GeacnOQA14+cH/sJnLxn3ilr9l67zCTbhYuXsnnkHy2+kHmQGMiHkPB/Uv9u3LiRIp6F+udLZcgVyA2RwZCcHiDd3ZL7NrQ5SAMRZTuk2D9lzL55CqWIZ/FvjTwN6Q0ZD+kDefgWtfsvyF4DSOshD96hcQV7gHRnxAfyFeS4sdirIUVuQfuvQ67pbSeeOTdi/qLlfrd5XGy/J2Q+pBnEywOkzEkg5DPIBMgrMrG25zpArhsgUvKSm30gD1qpt3n+wt9ntu3Y3RoLF3Cbx/8s5JK8dxnEWz6vB1kKeUd9dqeBlFdePhryh9j9Kv9gILWHXJWJPAvp5uC5WQ5ARHnOzT40gZzX2zyVeGZxnUYv/wsLlweSw422aRrrQBpAilt+30d771LhZNw0UfLZ35DvyQndARIHkD0DO7s75LCpoiGnIe2cqc105H7Io7fBHfaHzDT6OsrBs9/bQHTm7IUF+w7E5HOjD5yTaYY2Oh+550AHLFr4oN/GFDt1+iy1w3DRWj1krtNbixcllHBSwEA5AfkVEqQ921t7N59JgFw2xklN/GxmgRQG6SfqPj0APGRMBl+8TjRSgnx2CFIoExPNfswW9RshKtfrFgGpGiRO6zc3wAfGRCspCtmpPZt8+uz59fMWLqvmptaopc2R0kaLXmrdqQDA9Ma58xd3GYuaLGDI4WDj14essoD+uvbzG8O0nXKibZXQymTPKJBos7+WBpZDQp08W8Lo+Hkhb8q2f6BpqIaZJKGXtPbXOeExrgjt/QOQmgL0NIsEbXAMP+eIKUizWLt27//PseOnJkJDRBw5evzngUNGl6bWEPG1aJpg0aKOQgXU9j8b2ijxwMFD78UeSeirgZthgIvac0ch/7E4A10hF4wxnRIn4U9Ikny2Q+NCBN4JvPciNsYu/Dxngoh8je/LjEaiS7tfa6yJEz4UoT1HlfgOXkbk5oIEYkK6aqryxQwuenZT7cv/Mwui4mKitrqwA0/LJOteS5AGHFP0wGElyKeQ34Vb0Sw9b9EiJSEHDC2368TJ09OVqYFGeh8/qxqak6Bqa7T1tsUZoFlrRm2JdvuLNuPnezRwf8vPEo6dHDFt5rzKeJ8ZgrgRF3+sT+2GLYLRjldGgfSEQf52WwJj1DhjjJcOEwCFcnK79/zmIezeJYLqS/ujYiplcOFry+7Td8ciAeS9GWyLi7bRBQDp0lfATEC9iQntMGnyzAoWEBFgfC6fADXO0hY1RSujTx9aNGKimq+Tp05zE2bD4nY2eMvFs+cutNPaqSnA19vi97mpgwYPG1cKXC5S43VLCQr8O0Q8tRvrNm5tQi8R771scMC9YydOK682S0aBVFk8GL1jjDMU0J55Q/N2KCfmL1peTk1uo6av5sPOGql2Cez+gvZvvp/fBS5RUIj5CAGwbYH53r8gpTIwpt9dAQ8W7RRks/DD+uJ1JqrfY3EmGSAiP/IWZ2CZpc1k7d8RmiYg6DY4Gh9MZ08u9qYtOx/SQSB9PLZ5666q0s49Qj/StIEN/Ae9vtB8Je+BOR6n9SMJpvMDtO0vfPMCwBO7YPGKqtBaZn+uR8fEfaiNNVdGgUTyvM0yQAbfnoE8LuQ59XdQjd+pF86YvbASBj9Zs8lXgPimFhNgSnPIZvEeXNEYI1xMVZA8xzprK/HMuXWY4PdXrt5QDzuwLEB/Hya2n/kcPnuTsR0IVb2/aKIKwmP0Z49AJspP9dlOoQPsU2sHcalkzOVvzVt2DIAEAwQTDDByU84vX6nuvQLgd8x2ALSYP6bOJqcJPxx39Ds9f8dxdnyrx4MVqtQnAH+QMf26e29Ue/IkY6wbfxo4vKSsWxjfl1Eg5QAQPnMw6acsi3JpR+S+Nlu27WqBSRiDDh02dsd3JcpWvdcBKdU9qJMZND2jHXhYNnf4oEz4EQshvc7+65pm6/bIlzGOK8Zz1Di5jLYLiwOQRqNgETodjI6tL0RVmeUNQgloUqbbxoS5Wtmhc/e89Z5tFUpCb2h9tnF69brNjbiwi5etKmjRgldiDh35smzFp+/HWgzTxwCARS1csrIWxzd3/tIKwpVuQOu8BOvxhwHYJHz+sTYnmYsj7YzcVxCDWurigl4nmk37Khpp5NffDyioo9rBOyem844k8ILD2I2zqf3AVxpisQM1Uu4jZRi+8m+z/QYYT0N4Xs1pvgwesEf3wN77sFcRTKwZgEyGdmBsx1czzz4CZlO7/f5A8YrUBt/on8MszpSN9KThiaqFPvrnrAVPDB89qYwQblNjXYs5fOQrNZfxCcdfMNtBG8fh4PRBH9YY5nVdxKr1NdUYMX+Kn0Xs2LW3MbmQoY12DB05QVGVEI45U0DCF3ymz5pfEZO/wklqwJmQY/Xr/G7Pe9gZ2utZcxeR9FXH502FfxTVAp7rLDEPXZ3/NWbC1Ee509RkjP/9z+LoH03tJyT6EJrTqZAhkDYGp+OYArkQZttYkIESRU5pF2a4oakJ0O99Xd7/vLA8owDcwtRuWMg4EvLePwwqQdOg/45BxiIlK4cIib9pzrBBRpOToA2bGb4MDfVL1aebpPQzLH+pMPSpd3qbT5yVfpirQmp8nEds+p0C7rf37o/uaJi1JL5L50aZTpEIAvN89d1PxdBof2iDg5aItU2oyldiR9DtDOTAyY+wWL+hs9HGs4zZvCpg4qQsx+SPxETuM9uFJmmvBsadQkBwcdPpC3nLUzIeL2idEhjHIcNUXBFTwbaD23bsFohnBpht4X298hf9tzLPfmKiZpvPQQv15nMARE9jIywgWEeOnfyIja/RZJmaUvtdHMzVZ7rpHTVuclFJaZha7QT6H415XIl/MxZYRsxS6nexnoq27Fu9dlMZrM1g0+EAyW+uORPZ3cq16R2ge+ggWHUJLz5Mxg8Zd/xEYotv+w6k1gjlgmOXDcczpgd4WdM6pyW45ge7HUa1zvaMgcUOGDyqFPsBMlwfIF1vtBcvjsB6i8mIlsn0wuK+ZeEkEdwsGn8rLN9JE5BjjEWBTVznWqZnS2/qmz6/FKcQOPjeKcg+zMmEIcPHl+X3o6IPv26Zw0TZaKaTEYOxjqIDYHiKYdAkjEudM9q5DEryKs3jr0PH3idrGCCUIuW7y1euq6EcAGzEz2m2xOro2jeSWkvTRr7uAim7TFw4FuEjU90T/fB0ui9bsbaecIww1emZcxY+yRSCZYdNgnQ0Fus9ZXqwo22mZzDdWJDLp+mRGLkhEtLq4g09gN93spBpxmO8sVMX3KRp/m+nh8jOa2M+A74ypUmLDvk1bUSe96XFNA3nM3wWjgf7VAf8o5IeEsEiLbGM7zdqAGPDJePzj23Bz/4DhtHjWmgB5CnG7iSOl1utnZIp0+eWwTNqTTbC5S/0SvsuBdGn7QbHW620kQREJ7mTa0s1ceBKpbEIMWbHsdMm2gYKe/wYQLTJAqKJmLBQaLf8GFC89jmJX7YRY/4obpI+eiE0PTLgzUYs6YtPe/X1lkAZJy6gRZvO4Wj7qNEGVfkjejxINkLC0og1daXf/uAwfjZvan9UTFcNbF4OYkDJmjkIFWqQXUxDyryQI1m8sDPzF0XUWLN+y7OGRkoGyD81NZHEfzqYIQFp6xzn3rImoWs3bCUvUh7eUWhMEu+QTl17FsLarjLm5QhDIdB6/bVAKD3q2m6VkYitTTY7LfUyaTrNBQfAJlkGuRLqsgA7jwn6RGvvOrwMFlRlw8+2WuxJ7Y419KL2HYh522hzxdQZf+WTxU15NycFbX9hLhZU97tSHnLN8KJmtXvjvQdkgeiBlTZjWPSkJDGbSjohLc228Fz85GlzKipXWTZhLn1z4ZmDFm03lQ6EBUj83XRNE4bK+8trsamrxnwx2Pi+PBsiGik3QMOY1S6NSjRRfQsvUDoU/frRRlksimCOO0AqbCkB5eIs1bhFmHQ+D0hpO3OSueswUc8ojkOTqLmZW8iLJCwwx0JyU9xdvG+eMckjdAAPGzWxJNpaZoYg8P8L23fueR4/F5seDZOjGu/JLtFs01tcBK1XTMaYQ1IzG20BzY8+611U5iG7CFMnYV/1/rkwtOl8y2KdZ+xNuEttPeYkcxMJT7ei8Bwfifqvlt/Pwjv7mBucPA0b5F3MV138m3xsgZbwZZXBcxJYDNVAX8KWYxOQLtfygSfdAdLnFjV6VZCvdoofO3cwOpa2e4vFk/mWz85ftLw6JlRPe1zbvTeKkeLwDZu2lzG5DW2+IprmbmY8SUIBKer+UGz8Y8YJDMVbxixauuoppgHMHBJJqaZpHpRaqjTfZxZeufyr1mz0Mep39I21jBxIVTRKgtMbQHkQ755h/Q5AKpwmHH0polxy3bxhw/SUgCrzlCo2FAeQPU7KQbBZ2r4mTsc1I19aS3njukfGvq5YtaEa+jOH5p5gRLurJbzhg3+r9NK5zALpISk5MNMEW7TQuV5k1dpS+BVJlc9IsYQQUj23uPhjvf9TtUFetiMTlmTGjhi7YQiBLrDpLtNDUWZEPItZAsZL+P1Jmi5qTTGLaVQ1y0G0jeAt2ijJ3JFaxJv86WVpJ1lAm6zHj2bMXlhFSwFxXmoY8bFkB3mslEAt5meggxKO9domo8Z9UzZv+K7d+1/jhnMSArksJSQlBETB2rh9NNCnfE6tSq9TKYiSj9UMxDqkEHv0bz+/kxkgdbfEjpI0EhimTRwXY4otkEitYGiLowDOR8XKPHmf8nIwGWaa4To9Qv6+Zr0Xwm3mBCDdAVBX0Pp7H7Pi4GjdGLtSQUZ6U8biXIQm7KQy92inhllcpnggPVJOKvrbQOJe/N0EvHesRSsth7l5WTbUOG3xz4mXlcrd0M8DWkghZTPCjJfTzb5FOIdfGqaJYGrPSLSxES7InHVhfZlE5EO0dfM1nCp/C0kPwyZ8SwGVAVPOV0ZBVNCSiKQ22q5xI8Ut1PPHXKjxmXY04URdfSKwM98xCTIn2jA9P9g8FTGlNYxwRWrchMFBpiaMtg+qRYRJZe2V8jATzbgMJzIq+nArrZxlJ8xdfnho9SxxH0e5ydcAsqKqRERpRIY09M1IgFDLkuBb2jms1Xrl0cpXUgAAnvkos/pHjh7/EdIX0gamOK9oLn1OQm2Jc9FWqY7Lr0PHlmFQFX05qfjc6rWbGrCdzGijm8gXJrWHhlg/47xXkrEQf9O0UTViIhktrjtn3pJgvcOMP+G57bbYjWgs5VENdQCkGxIp7iA7Nc2uIo+ClphjbIad5DPwxiri3SoYF4XPu1oixbGaC8yNUIeLR+80PuH4oHRARC37vCLejMUxF0bNonm8QRJSUBshBK768wQa+r2K3AsyCPNYTcaXRzfJBucJt4QL9P+HKHNmk6URa0JBQeqCb36KudBzdTew+T9XnDQjICpiVO6pBdjAEgQzdC7S0cyR0ayRN3zbd2CRDp27+0i8JxVEBYpVuAeTNdwSWrgiNTMpEw0N0lJSL84WjdrhF+ykQlpglJMdhMUbZ/IrAPVPfL5d4xFNv+s36F4mhh20T1PRWkxEmEru0quUtEuSyEUpPe6K8bMvPvpC01SSL0rYQQ8pZNMi0SnBS0bz+/445OFyj9e+T9oI0wDip33H1wIaE1BBRi1YDimhriD1X79AlljSNzSnH7Vq/45KD4XYAJNPyjjrS51RZUmk2pKK1+CBvGQm8jTpZmok7lht1wWYQTJwme6mSVMRc+5KPofd0VBVHLKoDIszzxmgWEG5Y9feRyVO4iO7vYvNo9MDos88/woXI3R/VEw3i+YjuF4sUa6ar2aSAyk0T4wRMQ7EgCS8zCdWr9v8gCxuoLH4wRYNkcNiYkKdgCJcczCyGWBKo5m69fiyEKPZ4IP0+OpKhWZ3uVNgsZSRxBn14DeMQrx6dRu39NfGEZDNqCkeKy7hGSmrPS8cYa+Zw5ESivFa5t3URpTXTI1E9SwucZrd0bxlR2qinrK4F+WdV/WkIb2lvfuj22jBt81YsILcpTzzlY52WiuHElTfQmAaVjjgbF2Gjpzgpxahep2meclT8I65DIaSc2zfuaecLFSonk4RoIals+ipGkRAoj8f4CCb4GuCiYBlaUrLtm8FSU1UoBzzLi8gIbf6CFZjEPo9hcekoCk5D5HiSJwTrX01nbm7JvPXCtr2fr10WtEMVT/zriUX5VRIfLUknsmNlDxp7nq6i0uWr64pEx/0ZpePw8ATKhq1RyybeN1MD0hkVbVHXlFdaTWWaMA0TU6nvGWaXvQG4v4Ic1r0rJioBciHYNLLGS5xiIr4ctMw7CCEONTQLDk1t9k/HRDl0cmtvjDUcLImubSCN8azyqKvtQ5GxzZjkhdg/hTaeADmc7wEGLcJOM4LOK6JNUjORLlPkuCBXt8wALDuT4NGBEg/TQ2aWmrbVDM/xySvNEh+HnZSx3yUrrhEbvMocmhIKBZ/m8VFX4kFa4dJaC0qVS89ZQ2RPwj4wyx9cPD+I5IwTMMDyDEw2UOclLZclZxUai2SvrulrUDNHITJO7x0HmfRLKbb7CUTHGI8Fyyf55DjU2Hi2RaHG/0EqyQwr13lsMAoiYGtFA0Sn44pdlfOyR1M5HK9AdJmEavWF9JKiG2mNbd+iiRCO9eU6jLDZAQL2p0imuo+5tCRyo5IOhb2NUtQz1GQjOQuUGkAmhNqPklxXMFPnrUiH6oqJF3nJinHghjMZAGWEzAtUGfy9LyXA8mlgcPPlvh0Uiacwm2GDB8fCG6XL/ZIwiOYq+py2oWVDQOldmm7hAMu30aQ2GJPxzCXOzC/s3gsCevUbtWajeUrVW8UJhsg1ImJDtY0cGr2X5EqkmDvarWbhIAgNgIip2egCjJWyOtNhO/TXn3ziJZw9n0Wo3Wev2i5t7a4KQNicpSnSxm7gWfTFO0FGXwhj5HHCqf5sSWJtbjQk2b01qJlcjngKUFakX8OywHIQtL+i3KObYycttlhOR50J+QMALOHVgA8aRo03/cwj52Za5w2c95jYflLhbvI6dRc++pcWAfSMfFsYvCSweICn3CQO1rqpAIxWY73lDXLcxlfoUvMbLhZpMXaIeyI8hL/CDRsr59lkGGGRvAxaqUCVSzKyOHpPK2J8R1/AYcCiHcGL2RoDPlYQLNOvJ6/7zBgkiQftoHVA3QIGN+jV/3XgmVVmb7SqgZcFaWdAsQCeDm71uZnVzqJjg1g/GLB4hU1HdQVKWGs6S3t2hMvxTmoXZiQZXQULnVrmK5/V6n1XC7pqG6DA+R72U1uwqj00og1dfbsO9gB5LiJkFGTn/jLsZveNq16NOFEKzdO5t4vN3kQOHPlOFHinQIMzTxjVKwsYEkJTGZ/aus167c0ZgKcFahaPCo8E8AJ1TZUTosn7hBIBQAMhydDsKs3kqvoLjtD78y0OxnwJSGKNTST4OpAAnTka/mgMGbFAYJfMZnKdU0Us5HXMkD/zVt3vWg7xcJTIw6cA0cSKsnZYXK0+6QLLvOtAM0VZvIBmGncFABM5/WbtjWbOuOvStzUrLVSCe5MSqhscpVW8c3oxRdpKiS/+KZ/UWoJgGaZqOYVQP1IeGUvqJIGU5iBl8Tn9XQ8gcloq9HMOQsfovss3lGqduFnPLiHxX0FpLSYg856Fy1dxQ/g/dhC3M9IpeNN34PWa2aeQ2P4AR5hFWdpAeE64RKLGSIHPy/dLpNE8kuzzwoKgGYGDxWApzZjeIVeMTexSma7oWlUUVugeFt+EvNy556ltEBSZoWVcSwRuP/BckFOIqmpeTHuBqjWfpZdb5usSGbH4+KPfUvbzQIy7LKvQYrHy0kS8oqKTjqdnwC3RaCdVHH2MoEO8A8VMOd0cBXxw3JbyppMHrlKT66xDIShDWp1nnpl2Qfr2XltjRtgMbVMoGgZPzFR3qrO6LZdRupCAE2Jv3RGTxTyFEQPlRF2QzZLXMVRp0ubJzlI8A/FxtdwMDkFzRtGsHhRWoms7rbnksj+jy5UK2TGRJ1jGIN5Rh6QJJ+hadKOMt1KDXNbAOMqkLI7cINNt8/L8HZSr3QhH2EG3Y0JH+zC9TM3Vf4BSJ8YpSuqFHiGWcLKnW9cOZND7iz4LaORfRcunkiQdMpPLPA3sgAZBU0em0m6k4Bx+TJSLRobLB0Pdub2ad9JTbyS68B0jDQvHnBB9sJElkzHSwiwHeFmhJ1VhRLse0o8xvVmwpcVkXpiFCa2lHis8bdQ81xkqSzLiJmwZdrGDbOUS+Yjh8zz/xQwd+RWW9kpKfEdkkKWRLDWl1rAhZ0buXd/dGMtmuowhkOwWGJRSuJsZgkmJYKejl6LBC3R1VYunFmhJmZ4hBWYkjLKiHkKkvnzVaD5/3QD8C2/1VZUbipBp7dHz4OBMd6mIUHOZNm5Z6H215KoT/lz7uOGCXV2p7P3hk3bG8hlDsnppFtmM/E7evyUVK3Ad6EvM29VOoIVBzzfxkunXOA7JnB80ovRZEkgSaM5zfoXenYsw2WxPw/98QoVlrRS7euXPzgroTBLPxl6YDu790Z1ptcDcPSXkxwfyJWEZbbt2B2uczj8/xUS3lsAoOu8cY5HhhyFRgxTFZzZGE2WBZK22AEZyN84LOhywMkCzUg3L5hq3LxtoBSi+Qi/yKOATK/SFTObjlzlsWWa7XTc9LC7Tev8T/+EhGinIBcBFZJOgDBN7s5JjCtU/x2Tt5ImSXIHRCy1JRgtBXmmZ+uvVWBmib+ScEdu/hcNklNLioZa6nNyZyIs72NqJlNY03w04UQvd4OH4GMzGSx08q4gVemYFf/cxh3/WyQS88ghATN10iG7myD1Fu6RW8DqL2bN98zZC++6eHeTI1c+kdflOcmYBzsLjXiAdHdIQ0uNeYZM2c7IfW09AMraQCrs4PZdV0EUrU6rWHicnwdAWQNIPhdu/ouLGUlrHOYtdBYQBWZVDpRVgdTQyXmsdDmRkYtLLbn1aKGsB6ROmQ0yxh5J+F6vlRJT5usBS9YEUmEzYetSquP02QjWlWcmruUB0t1Ltltm5PwXa4W0q5BvOlDgkSwKpG49vvSXc20uAen4icTfnR129EgWBRJddBbF8yi2C9rob7nfx+FFDB7JukBKuRuIiVXeaWne6m9cFrpKS8AGe7wzD5BsJ2JTqzZ5eahcg2dezP6JfqOGBxgeINkG6K9XRfYfMOwR1pXTzeefR2ClpXYQINADCjeBpP5xF4qXi6djwqSaIJtHMi93+wC9JD8W4gRIqX/g1yMeIKUnOaTUxHZBZy4PEDxAyqhk1/7YTbB2C6wHDG7KfwFu+UTSHC5SwwAAAABJRU5ErkJggg==' /%3E%3C/svg%3E");
            background-position: center;
            background-repeat: repeat;
        }

        @media (max-width: 639px) {
            body{
                padding-top: 0;
            }
        }

    </style>
</head>

<body>

<div class="auth-background"></div>
<?php require 'templates/partials/login_modal.php'; ?>

<script>
    $('#loginModal').fadeIn();

    $(".send-new-password").on("click", function () {

        var email = $(".forgot-password-email").val();

        // Hide enter email modal
        $('#forgot-password-modal').hide();
        $('body > div.modal-backdrop.fade.in').hide();

        if (email !== '') {
            ajax("welcome/send_new_password", {
                email: email
            }, function (json) {
                show_alert_modal(json.data);
            });
        } else {
            show_alert_modal("Please enter the correct email address");
        }

    });

    $('#loginForm').submit(function () {
        login();
        return false;
    });

    function login() {
        ajax("login", {
            "email": $("#email").val(),
            "password": $("#password").val()
        }, function (json) {
            location.reload();
        }, function (json) {
            $(".login-error div").html(json.data);
            $(".login-error").fadeOut(10).fadeIn();
        })
    }
</script>

<?php require 'system/js_constants.php' ?>

</body>
</html>
