<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?= BASE_URL ?>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.png">

    <title><?= PROJECT_NAME ?></title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/components/bootstrap/css/bootstrap.min.css?<?= COMMIT_HASH ?>" rel="stylesheet">

    <!-- jQuery UI core CSS -->
    <link href="vendor/components/jqueryui/themes/base/jquery-ui.min.css?<?= COMMIT_HASH ?>" rel="stylesheet">

    <!-- Site core CSS -->
    <link href="assets/css/main.css?<?= COMMIT_HASH ?>" rel="stylesheet">

    <style>
        body {
            padding-top: 70px;
        }
    </style>

    <!-- jQuery -->
    <script src="vendor/components/jquery/jquery.min.js?<?= COMMIT_HASH ?>"></script>

    <link rel="stylesheet" type="text/css"
          href="vendor/components/font-awesome/css/font-awesome.min.css?<?= COMMIT_HASH ?>">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css?<?= COMMIT_HASH ?>">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css?<?= COMMIT_HASH ?>">
    <link rel="stylesheet" href="assets/css/app.css?<?= COMMIT_HASH ?>">

    <script src="vendor/components/bootstrap/js/bootstrap.min.js?<?= COMMIT_HASH ?>"></script>
    <script src="vendor/components/jqueryui/jquery-ui.min.js?<?= COMMIT_HASH ?>"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js?<?=COMMIT_HASH?>"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js?<?=COMMIT_HASH?>"></script>
    <![endif]-->

</head>

<body class="<?= $controller == 'welcome' ? 'welcome' : 'not-welcome' ?>">

<div id="offcanvas-full-screen" class="offcanvas-full-screen" data-off-canvas data-transition="overlap">
    <div class="offcanvas-full-screen-inner">
        <button class="close-button" aria-label="Close menu" type="button" data-close>
            <span aria-hidden="true">&times;</span>
        </button>
        <ul class="vertical menu">
            <li>
                <a href="admin/products" class="<?= $action == 'products' ? 'active' : '' ?>">
                    <?= __('Products') ?>
                </a>
            </li>

            <li>
                <a href="admin/prices" class="<?= $action == 'prices' ? 'active' : '' ?>">
                    <?= __('Prices') ?>
                </a>
            </li>

            <li>
                <a href="admin/users" class="<?= $action == 'users' ? 'active' : '' ?>">
                    <?= __('Users') ?>
                </a>
            </li>

            <li>
                <a href="admin/designers" class="<?= $action == 'designers' ? 'active' : '' ?>">
                    <?= __('Designers') ?>
                </a>
            </li>

            <li>
                <a href="admin/orders" class="<?= $action == 'orders' ? 'active' : '' ?>">
                    <?= __('Orders') ?>
                </a>
            </li>

            <li>
                <a href="admin/emails" class="<?= $action == 'emails' ? 'active' : '' ?>">
                    <?= __('Emails') ?>
                </a>
            </li>

            <li>
                <a href="admin/reviews" class="<?= $action == 'reviews' ? 'active' : '' ?>">
                    <?= __('Reviews') ?>
                </a>
            </li>

            <li>
                <a href="admin/pages" class="<?= $action == 'pages' ? 'active' : '' ?>">
                    <?= __('Pages') ?>
                </a>
            </li>

            <li>
                <a href="admin/posts" class="<?= $action == 'posts' ? 'active' : '' ?>">
                    <?= __('Posts') ?>
                </a>
            </li>

            <li>
                <a href="admin/settings" class="<?= $action == 'settings' ? 'active' : '' ?>">
                    <?= __('Settings') ?>
                </a>
            </li>

            <li>
                <a href="admin/translations" class="<?= $action == 'translations' ? 'active' : '' ?>">
                    <?= __('Translations') ?>
                </a>
            </li>
            <?php if ($auth->logged_in): ?>

                <li><a href="logout"><?= __('Log out') ?></a></li>
            <?php else: ?>
                <li><a data-open="loginModal"><?= __('Log in') ?></a></li>
            <?php endif; ?>
        </ul>
    </div>
</div>

<div class="page">
    <div class="off-canvas-wrapper">

        <div class="off-canvas-content" data-off-canvas-content>
            <div>
                <div class="title-bar">
                    <div class="row">
                        <div class="title-bar-left">
                            <a href="<?= BASE_URL ?>" class="logo-mob">
                                <img src="assets/img/svg/<?= $controller == 'welcome' ? 'logotext-white.svg' : 'logotext.svg' ?>">
                            </a>

                            <ul class="menu">
                                <li>
                                    <a href="<?= BASE_URL ?>" class="logo">
                                        <img src="assets/img/svg/<?= $controller == 'welcome' ? 'logotext-white.svg' : 'logotext.svg' ?>">
                                    </a>
                                </li>

                                <li>
                                    <a href="admin/products" class="<?= $action == 'products' ? 'active' : '' ?>">
                                        <?= __('Products') ?>
                                    </a>
                                </li>

                                <li>
                                    <a href="admin/prices" class="<?= $action == 'prices' ? 'active' : '' ?>">
                                        <?= __('Prices') ?>
                                    </a>
                                </li>

                                <li>
                                    <a href="admin/users" class="<?= $action == 'users' ? 'active' : '' ?>">
                                        <?= __('Users') ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="admin/designers" class="<?= $action == 'designers' ? 'active' : '' ?>">
                                        <?= __('Designers') ?>
                                    </a>
                                </li>

                                <li>
                                    <a href="admin/orders" class="<?= $action == 'orders' ? 'active' : '' ?>">
                                        <?= __('Orders') ?>
                                    </a>
                                </li>

                                <li>
                                    <a href="admin/emails" class="<?= $action == 'emails' ? 'active' : '' ?>">
                                        <?= __('Emails') ?>
                                    </a>
                                </li>

                                <li>
                                    <a href="admin/reviews" class="<?= $action == 'reviews' ? 'active' : '' ?>">
                                        <?= __('Reviews') ?>
                                    </a>
                                </li>

                                <li>
                                    <a href="admin/pages" class="<?= $action == 'pages' ? 'active' : '' ?>">
                                        <?= __('Pages') ?>
                                    </a>
                                </li>

                                <li>
                                    <a href="admin/posts" class="<?= $action == 'posts' ? 'active' : '' ?>">
                                        <?= __('Posts') ?>
                                    </a>
                                </li>

                                <li>
                                    <a href="admin/settings" class="<?= $action == 'settings' ? 'active' : '' ?>">
                                        <?= __('Settings') ?>
                                    </a>
                                </li>

                                <li>
                                    <a href="admin/translations" class="<?= $action == 'translations' ? 'active' : '' ?>">
                                        <?= __('Translations') ?>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="title-bar-right">
                            <button class="menu-icon" type="button" data-open="offcanvas-full-screen"></button>
                            <a class="cart-link cart-link-mob" href="<?= BASE_URL ?>">
                                <span class="badge"><?= $shopping_cart_size ?></span>
                                <img src="assets/img/svg/<?= $controller == 'welcome' ? 'cart-white.svg' : 'cart.svg' ?>">
                            </a>

                            <ul class="menu">
                                <li>
                                    <a class="cart-link" href="checkout">
                                        <span class="badge"><?= $shopping_cart_size ?></span>
                                        <img src="assets/img/svg/<?= $controller == 'welcome' ? 'cart-white.svg' : 'cart.svg' ?>">
                                    </a>
                                </li>

                                <?php if ($auth->logged_in): ?>
                                    <li><a href="profile"><?= __('Profile') ?></a></li>
                                    <li><a href="logout"><?= __('Log out') ?></a></li>
                                <?php else: ?>
                                    <li><a href="#" data-open="loginModal"><?= __('Log in') ?></a></li>
                                <?php endif; ?>
                            </ul>
                        </div>

                    </div>
                </div>
            </div> <!-- MENU -->

            <section class="home">

                <!-- Main component for a primary marketing message or call to action -->
                <?php if (!file_exists("views/$controller/{$controller}_$action.php")) error_out('The view <i>views/' . $controller . '/' . $controller . '_' . $action . '.php</i> does not exist. Create that file.'); ?>
                <?php @require "views/$controller/{$controller}_$action.php"; ?>

            </section>

            <footer>
                <div class="row">
                    <div class="column medium-6">
                        <ul class="menu menu-top">
                            <li>
                                <a href="admin/products">Products</a>
                            </li>
                        </ul>
                    </div>
                    <div class="column medium-6">
                        <ul class="menu text-right">
                            <li><a class="social-link" href="#"><img width="25" src="assets/img/svg/fb.svg" alt=""></a>
                            </li>
                            <li style="display:none;"><a class="social-link" href="#"><img width="25"
                                                                                           src="assets/img/svg/in.svg"
                                                                                           alt=""></a>
                            </li>
                        </ul>
                    </div>
                    <span class="monster-icon monster-icon-rotated"></span>
                </div>

                <hr>
                <div class="row">
                    <div class="column medium-6">
                        <ul class="menu menu-bottom">
                            <li><a href="<?= BASE_URL ?>">© <?= date('Y') ?> Letstickers</a></li>
                            <li><a href="privacy"><?= __('Privacy & Terms') ?></a></li>
                            <li><a href="faq"><?= __('FAQ') ?></a></li>
                        </ul>
                    </div>
                    <div class="column medium-6">
                        <p class="text-right">Crafted by CAMO</p>
                    </div>
                </div>
                <div class="row row-mob">
                    <div class="column medium-6 end">
                        <ul class="menu menu-bottom">
                            <li><a class="social-link" href="#"><img width="25" src="assets/img/svg/fb.svg" alt=""></a>
                            </li>
                            <li><a class="social-link" href="#"><img width="25" src="assets/img/svg/in.svg" alt=""></a>
                            </li>
                            <li><p>Crafted by CAMO</p></li>
                        </ul>
                    </div>
                </div>
            </footer> <!-- FOOTER -->

        </div>
    </div>

    <div class="reveal login-reveal" id="loginModal" data-reveal>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="row columns">
            <div class="spacer-s"></div>
            <h4 class="h-bold">Log in</h4>
            <div class="spacer-m"></div>
        </div>

        <form method="post" action="login">

            <div class="row columns">
                <label><span>Email address</span>
                    <input type="email" name="email" placeholder="email" required>
                </label>
            </div>

            <div class="spacer-xs"></div>

            <div class="row columns">
                <label><span>Password</span>
                    <a class="forgot-pwd" data-toggle="modal" href="#forgot-password-modal">Forgot password?</a>
                    <input type="password" name="password" placeholder="****" required>
                </label>
            </div>

            <div class="spacer-s"></div>

            <div class="row columns">
                <button class="button primary full-page" type="submit">Log in</button>
            </div>

        </form> <!-- LOGIN FORM -->

        <div class="row columns">
            <div class="spacer-xs"></div>
            <center>
                <div class="or-devider">or</div>
            </center>
            <div class="spacer-s"></div>
        </div>

        <div class="row">
            <div class="column medium-6">
                <a href="#" class="button small fb-login-button full-page"><span
                            class="fb-icon-white"></span>Log in with Facebook</a>
            </div>
            <div class="column medium-6">
                <a href="#" class="button small google-login-button full-page"><span
                            class="google-icon-white"></span>Log in with google+</a>
            </div>
        </div> <!-- LOGIN WITH Facebook/Google -->

        <div class="row columns">
            <div class="spacer-s"></div>
            <center>
                <a href="#" class="h-bold" data-open="registerModal">
                    <span>or </span>
                    <span class="h-green">create an account</span>
                </a>
            </center>
            <div class="spacer-xs"></div>
        </div> <!-- CREATE NEW ACCOUNT BTN-->

    </div> <!-- Login modal -->

    <div class="reveal register-reveal" id="registerModal" data-reveal>

        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>

        <div class="row columns">
            <div class="spacer-s"></div>
            <h4 class="h-bold"><?= __('Register') ?></h4>
            <div class="spacer-m"></div>
        </div>

        <div class="row columns">
            <label>
                <span><?= __('Name') ?></span>
                <input class="register-name" type="text" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Company name') ?></span>
                <input class="register-company-name" type="text" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Phone') ?></span>
                <input class="register-phone" type="number" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Password') ?></span>
                <input class="register-password" type="password" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Email') ?></span>
                <input class="register-email" type="email" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('State or county') ?></span>
                <input class="register-state" type="text">
            </label>
        </div>

        <div class="row columns">
            <label>
                <span><?= __('Country') ?></span>
                <input class="register-country" type="text" required>
            </label>
        </div>

        <div class="row columns">
            <label>
                <span><?= __('City') ?></span>
                <input class="register-city" type="text" required>
            </label>
        </div>

        <div class="row columns">
            <label>
                <span><?= __('Steet') ?></span>
                <input class="register-street" type="text" required>
            </label>
        </div>

        <div class="row columns">
            <label>
                <span><?= __('Postal code') ?></span>
                <input class="register-postal-code" type="text" required>
            </label>
        </div>

        <div class="spacer-s"></div>

        <div class="row columns">
            <button class="button primary full-page btn-register"><?= __('Register') ?></button>
        </div>

    </div> <!-- Register modal -->

    <div class="modal fade" id="forgot-password-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><?= __('Enter your email below') ?>..</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="email" class="form-control forgot-password-email">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
                    <button type="button"
                            class="btn btn-primary send-new-password"><?= __('Send me a new password') ?></button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- Forgot password modal -->
</div>

<?php require 'templates/partials/alert_modal.php'; ?>
<?php require 'templates/partials/error_modal.php'; ?>
<?php require 'templates/partials/progress_bar_modal.php'; ?>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="assets/js/main.js?<?= COMMIT_HASH ?>"></script>
<script src="assets/js/app.js?<?= COMMIT_HASH ?>"></script>
<script src="assets/js/sweetalert-2.1.0.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    // Default DataTable settings
    $.extend(true, $.fn.dataTable.defaults, {
        "info": false,
        "paging": false,
        "scrollX": true,
        "ordering": false,
        "autoWidth": false,
        "responsive": true,
    });

    $(".send-new-password").on("click", function () {

        var email = $(".forgot-password-email").val();
        if (email !== '') {
            ajax("welcome/send_new_password", {
                email: email
            }, function (json) {
                show_alert_modal(json.data);
            });
        } else {
            show_alert_modal("Please enter the correct email address");
        }

    });

</script>

<?php require 'system/js_constants.php' ?>
</body>
</html>