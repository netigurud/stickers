<h1><?= __('User data') ?></h1>

<ul>
    <li><?= _('Email:') ?> <?= $user_data['user']['user_email'] ?></li>
    <li><?= _('Full name:') ?> <?= $user_data['user']['user_full_name'] ?></li>
    <li><?= _('Company:') ?> <?= $user_data['user']['user_company'] ?></li>
    <li><?= _('Phone:') ?> <?= $user_data['user']['user_phone'] ?></li>
    <li><?= _('Country:') ?> <?= $user_data['user']['country_id'] ?></li>
    <li><?= _('State or county:') ?> <?= $user_data['user']['user_state'] ?></li>
    <li><?= _('City:') ?> <?= $user_data['user']['user_city'] ?></li>
    <li><?= _('Street:') ?> <?= $user_data['user']['user_street'] ?></li>
    <li><?= _('Postal code:') ?> <?= $user_data['user']['user_postal_code'] ?></li>
    <li><?= _('User created at:') ?> <?= $user_data['user']['user_created_at'] ?></li>
</ul>

<?php foreach ($user_data['orders'] as $order): ?>
    <h1><?= __('Order nr') ?> <?= $order['order_id'] ?></h1>

    <ul>
        <li><?= __('Order made by:') ?> <?= $order['user_full_name'] ?></li>
        <li><?= __('Order made at:') ?> <?= $order['order_made_at'] ?></li>
        <li><?= __('Order status:') ?> <?= $order['order_status_name'] ?></li>

        <?php if ($order['order_file_id']): ?>
            <li><?= __('Order file id:') ?> <?= $order['order_file_id'] ?></li>
        <?php endif; ?>

        <?php if ($order['order_promo_code']): ?>
            <li><?= __('Order promo code:') ?> <?= $order['order_promo_code'] ?></li>
        <?php endif; ?>
    </ul>

    <h4><?= __('Order rows') ?></h4>

    <?php foreach ($order['order_rows'] as $order_row): ?>
        <ul>
            <li><?= __('Width:') ?> <?= $order_row['width']; ?></li>
            <li><?= __('Height:') ?> <?= $order_row['height']; ?></li>
            <li><?= __('Price:') ?> <?= $order_row['price']; ?></li>
            <li><?= __('Quantity:') ?> <?= $order_row['quantity']; ?></li>
            <li><?= __('Product type:') ?> <?= $order_row['product_type_name']; ?></li>
        </ul>
    <?php endforeach; ?>

    <br><br>
<?php endforeach; ?>