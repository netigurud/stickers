<!doctype html>
<html lang="en">
<head>
    <base href="<?= BASE_URL ?>"/>
    <meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
</head>
<style>
    table#productRows {
        border: 1px solid #1b1b1b;
        border-collapse:collapse;
    }

    table#productRows td {
        padding:5px;
    }

    .total td {
        padding: 8px;
    }
</style>
<body style="background:#f5f5f5;padding-top:65px; padding-bottom:65px;">
<table cellspacing="0" cellpadding="0" style="max-width:800px;margin:0 auto;background:#fff;border-radius:15px;">
    <tr style="display:block;padding:0;margin:0;background: url('assets/img/bgSmall.png') no-repeat bottom;height:250px;">
        <td colspan="2" style="-webkit-border-radius:15px;-moz-border-radius:15px;border-radius:15px;width:100%;margin-top:0;padding-left:50px;height:250px;">
            <img src="assets/img/logo.png">
        </td>
    </tr>
    <tr>
        <td colspan="2"
            style="padding:45px 50px 10px 50px;font-family:'PT Sans',sans-serif;font-size:25pt;color:#1a1a1a;">
            <?= __('Invoice')?> #<?= $invoice->number; ?>
        </td>
    </tr>
    <tr>
        <td style="vertical-align:top;padding-left: 50px;font-size:15px;font-family:'PT Sans',sans-serif;color:#1a1a1a;">
            <br/>
            <br/><b><?= $invoice->client_name; ?></b>
            <br/><?= $invoice->client_address_street; ?>
            <br/><?= $invoice->client_address_postal_code; ?> <?= $invoice->client_address_city; ?>
            <?php if (!empty($invoice->client_address_county)): ?>
                <br/>
            <?php endif; ?>
            <?= $invoice->client_address_county; ?>
            <br/><?= $invoice->client_address_country; ?>
            <?php if ($invoice->client_kmknr): ?>
                <?= __('VAT no.') ?>: <?= $invoice->client_kmknr; ?>
            <?php endif; ?>
        </td>
        <td style="padding-right: 50px;font-size:15px;font-family:'PT Sans',sans-serif;color:#1a1a1a;text-align:right">
            <br/>
            <table>
                <tr>
                    <th style="text-align:left"><?= __('Date')?>:</th>
                    <td style="text-align:left"><?= $invoice->created; ?></td>
                </tr>
                <tr>
                    <th style="text-align:left"><?= __('Due date')?>:</th>
                    <td style="text-align:left"><?= $invoice->due; ?></td>
                </tr>
                <tr>
                    <th style="text-align:left"><?= __('Overdue charge')?>:</th>
                    <td style="text-align:left"><?= $invoice->overdue_charge_percent; ?>
                        % <?= __('a day') ?></td>
                </tr>
                <tr>
                    <th style="text-align:left"><?= __('Account number') ?>:</th>
                    <td style="text-align:left">LHV EE687700771001835053</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2"
            style="padding:0px 50px 0px 50px;font-size:15px;font-family:'PT Sans',sans-serif;color:#1b1b1b;">
            <br/>
            <br/>
            <table id="productRows" style="width:100%;text-align:center;">
                <thead style="border-bottom:1px solid #1b1b1b;">
                <tr>
                    <th style="border: 1px solid #1b1b1b;"><?= __('Product/Service')?></th>
                    <th style="border: 1px solid #1b1b1b;"><?= __('Price')?> EUR</th>
                    <th style="border: 1px solid #1b1b1b;"><?= __('Amount')?></th>
                    <th style="border: 1px solid #1b1b1b;"><?= __('VAT')?></th>
                    <th style="border: 1px solid #1b1b1b;"><?= __('Total')?> EUR</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($invoice->rows as $row): ?>

                    <tr>
                        <td style="border: 1px solid #1b1b1b;"><?= $row->name; ?></td>
                        <td style="border: 1px solid #1b1b1b;"><?= $row->price_per_unit; ?></td>
                        <td style="border: 1px solid #1b1b1b;"><?= $row->amount; ?></td>
                        <td style="border: 1px solid #1b1b1b;"><?= $row->vat; ?>%</td>
                        <td style="border: 1px solid #1b1b1b;"><?= $row->amount * $row->price_per_unit + ($row->amount * $row->price_per_unit * (0 / 100)); ?></td>
                    </tr>

                <?php endforeach; ?>

                <tr class="total">
                    <td colspan="4" style="text-align:right; border: 1px solid #1b1b1b;"><?= __('Sum')?> (EUR):</td>
                    <td style="border: 1px solid #1b1b1b;"><?= $invoice->sum; ?></td>
                </tr>
                <tr class="total">
                    <td colspan="4" style="text-align:right; border: 1px solid #1b1b1b;"><?= __('VAT')?> (EUR):</td>
                    <td style="border: 1px solid #1b1b1b;"><?= $invoice->vat; ?></td>
                </tr>
                <tr class="total">
                    <td colspan="4" style="text-align:right; border: 1px solid #1b1b1b;"><?= __('Total')?> (EUR):</td>
                    <td style="border: 1px solid #1b1b1b;"><?= $invoice->sum + $invoice->vat; ?>
                        €
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding:0px 50px 50px 50px;font-size:15px;font-family:'PT Sans',sans-serif;">
            <br/>
            <br/>

            <p style="color:#906ab8;"><b>Letsticker team.</b></p>
        </td>
    </tr>
    <tr>
        <td style="padding-left:50px;padding-bottom:20px;font-size:11px;font-family:'PT Sans',sans-serif;color:#1a1a1a;">
            Netguru OÜ<br/>
            Hiiu tn 20-2<br/>
            11619, Harjumaa, Tallinn <br/>
            Reg. no: 12962224<br/>
            VAT no.: EE102043907
        </td>

        <td style="padding-left: 50px;padding-bottom:20px;font-size:11px;font-family:'PT Sans',sans-serif;color:#1a1a1a;">

        </td>
    </tr>
</table>
</body>
</html>
