<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?= BASE_URL ?>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.png">

    <title><?= PROJECT_NAME ?></title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/components/bootstrap/css/bootstrap.min.css?<?= COMMIT_HASH ?>" rel="stylesheet">

    <!-- jQuery UI core CSS -->
    <link href="vendor/components/jqueryui/themes/base/jquery-ui.min.css?<?= COMMIT_HASH ?>" rel="stylesheet">

    <!-- Site core CSS -->
    <link href="assets/css/main.css?<?= COMMIT_HASH ?>" rel="stylesheet">

    <style>
        body {
            padding-top: 70px;
        }
    </style>

    <!-- jQuery -->
    <script src="vendor/components/jquery/jquery.min.js?<?= COMMIT_HASH ?>"></script>

    <link rel="stylesheet" type="text/css"
          href="vendor/components/font-awesome/css/font-awesome.min.css?<?= COMMIT_HASH ?>">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css?<?= COMMIT_HASH ?>">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css?<?= COMMIT_HASH ?>">
    <link rel="stylesheet" href="assets/css/app.css?<?= COMMIT_HASH ?>">
    <link rel="stylesheet" href="assets/components/videojs/videojs7.1.0.min.css?<?= COMMIT_HASH ?>">

    <script src="vendor/components/bootstrap/js/bootstrap.min.js?<?= COMMIT_HASH ?>"></script>
    <script src="vendor/components/jqueryui/jquery-ui.min.js?<?= COMMIT_HASH ?>"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js?<?=COMMIT_HASH?>"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js?<?=COMMIT_HASH?>"></script>
    <![endif]-->

    <!-- Custom JS -->
    <script src="assets/js/main.js?<?= COMMIT_HASH ?>"></script>

    <?php require 'templates/partials/smartlook.php' ?>

</head>

<div id="offcanvas-full-screen" class="offcanvas-full-screen" data-off-canvas data-transition="overlap">
    <div class="offcanvas-full-screen-inner">

        <button class="close-button" aria-label="Close menu" type="button" data-close>
            <span aria-hidden="true">&times;</span>
        </button>

        <ul class="vertical menu" style="max-width: 250px" data-dropdown-menu>
            <?php if ($auth->logged_in && $auth->is_admin): ?>
                <li><a href="admin/products">Admin</a></li>
            <?php endif; ?>

            <?php if ($auth->logged_in): ?>
                <li class="<?= $controller == 'orders' ? 'active' : '' ?>">
                    <a href="my_orders"><?= __('My Orders') ?></a>
                </li>
            <?php endif; ?>

            <?php if ($auth->logged_in && ($auth->is_admin || $auth->is_printing_house || $auth->is_designer)): ?>
                <li class="<?= $controller == 'orders' ? 'active' : '' ?>">
                    <a href="orders"><?= __('All Orders') ?></a>
                </li>
            <?php endif; ?>

            <li>
                <a><?= $_SESSION['language'] ?></a>
                <ul class="lang-dropdown">
                    <?php foreach ($supported_languages as $language): ?>
                        <li class="langitem"><a href="?language=<?= $language ?>"><?= $language ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </li>

            <?php if ($auth->logged_in): ?>
                <li class="<?= $controller == 'users' ? 'active' : '' ?>">
                    <a href="profile"><?= __('Profile') ?></a>
                </li>

                <li><a href="logout"><?= __('Log out') ?></a></li>
            <?php else: ?>
                <li><a data-open="loginModal"><?= __('Log in') ?></a></li>
            <?php endif; ?>
        </ul>
    </div>
</div>

<body class="<?= $controller == 'welcome' ? 'welcome' : 'not-welcome' ?>">

<div class="page">


    <div class="off-canvas-wrapper">

        <div class="off-canvas-content" data-off-canvas-content>
            <div>
                <div class="title-bar">
                    <div class="row">
                        <div class="title-bar-left">
                            <a href="<?= BASE_URL ?>" class="logo-mob">
                                <img src="assets/img/svg/<?= $controller == 'welcome' ? 'logotext-white.svg' : 'logotext.svg' ?>">
                            </a>

                            <ul class="menu">
                                <li>
                                    <a href="<?= BASE_URL ?>" class="logo">
                                        <img src="assets/img/svg/<?= $controller == 'welcome' ? 'logotext-white.svg' : 'logotext.svg' ?>">
                                    </a>
                                </li>

                                <?php if ($auth->logged_in && $auth->is_admin): ?>
                                    <li>
                                        <a href="admin/products"><?= __('Admin') ?></a>
                                    </li>
                                <?php endif; ?>

                                <?php if ($auth->logged_in): ?>
                                    <li class="<?= $controller == 'orders' ? 'active' : '' ?>">
                                        <a href="my_orders"><?= __('My Orders') ?></a>
                                    </li>
                                <?php endif; ?>

                                <?php if ($auth->logged_in && ($auth->is_admin || $auth->is_printing_house || $auth->is_designer)): ?>
                                    <li class="<?= $controller == 'orders' ? 'active' : '' ?>">
                                        <a href="orders"><?= __('All Orders') ?></a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>

                        <div class="title-bar-right" style="margin-top: 1.3rem">
                            <button class="menu-icon" type="button" data-toggle="offcanvas-full-screen"></button>

                            <a class="cart-link cart-link-mob" href="checkout">
                                <span class="badge"><?= $shopping_cart_size ?></span>
                                <img src="assets/img/svg/<?= $controller == 'welcome' ? 'cart-white.svg' : 'cart.svg' ?>">
                            </a>

                            <ul class="menu dropdown" style="z-index: 1000000 !important" data-dropdown-menu>
                                <li>
                                    <a class="cart-link" href="checkout">
                                        <span>&nbsp;</span>
                                        <span class="badge"><?= $shopping_cart_size ?></span>
                                        <img src="assets/img/svg/<?= $controller == 'welcome' ? 'cart-white.svg' : 'cart.svg' ?>">
                                    </a>
                                </li>

                                <li>
                                    <a><?= $_SESSION['language'] ?></a>
                                    <ul class="lang-dropdown">
                                        <?php foreach ($supported_languages as $language): ?>
                                            <li class="langitem"><a href="?language=<?= $language ?>"
                                                                    style="color: #0a0a0a !important"><?= $language ?></a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>

                                <?php if ($auth->logged_in): ?>
                                    <li><a href="profile"><?= __('Profile') ?></a></li>
                                    <li><a href="logout"><?= __('Log out') ?></a></li>
                                <?php else: ?>
                                    <li><a data-open="loginModal"><?= __('Log in') ?></a></li>
                                <?php endif; ?>
                            </ul>
                        </div>

                    </div>
                </div>
            </div> <!-- MENU -->

            <section class="<?= $controller == 'welcome' ? 'home' : 'innerpage' ?>">

                <!-- Main component for a primary marketing message or call to action -->
                <?php if (!file_exists("views/$controller/{$controller}_$action.php")) error_out('The view <i>views/' . $controller . '/' . $controller . '_' . $action . '.php</i> does not exist. Create that file.'); ?>
                <?php @require "views/$controller/{$controller}_$action.php"; ?>

            </section>

            <footer>
                <div class="row">
                    <div class="column medium-6">
                        <ul class="menu menu-top">
                            <li><a href="contact"><?= __('Contact') ?></a></li>

                            <?php foreach ($pages as $page): ?>
                                <?php if ($page['page_id'] != 2): ?>
                                    <li><a href="pages/<?= $page['page_slug'] ?>"><?= __($page['page_title']) ?></a>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>

                            <?php if ($auth->logged_in && $auth->is_admin): ?>
                                <li><a href="admin/products"><?= __('Admin') ?></a></li>
                            <?php endif; ?>

                            <?php if ($auth->logged_in && ($auth->is_admin || $auth->is_printing_house || $auth->is_designer)): ?>
                                <li class="<?= $controller == 'orders' ? 'active' : '' ?>">
                                    <a href="orders"><?= __('Orders') ?></a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>

                    <div class="column medium-6">
                        <ul class="menu text-right">
                            <li>
                                <a class="social-link" href="#"><img width="25" src="assets/img/svg/fb.svg" alt=""></a>
                            </li>

                            <li style="display:none;">
                                <a class="social-link" href="#"><img width="25" src="assets/img/svg/in.svg" alt=""></a>
                            </li>
                        </ul>
                    </div>

                    <span class="monster-icon monster-icon-rotated"></span>
                </div>

                <div class="spacer-xs"></div>

                <hr>

                <div class="row">
                    <div class="column medium-6">
                        <ul class="menu menu-bottom">
                            <li><a href="<?= BASE_URL ?>">© <?= date('Y') ?> Letsticker</a></li>
                            <li><a href="privacy"><?= __('Privacy & Terms') ?></a></li>
                            <li><a href="faq"><?= __('FAQ') ?></a></li>
                        </ul>
                    </div>

                    <div class="column medium-6">
                        <p class="text-right"><?= __('Crafted by CAMO') ?></p>
                    </div>
                </div>

                <div class="row row-mob">
                    <div class="column medium-6 end">
                        <ul class="menu menu-bottom">
                            <li>
                                <a class="social-link" href="#"><img width="25" src="assets/img/svg/fb.svg" alt=""></a>
                            </li>

                            <li>
                                <a class="social-link" href="#"><img width="25" src="assets/img/svg/in.svg" alt=""></a>
                            </li>

                            <li><p><?= __('Crafted by CAMO') ?></p></li>
                        </ul>
                    </div>
                </div>
            </footer> <!-- FOOTER -->

        </div>
    </div>
</div>

<?php require 'templates/partials/login_modal.php'; ?>
<?php require 'templates/partials/alert_modal.php'; ?>
<?php require 'templates/partials/error_modal.php'; ?>
<?php require 'templates/partials/progress_bar_modal.php'; ?>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="assets/js/app.js?<?= COMMIT_HASH ?>"></script>
<script src="assets/js/sweetalert-2.1.0.js"></script>
<script src="assets/components/videojs/videojs7.1.0.min.js"></script>
<script>

    <?php if($controller === 'welcome'):?>
    $(document).ready(function () {
        $('.video-play-button').click(function () {
            $('#my-video').show();
            $('.video-play-button').hide();
            $('.video-caption').hide();
            videojs('my-video').play();
        });
    });
    <?php else: ?>
    $(document).ready(function () {
        $('.video-play-button').click(function () {
            $('.video-play-button').hide();
            $('.video-caption').hide();
            videojs('my-video').play();
        });
    });
    <?php endif;?>

    $(".send-new-password").on("click", function () {

        var email = $(".forgot-password-email").val();

        // Hide enter email modal
        $('#forgot-password-modal').hide();
        $('body > div.modal-backdrop.fade.in').hide();

        if (email !== '') {
            ajax("welcome/send_new_password", {
                email: email
            }, function (json) {
                show_alert_modal(json.data);
            });
        } else {
            show_alert_modal("Please enter the correct email address");
        }

    });

    $('#loginForm').submit(function () {
        login();
        return false;
    });

    function login() {
        ajax("login", {
            "email": $("#email").val(),
            "password": $("#password").val()
        }, function (json) {
            location.reload();
        }, function (json) {
            $(".login-error div").html(json.data);
            $(".login-error").fadeOut(10).fadeIn();
        })
    }

</script>

<?php require 'system/js_constants.php' ?>
</body>
</html>