<div class="modal fade" id="alert-modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="alert-modal-title"></h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default close-alert-modal"
                        data-dismiss="modal"><?= __('Close') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
