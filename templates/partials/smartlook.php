<?php if (!$_SERVER['HTTP_HOST'] === 'localhost'): ?>
    <script type="text/javascript">
        window.smartlook || (function (d) {
            var o = smartlook = function () {
                o.api.push(arguments)
            }, h = d.getElementsByTagName('head')[0];
            var c = d.createElement('script');
            o.api = new Array();
            c.async = true;
            c.type = 'text/javascript';
            c.charset = 'utf-8';
            c.src = 'https://rec.smartlook.com/recorder.js';
            h.appendChild(c);
        })(document);
        smartlook('init', '7567d689cf840210aedd695b4e9402be72096ade');

        <?php if(!empty($_SESSION['user_id']) && $user = get_first("SELECT * FROM users WHERE user_id = $_SESSION[user_id]")):?>

        smartlook('tag', 'name', '<?=addcslashes($user['user_full_name'], "'")?>');
        smartlook('tag', 'phone', '<?=addcslashes($user['user_phone'], "'")?>');
        smartlook('tag', 'email', '<?=addcslashes($user['user_email'], "'")?>');
        smartlook('tag', 'session', '<?=session_id()?>');
        <?php endif?>
    </script>
<?php endif ?>
