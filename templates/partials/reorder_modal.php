<div class="row columns text-center">
    <img src="data:image/jpeg;base64,<?= $order['order']['file_preview'] ?>" class="reorder-image">
</div>
<div class="spacer-s"></div>
<div class="row columns">
    <table class="let-table">
        <thead>
        <tr>
            <th><?= __('Product') ?></th>
            <th><?= __('Width') ?></th>
            <th><?= __('Height') ?></th>
            <th><?= __('Quantity') ?></th>
        </tr>
        </thead>
        <tbody class="reorder-details-tbody">
        <?php foreach ($order['order_rows'] as $row): ?>
            <tr>
                <td style="width: initial;"><?= $row['product_type_name'] ?></td>
                <td><?= $row['width'] ?> cm</td>
                <td><?= $row['height'] ?> cm</td>
                <td>
                    <select class="form-control reorder-row-quantity"
                            data-order_row_id="<?= $row['order_row_id'] ?>">
                        <?php foreach ($quantities[$row['product_type_id']] as $quantity): ?>
                            <option value="<?= $quantity['quantity'] ?>"><?= $quantity['quantity'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>

        <?php endforeach; ?>
        <tr>
            <td colspan="12">
                <h5 class="h-bold h-dark-green text-right">
                    <?= __("Total") ?>: <?= number_format($order['order_total_price'], 2) ?>€
                </h5>

                <p class="text-right" style="margin-bottom: 0; font-size: .875rem; color: #6a7f71;">
                    <?= __('VAT') ?>:
                    <?php if ($order['order']['order_vat_percent']): ?>
                        <?= number_format($order['order_total_price'] / 1.2 * 0.2, 2) ?>€
                    <?php else: ?>
                        0€
                    <?php endif; ?>
                </p>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="spacer-s"></div>
<p class="help-text"><?= __('You will be able to remove rows from your order once you click "Yes"') ?></p>