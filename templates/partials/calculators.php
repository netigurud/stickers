<div class="counter-wrapper" style="z-index: 1">
    <div class="monster-floating"></div>

    <ul class="nav nav-tabs">
        <li class="active stickers">
            <a data-toggle="tab" href="#stickers-tab"><?= __('Stickers') ?></a>
        </li>

        <li class="magnets">
            <a data-toggle="tab" href="#magnets-tab"><?= __('Magnets') ?></a>
        </li>
    </ul>

    <div class="info-pan">

        <h5 class="h-bold">
            <span><?= __('Select size') ?></span>
            <a href="#" data-open="infoModal" class="text-right info-link">
                <img width="25" height="25" src="assets/img/svg/info.svg" alt="">
            </a>
        </h5>

        <div class="tab-content">
            <div id="stickers-tab" class="tab-pane fade in active">
                <?php foreach ($stickers as $key => $sticker): ?>
                    <div class="mate-checkbox">
                        <input type="radio" name="stickers-size-option" class="stickers-size-option"
                               id="stickers-size-option-<?= $key ?>"
                               data-price="<?= $sticker['product_price'] ?>"
                               data-height="<?= $sticker['product_height'] ?>"
                               data-width="<?= $sticker['product_width'] ?>">

                        <label for="stickers-size-option-<?= $key ?>">
                            <span>
                                <?= $sticker['product_height'] ?> cm x <?= $sticker['product_width'] ?> cm
                            </span>
                        </label>

                        <div class="check"></div>
                    </div>
                <?php endforeach; ?>

                <div class="mate-checkbox">

                    <input type="radio"
                           id="stickers-custom-size"
                           name="stickers-size-option"
                           data-size_id="0"
                           data-price="<?= CUSTOM_SIZE_PRICE ?>">

                    <label for="stickers-custom-size">
                        <span style="white-space: nowrap"><?= __('Custom size') ?></span>
                    </label>

                    <div class="check stickers-size-option"></div>


                    <div class="custom-sizes-container" style="text-align: left; margin-left: 15px;">
                        W: <input type="number" step="0.01" max="<?= CUSTOM_SIZE_MAX_WIDTH ?>"
                                  value="<?= CUSTOM_SIZE_MIN_WIDTH ?>" class="stickers-custom-size-width">
                        H: <input type="number" step="0.01" max="<?= CUSTOM_SIZE_MAX_HEIGHT ?>"
                                  value="<?= CUSTOM_SIZE_MIN_HEIGHT ?>"
                                  class="stickers-custom-size-height">
                    </div>
                </div>

                <div class="spacer-s"></div>

                <h5 class="h-bold">
                    <span><?= __('Select quantity') ?></span>
                    <a href="#" data-open="quantityModal" class="text-right info-link">
                        <img width="25" height="25" src="assets/img/svg/info.svg" alt="">
                    </a>
                </h5>

                <div class="stickersQuantities">
                    <?php foreach ($sticker_quantities as $sticker_quantity): ?>
                        <div class="mate-checkbox" data-type="quantity"
                             data-quantity_sum="<?= $sticker_quantity['quantity'] ?>">
                            <input type="radio" id="stickers-quantity-option-<?= ++$i ?>"
                                   class="stickers-quantity-option"
                                   name="stickers-quantity-option" value="<?= $sticker_quantity['quantity'] ?>">

                            <label for="stickers-quantity-option-<?= $i ?>">
                                <span class="quantity-sum"><?= $sticker_quantity['quantity'] ?></span>
                                <span class="quantity-price"></span>
                                <!--<span class="quantity-save h-green">Save 31%</span>-->
                            </label>

                            <div class="check"></div>
                        </div>
                    <?php endforeach; ?>
                </div>

            </div>

            <div id="magnets-tab" class="tab-pane fade">
                <?php foreach ($magnets as $key => $magnet): ?>
                    <div class="mate-checkbox">
                        <input type="radio" id="magnets-size-option-<?= $key ?>" name="magnets-size-option"
                               class="magnets-size-option"
                               data-price="<?= $magnet['product_price'] ?>"
                               data-height="<?= $magnet['product_height'] ?>"
                               data-width="<?= $magnet['product_width'] ?>">

                        <label for="magnets-size-option-<?= $key ?>">
                            <span>
                                <?= $magnet['product_height'] ?> cm x <?= $magnet['product_width'] ?> cm
                            </span>
                        </label>

                        <div class="check"></div>
                    </div>
                <?php endforeach; ?>

                <div class="mate-checkbox">

                    <input type="radio" id="magnets-custom-size" name="magnets-size-option" data-size_id="0"
                           data-price="<?= CUSTOM_SIZE_PRICE ?>">
                    <label for="magnets-custom-size"><span
                                style="white-space: nowrap"><?= __('Custom size') ?></span></label>
                    <div class="check"></div>


                    <div class="custom-sizes-container" style="text-align: left; margin-left: 15px;">
                        W: <input type="number" value="<?= CUSTOM_SIZE_MIN_WIDTH ?>" class="magnets-custom-size-width">
                        H: <input type="number" value="<?= CUSTOM_SIZE_MIN_HEIGHT ?>"
                                  class="magnets-custom-size-height">
                    </div>
                </div>

                <div class="spacer-s"></div>

                <h5 class="h-bold">
                    <span><?= __('Select quantity') ?></span>
                    <a href="#" data-open="quantityModal" class="text-right info-link">
                        <img width="25" height="25" src="assets/img/svg/info.svg" alt="">
                    </a>
                </h5>

                <div class="magnetsQuantities">
                    <?php foreach ($magnet_quantities as $magnet_quantity): ?>
                        <div class="mate-checkbox" data-type="quantity"
                             data-quantity_sum="<?= $magnet_quantity['quantity'] ?>">
                            <input type="radio" id="magnets-quantity-option-<?= ++$i ?>"
                                   name="magnets-quantity-option"
                                   class="magnets-quantity-option"
                                   value="<?= $magnet_quantity['quantity'] ?>">

                            <label for="magnets-quantity-option-<?= $i ?>">
                                <span class="quantity-sum"><?= $magnet_quantity['quantity'] ?></span>
                                <span class="quantity-price"></span>
                                <!-- <span class="quantity-save h-green">Save 31%</span> -->
                            </label>

                            <div class="check"></div>
                        </div>
                    <?php endforeach; ?>
                </div>

            </div>
        </div>

        <div class="spacer-s"></div>

        <a class="button button-block add-to-shopping-cart"><?= __('Continue') ?></a>

        <div class="mate-checkbox">
            <div id="prices-withouth-vat">
                <input type="checkbox" id="prices-withouth-vat-input" class="form-control">

                <label for="prices-withouth-vat-input"><?= __('Show prices without VAT') ?></label>

                <div class="check"></div>
            </div>
        </div>
        <div class="spacer-s"></div>
        <p class="text-center"><?= __('Next') ?>: <?= __('upload artwork') ?></p>
    </div>
</div>

<div class="reveal small info-reveal" id="infoModal" data-reveal>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>

    <div class="spacer-s"></div>

    <div class="row">
        <div class="column medium-6">
            <h4 class="h-bold"><?= __('How to pick a size?') ?></h4>

            <div class="spacer-m"></div>

            <p>
                <strong><?= __('Don’t worry, we’ll do the math part for you. All you have to do is pick an approximate size that your artwork should fit into. We’ll determine the exact size based on your artwork in proofing process.') ?></strong>
            </p>

            <p>
                <?= __('So pick an approximate size and when you receive your proofs, you’ll see exact sizes. If you have more questions about sizing, drop us a line:') ?>
                <a href="mailto:info@letsticker.com" class="h-dark-green"><strong>info@letsticker.com</strong></a>

            </p>
        </div>

        <div class="column medium-6">
            <img src="assets/img/sizes.png" alt="">
        </div>
    </div>
    <div class="spacer-s"></div>
</div>

<div class="reveal tiny" id="quantityModal" data-reveal>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>

    <div class="spacer-s"></div>

    <div class="row">
        <div class="column medium-10 small-12 end">
            <h5>
                <?= __('If you want to order a bigger quantity, then contact us at') ?>
                <a href="mailto:info@letsticker.com"><strong>info@letsticker.com</strong></a>
            </h5>
        </div>
    </div>

    <div class="spacer-s"></div>
</div>

<script>
    $(function () {

        // Size and quantity options for stickers and magnets
        var magnets_size_option = $(".magnets-size-option");
        var stickers_size_option = $(".stickers-size-option");
        var magnets_quantity_option = $(".magnets-quantity-option");
        var stickers_quantity_option = $(".stickers-quantity-option");
        var custom_size_options = $("#magnets-custom-size, #stickers-custom-size");

        // Custom size options for stickers and magnets
        var magnets_custom_size = $("#magnets-custom-size");
        var stickers_custom_size = $("#stickers-custom-size");
        var custom_sizes_container = $(".custom-sizes-container");
        var magnets_custom_size_width = $(".magnets-custom-size-width");
        var stickers_custom_size_width = $(".stickers-custom-size-width");
        var magnets_custom_size_height = $(".magnets-custom-size-height");
        var stickers_custom_size_height = $(".stickers-custom-size-height");
        var custom_size_inputs = $(".stickers-custom-size-height, .stickers-custom-size-width, .magnets-custom-size-height, .magnets-custom-size-width");

        // Maximum allowed values for custom sizes
        var custom_size_min_width = <?= CUSTOM_SIZE_MIN_WIDTH ?>;
        var custom_size_min_height = <?= CUSTOM_SIZE_MIN_HEIGHT ?>;
        var custom_size_max_width = <?= CUSTOM_SIZE_MAX_WIDTH ?>;
        var custom_size_max_height = <?= CUSTOM_SIZE_MAX_HEIGHT ?>;

        // Product type by default is stickers
        var product_type = 'stickers';

        // Make the first oprice selected by default
        stickers_size_option.first().click();

        // Hide the custom size inputs by default
        custom_sizes_container.hide();

        // Selected size and quantity
        var selected_size = $(".stickers-size-option:checked");

        // Calculate initial prices with default selections
        calculateQuantityPrices(selected_size);

        // Make the first quantity selected by default
        stickers_quantity_option.first().click();

        // On tab click
        $("ul.nav-tabs > li").on("click", function () {
            // Set product type
            product_type = $(this).hasClass('stickers') ? 'stickers' : 'magnets';

            // If the user chose magnets for the first time then that means nothing is selected, let's select first options for them
            if (product_type === 'magnets') {
                if (!magnets_size_option.is(":checked")) {
                    magnets_size_option.first().click();
                }
                if (!magnets_quantity_option.is(":checked")) {
                    magnets_quantity_option.first().click();
                }
                calculateQuantityPrices($("input[name='magnets-size-option']:checked"));
            } else {
                calculateQuantityPrices($("input[name='stickers-size-option']:checked"));
            }
        });

        // Show prices withouth VAT
        $("#prices-withouth-vat-input").on("change", function () {
            var selected_size = $("input[name='" + product_type + "-size-option']:checked");
            calculateQuantityPrices(selected_size);
        });

        // Hide custom size inputs and update prices
        $(".stickers-size-option, .magnets-size-option").on("click", function () {
            custom_sizes_container.hide();
            calculateQuantityPrices($(this));
        });

        // When custom size option is checked
        // Show custom size inputs and update prices
        custom_size_options.on("click", function () {
            var custom_size_input = $("#" + product_type + "-custom-size");
            updateCustomSizeInputData(custom_size_input);
            calculateQuantityPrices($(this));
            custom_sizes_container.show();
        });


        // When custom size boxes are changed
        // Update input's data-width and data-height
        $(".magnets-custom-size-width , .stickers-custom-size-width , .magnets-custom-size-height , .stickers-custom-size-height").on("input", function () {

            var custom_size_input = $("#" + product_type + "-custom-size");
            updateCustomSizeInputData(custom_size_input);
            calculateQuantityPrices(custom_size_input);
        });


        var custom_size_height = $(".stickers-custom-size-height, .magnets-custom-size-height");
        var custom_size_width = $(".stickers-custom-size-width, .magnets-custom-size-width");

        // Don't allow values over or below accepted width values and notify user
        custom_size_width.on("change", function () {
            if ($(this).val() > custom_size_max_width) {
                // Set input value to max
                $(this).val(custom_size_max_width);
                show_alert_modal("<?= __('The maximum width is ') ?>" + custom_size_max_width + "cm. <?= __('If you want to order in a larger size, then contact us at info@letsticker.com') ?>");
            }

            if ($(this).val() < custom_size_min_width) {
                // Set input value to min
                $(this).val(custom_size_min_width);
                show_alert_modal("<?= __('The minimum width is ') ?>" + custom_size_min_width + "cm. <?= __('Smaller sizes are not available') ?>");
            }
            var custom_size_input = $("#" + product_type + "-custom-size");
            updateCustomSizeInputData(custom_size_input);
        });

        // Don't allow values over or below accepted height values and notify user
        custom_size_height.on("change", function () {
            if ($(this).val() > custom_size_max_width) {
                // Set input value to max
                $(this).val(custom_size_max_width);
                show_alert_modal("<?= __('The maximum height is ') ?>" + custom_size_max_height + "cm. <?= __('If you want to order in a larger size, then contact us at info@letsticker.com') ?>");
            }

            if ($(this).val() < custom_size_min_height) {
                // Set input value to min
                $(this).val(custom_size_min_height);
                show_alert_modal("<?= __('The minimum height is ') ?>" + custom_size_min_height + "cm. <?= __('Smaller sizes are not available') ?>");
            }
            var custom_size_input = $("#" + product_type + "-custom-size");
            updateCustomSizeInputData(custom_size_input);
        });

        // Update the user selections for checkout page
        $(".add-to-shopping-cart").on("click", function () {
            var height = 0;
            var width = 0;
            var custom_size = $("#" + product_type + "-custom-size");


            var selected_size = $("." + product_type + "-size-option:checked");

            var selected_quantity = $("." + product_type + "-quantity-option:checked");
            if (selected_quantity.length) {
                var selectedQuantity = $(selected_quantity).val();

                if (custom_size.is(":checked")) {
                    height = $('.' + product_type + '-custom-size-height').val();
                    width = $('.' + product_type + '-custom-size-width').val();
                } else {
                    height = selected_size.data('height');
                    width = selected_size.data('width');
                }

            }

            // Don't let the user click this button more than once
            $(this).attr("disabled", "disabled");


            ajax("shopping_cart/update", {
                "product_height": height,
                "product_width": width,
                "quantity": selectedQuantity,
                "product_type": product_type
            }, 'checkout');
        });

        function calculateQuantityPrices(option, tax_percent = 0) {

            var m2 = option.data('height') * option.data('width');
            tax_percent = $('#prices-withouth-vat').children('input').is(":checked") ? 0 : 20;

            ajax("shopping_cart/listPrices", {
                "m2": m2,
                "product_type": product_type,
                "tax": tax_percent
            }, function (response) {
                $('.' + product_type + 'Quantities').empty();
                $(response.data).each(function (i, value) {

                    $('.' + product_type + 'Quantities').append(
                        value
                    );
                });
                $(".quantity-price").append(" €");

                if (product_type === 'stickers') {
                    var stickers_quantity_option = $(".stickers-quantity-option");
                    $('.stickers-quantity-option[value="' + stickers_quantity_option.first().val() + '"]').click();
                } else {
                    var magnets_quantity_option = $(".magnets-quantity-option");
                    $('.magnets-quantity-option[value="' + magnets_quantity_option.first().val() + '"]').click();
                }
            });
        }

        function updateCustomSizeInputData(custom_size_input) {
            custom_size_input.data('width', $("." + product_type + "-custom-size-width").val());
            custom_size_input.data('height', $("." + product_type + "-custom-size-height").val());
        }

    })

</script>