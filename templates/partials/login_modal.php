<div class="reveal login-reveal" id="loginModal" data-reveal>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>

    <div class="row columns">
        <div class="spacer-s"></div>
        <h4 class="h-bold"><?= __('Log in') ?></h4>
        <div class="spacer-m"></div>
    </div>

    <form id="loginForm" method="post">

        <div class="row columns login-error" style="display:none">
            <div class="alert alert-danger" role="alert">
                <?= __('Incorrect password or e-mail address') ?>
            </div>
        </div>

        <div class="row columns">
            <label><span><?= __('Email address') ?></span>
                <input type="email" name="email" placeholder="email" required id="email">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label><span><?= __('Password') ?></span>
                <a class="forgot-pwd" data-toggle="modal"
                   href="#forgot-password-modal"><?= __('Forgot password?') ?></a>
                <input type="password" name="password" placeholder="****" required id="password">
            </label>
        </div>

        <div class="spacer-s"></div>

        <div class="row columns">
            <button type="submit" class="button primary full-page btn-login"><?= __('Log in') ?></button>
        </div>

    </form>

    <div class="row columns">
        <div class="spacer-xs"></div>

        <center>
            <div class="or-devider"><?= __('or') ?></div>
        </center>

        <div class="spacer-s"></div>
    </div>

    <div class="row">
        <div class="column medium-6">
            <a href="<?= $this->fbLoginLink ?>" class="button small fb-login-button full-page">
                <span class="fb-icon-white"></span><?= __('Log in with Facebook') ?>
            </a>
        </div>

        <div class="column medium-6">
            <a href="login_google" class="button small google-login-button full-page">
                <span class="google-icon-white"></span><?= __('Log in with google+') ?>
            </a>
        </div>
    </div> <!-- LOGIN WITH Facebook/Google -->
</div> <!-- Login modal -->

<div class="modal fade" id="forgot-password-modal" class="reveal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Enter your email below') ?>..</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="email" class="form-control forgot-password-email">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="button"
                        class="button primary send-new-password"><?= __('Send me a new password') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- Forgot password modal -->

<div class="modal fade" id="loading-modal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Loading') ?>...</h4>
            </div>
            <div class="modal-body text-center">
                <img src="assets/img/loader.gif" alt="loading" style="margin: auto;">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- Loading modal -->