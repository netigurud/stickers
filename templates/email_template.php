<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Bellosta Responsive Email Template</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    <style type="text/css">


        body {
            width: 100%;
            background-color: transparent;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            font-family: 'Roboto', Helvetica, Arial, sans-serif
        }

        table {
            border-collapse: collapse;
        }

        @media only screen and (max-width: 640px) {
            body[yahoo] .deviceWidth {
                width: 440px !important;
                padding: 0;
            }

        }

        @media only screen and (max-width: 479px) {
            body[yahoo] .deviceWidth {
                width: 280px !important;
                padding: 0;
            }
        }
    </style>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix"
      style="font-family: 'Roboto', Helvetica, Arial, sans-serif; background-color: transparent">
<!-- image and text -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 60px 0; background-color: transparent">
    <tr>
        <td>
            <table width="680" class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center"
                   style="margin: 50px auto; border-radius: 15px;"
                   bgcolor="#ffffff">
                <tr>
                    <td width="100%">

                        <div style="border: 2px solid #ebebeb !important; border-radius: 15px; background-color: #ffffff">
                            <table class="devicewidth" align="center" border="0" cellpadding="0" cellspacing="0"
                                   width="100%">
                                <tr>
                                    <td valign="top" style="padding:0" width="100%">
                                        <a href="#"><img class="deviceWidth"
                                                         src="%BASE_URL%assets/img/bgEmail.png" alt=""
                                                         border="0" width="680"
                                                         style="display: block; border-radius: 15px;"/>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-size: 16px; color: #999999; text-align: center; font-family: 'Roboto', Helvetica, Arial, sans-serif; line-height: 30px; vertical-align: top; padding:20px;"
                                        width="100%">
                                        <table width="100%">
                                            <tr>
                                                <td style="font-family: 'Roboto', Helvetica, Arial, sans-serif; font-size: 28px; color: #333333; text-align:left;line-height: 36px; padding:5px 10px;">
                                                    Hi, %NAME%
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="15" width="100%"></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:justify; padding:5px 10px;">
                                                    %BODY%
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:left; padding:5px 10px;">
                                                    <p>Till next time,</p>
                                                    <p style="color:#906ab8;"><b>Letsticker team.</b></p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


</html>