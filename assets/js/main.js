var RELOAD = 33;
var error_modal = $("#error-modal");

function tryToParseJSON(jsonString) {
    try {
        var o = JSON.parse(jsonString);

        // Handle non-exception-throwing cases:
        // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
        // but... JSON.parse(null) returns null, and typeof null === "object",
        // so we must check for that, too. Thankfully, null is falsey, so this suffices:
        if (o && typeof o === "object") {
            return o;
        }
    }
    catch (e) {
    }

    return false;
}


function ajax(url, options, callback_or_redirect_url, error_callback) {

    $.post(url, options)
        .fail(function (jqXHR, textStatus, errorThrown) {
            console.log('Xhr error: ', jqXHR, textStatus, errorThrown);

            if (jqXHR.readyState === 4) {
                console.log("HTTP error (can be checked by XMLHttpRequest.status and XMLHttpRequest.statusText)");
                console.log(errorThrown);
                processAjaxResponse(jqXHR.responseText);

            }
            else if (jqXHR.readyState === 0) {
                console.log("No internet or server down");
                processAjaxResponse({description:"Network error (i.e. connection refused, access denied due to CORS, etc."});
            }
            else {
                console.log("No internet or server down");
            }

        })
        .done(function(response){processAjaxResponse(response)});

    function processAjaxResponse(response) {

        var json = tryToParseJSON(response);

        // Handle situation when server response was not json
        if (json === false) {

            // Show the error (generic if not on localhost)
            show_error_modal(response);

            return false;

        }

        // Decode data if it was encoded
        if (json.encoded) {
            json.data = decode_base64(json.data);
        }

        // Success
        if (json.status.toString().charAt(0) === '2') {

            // Call the succes callback function if given
            if (typeof callback_or_redirect_url === 'function') {
                callback_or_redirect_url(json);
            }

            // Redirect if calback was a string
            else if (typeof callback_or_redirect_url === 'string') {
                location.href = callback_or_redirect_url;
            }

            // Refresh page if callback was RELOAD
            else if (callback_or_redirect_url === RELOAD) {
                location.reload();
            }

        }

        // Show generic error message and report the actual error message to devs if status is 500
        else if (json.status === 500) {

            // Show error modal
            show_error_modal(json.data);

            return false;
        }

        else {

            // Call error callback if given
            if (typeof error_callback === 'function') {
                error_callback(json);
            }

            // Show error modal if callback was not given
            else {
                show_error_modal(json.data);
            }


        }

    }

}

function getTrData(element, data_name) {
    return element.parents('tr').data(data_name);
}

$('table.clickable-rows tr').on('click', function () {
    window.location = $(this).data('href');
});

function show_error_modal(message) {

    var title = SERVER_ERROR_OOPS;
    var description = 'Unknown error occurred';

    if (typeof message === 'string') {
        description = message;
    }
    else if (typeof message === 'object') {
        title = typeof message.title === 'undefined' ? title : message.title;
        description = typeof message.description === 'undefined' ? description : message.description;
    }

    // Show a proper error message depending on the env
    $(".error-modal-title").html(title);
    $(".error-modal-body").html(description);

    // Show the modal
    $("#error-modal").modal('show');

    // Hide potential loading modal
    $('#loading-modal').hide();
    $('body > div.modal-backdrop.fade.in').hide();

}

/* Tooltipify the entire DOM */
$(document).tooltip();
$(".mate-checkbox .check").click(function (e) {
    e.preventDefault();
    $(this).parent().find('label').click()
});

function requiredFieldsAreEmpty(req_fields, show_alert, suggested_fields) {

    var color;
    var fields;
    var something_empty = false;
    show_alert = typeof show_alert === "undefined" ? true : show_alert;
    suggested_fields = typeof suggested_fields === "undefined" ? [] : suggested_fields;

    // Blend required and suggested fields' arrays together
    fields = suggested_fields.concat(req_fields);

    $(fields).each(function (index, element) {

        // Check if elements value is longer than 1 character
        if (!$(element).val() || $(element).val().length < 1) {

            // Special case - select2 dropdown
            if ($(element).hasClass('select2')) {
                element = $(element).next();
            }

            // Show the user that this value is incorrect or needs evaluation
            color = suggested_fields.includes(element) ? 'blue' : 'red';
            $(element).css('border', '2px solid ' + color);

            // Focus on the incorrect element if it's not optional
            if (!suggested_fields.includes(element)) $(element).focus();

            something_empty = true;
        } else {
            $(element).css('border', '1px solid #e1e3e7');
        }
    });

    // Alert the user if a required input was empty
    if (something_empty) {
        if (show_alert) {
            alert("Please fill in the inputs with red borders and make sure fields with blue borders are correct");
        }
        return true;
    } else {
        return false;
    }
}

function show_alert_modal(alert_message, function_after_modal_close) {
    $(".alert-modal-title").html(alert_message);
    $("#alert-modal").modal('show');

    // Set closing function
    function_after_modal_close = typeof function_after_modal_close === "undefined" ? false : function_after_modal_close;

    if (function_after_modal_close) {
        $(".close-alert-modal").on("click", function () {
            if (typeof function_after_modal_close === 'function') {
                function_after_modal_close();
            }
        });
    }
}

function download_file(url) {
    var a = document.createElement('a');
    a.href = url;
    a.download = '';
    a.click();
}

function constructDownloadCheckedButtonUrlAndEnableOrDisableTheButton($type) {

    // Empty checked_order_ids
    var checked_order_ids = [];
    var download_checked_button = $('.download-checked-button');
    var type = $type;

    // Check if checkbox is checked and add it to the array
    $('.check-file').each(function () {

        if ($(this).is(':checked')) {
            checked_order_ids.push(getTrData($(this), 'order_id'))
        }
    });

    // Set Download checked button's src to selected order ids
    if (checked_order_ids.length) {
        download_checked_button.attr('href', 'files/download_zip?i=' + checked_order_ids.join(',') + '&type=' + type);
    } else {
        download_checked_button.removeAttr('href');
    }

    // Disable Download checked button if nothing is selected
    download_checked_button.attr('disabled', !checked_order_ids.length)

}


// Polyfill for older browsers which don't have ['hay', 'stack'].includes('needle') functionality
if (![].includes) {
    Array.prototype.includes = function (searchElement /*, fromIndex*/) {
        'use strict';
        var O = Object(this);
        var len = parseInt(O.length) || 0;
        if (len === 0) {
            return false;
        }
        var n = parseInt(arguments[1]) || 0;
        var k;
        if (n >= 0) {
            k = n;
        } else {
            k = len + n;
            if (k < 0) {
                k = 0;
            }
        }
        var currentElement;
        while (k < len) {
            currentElement = O[k];
            if (searchElement === currentElement ||
                (searchElement !== searchElement && currentElement !== currentElement)) {
                return true;
            }
            k++;
        }
        return false;
    };
}

function decode_base64(s) {
    var e = {}, i, k, v = [], r = '', w = String.fromCharCode;
    var n = [[65, 91], [97, 123], [48, 58], [43, 44], [47, 48]];

    for (z in n) {
        for (i = n[z][0]; i < n[z][1]; i++) {
            v.push(w(i));
        }
    }
    for (i = 0; i < 64; i++) {
        e[v[i]] = i;
    }

    for (i = 0; i < s.length; i += 72) {
        var b = 0, c, x, l = 0, o = s.substring(i, i + 72);
        for (x = 0; x < o.length; x++) {
            c = e[o.charAt(x)];
            b = (b << 6) + c;
            l += 6;
            while (l >= 8) {
                r += w((b >>> (l -= 8)) % 256);
            }
        }
    }
    return r;
}

/**
 * Adds or changes a query parameter in url
 * @param url string input url
 * @param key string query parameter name
 * @param value string new value for the query parameter
 * @returns {string} modified url
 */
function setUrlParameter(url, key, value) {
    var parts = url.split("#", 2), anchor = parts.length > 1 ? "#" + parts[1] : '';
    var query = (url = parts[0]).split("?", 2);
    if (query.length === 1)
        return url + "?" + key + "=" + value + anchor;

    for (var params = query[query.length - 1].split("&"), i = 0; i < params.length; i++)
        if (params[i].toLowerCase().startsWith(key.toLowerCase() + "="))
            return params[i] = key + "=" + value, query[query.length - 1] = params.join("&"), query.join("?") + anchor;

    return url + "&" + key + "=" + value + anchor
}