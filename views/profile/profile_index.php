<style>
    section.user .tabs-content .let-table tbody tr td:first-child {
        padding-left: 0
    }

    .let-table thead tr th {
        text-align: left !important;
    }
    p {
        margin-bottom: 0;
    }

    th {
        text-align: center !important;
    }
</style>
<section class="user">

    <div class="spacer-l"></div>

    <div class="row">
        <div class="columns">

            <h4 class="h-bold"><?= __('Customer profile') ?></h4>

            <div class="spacer-m"></div>

            <div class="white-panel profile-wrapper">
                <div class="row">
                    <div class="column medium-6">
                        <div class="pull-left">
                            <h4 class="h-bold"><?= $auth->user_full_name ?></h4>

                            <a href="#"><?= $auth->user_email ?></a>
                        </div>
                    </div>

                    <div class="column medium-6 text-right">
                        <div class="spacer-s"></div>

                        <a class="button rect small success" data-toggle="modal" href="#edit-profile-modal">
                            <?= __('Edit profile') ?>
                        </a>

                        <a class="button rect small success" data-toggle="modal" href="#edit-password-modal">
                            <?= __('Change password') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="spacer-m"></div>

    <div class="row">
        <div class="columns">
            <div class="white-panel ">
                <div class=" text-right">
                    <div class="spacer-s"></div>
                    <a class="button rect small btn-danger delete-account">
                        <?= __('Delete account') ?>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="spacer-m"></div>
    <div class="spacer-l"></div>
</section>

<div class="modal fade" id="edit-profile-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Edit profile') ?></h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-2">
                        <?= __('Email') ?>
                    </div>

                    <div class="col-sm-10">
                        <input type="text" class="form-control user-email" value="<?= $user['user_email'] ?>">
                    </div>

                    <span class="col-sm-2"></span>

                    <span style="display: none; color: red" id="email-warning" class="col-sm-10">
                        <i class="fa fa-exclamation-triangle"></i>
                        <?= __('You are changing your email address!') ?>
                        <a data-toggle="modal" href="#email-warning-modal"><?= __("Learn about the consequences") ?></a>
                        <div class="spacer-s"></div>
                    </span>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <?= __('Name') ?>
                    </div>

                    <div class="col-sm-10">
                        <input type="text" class="form-control user-name" value="<?= $user['user_full_name'] ?>">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <?= __('Country') ?>
                    </div>

                    <div class="col-sm-10">
                        <select name="country_id" id="country" class="select2 form-control user-country"
                                style="width:100%">
                            <option disabled value=""><?= __('Please select one') ?></option>
                            <?php foreach ($countries as $country): ?>
                                <option data-country_id="<?= $country['country_id'] ?>"
                                        value="<?= $country['country_id'] ?>"
                                    <?= $country['country_id'] === $user['country_id'] ? 'selected' : '' ?>
                                >
                                    <?= $country['country_name'] ?>
                                </option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <?= __('State or county') ?>
                    </div>

                    <div class="col-sm-10">
                        <input type="text" class="form-control user-state" value="<?= $user['user_state'] ?>">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <?= __('City') ?>
                    </div>

                    <div class="col-sm-10">
                        <input type="text" class="form-control user-city" value="<?= $user['user_city'] ?>">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <?= __('Street') ?>
                    </div>

                    <div class="col-sm-10">
                        <input type="text" class="form-control user-street" value="<?= $user['user_street'] ?>">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <?= __('Postal code') ?>
                    </div>

                    <div class="col-sm-10">
                        <input type="text" class="form-control user-postal-code"
                               value="<?= $user['user_postal_code'] ?>">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
                <a class="btn btn-default edit-profile-save" data-dismiss="modal"
                   href="#enter-password-modal"><?= __('Save') ?></a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.edit-user-modal -->

<div class="modal fade" id="edit-password-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Change password') ?></h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-4">
                        <?= __('Current password') ?>
                    </div>

                    <div class="col-sm-8">
                        <input type="password" class="form-control user-current-password">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <?= __('New Password') ?>
                    </div>

                    <div class="col-sm-8">
                        <input type="password" class="form-control user-new-password">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <?= __('Retype new password') ?>
                    </div>

                    <div class="col-sm-8">
                        <input type="password" class="form-control user-new-password-retyped">
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
                <button type="button" class="btn btn-primary edit-password-btn"><?= __('Save') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.edit-password-modal -->

<div class="modal fade" id="enter-password-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Edit profile') ?></h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-4">
                        <?= __('Please enter your password to continue') ?>
                    </div>

                    <div class="col-sm-8">
                        <input type="password" class="form-control requested-user-password">
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Cancel') ?></button>
                <button type="button" class="btn btn-primary edit-profile-btn"><?= __('Confirm') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.enter-password-modal -->

<div class="modal fade" id="view-order-details">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= __('Order details') ?></h4>
            </div>

            <div class="modal-body">
                <table class="let-table">
                    <thead>
                    <tr>
                        <th><?= __('Product') ?></th>
                        <th><?= __('Width') ?></th>
                        <th><?= __('Height') ?></th>
                        <th><?= __('Price') ?></th>
                        <th><?= __('Quantity') ?></th>
                    </tr>
                    </thead>
                    <tbody class="order-details-tbody"></tbody>
                </table>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- View order details -->



<div class="modal fade" id="email-warning-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="spacer-s"></div>
                <h4 class="h-bold"><?= __('Changing your email address') ?></h4>
                <div class="spacer-s"></div>
            </div>
            <div class="modal-body">
                <p><?= __('Changing the order email address will change the email you use to login to this site.') ?></p>
                <p><?= __('Make sure you can receive emails from the new address.') ?></p>
                <p><?= __('If you use Facebook/Google to log in, its email must match this email.') ?></p>
                <p><?= __('If you want to start using an email which is not your Facebook/Google email, use the "Forgot password?" link in the login page to get a password.') ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="button primary alert" data-dismiss="modal">
                    <?= __('I understand') ?>
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

    var user_email;
    var user_full_name;
    var country_id;
    var user_state;
    var user_city;
    var user_street;
    var user_postal_code;

    $(".edit-profile-save").on("click", function () {
        $('#enter-password-modal').modal("show");
        user_email = $(".user-email").val();
        user_full_name = $(".user-name").val();
        country_id = $(".user-country").val();
        user_state = $(".user-state").val();
        user_city = $(".user-city").val();
        user_street = $(".user-street").val();
        user_postal_code = $(".user-postal-code").val();
    });

    // Edit user data
    $(".edit-profile-btn").on("click", function () {
        ajax("users/edit", {
            "user_email": user_email,
            "user_full_name": user_full_name,
            "country_id": country_id,
            "user_state": user_state,
            "user_city": user_city,
            "user_street": user_street,
            "user_postal_code": user_postal_code,
            "requested_user_password": $(".requested-user-password").val()
        }, RELOAD)
    });

    // Edit user password
    $(".edit-password-btn").on("click", function () {
        ajax("users/edit_password", {
            "current_password": $(".user-current-password").val(),
            "new_password": $(".user-new-password").val(),
            "new_password_retyped": $(".user-new-password-retyped").val()
        }, RELOAD)
    });



    $(".delete-account").on("click", function () {
        swal({
            title: "<?= __('Do you wish to delete your user account?') ?>",
            icon: "warning",
            buttons: true,
            dangerMode: true
        }).then(function (willDelete) {
            if (willDelete) {
                ajax("users/delete", null, function () {
                    location.href = "logout"
                })
            }
        })
    });

    $(".user-email").on("change", function () {
        $("#email-warning").show()
    });
</script>