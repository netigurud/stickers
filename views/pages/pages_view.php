<section class="cart-cart">
    <div class="row">
        <div class="column medium-12 contactpage">
            <div class="spacer-m"></div>

            <h4 class="h-bold"><?= __($page['page_title']) ?></h4>

            <div class="spacer-m"></div>

            <div class="white-panel">

                <?php if ($page['page_excerpt']): ?>
                    <i><?= __($page['page_excerpt']); ?></i>
                    <div class="spacer-s"></div>
                <?php endif; ?>

                <?= __($page['page_content_html']) ?>

                <?php if ($page['file_id']): ?>
                    <div class="spacer-s"></div>
                    <div class="row">
                        <div class="column medium-12">
                            <div class="player-wrapper">
<!--                                <img class="player-poster" src="assets/img/video-bg.png">-->

<!--                                <button class="video-play-button"></button>-->

                                <video id="my-video" class="video-js" controls
                                        data-setup="{}" width="670" height="376">
                                    <source src="files/view/<?= $page['file_id']; ?>" type='video/mp4'>

                                    <p class="vjs-no-js">
                                        To view this video please enable JavaScript, and consider upgrading to a web browser
                                        that
                                        <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5
                                            video</a>
                                    </p>
                                </video>
                            </div>
                        </div>
                    </div>

                <?php endif; ?>

            </div>

            <div class="spacer-l"></div>
        </div>
    </div>
</section>