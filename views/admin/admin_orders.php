<style>
    .let-table tbody tr td {
        vertical-align: middle;
    }



    th {
        text-align: center !important;
    }

    p {
        margin-bottom: 0;
    }

    .let-table thead tr th {
        text-align: left !important;
    }

    .let-table tbody tr td {
        padding: 0.4rem 0.4rem 0.4rem 0rem;
        text-align: left;
    }
</style>
<section class="admin-orders">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-header">
                    <h3>Orders</h3>
                </div>

                <div class="panel-body">
                    <table class="let-table">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Order by</th>
                            <th>Date</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th>File</th>
                            <th>Generate Invoice</th>
                            <th>Promo</th>
                            <th>Designer</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php foreach ($orders['orders'] as $order): ?>
                            <tr data-order_id="<?= $order['order_id'] ?>">
                                <td style="width: auto">
                                    <?php if ($order['simplbooks_invoice_id']): ?>
                                        <input type="checkbox" class="check-file">
                                    <?php endif; ?>
                                </td>
                                <td><?= $order['order_id'] ?></td>
                                <td><?= $order['user_full_name'] ?></td>
                                <td><?= $order['order_made_at'] ?></td>
                                <td>
                                    <p class="price-without-vat"
                                       style="display: none;"><?= $order['order_total_price'] ?> €</p>

                                    <p class="price-with-vat">
                                        <?= $order['order_total_price_with_vat'] ?> €
                                    </p>
                                </td>
                                <td>
                                    <select class="form-control order-status">
                                        <?php foreach ($order_statuses as $order_status): ?>
                                            <option value="<?= $order_status['order_status_id'] ?>"
                                                <?= $order['order_status_id'] == $order_status['order_status_id'] ? 'selected' : '' ?>>
                                                <?= $order_status['order_status_name'] ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                    <?php if ($order['order_status_id'] >= ORDER_STATUS_WAITING_FOR_DELIVERY): ?>
                                        <a target="_blank"
                                           href="https://www.tnt.com/express/en_us/site/tracking.html?searchType=con&cons=<?= $order['order_con_number'] ?>">
                                            <?= $order['order_con_number'] ?>
                                        </a>
                                    <?php endif; ?>
                                    </p>
                                </td>
                                <td class="order-image-column" data-open="order-image-modal"
                                    data-file_id="<?= $order['order_file_id'] ?>">
                                    <?php if ($order['order_file_id']): ?>
                                        <img class="order-file" style="max-width: 50px"
                                             src="data:image/jpeg;base64,<?= $order['file_preview'] ?>">
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <?php if ($order['order_status_id'] == ORDER_STATUS_QUEUED_FOR_PRINTING || $order['order_status_id'] == ORDER_STATUS_IN_PRODUCTION || $order['order_status_id'] == ORDER_STATUS_WAITING_FOR_DELIVERY): ?>
                                        <?php if (empty($order['simplbooks_invoice_id'])): ?>
                                            <a class="btn btn-default generate-simplbooks-invoice">
                                                <span class="glyphicon glyphicon-file"></span>
                                            </a>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td><?= $order['order_promo_code'] ?></td>
                                <td><?= $order['designer_full_name'] ?></td>
                            </tr>
                            <tr>
                                <?php if (!empty($order['request_changes_reason'])): ?>
                                    <td colspan="3"><b>Req. changes reason: </b></td>
                                    <td colspan="7" style="text-align: left"><?= $order['request_changes_reason'] ?></td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <a class="btn btn-primary download-checked-button" disabled download>
                <span class="glyphicon glyphicon-download-alt"></span>&nbsp;&nbsp;Download checked
            </a>

            <a class="btn btn-primary toggle-price-vat">
                <span class="price-without-vat" style="display: none;">
                   Show prices with VAT
                </span>

                <span class="price-with-vat">
                    Show prices without VAT
                </span>
            </a>

            <div class="spacer-m"></div>
        </div>
    </div>

    <div class="reveal login-reveal" id="order-image-modal" data-reveal>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>

        <div class="row columns">
            <div class="spacer-s"></div>
            <h4 class="h-bold">Orders file</h4>
            <div class="spacer-m"></div>
        </div>

        <div class="row columns">
            <img src="" class="order-image">
        </div>

        <div class="spacer-s"></div>


        <div class="row columns">
            <div class="column medium-12">
                <a class="button primary full-page download-file" type="button"
                   download="download">Download</a>
            </div>

            <div class="column medium-12">
                <button class="button primary full-page resumable-browse" type="button">
                    Change
                </button>
            </div>
        </div>
    </div>


</section>
<script src="assets/js/resumable.js"></script>

<script>

    var order_file_id;
    var r;


    $(function () {

        $('.check-file').on('click', function () {

            constructDownloadCheckedButtonUrlAndEnableOrDisableTheButton('invoices')
        });

        $('.order-image-column').on('click', function () {
            order_file_id = $(this).data('file_id');
            $('.download-file').prop('href', 'files/' + order_file_id);
            $('#change-file-order-id').val(getTrData($(this), 'order_id'))
        });

        // File upload

        resumable('files/upload/', false, function () {


            // Show the loading modal
            $('#progress-modal').modal('show');

            $('#upload-progress').removeClass('hide').find('.progress-bar').css('width', '0%');

            // Actually start the upload
            r.upload()

        }, function () {
            location.reload()
        });

        // Set the image src
        $('.order-file').on('click', function () {

            r.opts.target = 'files/upload/' + getTrData($(this), 'order_id');
            $('.order-image').attr('src', $(this).attr('src'))
        });

        $('.generate-simplbooks-invoice').on('click', function () {
            var that = $(this);

            swal({
                title: "Are you sure you want to generate an invoice to SimplBooks for this order?",
                icon: 'warning',
                buttons: true,
                dangerMode: true
            }).then(function (generate) {
                if (generate) {
                    ajax('admin/generate_invoice_for_order', {
                        order_id: getTrData(that, 'order_id')
                    }, RELOAD)
                }
            })
        });

        // Update order file
        $('.change-file').on('click', function () {
            swal({
                title: "Are you sure you want to change this orders file?",
                icon: 'warning',
                buttons: true,
                dangerMode: true
            }).then(function (updateFile) {
                if (updateFile) {
                    var new_order_file = $('#new-order-file');

                    // Show the file choosing popup
                    new_order_file.click();

                    // When the admin has chosen a file, submit the form
                    new_order_file.on('change', function () {
                        $('form#edit-order-file').submit()
                    })
                }
            })
        });

        var previous_status_value;

        // Let the admin update the orders status
        $('.order-status').on('focus', function () {
            // Store the current value on focus and on change
            previous_status_value = this.value
        }).change(function () {
            var that = $(this);

            swal({
                title: "Change this orders status?",
                icon: 'warning',
                buttons: true,
                dangerMode: true
            }).then(function (change) {
                if (change) {
                    ajax('admin/change_order_status', {
                        order_id: getTrData(that, 'order_id'),
                        order_status_id: that.val()
                    })
                } else {
                    that.val(previous_status_value)
                }
            });
        });

        // Show/hide prices with VAT
        $('.toggle-price-vat').on('click', function () {
            $('.price-without-vat').toggle();
            $('.price-with-vat').toggle()
        });

        $(".download-checked-button").on("click", function() {
            setTimeout(function () {
                location.reload()
            }, 300);
        });
    });
</script>