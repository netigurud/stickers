<style>
    select.form-control {
        padding: 5px;
    }

    .btn {
        padding: .500rem 1.875rem;
    }

    .let-table tbody tr td:first-child {
        width: auto;
        padding: 0;
    }

    .let-table tbody tr td {
        padding: 1.25rem 1.25rem;
    }

    .form-control {
        margin: 0;
    }

</style>
<section class="admin-prices">
    <div class="row">
        <div class="column medium-12">
            <div class="spacer-m"></div>
            <a class="button button-block" data-toggle="modal" href="#add-price-modal">Add price</a>
            <div class="spacer-s"></div>
        </div>

    </div>

    <div class="row">
        <div class="medium-12 column">
            <div class="panel panel-default">
                <div class="panel-header">
                    <h3>Prices</h3>
                </div>

                <div class="panel-body">
                    <table class="let-table">
                        <thead>
                        <tr>
                            <th>cm<sup>2</sup></th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Product type</th>
                            <th>Delete</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php foreach ($prices as $price): ?>
                            <tr data-price_id="<?= $price['price_id'] ?>">
                                <td><input data-field="m2" class="form-control" type="number"
                                           value="<?= $price['m2'] ?>"></td>
                                <td><input data-field="quantity" class="form-control" type="number"
                                           value="<?= $price['quantity'] ?>"></td>
                                <td><input data-field="price" class="form-control" type="number"
                                           value="<?= $price['price'] ?>"></td>
                                <td>
                                    <select data-field="product_type_id" class="form-control">
                                        <?php foreach ($product_types as $product_type): ?>
                                            <option value="<?= $product_type['product_type_id'] ?>"
                                                <?= $price['product_type_id'] == $product_type['product_type_id'] ? 'selected' : '' ?>>
                                                <?= $product_type['product_type_name'] ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td>
                                    <button class="btn delete-price">Delete</button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="column medium-12">
            <div class="spacer-m"></div>
            <a class="button button-block" data-toggle="modal" href="#add-price-modal">Add price</a>
            <div class="spacer-s"></div>
        </div>

    </div>
    <div class="modal fade" id="add-price-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add price</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="new-price-m2">cm<sup>2</sup></label>
                        </div>

                        <div class="col-sm-6">
                            <input type="number" id="new-price-m2" class="form-control" required="required">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="new-price-quantity">Quantity</label>
                        </div>

                        <div class="col-sm-6">
                            <input type="number" id="new-price-quantity" class="form-control" required="required">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="new-price">Price</label>
                        </div>

                        <div class="col-sm-6">
                            <input type="number" id="new-price" class="form-control" required="required">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="new-price-product-type-id">Product type</label>
                        </div>

                        <div class="col-sm-6">
                            <select id="new-price-product-type-id" class="form-control">
                                <?php foreach ($product_types as $product_type): ?>
                                    <option value="<?= $product_type['product_type_id'] ?>">
                                        <?= $product_type['product_type_name'] ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary add-new-price-btn">Add</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</section>

<script src="assets/js/sweetalert-2.1.0.js"></script>
<script>
    $(function () {
        // Delete price
        $(".delete-price").on("click", function () {
            that = $(this);

            swal({
                title: "Are you sure you want to delete this price?",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function (willDelete) {
                if (willDelete) {
                    ajax("admin/delete_price", {
                        price_id: getTrData(that, "price_id")
                    }, RELOAD);
                } else {
                    swal("Cancelled");
                }
            });
        });

        $(".add-new-price-btn").on("click", function () {
            ajax("admin/add_new_price", {
                m2: $("#new-price-m2").val(),
                quantity: $("#new-price-quantity").val(),
                price: $("#new-price").val(),
                product_type_id: $("#new-price-product-type-id").val()
            }, RELOAD);
        });

        $("td > input, select").on("change", function () {
            var that = $(this);
            ajax("admin/edit_price", {
                price_id: getTrData(that, "price_id"),
                field: $(this).data('field'),
                value: $(this).val()
            }, function (json) {
                that.css("background", '#6bf76b');
            });
        });
    });
</script>