<section class="admin-users">
    <div class="row">
        <div class="col-sm-12">
            <a class="button button-block add-page-modal-btn" data-open="add-edit-modal-modal">Add page</a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-header">
                    <h3>Pages</h3>
                </div>

                <div class="panel-body">
                    <table id="admin-pages-table" class="let-table">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Slug</th>


                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php foreach ($pages as $page): ?>
                            <tr data-page_id="<?= $page['page_id'] ?>">
                                <td><?= $page['page_title'] ?></td>
                                <td><?= $page['page_slug'] ?></td>


                                <td>
                                    <a class="btn btn-danger edit-page-modal-btn" data-open="add-edit-modal-modal">
                                        Edit
                                    </a>
                                </td>
                                <td><a class="btn btn-danger delete-page">Delete</a></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- ADD/EDIT PAGE MODAL
    ===================================================================================-->
    <div class="reveal" id="add-edit-modal-modal" data-reveal>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>

        <div class="row columns">
            <div class="spacer-s"></div>
            <h4 class="h-bold">Edit page</h4>
        </div>

        <div class="spacer-m"></div>


        <div class="row columns">
            <label>
                <span>Title <em class="required">Required</em></span>
                <input class="add-edit-modal-title" type="text" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Slug <em class="required">Required</em></span>
                <input class="add-edit-modal-slug" type="email" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Excerpt</span>
                <textarea class="add-edit-modal-excerpt"></textarea>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Content <em class="required">Required</em></span>
                <textarea class="add-edit-modal-content-html"></textarea>
            </label>
        </div>

        <div class="spacer-s"></div>

        <div class="row columns">
            <span>Video</span>
        </div>

        <form action="">

            <?php foreach ($videos as $video): ?>
                <div class="row columns">

                    <div class="columns small-2">
                        <input id="add-edit-modal-file-id<?= $video['file_id'] ?>"
                               class="add-edit-modal-file-id"
                               type="radio"
                               name="add-edit-modal-file-id"
                               data-file_id="<?= $video['file_id']; ?>"/>
                        <img style="max-height: 40px;"
                             src="data:image/jpeg;base64,<?= $video['file_preview'] ?>">
                    </div>

                    <div class="columns small-8">
                        <label for="add-edit-modal-file-id<?= $video['file_id'] ?>"><?= $video['file_name'] ?></label>
                    </div>

                    <div class="columns small-2">
                        <a class="delete-video-btn">Delete</a>
                    </div>

                </div>
            <?php endforeach; ?>

            <!-- the "no video" option -->
            <div class="row columns">
                <div class="columns small-2">
                    <input id="no-video"
                           class="add-edit-modal-file-id"
                           type="radio"
                           name="add-edit-modal-file-id"
                           data-file_id="null"/>
                </div>
                <div class="columns small-10">
                    <label for="no-video" style="white-space: nowrap">No video</label>
                </div>
            </div>

        </form>

        <div class="spacer-xs"></div>


        <div class="row columns">
            <div class="columns small-12">
                <button class="btn resumable-browse" type="button">
                    Upload video
                </button>
            </div>
        </div>


        <div class="spacer-m"></div>

        <div class="row columns">
            <div class="column medium-12">
                <button class="button primary full-page add-page-modal-add-btn" type="button">Add</button>
                <button class="button primary full-page edit-page-modal-save-btn" type="button">Save</button>
            </div>
        </div>
    </div>

</section>


<script src="assets/js/resumable.js"></script>


<!-- SUMMERNOTE -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css?<?= COMMIT_HASH ?>" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js?<?= COMMIT_HASH ?>"></script>

<script>
    var btn;
    $(function () {
        var page_id;

        var add_edit_modal_title = $(".add-edit-modal-title");
        var add_edit_modal_slug = $(".add-edit-modal-slug");
        var add_edit_modal_content_html = $(".add-edit-modal-content-html");
        var add_edit_modal_file_id = $(".add-edit-modal-file-id");
        var add_edit_modal_excerpt = $(".add-edit-modal-excerpt");

        $(".add-edit-modal-content-html, .add-edit-modal-excerpt").summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ],
            height: 300
        });

        // Fill in the page adding/editing modal
        $(".edit-page-modal-btn").on("click", function () {

            $('.add-page-modal-add-btn').hide();
            $('.edit-page-modal-save-btn').show();

            page_id = getTrData($(this), 'page_id');

            ajax("pages/get", {
                page_id: page_id

            }, function (json) {
                add_edit_modal_title.val(json.data.page_title);
                add_edit_modal_slug.val(json.data.page_slug);
                add_edit_modal_content_html.summernote("code", json.data.page_content_html);
                add_edit_modal_excerpt.summernote("code", json.data.page_excerpt);

                // Set the correct video as selected by default
                add_edit_modal_file_id.filter('[data-file_id=' + json.data.file_id + ']').prop('checked', true);

            });
        });

        // Add new page button hides the "wrong" button
        $(".add-page-modal-btn").on("click", function () {
            $('.add-page-modal-add-btn').show();
            $('.edit-page-modal-save-btn').hide();

            // Clear modal form fields
            add_edit_modal_title.val('');
            add_edit_modal_slug.val('');
            add_edit_modal_content_html.summernote("code", '');
            add_edit_modal_excerpt.summernote("code", '');

            // Set the correct video as selected by default
            $('#no-video').prop('checked', true);
        });

        // Delete product

        $(".delete-video-btn").on("click", function () {
            btn = $(this);
            swal({
                title: "Are you sure you want to delete this product?",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function (willDelete) {
                if (willDelete) {
                    ajax("admin/delete_file", {
                        file_id: btn.parent().parent().children().find('input').data('file_id')
                    }, function (json) {
                        if (json.status === 200) {
                            btn.parent().parent().remove();
                        }
                    });
                    // Set No video option
                    $('#no-video').prop('checked', true);

                } else {
                    swal("Cancelled");
                }
            });
        });

        // Add page
        $(".add-page-modal-add-btn").on("click", function () {

            var required_fields = [
                '.add-edit-modal-title',
                '.add-edit-modal-content-html'
            ];

            if (requiredFieldsAreEmpty(required_fields)) {
                return false;
            }


            ajax("admin/add_page", {
                page_title: $(".add-edit-modal-title").val(),
                file_id: $(".add-edit-modal-file-id:checked").data('file_id'),
                page_content_html: add_edit_modal_content_html.summernote('code'),
                page_excerpt: add_edit_modal_excerpt.summernote('code')
            }, RELOAD);
        });

        // Edit the page
        $(".edit-page-modal-save-btn").on("click", function () {
            var required_fields = [
                '.add-edit-modal-title',
                '.add-edit-modal-slug',
                '.add-edit-modal-content-html'
            ];

            if (requiredFieldsAreEmpty(required_fields)) {
                return false;
            }


            ajax("admin/edit_page", {
                page_id: page_id,
                page_title: add_edit_modal_title.val(),
                file_id: $(".add-edit-modal-file-id:checked").data('file_id'),
                page_slug: add_edit_modal_slug.val(),
                page_content_html: add_edit_modal_content_html.summernote('code'),
                page_excerpt: add_edit_modal_excerpt.summernote('code')
            }, RELOAD);
        });

        // Delete the page
        $(".delete-page").on("click", function () {
            page_id = getTrData($(this), 'page_id');

            swal({
                title: "Delete this page?",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function (willDelete) {
                if (willDelete) {
                    ajax("admin/delete_page", {
                        page_id: page_id
                    }, RELOAD);
                }
            });
        });

        resumable('files/upload/', false, function () {

            // Show the loading modal
            $('#progress-modal').modal('show');

            $('#upload-progress').removeClass('hide').find('.progress-bar').css('width', '0%');

            // Actually start the upload
            r.upload()

        }, function () {
            location.reload()
        }, ['mp4'], {file_is_video: 1});


    });
</script>