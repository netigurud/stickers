<?php use App\DateTime; ?>
<style>
    .unsettled {
        background: #ffcccc;
    }
    .settled {
        background: #ccffcc;
    }
    td, th {
        text-align: center !important;
        white-space: nowrap;
    }
</style>
<section class="admin-users">

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-header">
                    <h3><?= __('Designers') ?></h3>
                </div>
                <div class="panel-body">

                    <select name="year" id="year">
                        <option value=""> -- Select Year --</option>
                        <?php foreach ($years as $year): ?>
                            <option value="<?= $year ?>" <?= $year == $selected_year ? 'selected' : '' ?>>
                                <?= $year ?>
                            </option>
                        <?php endforeach; ?>
                    </select>

                    <select name="month" id="month">
                        <option value=""> -- Select Month --</option>
                        <?php foreach ($months as $month): ?>
                            <option value="<?= DateTime::padMonth($month) ?>"
                                <?= $month == $selected_month ? 'selected' : '' ?>>
                                <?= DateTime::padMonth($month) ?>
                            </option>
                        <?php endforeach; ?>
                    </select>

                    <table id="admin-designers-table" class="let-table">
                        <thead>
                        <tr>
                            <th><?= __('Full name') ?></th>
                            <?php for ($m = $begin_month; $m <= $end_month; $m++): ?>
                                <th><?= DateTime::period($selected_year, $m) ?></th>
                            <?php endfor; ?>
                        </tr>
                        </thead>

                        <tbody>
                        <?php foreach ($designers as $user_id=>$designer): ?>
                            <tr data-user_id="<?= $user_id ?>">
                                <td class="designer-row"><?= $designer['user_full_name'] ?></td>
                                <?php for ($m = $begin_month; $m <= $end_month; $m++):
                                    $c = $designer['periods'][DateTime::period($selected_year, $m)];?>
                                    <td <?= $c['settled'] ?
                                        'class="settled"' : ($c['settled'] === 0 ? 'class="unsettled"' : '') ?>>
                                        <?= $c['count'] ?>
                                    </td>
                                <?php endfor; ?>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="reveal login-reveal" id="add-user-modal" data-reveal>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>

        <div class="row columns">
            <div class="spacer-s"></div>
            <h4 class="h-bold"><?= __('Add user') ?></h4>
        </div>

        <div class="spacer-m"></div>


        <div class="row columns">
            <label>
                <span><?= __('Full name') ?> <em class="required"><?= __('Required') ?></em></span>
                <input class="add-user-full-name" type="text" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Email') ?> <em class="required"><?= __('Required') ?></em></span>
                <input class="add-user-email" type="email" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Password') ?> <em class="required"><?= __('Required') ?></em></span>
                <input class="add-user-password" type="text" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Company') ?></span>
                <input class="add-user-company" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Phone') ?></span>
                <input class="add-user-phone" type="number">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Country') ?></span>
                <input class="add-user-country" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('State or county') ?></span>
                <input class="add-user-state" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('City') ?> <em class="required"><?= __('Required') ?></em></span>
                <input class="add-user-city" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Street') ?> <em class="required"><?= __('Required') ?></em></span>
                <input class="add-user-street" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Postal code') ?> <em class="required"><?= __('Required') ?></em></span>
                <input class="add-user-postal-code" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Is admin') ?></span>
                <select class="add-user-is-admin">
                    <option value="0"><?= __('No') ?></option>
                    <option value="1"><?= __('Yes') ?></option>
                </select>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Is printing house') ?></span>
                <select class="add-user-is-printing-house">
                    <option value="0"><?= __('No') ?></option>
                    <option value="1"><?= __('Yes') ?></option>
                </select>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Is designer') ?></span>
                <select class="add-user-is-designer">
                    <option value="0"><?= __('No') ?></option>
                    <option value="1"><?= __('Yes') ?></option>
                </select>
            </label>
        </div>

        <div class="spacer-m"></div>

        <div class="row columns">
            <div class="column medium-12">
                <button class="button primary full-page add-user" type="button"><?= __('Save') ?></button>
            </div>
        </div>
    </div>

    <div class="reveal login-reveal" id="edit-user-modal" data-reveal>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>

        <div class="row columns">
            <div class="spacer-s"></div>
            <h4 class="h-bold"><?= __('Edit user') ?></h4>
        </div>

        <div class="spacer-m"></div>


        <div class="row columns">
            <label>
                <span><?= __('Full name') ?> <em class="required"><?= __('Required') ?></em></span>
                <input class="edit-user-full-name" type="text" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Email') ?> <em class="required"><?= __('Required') ?></em></span>
                <input class="edit-user-email" type="email" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Company') ?></span>
                <input class="edit-user-company" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Phone') ?></span>
                <input class="edit-user-phone" type="number">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Country') ?></span>
                <input class="edit-user-country" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('State or county') ?></span>
                <input class="edit-user-state" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('City') ?> <em class="required"><?= __('Required') ?></em></span>
                <input class="edit-user-city" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Street') ?> <em class="required"><?= __('Required') ?></em></span>
                <input class="edit-user-street" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Postal code') ?> <em class="required"><?= __('Required') ?></em></span>
                <input class="edit-user-postal-code" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Is admin') ?></span>
                <select class="edit-user-is-admin">
                    <option value="0"><?= __('No') ?></option>
                    <option value="1"><?= __('Yes') ?></option>
                </select>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Is printing house') ?></span>
                <select class="edit-user-is-printing-house">
                    <option value="0"><?= __('No') ?></option>
                    <option value="1"><?= __('Yes') ?></option>
                </select>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span><?= __('Is designer') ?></span>
                <select class="edit-user-is-designer">
                    <option value="0"><?= __('No') ?></option>
                    <option value="1"><?= __('Yes') ?></option>
                </select>
            </label>
        </div>

        <div class="spacer-m"></div>

        <div class="row columns">
            <div class="column medium-12">
                <button class="button primary full-page edit-user" type="button"><?= __('Save') ?></button>
            </div>
        </div>
    </div>

    <div class="spacer-l"></div>

</section>

<script src="assets/js/BootstrapMenu.js"></script>
<script>
    $(function () {
        var user_id;

        var edit_user_full_name = $(".edit-user-full-name");
        var edit_user_email = $(".edit-user-email");
        var edit_user_company = $(".edit-user-company");
        var edit_user_phone = $(".edit-user-phone");
        var edit_country_id = $(".edit-user-country");
        var edit_user_state = $(".edit-user-state");
        var edit_user_city = $(".edit-user-city");
        var edit_user_street = $(".edit-user-street");
        var edit_user_postal_code = $(".edit-user-postal-code");
        var edit_user_is_admin = $(".edit-user-is-admin");
        var edit_user_is_printing_house = $(".edit-user-is-printing-house");
        var edit_user_is_designer = $(".edit-user-is-designer");

        $('#admin-users-table').DataTable();

        // Fill in the user editing modal
        $(".edit-user-modal-btn").on("click", function () {
            user_id = getTrData($(this), 'user_id');

            ajax("users/get", {
                user_id: user_id
            }, function (json) {
                edit_user_full_name.val(json.data.user_full_name);
                edit_user_email.val(json.data.user_email);
                edit_user_company.val(json.data.user_company);
                edit_user_phone.val(json.data.user_phone);
                edit_country_id.val(json.data.country_id);
                edit_user_state.val(json.data.user_state);
                edit_user_city.val(json.data.user_city);
                edit_user_street.val(json.data.user_street);
                edit_user_postal_code.val(json.data.user_postal_code);
                edit_user_is_admin.val(json.data.is_admin);
                edit_user_is_printing_house.val(json.data.is_printing_house);
                edit_user_is_designer.val(json.data.is_designer);
            });
        });

        // Add user
        $(".add-user").on("click", function () {
            var something_empty = false;

            var required_fields = [
                '.add-user-full-name',
                '.add-user-email',
                '.add-user-password',
                '.add-user-city',
                '.add-user-street',
                '.add-user-postal-code'
            ];

            if (requiredFieldsAreEmpty(required_fields)) {
                return false;
            }

            ajax("email/available", {
                email: $(".add-user-email").val()
            }, function () {

                // If the given email doesn't exist then insert the user
                ajax("admin/add_user", {
                    user_full_name: $(".add-user-full-name").val(),
                    user_email: $(".add-user-email").val(),
                    password: $(".add-user-password").val(),
                    user_company: $(".add-user-company").val(),
                    user_phone: $(".add-user-phone").val(),
                    country_id: $(".add-user-country").val(),
                    user_state: $(".add-user-state").val(),
                    user_city: $(".add-user-city").val(),
                    user_street: $(".add-user-street").val(),
                    user_postal_code: $(".add-user-postal-code").val(),
                    is_admin: $(".add-user-is-admin").val(),
                    is_printing_house: $(".add-user-is-printing-house").val(),
                    is_designer: $(".add-user-is-designer").val()
                }, RELOAD);
            });
        });

        // Edit the user
        $(".edit-user").on("click", function () {
            var something_empty = false;

            var required_fields = [
                '.edit-user-full-name',
                '.edit-user-email',
                '.edit-user-city',
                '.edit-user-street',
                '.edit-user-postal-code'
            ];

            if (requiredFieldsAreEmpty(required_fields)) {
                return false;
            }

            ajax("admin/edit_user", {
                user_id: user_id,
                user_full_name: edit_user_full_name.val(),
                user_email: edit_user_email.val(),
                user_company: edit_user_company.val(),
                user_phone: edit_user_phone.val(),
                country_id: edit_country_id.val(),
                user_state: edit_user_state.val(),
                user_city: edit_user_city.val(),
                user_street: edit_user_street.val(),
                user_postal_code: edit_user_postal_code.val(),
                is_admin: edit_user_is_admin.val(),
                is_printing_house: edit_user_is_printing_house.val(),
                is_designer: edit_user_is_designer.val(),
            }, RELOAD);
        });

        // Delete the user
        $(".delete-user").on("click", function () {
            user_id = getTrData($(this), 'user_id');

            swal({
                title: "<?= __('Delete this user?') ?>",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function (willDelete) {
                if (willDelete) {
                    ajax("admin/delete_user", {
                        user_id: user_id
                    }, RELOAD);
                }
            });
        });

        // Send the user their data
        $(".send-user-data").on("click", function () {
            user_id = getTrData($(this), 'user_id');

            swal({
                title: "<?= __('Send this users data to the its email?') ?>",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function (willSend) {
                if (willSend) {
                    ajax("admin/send_user_data", {
                        user_id: user_id
                    }, RELOAD);
                }
            });
        });

        $("#month, #year").on("change", function () {
            location.href = setUrlParameter(location.href, this.id, this.value);
        });
    });

    var menu = new BootstrapMenu('td', {
        fetchElementData: function($rowElem) {
            return $rowElem;
        },
        actions: [{
            name: 'Mark',
            onClick: function (cell) {
                change_mark(cell, 'mark')
            }
        }, {
            name: 'Unmark',
            onClick: function (cell) {
                change_mark(cell, 'unmark')
            }
        }]
    });

    function change_mark(cell, action) {
        var nthChild = cell.get(0).cellIndex + 1;
        var period = $('tr:first-child th:nth-child('+nthChild+')').html();
        var user_id = (cell.parent().data('user_id'));
        ajax('admin/change_mark', {
            period: period,
            user_id: user_id,
            action: action
        }, function (json) {
            cell.attr('class', action === 'mark' ? 'settled' : 'unsettled')
        })
    }

</script>