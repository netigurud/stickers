<style>
    #admin-users-table .deleted {
        background: #ffcccc
    }
</style>
<section class="admin-users">
    <div class="row">
        <div class="col-sm-12">
            <a class="button button-block" data-open="add-user-modal">Add user</a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-header">
                    <h3>Users</h3>
                </div>

                <div class="panel-body">
                    <table id="admin-users-table" class="let-table">
                        <thead>
                        <tr>
                            <th>Full name</th>
                            <th>Email</th>
                            <th>Company</th>
                            <th>Phone</th>
                            <th>Country</th>
                            <th>State or county</th>
                            <th>City</th>
                            <th>Street</th>
                            <th>Postal code</th>
                            <th>Is admin</th>
                            <th>Is priting house</th>
                            <th>Is designer</th>
                            <th>Edit</th>
                            <th>Send user data</th>
                            <th>Delete user</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php foreach ($users as $user): ?>
                            <tr class="<?= $user['deleted'] ? 'deleted' : ''?>"
                                    data-user_id="<?= $user['user_id'] ?>">

                                <td><?= $user['user_full_name'] ?></td>
                                <td><?= $user['user_email'] ?></td>
                                <td><?= $user['user_company'] ?></td>
                                <td><?= $user['user_phone'] ?></td>
                                <td><?= $user['country_name'] ?></td>
                                <td><?= $user['user_state'] ?></td>
                                <td><?= $user['user_city'] ?></td>
                                <td><?= $user['user_street'] ?></td>
                                <td><?= $user['user_postal_code'] ?></td>
                                <td><?= $user['is_admin'] == 1 ? "Yes" : "No" ?></td>
                                <td><?= $user['is_printing_house'] == 1 ? "Yes" : "No" ?></td>
                                <td><?= $user['is_designer'] == 1 ? "Yes" : "No" ?></td>
                                <td>
                                    <a class="btn btn-success edit-user-modal-btn" data-open="edit-user-modal">
                                        Edit
                                    </a>
                                </td>
                                <td><a class="btn btn-success send-user-data">Send data</a></td>
                                <td>
                                    <?php if ($user['deleted'] == 0) : ?>
                                        <a class="btn btn-danger delete-user">Delete</a>
                                    <?php else : ?>
                                        <a class="btn btn-warning restore-user">Restore</a>
                                    <?php endif ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="reveal login-reveal" id="add-user-modal" data-reveal>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>

        <div class="row columns">
            <div class="spacer-s"></div>
            <h4 class="h-bold">Add user</h4>
        </div>

        <div class="spacer-m"></div>


        <div class="row columns">
            <label>
                <span>Full name <em class="required">Required</em></span>
                <input class="add-user-full-name" type="text" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Email <em class="required">Required</em></span>
                <input class="add-user-email" type="email" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Password <em class="required">Required</em></span>
                <input class="add-user-password" type="text" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Company</span>
                <input class="add-user-company" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Phone</span>
                <input class="add-user-phone" type="number">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Country</span>
                <input class="add-user-country" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>State or county</span>
                <input class="add-user-state" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>City <em class="required">Required</em></span>
                <input class="add-user-city" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Street <em class="required">Required</em></span>
                <input class="add-user-street" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Postal code <em class="required">Required</em></span>
                <input class="add-user-postal-code" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Is admin</span>
                <select class="add-user-is-admin">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Is printing house</span>
                <select class="add-user-is-printing-house">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Is designer</span>
                <select class="add-user-is-designer">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </label>
        </div>

        <div class="spacer-m"></div>

        <div class="row columns">
            <div class="column medium-12">
                <button class="button primary full-page add-user" type="button">Save</button>
            </div>
        </div>
    </div>

    <div class="reveal login-reveal" id="edit-user-modal" data-reveal>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>

        <div class="row columns">
            <div class="spacer-s"></div>
            <h4 class="h-bold">Edit user</h4>
        </div>

        <div class="spacer-m"></div>


        <div class="row columns">
            <label>
                <span>Full name <em class="required">Required</em></span>
                <input class="edit-user-full-name" type="text" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Email <em class="required">Required</em></span>
                <input class="edit-user-email" type="email" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Company</span>
                <input class="edit-user-company" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Phone</span>
                <input class="edit-user-phone" type="number">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Country</span>
                <input class="edit-user-country" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>State or county</span>
                <input class="edit-user-state" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>City <em class="required">Required</em></span>
                <input class="edit-user-city" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Street <em class="required">Required</em></span>
                <input class="edit-user-street" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Postal code <em class="required">Required</em></span>
                <input class="edit-user-postal-code" type="text">
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Is admin</span>
                <select class="edit-user-is-admin">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Is printing house</span>
                <select class="edit-user-is-printing-house">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Is designer</span>
                <select class="edit-user-is-designer">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </label>
        </div>

        <div class="spacer-m"></div>

        <div class="row columns">
            <div class="column medium-12">
                <button class="button primary full-page edit-user" type="button">Save</button>
            </div>
        </div>
    </div>
</section>

<script>
    $(function () {
        var user_id;

        var edit_user_full_name = $(".edit-user-full-name");
        var edit_user_email = $(".edit-user-email");
        var edit_user_company = $(".edit-user-company");
        var edit_user_phone = $(".edit-user-phone");
        var edit_country_id = $(".edit-user-country");
        var edit_user_state = $(".edit-user-state");
        var edit_user_city = $(".edit-user-city");
        var edit_user_street = $(".edit-user-street");
        var edit_user_postal_code = $(".edit-user-postal-code");
        var edit_user_is_admin = $(".edit-user-is-admin");
        var edit_user_is_printing_house = $(".edit-user-is-printing-house");
        var edit_user_is_designer = $(".edit-user-is-designer");

        $('#admin-users-table').DataTable();

        // Fill in the user editing modal
        $(".edit-user-modal-btn").on("click", function () {
            user_id = getTrData($(this), 'user_id');

            ajax("users/get", {
                user_id: user_id
            }, function (json) {
                edit_user_full_name.val(json.data.user_full_name);
                edit_user_email.val(json.data.user_email);
                edit_user_company.val(json.data.user_company);
                edit_user_phone.val(json.data.user_phone);
                edit_country_id.val(json.data.country_id);
                edit_user_state.val(json.data.user_state);
                edit_user_city.val(json.data.user_city);
                edit_user_street.val(json.data.user_street);
                edit_user_postal_code.val(json.data.user_postal_code);
                edit_user_is_admin.val(json.data.is_admin);
                edit_user_is_printing_house.val(json.data.is_printing_house);
                edit_user_is_designer.val(json.data.is_designer);
            });
        });

        // Add user
        $(".add-user").on("click", function () {
            var something_empty = false;

            var required_fields = [
                '.add-user-full-name',
                '.add-user-email',
                '.add-user-password',
                '.add-user-city',
                '.add-user-street',
                '.add-user-postal-code'
            ];

            if (requiredFieldsAreEmpty(required_fields)) {
                return false;
            }

            ajax("email/available", {
                email: $(".add-user-email").val()
            }, function () {

                // If the given email doesn't exist then insert the user
                ajax("admin/add_user", {
                    user_full_name: $(".add-user-full-name").val(),
                    user_email: $(".add-user-email").val(),
                    password: $(".add-user-password").val(),
                    user_company: $(".add-user-company").val(),
                    user_phone: $(".add-user-phone").val(),
                    country_id: $(".add-user-country").val(),
                    user_state: $(".add-user-state").val(),
                    user_city: $(".add-user-city").val(),
                    user_street: $(".add-user-street").val(),
                    user_postal_code: $(".add-user-postal-code").val(),
                    is_admin: $(".add-user-is-admin").val(),
                    is_printing_house: $(".add-user-is-printing-house").val(),
                    is_designer: $(".add-user-is-designer").val()
                }, RELOAD);
            });
        });

        // Edit the user
        $(".edit-user").on("click", function () {
            var something_empty = false;

            var required_fields = [
                '.edit-user-full-name',
                '.edit-user-email',
                '.edit-user-city',
                '.edit-user-street',
                '.edit-user-postal-code'
            ];

            if (requiredFieldsAreEmpty(required_fields)) {
                return false;
            }

            ajax("admin/edit_user", {
                user_id: user_id,
                user_full_name: edit_user_full_name.val(),
                user_email: edit_user_email.val(),
                user_company: edit_user_company.val(),
                user_phone: edit_user_phone.val(),
                country_id: edit_country_id.val(),
                user_state: edit_user_state.val(),
                user_city: edit_user_city.val(),
                user_street: edit_user_street.val(),
                user_postal_code: edit_user_postal_code.val(),
                is_admin: edit_user_is_admin.val(),
                is_printing_house: edit_user_is_printing_house.val(),
                is_designer: edit_user_is_designer.val(),
            }, RELOAD);
        });

        // Delete the user
        $(".delete-user").on("click", function () {
            user_id = getTrData($(this), 'user_id');

            swal({
                title: "Delete this user?",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function (willDelete) {
                if (willDelete) {
                    ajax("admin/delete_user", {
                        user_id: user_id
                    }, RELOAD);
                }
            });
        });

        $(".restore-user").on("click", function () {
            user_id = getTrData($(this), 'user_id');

            swal({
                title: "Restore this user?",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function (willDelete) {
                if (willDelete) {
                    ajax("admin/restore_user", {
                        user_id: user_id
                    }, RELOAD);
                }
            });
        });


        // Send the user their data
        $(".send-user-data").on("click", function () {
            user_id = getTrData($(this), 'user_id');

            swal({
                title: "Send this users data to the its email?",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function (willSend) {
                if (willSend) {
                    ajax("admin/send_user_data", {
                        user_id: user_id
                    }, RELOAD);
                }
            });
        });
    });
</script>