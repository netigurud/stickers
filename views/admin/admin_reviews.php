<section class="admin-products">
    <div class="row">
        <div class="col-sm-12">
            <a class="button button-block" data-toggle="modal" href="#add-review-modal">Add review</a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <?php if (!empty($reviews)): ?>
                    <div class="panel-header">
                        <h3>Reviews</h3>
                    </div>

                    <div class="panel-body">
                        <table class="let-table">
                            <tbody>
                            <?php foreach ($reviews as $review): ?>
                                <tr data-review_id="<?= $review['review_id'] ?>">
                                    <td><?= $review['review_created_by'] ?></td>
                                    <td><?= $review['review'] ?></td>
                                    <td>
                                        <?= $review['review_rating'] ?><span class="glyphicon glyphicon-star"></span>
                                    </td>
                                    <td><?= $review['review_created_at'] ?></td>
                                    <td>
                                        <a class="btn btn-success fill-review-modal" data-toggle="modal"
                                           href="#edit-review-modal">
                                            <span class="glyphicon glyphicon-pencil"></span>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>

                <?php else: ?>

                    <div class="panel-header">
                        <h3>No reviews yet, add one above.</h3>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-review-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit review</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <label for="edit-review-by" class="col-sm-2">Review by</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit-review-by">
                        </div>
                    </div>

                    <div class="row">
                        <label for="edit-review" class="col-sm-2">Review</label>
                        <div class="col-sm-10">
                            <textarea id="edit-review" cols="30" rows="10"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <label for="edit-review-rating" class="col-sm-2">Rating</label>
                        <div class="col-sm-10">
                            <input type="number" min="0" max="5" class="form-control" id="edit-review-rating">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left delete-review">Delete</button>
                    <button type="button" class="btn btn-success edit-review">Save</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="add-review-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add review</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="new-review-by">Review by</label>
                        </div>

                        <div class="col-sm-6">
                            <input type="text" id="new-review-by" class="form-control" required="required">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="new-review">Review</label>
                        </div>

                        <div class="col-sm-6">
                            <textarea id="new-review" cols="30" rows="10"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="new-review-rating">Rating</label>
                        </div>

                        <div class="col-sm-6">
                            <input type="number" id="new-review-rating" min="0" max="5" class="form-control"
                                   required="required">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary add-review-btn">Add</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</section>

<script>
    $(function () {
        var review_id;
        var edit_review = $("#edit-review");
        var edit_review_by = $("#edit-review-by");
        var edit_review_rating = $("#edit-review-rating");

        // Fill modal for review editing
        $(".fill-review-modal").on("click", function () {
            ajax("reviews/get", {
                review_id: getTrData($(this), 'review_id')
            }, function (json) {
                // Fill the modal with values
                edit_review.val(json.data.review);
                edit_review_by.val(json.data.review_created_by);
                edit_review_rating.val(json.data.review_rating);

                review_id = json.data.review_id
            });
        });

        // Edit review
        $(".edit-review").on("click", function () {
            ajax("reviews/edit", {
                review_id: review_id,
                review: edit_review.val(),
                review_rating: edit_review_rating.val(),
                review_created_by: edit_review_by.val(),
            }, RELOAD)
        });

        // Delete review
        $(".delete-review").on("click", function () {
            swal({
                title: "Are you sure you want to delete this review?",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function (willDelete) {
                if (willDelete) {
                    ajax("reviews/delete", {
                        review_id: review_id
                    }, RELOAD);
                } else {
                    swal("Cancelled");
                }
            });
        });

        // Add review
        $(".add-review-btn").on("click", function () {
            ajax("reviews/add", {
                review: $("#new-review").val(),
                review_rating: $("#new-review-rating").val(),
                review_created_by: $("#new-review-by").val(),
            }, RELOAD);
        });
    });
</script>