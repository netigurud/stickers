<section class="admin-products">
    <div class="row">
        <div class="col-sm-12">
            <a class="button button-block" data-toggle="modal" href="#add-email-modal">Add email</a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-header">
                    <h3>Emails</h3>
                </div>

                <div class="panel-body">
                    <table class="let-table">
                        <tbody>
                        <?php foreach ($emails as $email): ?>
                            <tr data-email_id="<?= $email['email_id'] ?>">
                                <td><?= $email['email'] ?></td>
                                <td>
                                    <a class="btn btn-success fill-email-modal" data-toggle="modal"
                                       href="#edit-email-modal">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-email-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit email</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <label for="edit-email" class="col-sm-2">Email</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit-email">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left delete-email">Delete</button>
                    <button type="button" class="btn btn-success edit-email">Save</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="add-email-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add email</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="new-email">Email</label>
                        </div>

                        <div class="col-sm-6">
                            <input type="text" id="new-email" class="form-control" required="required">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary add-email-btn">Add</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</section>

<script>
    $(function () {
        var email_id;
        var edit_email = $("#edit-email");

        // Fill modal for email editing
        $(".fill-email-modal").on("click", function () {
            ajax("email/get", {
                email_id: getTrData($(this), 'email_id')
            }, function (json) {
                // Fill the modal with values
                edit_email.val(json.data.email);

                email_id = json.data.email_id
            });
        });

        // Edit email
        $(".edit-email").on("click", function () {
            ajax("email/edit", {
                email_id: email_id,
                email: edit_email.val()
            }, RELOAD)
        });

        // Delete email
        $(".delete-email").on("click", function () {
            swal({
                title: "Are you sure you want to delete this email?",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function (willDelete) {
                if (willDelete) {
                    ajax("email/delete", {
                        email_id: email_id
                    }, RELOAD);
                } else {
                    swal("Cancelled");
                }
            });
        });

        // Add email
        $(".add-email-btn").on("click", function () {
            ajax("email/add", {email: $("#new-email").val()}, RELOAD);
        });
    });
</script>