<section class="admin-users">
    <div class="row">
        <div class="col-sm-12">
            <a class="button button-block" data-open="add-post-modal">Add post</a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-header">
                    <h3>Posts</h3>
                </div>

                <div class="panel-body">
                    <table id="admin-pages-table" class="let-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Content</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php foreach ($posts as $post): ?>
                            <tr data-post_id="<?= $post['post_id'] ?>">
                                <td><?= $post['post_id'] ?></td>
                                <td><?= $post['post_title'] ?></td>
                                <td><?= $post['post_content_html'] ?></td>
                                <td>
                                    <a class="btn btn-danger edit-post-modal-btn" data-open="edit-post-modal">
                                        Edit
                                    </a>
                                </td>
                                <td><a class="btn btn-danger delete-post">Delete</a></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="reveal" id="edit-post-modal" data-reveal>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>

        <div class="row columns">
            <div class="spacer-s"></div>
            <h4 class="h-bold">Edit post</h4>
        </div>

        <div class="spacer-m"></div>


        <div class="row columns">
            <label>
                <span>Title <em class="required">Required</em></span>
                <input class="edit-post-title" type="text" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Content <em class="required">Required</em></span>
                <textarea class="edit-post-content-html"></textarea>
            </label>
        </div>


        <div class="spacer-m"></div>

        <div class="row columns">
            <div class="column medium-12">
                <button class="button primary full-post edit-post" type="button">Save</button>
            </div>
        </div>
    </div>

    <div class="reveal" id="add-post-modal" data-reveal>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>

        <div class="row columns">
            <div class="spacer-s"></div>
            <h4 class="h-bold">Add post</h4>
        </div>

        <div class="spacer-m"></div>


        <div class="row columns">
            <label>
                <span>Title <em class="required">Required</em></span>
                <input class="add-post-title" type="text" required>
            </label>
        </div>

        <div class="spacer-xs"></div>

        <div class="row columns">
            <label>
                <span>Content <em class="required">Required</em></span>
                <textarea class="add-post-content-html"></textarea>
            </label>
        </div>


        <div class="spacer-m"></div>

        <div class="row columns">
            <div class="column medium-12">
                <button class="button primary full-post add-post" type="button">Add</button>
            </div>
        </div>
    </div>
</section>

<!-- SUMMERNOTE -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css?<?= COMMIT_HASH ?>" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js?<?= COMMIT_HASH ?>"></script>

<script>
    $(function () {
        var post_id;

        var edit_post_title = $(".edit-post-title");
        var edit_post_content_html = $(".edit-post-content-html");
        var add_post_content_html = $(".add-post-content-html");

        $(".edit-post-content-html, .add-post-content-html").summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ],
            height: 300
        });

        // Fill in the page editing modal
        $(".edit-post-modal-btn").on("click", function () {
            post_id = getTrData($(this), 'post_id');

            ajax("posts/get", {
                post_id: post_id
            }, function (json) {
                edit_post_title.val(json.data.post_title);
                edit_post_content_html.summernote("code", json.data.post_content_html);
            });
        });

        // Add page
        $(".add-post").on("click", function () {
            var required_fields = [
                '.add-post-title',
                '.add-post-content-html'
            ];

            if (requiredFieldsAreEmpty(required_fields)) {
                return false;
            }

            ajax("admin/add_post", {
                post_title: $(".add-post-title").val(),
                post_content_html: add_post_content_html.summernote('code')
            }, RELOAD);
        });

        // Edit the page
        $(".edit-post").on("click", function () {
            var required_fields = [
                '.edit-post-title',
                '.edit-post-content-html'
            ];

            if (requiredFieldsAreEmpty(required_fields)) {
                return false;
            }

            ajax("admin/edit_post", {
                post_id: post_id,
                post_title: edit_post_title.val(),
                post_content_html: edit_post_content_html.summernote('code')
            }, RELOAD);
        });

        // Delete the page
        $(".delete-post").on("click", function () {
            post_id = getTrData($(this), 'post_id');

            swal({
                title: "Delete this post?",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function (willDelete) {
                if (willDelete) {
                    ajax("admin/delete_post", {
                        post_id: post_id
                    }, RELOAD);
                }
            });
        });
    });
</script>