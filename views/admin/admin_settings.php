<style>

    .label-success {
        background-color: green;
    }

</style>
<section class="admin-products">

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-header">
                    <h3>Settings</h3>
                </div>

                <div class="panel-body">
                    <table class="let-table">
                        <tbody>
                        <?php foreach ($settings as $setting): ?>
                            <tr data-setting_short_name="<?= $setting['setting_short_name'] ?>">
                                <td><?= $setting['setting_name'] ?></td>
                                <td><?= $setting['setting_value'] ?></td>
                                <td>
                                    <a class="btn btn-success fill-setting-modal" data-toggle="modal"
                                       href="#edit-setting-modal">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <a class="button button-block" data-toggle="modal" id="add-country-button"
               href="#add-edit-country-modal">Add country</a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-header">
                    <h3>Shippable countries</h3>
                </div>

                <div class="panel-body">
                    <table class="let-table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Continent</th>
                            <th>Shippable</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($countries as $country): ?>
                            <tr data-country_id="<?= $country['country_id'] ?>">
                                <td><?= $country['country_name'] ?></td>
                                <td><?= $country['country_code'] ?></td>
                                <td><?= $country['continent_name'] ?></td>
                                <td>
                                    <span class="<?= $country['country_active'] ? 'label label-success' : '' ?>">
                                        <?= $country['country_active'] ? Yes : '' ?>
                                    </span>
                                </td>
                                <td>
                                    <a class="btn btn-success add-edit-country-modal-link" data-toggle="modal"
                                       href="#add-edit-country-modal">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-setting-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit setting</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <label for="edit-setting-name" class="col-sm-2">setting name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit-setting-name">
                        </div>
                    </div>

                    <div class="row">
                        <label for="edit-setting-value" class="col-sm-2">setting value</label>
                        <div class="col-sm-10">
                            <textarea id="edit-setting-value" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-success edit-setting">Save</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="add-edit-country-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><span id="country-being-edited"></span></h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <label for="country-name" class="col-sm-2">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="country-name">
                        </div>
                    </div>

                    <div class="row">
                        <label for="country-code" class="col-sm-2">Code</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="country-code">
                            <p>
                                Code must be <a
                                        href="https://en.wikipedia.org/wiki/ISO_3166-1#Officially_assigned_code_elements">officially
                                    assigned ISO 3166-1 alpha-2 code</a>. It is used to match countries from Google Maps
                                API.
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <label for="country-continent" class="col-sm-2">Continent</label>
                        <div class="col-sm-10">
                            <select name="country-continent" id="country-continent">
                                <?php foreach ($continents as $continent): ?>
                                    <option value="<?= $continent['continent_id'] ?>"><?= $continent['continent_name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label for="country-active" class="col-sm-2">Shippable</label>
                        <div class="col-sm-10">
                            <input type="checkbox" class="form-control" id="country-active">
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger delete-country-button">Delete</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success edit-country-save">Save</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</section>

<script>
    $(function () {
        var country_id;
        var setting_short_name;
        var edit_setting_name = $("#edit-setting-name");
        var edit_setting_value = $("#edit-setting-value");
        var edit_setting_description = $("#edit-setting-description");
        var country_name = $("#country-name");
        var country_code = $("#country-code");
        var country_continent = $("#country-continent");
        var country_active = $("#country-active");

        // Fill modal for setting editing
        $(".fill-setting-modal").on("click", function () {
            ajax("admin/get_setting", {
                setting_short_name: getTrData($(this), 'setting_short_name')
            }, function (json) {
                // Fill the modal with values
                edit_setting_name.val(json.data.setting_name);
                edit_setting_value.val(json.data.setting_value);
                edit_setting_description.val(json.data.setting_description);

                setting_short_name = json.data.setting_short_name
            });
        });

        // Fill modal for country editing
        $(".add-edit-country-modal-link").on("click", function () {
            $('.delete-country-button').show();
            $('#country-being-edited').html($(this).parents('tr').find('td:first').html());
            country_id = getTrData($(this), 'country_id');

            ajax("admin/get_country", {
                country_id: country_id
            }, function (json) {
                // Fill the modal with values
                country_name.val(json.data.country_name);
                country_code.val(json.data.country_code);
                country_continent.val(json.data.continent_id);
                country_active.prop('checked', json.data.country_active !== '0');


            });
        });

        // Edit setting
        $(".edit-setting").on("click", function () {
            ajax("admin/edit_setting", {
                setting_short_name: setting_short_name,
                setting_name: edit_setting_name.val(),
                setting_value: edit_setting_value.val(),
                setting_description: edit_setting_description.val()
            }, RELOAD)
        });

        // Edit country
        $(".edit-country-save").on("click", function () {
            ajax("admin/edit_country", {
                country_id: country_id,
                country_name: country_name.val(),
                country_code: country_code.val(),
                continent_id: country_continent.val(),
                country_active: country_active.prop('checked') ? 1 : 0
            }, RELOAD)
        });

        // Add country
        $("#add-country-button").on("click", function () {

            $('#country-being-edited').html('Add new country');
            country_id = -1;

            // Fill the modal with values
            country_name.val('');
            country_code.val('');
            country_continent.val('');
            country_active.prop('checked', true);
            $('.delete-country-button').hide();


        });

        // Delete country
        $(".delete-country-button").on("click", function () {
            ajax("admin/delete_country", {
                country_id: country_id
            }, RELOAD)
        });

    });
</script>