<section class="admin-products">
    <div class="row">
        <div class="col-sm-12">
            <a class="button button-block" data-toggle="modal" href="#add-product-modal">Add product</a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-header">
                    <h3>Stickers</h3>
                </div>

                <div class="panel-body">
                    <table class="let-table">
                        <thead>
                        <tr>
                            <th>Height</th>
                            <th>Width</th>
                            <th>Edit</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php foreach ($stickers as $sticker): ?>
                            <tr data-product_id="<?= $sticker['product_id'] ?>">
                                <td><?= $sticker['product_height'] ?>cm</td>
                                <td><?= $sticker['product_width'] ?>cm</td>
                                <td>
                                    <a class="btn btn-success fill-product-modal" data-toggle="modal"
                                       href="#edit-product-modal">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-header">
                    <h3>Magnets</h3>
                </div>

                <div class="panel-body">
                    <table class="let-table">
                        <thead>
                        <tr>
                            <th>Height</th>
                            <th>Width</th>
                            <th>Edit</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php foreach ($magnets as $magnet): ?>
                            <tr data-product_id="<?= $magnet['product_id'] ?>">
                                <td><?= $magnet['product_height'] ?>cm</td>
                                <td><?= $magnet['product_width'] ?>cm</td>
                                <td>
                                    <a class="btn btn-success fill-product-modal" data-toggle="modal"
                                       href="#edit-product-modal">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-quantity-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit quantity</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <label for="edit-quantity" class="col-sm-2">Quantity (pc)</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit-quantity">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left delete-quantity">Delete</button>
                    <button type="button" class="btn btn-success edit-quantity">Save</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- Edit quantity modal -->

    <div class="modal fade" id="edit-product-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit product</h4>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <label for="edit-product-height" class="col-sm-2">Height (cm)</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="edit-product-height">
                        </div>
                    </div>

                    <div class="row">
                        <label for="edit-product-width" class="col-sm-2">Width (cm)</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="edit-product-width">
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left delete-product">Delete</button>
                    <button type="button" class="btn btn-success edit-product">Save</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- Edit product modal -->

    <div class="modal fade" id="add-product-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add product</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="new-product-width">Width</label>
                        </div>

                        <div class="col-sm-6">
                            <input type="number" id="new-product-width" class="form-control" required="required">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="new-product-width">Height</label>
                        </div>

                        <div class="col-sm-6">
                            <input type="number" id="new-product-height" class="form-control" required="required">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="new-product-type">Product type</label>
                        </div>

                        <div class="col-sm-6">
                            <select id="new-product-type" class="form-control">
                                <?php foreach ($product_types as $product_type): ?>
                                    <option value="<?= $product_type['product_type_id'] ?>">
                                        <?= $product_type['product_type_name'] ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary add-new-product-btn">Add</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</section>

<script src="assets/js/sweetalert-2.1.0.js"></script>
<script>
    $(function () {

        // Clicked product IDs
        var product_id;

        // Edit product modal fields
        var edit_product_height = $("#edit-product-height");
        var edit_product_width = $("#edit-product-width");

        // Fill modal for product editing
        $(".fill-product-modal").on("click", function () {
            ajax("admin/get_product", {
                product_id: getTrData($(this), 'product_id')
            }, function (json) {
                // Fill the modal with values
                edit_product_height.val(json.data.product_height);
                edit_product_width.val(json.data.product_width);

                product_id = json.data.product_id
            });
        });

        // Edit product
        $(".edit-product").on("click", function () {
            ajax("admin/edit_product", {
                product_id: product_id,
                product_height: edit_product_height.val(),
                product_width: edit_product_width.val(),
            }, RELOAD)
        });

        // Delete product
        $(".delete-product").on("click", function () {
            swal({
                title: "Are you sure you want to delete this product?",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function (willDelete) {
                if (willDelete) {
                    ajax("admin/delete_product", {
                        product_id: product_id
                    }, RELOAD);
                } else {
                    swal("Cancelled");
                }
            });
        });

        $(".add-new-product-btn").on("click", function () {
            ajax("admin/add_new_product", {
                product_width: $("#new-product-width").val(),
                product_height: $("#new-product-height").val(),
                product_type_id: $("#new-product-type").val()
            }, RELOAD);
        });
    });
</script>