<style>
    span[contenteditable] {
        display: inline-block;
    }

    span[contenteditable]:empty::before {
        content: 'Add translation';
        display: inline-block;
        color: lightgrey;
    }

    /* Display last active date time as a single row */
    #datatable > tbody > tr > td:nth-child(5) {
        white-space:nowrap
    }

</style>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.18/css/dataTables.bootstrap.min.css" integrity="sha256-PbaYLBab86/uCEz3diunGMEYvjah3uDFIiID+jAtIfw=" crossorigin="anonymous" />

<div class="card-box">

    <div class="row">
        <div class="col-lg-12">
            <h2 class="header-title m-t-0 m-b-30">Translations</h2>
            <span class="pull-right">To edit a translation, click on a cell and insert a value. To save the value, click on any other cells. Only the first 255 characters of phrases are shown. Translations can be up to 32768 characters long.</span>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <ul id="pills-languages" class="nav nav-pills">
                <?php foreach ($supported_languages as $language): ?>
                    <li role="presentation" <?= $language == $this->params[0] ? 'class="active"' : '' ?>><a
                            href="admin/translations/<?= $language ?>"><?= $language ?></a>
                    </li>
                <?php endforeach ?>
            </ul>
        </div>

        <div class="col-xs-6">
            <ul id="pills-translatedness" class="nav nav-pills pull-right">
                <li role="presentation" <?= $this->params[1] == 'untranslated' ? 'class="active"' : '' ?>><a
                        href="admin/translations/<?= $this->params[0] ?>/untranslated">Untranslated</a>
                </li>
                <li role="presentation" <?= $this->params[1] == 'translated' ? 'class="active"' : '' ?>><a
                        href="admin/translations/<?= $this->params[0] ?>/translated">Translated</a>
                </li>
                <li role="presentation" <?= $this->params[1] == 'all' ? 'class="active"' : '' ?>><a
                        href="admin/translations/<?= $this->params[0] ?>/all">All</a></li>

            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">&nbsp;</div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Appeared</th>
                        <th>Language</th>
                        <th>Phrase</th>
                        <th>Translation</th>
                        <th>Last active</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($translations as $translation): ?>
                        <tr data-translation_id="<?= $translation['translation_id'] ?>">
                            <td><?= substr($translation['translation_id'], 0, 10) ?></td>
                            <td><?= substr($translation['appeared'], 0, 10) ?></td>
                            <td><?= $translation['language'] ?></td>
                            <td><?= htmlentities($translation['phrase']) ?></td>
                            <td class="translation">
                                <span class="loadNum"
                                      data-id="<?= $translation['translation_id'] ?>"><?= $translation['translation'] ?></span>
                            </td>
                            <td><?= $translation['last_active'] ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <button id="delete-selected-rows" class="btn btn-danger">Delete selected</button>
            </div>
        </div>
    </div><!-- end col -->
</div>

<script>
    $(document).ready(function () {
        $('#datatable').DataTable();

        $('#datatable tbody').on('click', 'tr', function () {
            $(this).toggleClass('selected');
        });

        $("span.loadNum").attr("contenteditable", true);

        $('#delete-selected-rows').on('click', function () {
            var selectedTranslationIds = $("tr.selected").map(function () {
                return $(this).data('translation_id');
            }).get();
            ajax("admin/delete_translations", {translation_ids: selectedTranslationIds}, RELOAD)
        })
    });


    // Database update
    var switchToInput = function () {
        var that = $(this).text();

        var $input = $("<textarea>", {
            val: that,
            type: "text",
            placeholder: 'Start typing'
        });
        $input.css("width", "100%");
        $input.css("height", "100%");
        $input.addClass("loadNum");
        $input.data('id', $(this).data('id'));
        $(this).replaceWith($input);
        $input.on("blur", switchToSpan);
        $input.select();
    };

    var switchToSpan = function () {

        $.post('admin/edit_translation', {
                translation_id: $(this).data('id'),
                key: $(this).parent().attr('class'),
                value: $(this).val()
            }, function (response) {
                if (response !== "Ok") {
                    console.log(response.result);
                } else {
                    //swal("Edited!", "Selected user has been edited.", "success");
                    //window.location.href = 'admin/users';
                }
            }
        );

        var $span = $("<span>", {
            text: $(this).val()
        });
        $span.addClass("loadNum");
        $span.data('id', $(this).data('id'));
        $(this).replaceWith($span);
        $span.on("click", switchToInput);
        $("span.loadNum").attr("contenteditable", true);
    };

    $(".loadNum").on("click", switchToInput);

</script>