<div class="spacer-s"></div>

<div class="row">

    <div class="column medium-5">
        <h2 class="h-white h-bold"> <?= __("Custom-made stickers") ?><br><?= __("that rock!") ?></h2>

        <div class="spacer-xs"></div>

        <div class="hr-orange"></div>

        <div class="spacer-s"></div>

        <h5 class="h-white"><?= __('Easy-to-use online ordering') ?></h5>

        <h5 class="h-white"><?= __('Free online proofs') ?></h5>

        <h5 class="h-white"><?= __('FREE shipping – we deliver now only to EU') ?></h5>

        -
        <div>
            <div class="charity">
                <img src="assets/img/Charity.png" alt="" width="100"/>
            </div>
        </div>
    </div>

    <div class="column medium-7">
        <?php require "templates/partials/calculators.php"; ?>
    </div>

    <button class="js-scroll-button scroll-button">
        <div>
            <small><?= __('Scroll down') ?></small>
        </div>
        <div><img src="assets/img/svg/scroll.svg" alt=""/></div>
    </button>

</div>

</section>

<section class="home-info">
    <div class="spacer-m"></div>

    <?php foreach ($posts as $key => $post): ?>
        <div class="row columns">
            <div class="<?= $key % 2 == 0 ? 'magnetspan' : 'info-pan'; ?>">
                <h4 class="h-bold"><?= __($post['post_title']); ?></h4>

                <p><?= __($post['post_content_html']); ?></p>

                <span class="monster-icon"></span>
            </div>
        </div>
        <div class="spacer-m"></div>
    <?php endforeach; ?>


</section>

<?php if (!empty($shipping)): ?>
    <section class="home-video">
        <div class="spacer-m"></div>

        <div class="row">
            <div class="medium-5 medium-push-7 columns">
                <div class="spacer-m"></div>

                <h3 class="h-bold"><?= __($shipping['page_title']) ?></h3>

                <div class="spacer-s"></div>

                <p>
                    <?= __($shipping['page_excerpt']) ?>
                </p>

                <div class="spacer-s"></div>

                <a class="h-bold" href="pages/<?= $shipping['page_slug']; ?>"><?= __('read more about shipping') ?>
                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                </a>

                <div class="spacer-mob-m"></div>
            </div>

            <div class="medium-7 medium-pull-5 columns">
                <div class="player-wrapper">
                    <img class="player-poster" src="assets/img/video-bg.png">


                    <?php if ($shipping['file_id']): ?>
                        <button class="video-play-button"></button>
                        <video id="my-video" class="video-js" controls preload="auto"
                               poster="assets/img/video-bg.png" data-setup="{}" width="670" height="376">
                            <source src="files/view/<?= $shipping['file_id']; ?>" type='video/mp4'>

                            <p class="vjs-no-js">
                                To view this video please enable JavaScript, and consider upgrading to a web browser
                                that
                                <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5
                                    video</a>
                            </p>
                        </video>
                    <?php endif ?>


                    <div class="video-caption"><p><?= __('How to order') ?> <br><?= __('Custom stickers') ?></p></div>
                </div>
                <div class="spacer-mob-m"></div>
            </div>
        </div>

        <div class="spacer-m"></div>
    </section>
<?php endif; ?>

<section class="home-testimonials">
    <div class="spacer-m"></div>

    <div class="row columns">

        <h4 class="h-bold text-center"><?= __('What people are saying about Letsticker') ?></h4>

        <div class="spacer-s"></div>

    </div>

    <div class="spacer-m"></div>

    <div class="multiple-items slider-wrapper">
        <?php foreach ($reviews as $review): ?>
            <div class="slider-slide">
                <div class="slide-top-info">
                    <div class="row">
                        <div class="column medium-6 small-12">

                            <span class="info-name"><a class="h-bold"
                                                       href="#"><?= $review['review_created_by'] ?></a></span>

                            <em class="info-date h-pale-purple"><?= $review['review_created_at'] ?></em>
                        </div>

                        <div class="column medium-6 small-12">
                        <span class="info-rating h-bold">
                            <span class="rating-stars">
                                <?php for ($n = 1; $n < 6; $n++): ?>
                                    <i class="fa <?= $n <= round($review['review_rating']) ? 'fa-star' : 'fa-star-o' ?>"
                                       aria-hidden="true"></i>
                                <?php endfor; ?>
                            </span>
                            <span><?= $review['review_rating'] ?>/5</span>
                        </span>
                        </div>
                    </div>
                </div>

                <div class="slide-content row columns">
                    <p>
                        <?= $review['review'] ?>
                    </p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <div class="spacer-m"></div>

    <div class="spacer-l"></div>
</section>
