<style>
    p {
        margin-bottom: 0;
    }

    .btn {
        padding: .525rem 1.075rem;
        font-size: .775rem;
    }

    .button {
        border-radius: 3px;

        margin: 0;
    }

    input[type="text"] {
        margin: 0;
    }

    input.order-row-final-width, input.order-row-final-height {
        width: 5rem;
    }

    .let-table tbody tr td:last-child {
        width: 5rem;
    }

    .let-table tbody tr td:first-child.button-row {
        padding: 15px 0 15px 0;
    }

    @media screen and (max-width: 768px) {
        .button {
            padding: 5px;
        }
    }

    table th, table td {
        text-align: center !important;
    }

    .printing-house-has-downloaded-the-file {
        background: #4dff4d;
    }

    .download-invoice-info-modal {

        text-align: center;
        margin: 20px auto 0 auto;

        background: lightyellow;
        padding: 20px;
        border-radius: 10px;

    }
</style>
<section class="user">
    <div class="row">
        <div class="download-invoice-info-modal" style="display: none">
            <?= __('Invoice is now being generated on the server. It should finish in a few seconds and start downloading.') ?>
        </div>

    </div>
    <div class="spacer-l"></div>
    <div class="row">
        <div class="columns">
            <div class="white-panel">
                <div class="row">
                    <div class="columns">
                        <div class="white-panel">
                            <div>
                                <h3><?= __('My Orders') ?></h3>

                                <div class="spacer-s"></div>

                                <div class="tabs-content" data-tabs-content="user-orders-tabs">
                                    <div class="tabs-panel is-active" id="all-orders">
                                        <table class="let-table">
                                            <thead>
                                            <tr>
                                                <th><?= __('ID') ?></th>
                                                <th><?= __('Image') ?></th>
                                                <th><?= __('Date') ?></th>
                                                <th><?= __('Status') ?></th>
                                                <th><?= __('Price') ?></th>
                                                <th><?= __('Actions') ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($orders['orders'] as $order): ?>
                                                <tr data-order_id="<?= $order['order_id'] ?>">
                                                    <td><?= $order['order_id'] ?></td>
                                                    <td>
                                                        <?php if ($order['order_file_id']): ?>
                                                            <img
                                                                    style="max-height: 50px;"
                                                                    src="data:image/jpeg;base64,<?= $order['file_preview'] ?>">
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <p><?= $order['order_made_at'] ?></p>
                                                    </td>
                                                    <td>
                                                        <p><?= $order['order_status_name'] ?>
                                                            <?php if ($order['order_status_id'] >= ORDER_STATUS_WAITING_FOR_DELIVERY): ?>
                                                                <br>
                                                                <a target="_blank"
                                                                   href="https://www.tnt.com/express/en_us/site/tracking.html?searchType=con&cons=<?= $order['order_con_number'] ?>">
                                                                    <?= $order['order_con_number'] ?>
                                                                </a>
                                                            <?php endif; ?>
                                                        </p>
                                                    </td>
                                                    <td>
                                                        <p><?= $order['order_total_price_with_vat'] ?>€</p>
                                                    </td>
                                                    <td style="width: auto">
                                                        <?php if ($order['order_status_id'] == ORDER_STATUS_QUEUED_FOR_PRINTING || $order['order_status_id'] == ORDER_STATUS_WAITING_FOR_DELIVERY): ?>
                                                            <a class="button success small reorder-modal-btn"
                                                               data-open="reorder-modal">
                                                                <?= __('Reorder') ?>
                                                            </a>
                                                        <?php endif; ?>

                                                        <?php if ($order['order_status_id'] == ORDER_STATUS_WAITING_FOR_CUSTOMER_REVIEW): ?>
                                                            <a class="button success small"
                                                               href="orders/<?= $order['order_id'] ?>">
                                                                <?= __('Confirm or request changes') ?>
                                                            </a>
                                                        <?php endif; ?>

                                                        <?php if ($order['simplbooks_invoice_id']): ?>
                                                            <a class="button success small download-invoice"
                                                               download
                                                               href="files/download_invoice/<?= $order['simplbooks_invoice_id'] ?>">
                                                                <?= __('Invoice') ?>
                                                            </a>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="spacer-l"></div>
</section>


<div class="reveal login-reveal" id="reorder-modal" data-reveal>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>

    <div class="row columns">
        <div class="spacer-s"></div>
        <h4 class="h-bold"><?= __('Reorder the following product(s)?') ?></h4>
        <div class="spacer-m"></div>
    </div>

    <div class="reorder-modal-body">

    </div>

    <div class="spacer-s"></div>

    <div class="row columns">
        <button class="button primary full-page reorder-btn" type="button"><?= __('Yes') ?></button>
    </div>
</div>

<div class="reveal login-reveal" id="order-image-modal" data-reveal>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>

    <div class="row columns">
        <div class="spacer-s"></div>
        <h4 class="h-bold"><?= __('Orders file') ?></h4>

        <div class="spacer-m"></div>
    </div>

    <div class="row columns">
        <img src="" class="order-image-in-modal">
    </div>

    <div class="spacer-s"></div>

    <div class="row columns">

        <a href="#" download class="button primary full-page download-image">
            <?= __('Download image') ?>
        </a>

        <div class="spacer-xs"></div>

        <?php if ($is_designer): ?>
            <a href="#" download class="button primary full-page download-image take-order">
                <?= __('Take order and download image') ?>
            </a>

            <div class="spacer-xs"></div>

            <button type="button" class="button primary full-page upload-new-file resumable-browse">
                <?= __('Upload new image') ?>
            </button>

        <?php endif; ?>
    </div>
</div><!-- Order image modal -->

<script>
    var order_id = null;

    // Download invoice
    $(document).on("click", ".download-invoice", function () {

        $('.download-invoice-info-modal').fadeIn();

        setTimeout(function () {

            $('.download-invoice-info-modal').fadeOut();
        }, 3000);


    });


    // Reorder
    $(document).on("click", ".reorder-modal-btn", function () {
        order_id = $(this).parents('tr').data('order_id');
        ajax("orders/get_reorder_modal", {
            order_id: getTrData($(this), 'order_id')
        }, function (json) {
            order_rows = json.data.order_rows;

            $(".reorder-modal-body").html(json.data.html)
        });
    });

    $(document).on("click", ".reorder-btn", function () {
        ajax("orders/reorder", {order_id: order_id}, function (json) {
            show_alert_modal("<?= __('Products have been added to your cart') ?>");
            window.location.href = "checkout";
        });
    });


</script>
