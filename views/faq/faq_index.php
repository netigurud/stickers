<link href="assets/css/foundation-accordion.css?<?= COMMIT_HASH ?>" rel="stylesheet">
<section class="cart-cart">
    <div class="row">
        <div class="column medium-12 contactpage">
            <div class="spacer-m"></div>

            <h4 class="h-bold"><?= __('FAQ') ?></h4>

            <div class="spacer-m"></div>

            <div class="white-panel">


                <div class="spacer-s"></div>
                <h3>Artwork</h3>
                <div class="spacer-s"></div>
                <ul class="accordion" data-accordion>
                    <li class="accordion-item is-active" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">What kind of file formats do you accept?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>Our trolls accept all kind of artwork formats but would be very happy if your file is in
                                vector format or at least 300 pixels per inch. We will always send you a proof of your
                                artwork so you can be sure it is the way you expect it.</p>

                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">How can I make a cut line?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>You don’t have to worry about the cut line! Our trolls will do it for you and send a
                                proof of how it will look like. You can request changes until you’ll find the perfect
                                fit.</p>

                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">How many times can I change my proof?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>You can change your proof as much as you like before approving. After approving no
                                additional changes can be made.
                                After approval our trolls start working right away, so you could get your artwork as
                                soon as possible. </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Why was my size changed?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>It is in our best interest to maintain your original artwork size. However, few
                                additional changes can be made to ensure your artwork proportions.
                                We will indicate the new size and price with the proof. If you’re not satisfied, you can
                                always request a change, and the trolls will be happy to help you.</p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Can you change my proof?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>Our trolls are always happy to help you! We can change your artwork so you get the design
                                you desire.
                                You can leave a comment in the proof or send us an email to info@letstickers.com.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">How do I know how many pixels (PPI) my artwork has?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>PPI stands for "pixels per inch" and shows the resolution of your artwork. Your artwork
                                should be at least 300 PPI at the size you need. Follow the steps to check your
                                artwork's PPI. If you still aren't sure, please contact us and we'll be happy to help.
                                <br><br>
                                Photoshop<br>
                                Open file in Photoshop
                                Click "OK" on prompts (if any)
                                If there is extra space around the artwork that you want to be printed, remove it by
                                using the Crop tool or Image > Trim. By removing the unwanted space, you'll be able to
                                accurately tell the DPI of your artwork.
                                Go to Image > Image Size
                                Uncheck "Resample"
                                Change the Width and Height units to Inches
                                Set the Width and/or Height to the size you need
                                The Resolution box will update to display your file's DPI
                                <br><br>
                                Mac<br>
                                Open your image in Preview.
                                Up at the top, click "Tools" > "Adjust Size"
                                Uncheck the box labeled "Resample Image"
                                Next, using inches, change the size of your artwork to your desired print size.
                                The resolution should be at least 300 pixels per inch.
                                <br><br>
                                PC<br>
                                Right-click on the image file; then select "Properties."
                                Click on the tab "Details" in the image properties window.
                                Note the image dimensions such as 1200 x 600.
                                Your PPI should be at least 300 for your artwork to be sufficient quality for printing.
                                If your image is at a lower resolution, it will not help to increase the pixels/inch or
                                enlarge the image. This will only stretch the image to a larger size and it will still
                                print fuzzy and pixelated.
                                In that case, we recommend contacting the original designer to get a better version of
                                your artwork. If that's not possible, you can send us the artwork as is and we will
                                redraw it from scratch to ensure optimal print quality. Following our recommendations
                                makes our life easier, but is not necessary.

                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">How long can I approve my proof?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>The sooner you approve, the sooner you’ll receive! 24 hours is ideal to approve our
                                feedback.
                                If you approve after 24 hours of our feedback the delivery date will also move forward
                                the corresponding number of days.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Why was my item sent back to proofing?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>Seldom if ever our trolls run into unforeseeable production problems. It may happen in
                                some complicated designs that will require improvement. In that case, we will notify you
                                and require your approval of a new proof before our trolls start working again.
                                Raster vs. Vector - Raster images are made up of a bunch of tiny pixels, while vector
                                images are made from mathematical paths.
                            </p>
                        </div>
                    </li>
                </ul>
                <div class="spacer-s"></div>
                <h3>Stickers</h3>
                <div class="spacer-s"></div>
                <ul class="accordion" data-accordion>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Are your stickers weatherproof?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                Yes, our trolls make the most weatherproof stickers and magnets. Our die cut stickers
                                and magnets are made out of thick durable vinyl with UV laminate, which makes them super
                                resistant to scratches, sunlight, and rain.
                                You can attach them outside without fearing the damages wind, rain and sunlight can do.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">What size transfer do you make to stickers?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                Automatically through our page you can order minimum size is 2.54 x 2.54 and maximum
                                size is 29 x 29 cm stickers and magnets.
                                If you would like a bigger one you can send us an email to info@letstickers.com and the
                                Trolls can prepare the proof for you.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Do your stickers have a matte or gloss finish?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                Our trolls currently do a nice matte finish only.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">What size sticker should I order?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                You can choose the size you would like to have approximately. We will determinate your
                                ideal size and send a proof of how your artwork would look like when finished.
                                You can see all the sizing and price changes made in the proof.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Can I order really large stickers?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                Yes, you can send us an email with all the questions you have to info@letstickers.com
                                and we can prepare the proof.
                            </p>
                        </div>
                    </li>
                </ul>
                <div class="spacer-s"></div>
                <h3>Magnets</h3>
                <div class="spacer-s"></div>
                <ul class="accordion" data-accordion>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Magnet care guide?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                You can ensure a longer time with your magnets if you clean them regularly. Our trolls
                                made you a little summary to follow.
                                <br><br>
                                Preparation
                                - Always place the magnet on a dry, clean surface.
                                -Make sure the magnet is also clean. If necessary, clean it with mild detergent, wipe
                                with a soft cloth or allow to dry.
                                <br><br>
                                Applying
                                -The magnet will hold better on a smooth, flat or gently curved metallic surface.
                                -Make sure the entire magnet is sealed tightly to the metal surface without any air
                                pockets.
                                -Apply magnet at a room temperature for best grip. Do not use magnets, when temperatures
                                exceed over 71°C or below -26°C.
                                <br><br>
                                Cleaning
                                - Clean your magnets on a regular basis to make their lifespan longer.
                                - Clean surface and magnet with a mild detergent from dirt build-up and allow to dry.
                                Then apply magnet tightly to the surface without any air pockets and you’re good to go.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Can I put my magnet on a car?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                Yes, our magnets fixate on most cars. Depending on conditions they can last 2 - 3 years.
                                We recommend sizes starting from 10 x 10 cm for outdoor uses for a better grip.
                                Magnets will hold better and longer if cleaned regularly from moisture and dirt.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Will your magnets work on my computer?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                No, unfortunately, magnets and computers don’t get along very well. We recommend
                                decorating your electronics with stickers instead.
                                You can always change the sticker to new one or remove it cleanly without it splitting
                                into pieces.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Are your magnets weatherproof?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                Yes, our trolls make the most weatherproof stickers and magnets. Our die cut stickers
                                and magnets are made out of thick durable vinyl with UV laminate, which makes them super
                                resistant to scratches, sunlight, and rain.
                                You can attach them outside without fearing the damages wind, rain and sunlight can do.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Do you offer business card magnets?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                Yes, we offer really cool business card magnets. For a standard size business card,
                                select custom size and order 8,9 x 5 cm.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Do you offer calendar magnets?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                Yes, we are happy to print you any kind of design you like.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">What can I stick my magnets on?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                You can stick your magnets to cars, refrigerators, lockers and other metal stuff.
                            </p>
                        </div>
                    </li>
                </ul>
                <div class="spacer-s"></div>
                <h3>Ordering</h3>
                <div class="spacer-s"></div>
                <ul class="accordion" data-accordion>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Can I see a proof before ordering?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                All orders receive a proof to ensure you get the design you desire.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Do your prices include full-color printing?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                Yes, all prices include full-color printing. Our trolls will send you a proof of how
                                your artwork will look like.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">How do I place a reorder?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                Log in to your account and select orders. Now you should be able to see all the items
                                you have ordered.
                                Select your favorites and press order. Since approval is already done reorders ship 3
                                days sooner than regular orders.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">How can I get my invoice?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                You can see and download your invoice from your account. We also send it to your email,
                                when you do the order.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Why can't I make changes after approval?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                You can only make changes before approval. After approval, the trolls start making your
                                masterpiece right away.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Do I need to approve my proof when reordering?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                No, you don’t need to approve your proof when reorder, but you have to order exact same
                                image and size. You can change only quantity to skip our Trolls proofing process.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Can I place a reorder with design changes?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                If you want a reorder with changes, you have to submit a new order. Otherwise, trolls
                                will make your previous order. </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Do you accept PayPal?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                Yes, we accept PayPal. You can select PayPal as a payment method during checkout. You
                                will be charged after proofing is approved until then you can cancel anytime with no
                                charges to your PayPal. </p>
                        </div>
                    </li>
                </ul>
                <div class="spacer-s"></div>
                <h3>Shipping</h3>
                <div class="spacer-s"></div>
                <ul class="accordion" data-accordion>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">How much does shipping cost?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                Our shipping is free inside European Union
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">When will my order arrive?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                Your order will arrive usually within 5-7 workdays inside European Union if the proof is
                                approved within 24 hours.
                                If there is a delay with your approving you design, your delivery date will be pushed
                                forward a corresponding number of days.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">What is your turnaround time?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                Our Trolls can manage production in 2 - 3 business days and out shipping partner
                                delivers all packages in 2-4 days in European Union.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Why was my order returned?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                Orders are most commonly sent back because of missing information and typos like a
                                missing apartment or suite number. If your package is being returned to us please let us
                                know at info@letstickers.com and our trolls would be very happy to resend the order to
                                an updated address.
                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Where do you ship?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                We currently ship to European Union only.

                            </p>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <!-- Accordion tab title -->
                        <a href="#" class="accordion-title">Will my stickers leave a residue behind when removed?</a>

                        <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                        <div class="accordion-content" data-tab-content>
                            <p>
                                Our stickers usually don’t leave a residue. However, sometimes when they are applied
                                longer, they may leave some remains which can be easily washed away with soap and water.
                            </p>
                        </div>
                    </li>
                </ul>
                <div class="spacer-s"></div>
                <h3>Troll stuff</h3>
                <div class="spacer-s"></div>
                <p>Our Trolls try to add more info to our FAQ weekly. If you don’t find your answer here, please write
                    us to our email info@letstickers.com and we try to answer you as soon as possible.</p>

            </div>

            <div class="spacer-l"></div>
        </div>
    </div>
</section>
<script src="assets/js/accordion.js?<?= COMMIT_HASH ?>"></script>