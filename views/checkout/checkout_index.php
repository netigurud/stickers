<?php use App\Quantity; ?>
<style>.resumable-browse {
        display: block;
        max-width: 230px;
        margin: 20px auto;
    }

    #addressDetail span.select2.select2-container span.selection > span {
        height: 45px;
    }

    .help-text {
        color: #999;
    }

    .important {
        color: red;
        font-weight: bold;
    }

    .user-file[data-selected="true"] {
        border: 2px solid red;
    }

    [v-cloak] > * {
        display: none;
    }

    [v-cloak]::before {
        content: "";
    }

    p#apartment-notice {
        display: block;
        background: rgb(131, 78, 182);
        color: #fff;
        padding: 5px 10px 5px 10px;
        margin-top: -22px;
    }
</style>
<?php //var_dump($user)?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">

<form method="post" action="checkout/place_order" enctype="multipart/form-data" novalidate>
    <section class="cart-cart">
        <div class="spacer-m"></div>

        <div class="row">
            <div class="vue-container" v-cloak>
                <div class="column medium-12">
                    <h4 class="h-bold"><?= __('Cart') ?></h4>
                    <div class="spacer-m"></div>
                    <div class="white-panel">

                        <table class="let-table">
                            <thead>
                            <tr>
                                <th><?= __('Description') ?></th>
                                <th width="150">
                                    <span><?= __('Quantity') ?></span>
                                    <a data-open="quantityModal"
                                       class="text-right info-link">
                                        <img width="25" height="25" src="assets/img/svg/info.svg">
                                    </a>
                                </th>
                                <th><?= __('Total') ?></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php $order_sum = 0; ?>
                            <?php foreach ($products as $key => $product): ?>
                                <?php $order_sum += $product['product_price']; ?>
                                <tr data-key="<?= $key ?>">
                                    <td>
                                        <div class="row">
                                            <div class="column medium-3">
                                                <img v-if="chosen_file.file_id"
                                                     :src="'data:image/jpeg;base64,' + chosen_file.file_preview"
                                                     class="chosen-file">
                                                <span v-if="!chosen_file.file_id" class="monster-double-icon"></span>
                                            </div>

                                            <div class="column medium-9">
                                                <?php if ($product['product_type_id'] == PRODUCT_TYPE_STICKER): ?>
                                                    <p class="h-bold h-dark-green"><?= __('Sticker') ?></p>
                                                <?php else: ?>
                                                    <p class="h-bold h-dark-green"><?= __('Magnet') ?></p>
                                                <?php endif; ?>
                                                <p>
                                                    <?= $product['product_height'] ?> x <?= $product['product_width'] ?>
                                                    cm
                                                </p>
                                            </div>
                                        </div>
                                    </td>

                                    <td>
                                        <select class="form-control update-quantity">
                                            <?php foreach ($product['quantityList'] as $quantity): ?>
                                                <option value="<?= $quantity['quantity'] ?>"
                                                    <?= $product['quantity'] == $quantity['quantity'] ? 'selected' : '' ?>>
                                                    <?= $quantity['quantity'] ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>

                                    <td>
                                        <p class="h-dark-green h-bold">
                                        <span class="row-price">{{ (user_vat ? <?= $product['product_price'] ?>
                                            : <?= $product['product_price'] ?> * 1.2).toFixed(2) }}&nbsp;€&nbsp;</span>
                                        </p>
                                    </td>

                                    <td style="text-align: center; padding-top: 1rem;">
                                        <?php $product['listQuantities'] = Quantity::listPrices($product['product_type_id'] == PRODUCT_TYPE_MAGNET ? PRODUCT_TYPE_STICKER : PRODUCT_TYPE_MAGNET,
                                            $product['product_width'] *
                                            $product['product_height']); ?>

                                        <?php if (!\App\ShoppingCart::containsOppositeProduct($product['product_width'],
                                                $product['product_height'],
                                                $product['product_type_id']) && !empty($product['listQuantities'])
                                        ): ?>

                                            <a data-open="add-product-modal"
                                               class="button button-purple add-product-button"
                                               data-opposite_product_type="<?= $product['product_type_id'] == PRODUCT_TYPE_MAGNET ? PRODUCT_TYPE_STICKER : PRODUCT_TYPE_MAGNET ?>"
                                               data-current_product='<?= json_encode($product) ?>'>
                                                <?= $product['product_type_id'] == PRODUCT_TYPE_MAGNET ? __("Add stickers") : __("Add magnets") ?>
                                                &nbsp;<span class="plus-icon">+</span>

                                            </a>
                                        <?php endif; ?>
                                    </td>

                                    <td>
                                        <button type="button" class="cart__close-button--grey remove-from-cart">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>

                        <div class="reveal tiny" id="quantityModal" data-reveal>
                            <button class="close-button" data-close aria-label="Close modal" type="button">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="spacer-s"></div>
                            <div class="row">
                                <div class="column medium-10 small-12 end">
                                    <h5><?= __('If you want to order bigger quantity, then contact us') ?> <a
                                                href="#"><strong>info@letsticker.com</strong></a>
                                    </h5>
                                </div>
                            </div>
                            <div class="spacer-s"></div>
                        </div>

                        <div class="spacer-s"></div>

                        <div class="row">
                            <div class="column medium-6 order-button-wrapper">

                                <label>
                                    <span><?= __('VAT number') ?></span><em><?= __("Optional") ?></em>

                                    <input id="user-vat" type="text" name="user_vat" v-model="user_vat"
                                           placeholder="<?= __('Vat number') ?>"
                                           value="<?= $auth->logged_in ? $user['user_vat'] : '' ?>">
                                </label>

                            </div>

                            <div class="column medium-6 sub-total-wrapper">
                                <h5 class="h-bold h-dark-green text-right">
                                    <?= __('Total') ?>:
                                    <span id="">{{ user_vat ? getTotal : getTotalWithVat }}</span>
                                    €
                                </h5>

                                <p class="text-right"><?= __('Including VAT') ?>
                                    {{user_vat ? 0 : getVat }} €
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="spacer-m"></div>
                </div>
                <div class="column medium-12 upload-wrapper">
                    <h4 class="h-bold">
                        <span><?= __('Upload your work') ?></span>
                        <a href="#" onclick="return false" data-open="uploadInfoModal" class="text-right info-link">
                            <img width="25" height="25" src="assets/img/svg/info.svg">
                        </a>
                    </h4>

                    <div class="spacer-m"></div>

                    <div class="white-panel text-center resumable-drop">

                        <div class="upload-box-placeholder-container">
                            <span class="cloud-upload-icon"></span>

                            <div class="spacer-s"></div>

                            <h6 class="h-bold"><?= __('Drag and drop files here to upload') ?></h6>

                            <h6 class="h-bold"><?= __('or..') ?></h6>
                        </div>

                        <label for="file" class="button success resumable-browse">
                            <?= __('Select file to upload') ?>
                        </label>

                        <p style="opacity: 0.3;">
                            <?= __('You can only upload these files') ?>: PDF, SVG, AI, PSD
                        </p>

                        <div v-if="user_files.length">
                            <span class="h-green"><?= __('Choose an uploaded file') ?>:</span>
                        </div>

                        <img class="user-file"
                             :data-selected="chosen_file.file_id == user_file.file_id"
                             v-for="user_file in user_files"
                             width="50" height="50"
                             @click='changeUserFile(user_file.file_id)'
                             :src="'data:image/jpeg;base64,' + user_file.file_preview">


                        <div class="upload-box-file-name-container" v-if="chosen_file.file_id">
                            <?= __('Uploaded file') ?>: <span
                                    class="uploaded-file-name">{{chosen_file.file_name}}</span>
                        </div>

                        <div>
                            <img v-if="chosen_file.file_id" class="upload-box-chosen-file"

                                 :src="'data:image/jpeg;base64,' + chosen_file.file_preview" style="max-width:300px;">
                        </div>

                    </div>

                    <div class="reveal tiny" id="uploadInfoModal" data-reveal>
                        <button class="close-button" data-close aria-label="Close modal" type="button">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <div class="spacer-s"></div>

                        <div class="row">
                            <div class="column medium-10 small-12 end">
                                <h5><?= __('When possible we recommend uploading vector artwork. Otherwise artwork should be at least 300 PPI (pixels per inch) and the maximum preferred file size should be 50MB. If you have any questions, then contact us info@letsticker.com') ?></h5>
                            </div>
                        </div>

                        <div class="spacer-s"></div>
                    </div>
                </div>

                <div class="column medium-12 upload-wrapper text-center">
                    <h5 style="opacity: 0.5; margin-top:30px;">
                        <?= __('You must create a new order to add different file.') ?>
                    </h5>
                </div>
            </div>
        </div>

        <div class="coupone-wrapper">
            <div class="spacer-s"></div>
            <div class="row columns" style="display:none;">
                <div class="white-panel">
                    <div class="column medium-4">
                        <div class="spacer-xs"></div>
                        <div class="input-group">
                            <input class="input-group-field" type="text" placeholder="Coupone code" id="promo-code">
                            <div class="input-group-button">
                                <input type="button" class="button" value="Confirm" id="promo-code-submit">
                            </div>
                        </div>
                    </div>
                    <div class="column medium-8">

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="cart-checkout">
        <div class="spacer-m"></div>

        <div class="row">
            <div class="columns">
                <h4 class="h-bold"><?= __('Checkout') ?></h4>
                <div class="spacer-m"></div>
                <div class="white-panel">
                    <h4 class="h-bold"><?= __('Your contact and shipping details') ?></h4>

                    <div class="spacer-s"></div>

                    <div class="row">
                        <div class="column medium-12">
                            <label>
                                <span><?= __('Quick address lookup') ?></span>
                                <input id="autocomplete"
                                       placeholder="<?= __('Type here and pick your delivery address') ?>"
                                       onFocus="geolocate()" type="text">

                                <p class="help-text">
                                    <?= __('Important: always thoroughly review all the address fields below after they are automatically filled') ?>
                                    :
                                    <?= __('In many countries, if the address is within a city, province should be left empty') ?>
                                    .
                                    <?= __('If the address includes an apartment or a floor number, it will not be included in the Street field and must be manually added after picking the address') ?>
                                    .
                                    <?= __('If a remote place name you need to ship to does not appear, search for closest less remote address and review the autocompleted values') ?>
                                    .
                                </p>
                            </label>
                        </div>
                    </div>

                    <div id="addressDetail" class="hide">
                        <div class="row">
                            <div class="column medium-6">
                                <label>
                                    <span><?= __('Email address') ?></span> <span class="required">*</span>
                                    <input data-current_email="<?= $auth->logged_in ? $user['user_email'] : '' ?>"
                                           id="user-email" type="email" name="user_email"
                                           placeholder="<?= __('To notify you when your order changes') ?>"
                                           value="<?= $auth->logged_in ? $user['user_email'] : '' ?>" required>

                                </label>
                                <p style="display: none; color: red" id="email-warning">
                                    <i class="fa fa-exclamation-triangle"></i>
                                    <?= __('You are changing your email address!') ?>
                                    <a data-open="emailWarningModal"><?= __("Learn about the consequences") ?></a>
                                </p>
                                <div class="reveal tiny" id="emailWarningModal" data-reveal>
                                    <button class="close-button" data-close aria-label="Close modal" type="button">
                                        <span id="close-modal" aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="row columns">
                                        <div class="spacer-s"></div>
                                        <h4 class="h-bold"><?= __('Changing your email address') ?></h4>
                                        <div class="spacer-s"></div>
                                    </div>
                                    <div class="row">
                                        <div class="column medium-10 small-12 end">

                                            <p><?= __('Changing the order email address will change the email you use to login to this site.') ?></p>
                                            <p><?= __('Make sure you can receive emails from the new address.') ?></p>
                                            <p><?= __('If you use Facebook/Google to log in, its email must match this email.') ?></p>
                                            <p><?= __('If you want to start using an email which is not your Facebook/Google email, use the "Forgot password?" link in the login page to get a password.') ?></p>
                                        </div>
                                    </div>
                                    <div class="spacer-s"></div>
                                    <div class="modal-footer">


                                        <button type="button"
                                                class="button primary alert close-modal"><?= __('I understand') ?>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="column medium-6">
                                <label>
                                    <span><?= __('Country') ?></span> <span class="required">*</span>


                                    <select name="country_id" id="country" class="select2" style="width:100%">
                                        <option disabled value=""><?= __('Please select one') ?></option>
                                        <?php foreach ($countries as $country): ?>
                                            <option data-country_id="<?= $country['country_id'] ?>"
                                                    value="<?= $country['country_code'] ?>"
                                                <?= $country['country_id'] === $user['country_id'] ? 'selected' : '' ?>
                                            >
                                                <?= $country['country_name'] ?>
                                            </option>
                                        <?php endforeach ?>
                                    </select>


                                </label>
                            </div>
                        </div> <!-- Email & country -->

                        <div class="row">
                            <div class="column medium-6">
                                <label><span><?= __('Full name') ?></span> <span class="required">*</span>
                                    <input type="text" name="user_full_name" id="user_full_name"
                                           value="<?= $auth->logged_in ? $user['user_full_name'] : '' ?>"
                                           placeholder="<?= __('To send you the product') ?>" required>
                                </label>

                            </div>

                            <div class="column medium-6">
                                <div class="row">
                                    <div class="column medium-12 large-6">
                                        <label><span><?= __('Company name') ?></span><em><?= __('For businesses') ?></em>
                                            <input type="text" name="user_company" id="user-company"
                                                   value="<?= $auth->logged_in ? $user['user_company'] : '' ?>"
                                                   placeholder="<?= __('For company invoice') ?>">
                                        </label>

                                    </div>

                                    <div class="column medium-12 large-6">
                                        <label><span><?= __('Company reg no') ?></span><em><em><?= __('For businesses') ?></em></em>
                                            <input type="text" name="user_company_reg_nr" id="user-company-reg-nr"
                                                   value="<?= $auth->logged_in ? $user['user_company_reg_nr'] : '' ?>"
                                                   placeholder="<?= __('For company invoice') ?>">
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- Full name, company name & company registry number -->

                        <div class="row">
                            <div class="column medium-6">
                                <label>
                                    <span><?= __('City, town or village') ?></span> <span class="required">*</span>
                                    <input type="text" name="user_city" id="locality"
                                           value="<?= $auth->logged_in ? $user['user_city'] : '' ?>"
                                           placeholder="<?= __('To send you the product') ?>"
                                           required>
                                </label>
                                <p class="help-text"><?= __('Must match with the postal code') ?></p>

                            </div>

                            <div class="column large-3 medium-6">

                                <div class="input-group">
                                    <span class="input-group-label">+</span>
                                    <label><span><?= __('Your phone number\'s country code') ?></span> <span
                                                class="required">*</span>
                                        <input type="text" name="user_dial_code" id="user_dial_code"
                                               onkeyup="this.value=this.value.replace(/[^\d]/,'')"
                                               placeholder="<?= __('For courier, to deliver') ?>"
                                               maxlength="6"
                                               value="<?= $auth->logged_in ? $user['user_dial_code'] : '' ?>">
                                    </label>

                                </div>
                            </div>

                            <div class="column large-3 medium-6">
                                <label><span><?= __('Your phone number') ?></span> <span class="required">*</span>
                                    <input type="tel" name="user_phone" id="user_phone"
                                           onkeyup="this.value=this.value.replace(/[^\d]/,'')"
                                           value="<?= $auth->logged_in ? $user['user_phone'] : '' ?>"
                                           placeholder="<?= __('For courier, to deliver you the product') ?>">
                                </label>
                                <p class="help-text"><?= __('The part of your phone number after the country code') ?></p>
                            </div>
                        </div> <!-- City & phone -->

                        <div class="row">
                            <div class="column medium-6">
                                <div class="row">
                                    <div class="column medium-12 large-12">
                                        <label>
                                            <span><?= __('Street address') ?></span>
                                            <span class="required">*</span>
                                            <input type="text" name="user_street" id="route"
                                                   value="<?= $auth->logged_in ? $user['user_street'] : '' ?>"
                                                   placeholder="<?= __('To send you the product') ?>"
                                                   required/>
                                        </label>
                                        <p style="display: none; color: #834eb6: " id="apartment-notice">
                                            <?= __('Please add apartment number if necessary') ?>
                                        </p>
                                        <p class="help-text"><?= __('Street and/or building number or name, floor or apartment number, etc') ?></p>
                                    </div>


                                </div>
                            </div>

                            <div class="column medium-6">
                                <div class="row">
                                    <div class="column large-6 medium-12">
                                        <label><span><?= __('Postal code') ?></span> <span class="required">*</span>
                                            <input type="text" name="user_postal_code" id="postal_code"
                                                   value="<?= $auth->logged_in ? $user['user_postal_code'] : '' ?>"
                                                   placeholder="<?= __('To send you the product') ?>" required>
                                        </label>
                                    </div>

                                    <div class="column large-6 medium-12">
                                        <label><span><?= __('State, region, province or county') ?></span>
                                            <input type="text" name="user_state" id="administrative_area_level_1"
                                                   value="<?= $auth->logged_in ? $user['user_state'] : '' ?>"
                                                   placeholder="<?= __('To send you the product') ?>">
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- Street address, postal code & state -->
                    </div>


                </div>

            </div>
        </div> <!-- Shipping/user info -->

        <div class="row">
            <div class="columns">
                <div class="spacer-m"></div>
                <div class="white-panel">
                    <h4 class="h-bold"><?= __('Your estimated delivery date') ?></h4>
                    <hr>
                    <div class="spacer-s"></div>
                    <div class="delivery-form">
                        <div class="row columns">
                            <h3 class="h-bold">
                                <?= date("Y-m-d", strtotime("+8 day")) ?>
                            </h3>
                            <p><?= __('FREE delivery with TNT Economy - Expected delivery date is 7 working days after approving your sticker design.') ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- Delivery date -->

        <div id="billing" style="display:none;">
            <div class="row">
                <div class="columns">
                    <div class="spacer-m"></div>
                    <div class="white-panel">
                        <h4 class="h-bold" style="display: inline-block;"><?= __('Your billing info') ?></h4>
                        <h5 style="display: inline-block;">
                            ( <?= __('You will not be charged until design approval') ?>
                            )</h5>

                        <hr>

                        <div class="spacer-s"></div>

                        <div class="billing-form">
                            <div class="row columns">
                                <div id="paypal-button"></div>
                            </div>

                            <div class="spacer-s"></div>

                            <div class="row columns text-center">
                                <div class="or-devider"><?= __('OR') ?></div>
                            </div>

                            <div class="spacer-s"></div>

                            <div class="row">
                                <div class="text-center">
                                    <h3><?= __('Credit card') ?></h3>
                                    <span class="cards-icon text-right" style="float:initial"></span>
                                </div>

                                <div class="column medium-6">
                                    <label>
                                        <span><?= __('Card Number') ?></span> <span class="required">*</span>
                                        <!--  Hosted Fields div container -->
                                        <div id="card-number"></div>
                                    </label>
                                </div>

                                <div class="column medium-6">
                                    <div class="row">
                                        <div class="column medium-6">
                                            <div class="row">
                                                <div class="column medium-6 expires-col">
                                                    <label>
                                                        <span><?= __('Expires') ?> *</span>

                                                        <!--  Hosted Fields div container -->
                                                        <div id="expiration-month"></div>
                                                    </label>
                                                </div>

                                                <div class="column medium-6 expires-col">
                                                    <label>
                                                        <span></span>
                                                        <!--  Hosted Fields div container -->
                                                        <div id="expiration-year"></div>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="column medium-6">
                                            <label>
                                                <span><?= __('CVC') ?> *</span>

                                                <!--  Hosted Fields div container -->
                                                <div id="cvv"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div> <!-- Billing info -->

            <div class="spacer-m"></div>

            <div class="row">
                <div class="columns">
                    <div class="mate-checkbox agreeterms">

                        <input type="checkbox" id="agree-to-terms-and-conditions"
                               class="agree-to-terms-and-conditions">

                        <label for="agree-to-terms-and-conditions"><?= __("I have read and agreed to the") ?> <a
                                    target="_blank" href="privacy"><?= __('terms & conditions') ?></a></label>

                        <div class="check"></div>

                    </div>

                </div>
            </div>

            <div class="spacer-m"></div>

            <div class="row">
                <div class="columns">
                    <button type="button" class="button success large full-page place-order-button">
                        <?= __('Confirm design') ?>
                    </button>
                </div>
            </div> <!-- Save order button -->
        </div>

        <div id="proceed" class="row">
            <div class="spacer-m"></div>

            <div class="columns">
                <button type="button" class="button success large full-page proceed-btn">
                    <?= __('Proceed to payment') ?>
                </button>
            </div>
        </div>

        <div class="spacer-l"></div>
    </section>
</form>
<div class="reveal addproduct-reveal" id="add-product-modal" data-reveal>

    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>

    <div class="row columns">
        <div class="spacer-s"></div>
        <h4 class="h-bold"><?= __('Select quantity') ?></h4>
        <div class="spacer-s"></div>
    </div>


    <div class="row columns">

        <select class="form-control sticker-quantities opposite-product-quantity">

        </select>

    </div>

    <div class="row columns">
        <div class="spacer-s"></div>
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __('Close') ?></button>
        <button type="button" class="btn btn-success add-to-shopping-cart-btn"><?= __('Add') ?></button>
    </div>


</div><!-- /.modal -->

<script src="assets/js/resumable.js"></script>

<!-- BrainTree and PayPal -->
<script src="https://js.braintreegateway.com/web/dropin/1.9.2/js/dropin.min.js"></script>
<script src="https://js.braintreegateway.com/web/3.31.0/js/hosted-fields.min.js"></script> <!-- Load Hosted Fields component. -->
<script src="https://www.paypalobjects.com/api/checkout.js" data-version-4
        log-level="warn"></script> <!-- Load PayPal's checkout.js Library. -->
<script src="https://js.braintreegateway.com/web/3.31.0/js/client.min.js"></script> <!-- Load the client component. -->
<script src="https://js.braintreegateway.com/web/3.31.0/js/paypal-checkout.min.js"></script> <!-- Load the PayPal Checkout component. -->

<script>

    // Global vars
    var r;
    var file_name;
    var country_short_name = '';
    var autocompleteField = $("#autocomplete");
    var products = <?= json_encode(array_values($products)) ?>;
    var user_files = <?= json_encode(array_values($user_files)) ?>;
    var countries = <?= json_encode(array_values($countries)) ?>;
    var order_sum = '<?= number_format($order_sum, 2, '.', '') ?>';
    var logged_in = '<?= $auth->logged_in ?>';

    var required_fields = [
        '#postal_code',
        '#locality',
        '#user_dial_code',
        '#user_phone',
        '#user_full_name',
        '#country',
        '#user-email',
        '#route'
    ];

    // When empty, draw a blue border to draw attention
    var suggested_fields = [];


    function fieldCheck(fields) {
        var something_empty = false;

        $(fields).each(function (index, element) {
            // Check if elements value is longer than 1 character
            if (!$(element).val() || $(element).val().length < 1) {


                something_empty = true;
            } else {
                $(element).css('border', '1px solid #e1e3e7');
            }
        });

        // Alert the user if a required input was empty
        if (something_empty) {
            something_empty = false;
            return true;
        } else {
            return false;
        }
    }

    $(document).ready(function () {


        // Turn ugly dropdowns into beautiful ones
        $('.select2').select2({});


        if (!fieldCheck(required_fields)) {
            $('#addressDetail').removeClass('hide');
        }

        var vueObj = new Vue({
            el: '.vue-container',
            data: {
                products: products,
                user_files: user_files,
                chosen_file: {file_id: 0, file_preview: ''},
                user_vat: '<?= $auth->logged_in ? $auth->user_vat : '' ?>'
            },
            methods: {
                changeUserFile: function (file_id) {

                    // Remember old file
                    var prevFileId = this.chosen_file.file_id;

                    // Set another file
                    setChosenFile(file_id);

                    // Notify server
                    ajax("shopping_cart/update_file", {
                        file_id: this.chosen_file.file_id,
                        file_name: this.chosen_file.file_name
                    }, null, function () {

                        // An error occurred, revert
                        setChosenFile(prevFileId);
                    });
                }
            },
            computed: {
                getTotal: function () {
                    return this.products.reduce(function (totalPrice, currentProduct) {
                        return (parseFloat(totalPrice) + parseFloat(currentProduct.product_price)).toFixed(2);
                    }, 0);
                },
                getTotalWithVat: function () {
                    return (this.getTotal * 1.2).toFixed(2);
                },
                getVat: function () {
                    return (this.getTotalWithVat - this.getTotal).toFixed(2);
                }
            }
        });

        var product;
        var previous_file;
        var quantity;
        var chosen_file = $(".chosen-file");
        var place_order_btn = $('.place-order-button');
        var PRODUCT_TYPE_STICKER = <?= PRODUCT_TYPE_STICKER ?>;
        var chosen_file_id = <?= empty($_SESSION['shopping_cart']['file']['file_id'])
            ? 0 : $_SESSION['shopping_cart']['file']['file_id'] ?>;

        // Show the uploaded file section if anything uploaded, else hide
        if (chosen_file_id) {

            setChosenFile(chosen_file_id);


        }

        // Update the total prices when the page is loaded fully
        updateTotalPrice(order_sum);

        // Disable "Confirm design" button before the user has agreed to terms and conditions
        place_order_btn.attr("disabled", "disabled");

        // Enable the "Confirm design" button after the user has agreed
        $(".agree-to-terms-and-conditions").on("click", function () {
            if ($(this).is(":checked")) {
                place_order_btn.removeAttr("disabled");
            } else {
                place_order_btn.attr("disabled", "disabled");
            }
        });

        // Disable confirm order button when clicked
        place_order_btn.on("click", function () {
            place_order_btn.attr("disabled", "disabled");
        });


        $("#user-vat").on("change", function () {

            // Create purple borders around company details if they are empty
            if (!($('input[name="user_company"]').val())) {
                $("#user-company").css("border", "2px solid red");
            }

            // Check if given VAT number is correct
            if (vueObj.user_vat) {
                ajax("checkout/check_vat", {
                    vat: vueObj.user_vat
                }, function () {
                }, function (json) {
                    vueObj.user_vat = '';
                    show_alert_modal(json.data);

                    // Remove borders if not valid VAT
                    $("#user-company").css('border', '1px solid #e1e3e7');
                });

            }


        });

        $(".add-product-button").on("click", function (e) {
            e.preventDefault();
            options = $(this).data('current_product').listQuantities;


            $.each(options, function (key, value) {
                $('.opposite-product-quantity').append('<option value=' + value.quantity + '>' + value.quantity + '</option>');
            });

            // Set add stickers/magnets modal default quantity
            quantity = $('.opposite-product-quantity').val();

            $('#add-product-modal').show();
            product = $(this).data("current_product");
        });

        $(".opposite-product-quantity").on("change", function () {
            quantity = $(this).val();
        });

        $(".close-modal").on("click", function () {
            $("#close-modal").click();
        });

        $(".add-to-shopping-cart-btn").on("click", function () {

            if (!quantity) {
                show_alert_modal("<?= __('Please select quantity.') ?>");
                return false;
            }

            ajax("shopping_cart/update", {
                product_height: product.product_height,
                product_width: product.product_width,
                product_price: product.product_price,
                quantity: quantity,
                product_type: product.product_type_id === PRODUCT_TYPE_STICKER ? 'magnets' : 'stickers'
            }, RELOAD)
        });

        // Check if users email already exists
        $("#user-email").on("change", function () {
            ajax("email/check_existence", {'email': $(this).val()});
            if (logged_in && $(this).val() !== $(this).data('current_email')) {
                $('#email-warning').show()
            } else {
                $('#email-warning').hide()
            }
        });


        // Update sticker or magnet quantity
        $(".update-quantity").on("change", function () {
            ajax("shopping_cart/update_quantity", {
                key: getTrData($(this), 'key'),
                quantity: $(this).val()
            }, RELOAD);
        });

        // Remove rows from shopping cart
        $(".remove-from-cart").on("click", function () {
            ajax("shopping_cart/remove_row", {
                key: getTrData($(this), 'key')
            }, RELOAD);
        });

        // Apply the promo code
        $("#promo-code-submit").on("click", function () {
            ajax("shopping_cart/check_promo_code", {
                promo_code: $('#promo-code').val()
            }, function (json) {
                show_alert_modal("<?= __('Promo code applied!') ?>");

                // Update the prices with the promo code applied
                // promo_code_discount_percent is a db field and hard coded to db
                updateTotalPrice(order_sum * ((100 - json.data.promo_code_discount_percent) / 100));

                // Update every rows price
                $.each($(".row-price"), function () {
                    var current_price = $(this).html().trim();
                    $(this).html(current_price * ((100 - json.data.promo_code_discount_percent) / 100));
                });
            });
        });


        // Check that the file exists and user input is correct
        $(".proceed-btn").on("click", function () {
            if (!vueObj.chosen_file.file_id) {
                show_alert_modal("<?= __('You must add a file to your order') ?>");
                return false;
            }
            var somerequiredFieldsAreEmpty = requiredFieldsAreEmpty(required_fields);
            if (somerequiredFieldsAreEmpty && $('#addressDetail').hasClass('hide')) {
                autocompleteField.css('border', '2px solid red');
                return false;
            } else if (somerequiredFieldsAreEmpty) {
                return false;
            } else {
                autocompleteField.css('border', '1px solid #e1e3e7');
            }

            if ($('input[name="user_email"]').val().length < 4) {
                show_alert_modal("<?= __('Please enter the correct email address (minimum 4 characters)') ?>");
                return false;
            }

            // check if there are at least two names given;
            if ($('input[name="user_full_name"]').val().trim().indexOf(' ') === -1) {
                show_alert_modal("<?= __('Please enter your first and last name') ?>");
                return false;
            }

            // Check if company name was left empty
            if (!$('input[name="user_company"]').val() && $('#user-vat').val()) {
                show_alert_modal("<?= __('Please enter your company name') ?>");
                return false;
            }

            // When everything is OK, show the billing div and hide the proceed button
            $("#billing").show();
            $("#proceed").hide();

            // Remove title attribute
            $("iframe[title='ppbutton']").removeAttr("title");
        });

        $('section.cart-checkout input').on('change', function () {

            // Redraw red borders
            requiredFieldsAreEmpty(required_fields, false, suggested_fields);
        });

        resumable('files/upload', true, function (file) {


            // Remove previous file
            if (previous_file) {
                r.removeFile(previous_file);
            }

            previous_file = file;

            file_name = file.file.name;
            file_size = file.file.size;
            console.log(file_size);

            var max =  <?= MAX_UPLOAD_FILE_SIZE ?>;

            if (file && file_size > max) {
                show_error_modal("<?= __('Uploaded file is too big! Max allowed file size: ') . MAX_UPLOAD_FILE_SIZE / 1000000 ?>");
                return false;
            } else {
                // Show the loading modal
                $("#progress-modal").modal('show');

                $('#upload-progress').removeClass('hide').find('.progress-bar').css('width', '0%');

                // Actually start the upload
                r.upload();
            }
        }, function () {
            // Get file id by file name to replace the images in the products table
            ajax("files/get_file_meta_data_from_session", null, function (json) {

                // Add new file to array
                user_files.push(json.data);

                // Set vue.chosen_file to point to uploaded file
                setChosenFile(json.data.file_id);

                // Hide the loading modal
                $("#progress-modal").modal('hide');

                return;

            });
        });

        // Create a client.
        braintree.client.create({
            authorization: '<?= $client_token ?>'
        }, function (err, clientInstance) {
            // Stop if there was a problem creating the client. (Network error or invalid authorization)
            if (err) {
                console.error(err);
                return;
            }

            braintree.hostedFields.create({
                client: clientInstance,
                styles: {
                    'input': {
                        'font-size': '1rem',
                        'font-family': 'pt sans, sans-serif',
                        'font-weight': '400',
                        'color': 'black'

                    },
                    ':focus': {
                        'color': 'black'
                    },
                    '.valid': {
                        'color': 'black'
                    },
                    '.invalid': {
                        'color': 'black'
                    }

                },
                fields: {
                    number: {
                        selector: '#card-number',
                        placeholder: '4111 1111 1111 1111'
                    },
                    cvv: {
                        selector: '#cvv',
                        placeholder: '123'
                    },
                    expirationMonth: {
                        selector: '#expiration-month',
                        placeholder: 'MM'
                    },
                    expirationYear: {
                        selector: '#expiration-year',
                        placeholder: 'YY'
                    }
                }
            }, function (err, hostedFieldsInstance) {
                if (err) {
                    console.log(err);
                    return;
                }

                // Remove all styles from iframe
                $("iframe").removeAttr('style');

                place_order_btn.on("click", function () {

                    hostedFieldsInstance.tokenize(function (err, payload) {
                        if (err) {
                            if (err.message === 'All fields are empty. Cannot tokenize empty card fields.' ||
                                err.message === 'Some payment input fields are invalid. Cannot tokenize invalid card fields.') {
                                show_alert_modal('<?= __('Please fill in the credit card info.') ?>');
                            } else {
                                show_error_modal(err.message);
                            }

                            return false;
                        }

                        send_data(payload.nonce);
                    });
                });
            });

            // Create a PayPal Checkout component.
            braintree.paypalCheckout.create({
                client: clientInstance
            }, function (paypalCheckoutErr, paypalCheckoutInstance) {

                // Stop if there was a problem creating PayPal Checkout.
                // This could happen if there was a network error or if it's incorrectly configured.
                if (paypalCheckoutErr) {
                    console.error('Error creating PayPal Checkout:', paypalCheckoutErr);
                    return;
                }

                // Set up PayPal with the checkout.js library
                paypal.Button.render({
                    env: '<?= BT_ENVIRONMENT ?>',

                    style: {
                        size: 'responsive',
                        color: 'blue',
                        shape: 'rect',
                        label: 'checkout',
                        tagline: false
                    },

                    payment: function () {
                        // Available options: http://braintree.github.io/braintree-web/current/PayPalCheckout.html#createPayment
                        return paypalCheckoutInstance.createPayment({
                            flow: 'vault'
                        });
                    },

                    onAuthorize: function (data, actions) {
                        return paypalCheckoutInstance.tokenizePayment(data).then(function (payload) {
                            send_data(payload.nonce);
                        });
                    },

                    onCancel: function (data) {
                        console.log('checkout.js payment cancelled', JSON.stringify(data, 0, 2));
                    },

                    onError: function (err) {
                        console.error('checkout.js error', err);
                    }
                }, '#paypal-button').then(function () {
                    // The PayPal button will be rendered in an html element with the id
                    // `paypal-button`. This function will be called when the PayPal button
                    // is set up and ready to be used.
                });

            });
        });

        function setChosenFile(file_id) {

            vueObj.chosen_file = user_files.find(function (file) {

                // file.file_id is string whereas file_id is int
                return file.file_id == file_id;

            });

        }

        function send_data(payload_nonce) {
            // Don't let the user click this button more than once
            place_order_btn.attr("disabled", "disabled");

            ajax("checkout/place_order", {
                "payment_method_nonce": payload_nonce,
                "user_email": $('#user-email').val(),
                "user_state": $('input[name="user_state"]').val(),
                "user_full_name": $('input[name="user_full_name"]').val(),
                "user_company": $('input[name="user_company"]').val(),
                "user_company_reg_nr": $('input[name="user_company_reg_nr"]').val(),
                "user_phone": $('input[name="user_phone"]').val(),
                "user_dial_code": $('input[name="user_dial_code"]').val(),
                "country_id": $('#country').find(":selected").data('country_id'),
                "user_city": $('#locality').val(),
                "user_street": $('#route').val(),
                "user_postal_code": $('#postal_code').val(),
                "promo_code": $('#promo-code').val(),
                "user_vat": $('#user-vat').val()
            }, function (json) {
                show_alert_modal('<?=addslashes(__('Your order has been received. Please wait for an email for approving the image file'))?>', function () {
                    location.href = "my_orders";
                });
            }, function (json) {

                show_error_modal(json.data);

                place_order_btn.removeAttr("disabled");
            });

        }

    });

    function updateTotalPrice(sum) {
        $("#order-sum-with-vat").html(parseFloat(sum * 1.2).toFixed(2));
        $("#order-sum-vat").html(parseFloat(sum * 0.2).toFixed(2));
    }

    var placeSearch, autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        country: 'long_name',
        administrative_area_level_1: 'short_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        var street = {route: '', street_number: ''};

        // show address fields when address has been autocompleted
        $('#addressDetail').removeClass('hide');

        // Clear form
        for (var component in componentForm) {
            $('#' + component).val('');
        }

        // Clear autocomplete field
        autocompleteField.val('');


        // Check country
        var i;

        for (i = 0; i < place.address_components.length; i++) {
            if (place.address_components[i].types[0] === 'country') {

                country_short_name = place.address_components[i].short_name;
                country_option = $('#country option[value="' + country_short_name + '"]');


                if (false && country_option.length === 0) { // Unsupported country, show error

                    swal('<?= __('Apologies') ?>', '<?= addslashes(__('We only ship to EU at the moment')) ?>.', "error");


                    // Cancel autofill
                    return;


                } else { // Supported country

                    // Set correct country from dropdown
                    $("#country").val(country_short_name).trigger('change');

                    // Add default dial code
                    if ($('#user_phone').val() === '') {

                        var selectedCountry = countries.find(function (country) {
                            return country.country_code === country_short_name;
                        });
                        $('#user_dial_code').val(selectedCountry.country_dial_code);
                    }

                }
            }
        }

        // Fill in address fields
        for (i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];


            if (componentForm[addressType] && addressType !== 'country') {

                if (['route', 'street_number'].includes(addressType)) {

                    street[addressType] = place.address_components[i][componentForm[addressType]];

                } else {

                    document.getElementById(addressType).value = place.address_components[i][componentForm[addressType]];
                }
            }
        }

        // Build street address
        $('#route').val(street.route + ' ' + street.street_number);

        // Redraw red borders without showing the alert
        requiredFieldsAreEmpty(required_fields, false, suggested_fields);

        // Clear red border from address autocomplete box
        autocompleteField.css('border', '1px solid #e1e3e7');

        // Add purple border to address so user would notice if apartment number is missing
        $("#route").css('border', '2px solid #834eb6');
        $("#apartment-notice").css('display', 'block');
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });

                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    // Remove border from company name after change, if it's filled
    $("#user-company").on("change", function () {
        if (!$('input[name="user_company"]').val() && $('input[name="user_vat"]').val()) {
            $("#user-company").css("border", "2px solid red");
        } else {
            $("#user-company").css('border', '1px solid #e1e3e7');
        }
    });


    $("#route").on("change", function () {
        $("#route").css('border', '1px solid #e1e3e7');
        $("#apartment-notice").css('display', 'none');
    });

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLE_MAPS_API_KEY ?>&libraries=places&callback=initAutocomplete&language=en-US"
        async defer></script>

<!-- development version, includes helpful console warnings -->
<script src="assets/js/vuejs-2.5.16.js"></script>
<script src="assets/js/select2-4.0.5.js"></script>
