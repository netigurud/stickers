<section class="cart-cart">
    <div class="row">
        <div class="column medium-12 contactpage">
            <div class="spacer-m"></div>

            <h4 class="h-bold"><?= __('Privacy & Terms') ?></h4>

            <div class="spacer-m"></div>

            <div class="white-panel">
                <div class="spacer-s"></div>
                <p><?= __('We collect information, such as your name and email address, when you (or other users) provide it to us while using our website or volunteer it through other forms of communication. Our Privacy Policy explains how we use that information.') ?></p>
                <h3><?= __('Using') ?></h3>
                <p><?= __('The information we collect is used for communication, to fulfill our services, and to evaluate the quality of our site and find opportunities to improve.') ?></p>
                <h3><?= __('Third-party services') ?></h3>
                <p><?= __('We use cookies from third-party partners such as Google and Facebook for marketing purposes and analyze user activity in order to improve the Site.') ?></p>
                <p><?= __('In general, the third-party providers used by us will only collect, use and disclose your information to the extent necessary to allow them to perform the services they provide to us.') ?></p>
                <p><?= __('However, certain third-party service providers, such as payment gateways and other payment transaction processors, have their own privacy policies in respect to the information we are required to provide to them for your purchase-related transactions.') ?></p>
                <p><?= __('For these providers, we recommend that you read their privacy policies so you can understand the manner in which your personal information will be handled by these providers.') ?></p>
                <h3><?= __('Sharing') ?></h3>
                <p><?= __('The information we collect is not shared with other organizations, except if we are required by law to do so or if you violate our Terms of Service.') ?></p>
                <p><?= __('To protect your personal information, we take reasonable precautions and follow industry best practices to make sure it is not inappropriately lost, misused, accessed, disclosed, altered or destroyed.') ?></p>
                <h3><?= __('Cookies') ?></h3>
                <p><?= __('We may assign device of our web pages user with one or more cookies which collect information to facilitate access to our website and personalize your experience.') ?></p>
                <h3><?= __('Changes') ?></h3>
                <p><?= __('We reserve the right to modify our Privacy Policy at any time, so please review it frequently. If significant changes are made, we will notify you here that it has been updated, so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we use and/or disclose it.') ?></p>
                <h3><?= __('Questions and contact information') ?></h3>
                <p><?= __('If you have questions about our Privacy Policy contact us by email or postal mail at:') ?></p>
                <p><?= __('Netguru OÜ') ?></p>
                <p><?= __('Harjumaa') ?></p>
                <p><?= __('Tallinn 11619') ?></p>
                <p><?= __('Hiiu tn 20-2') ?></p>
                <p><?= __('info@letsticker.com') ?></p>
            </div>
            <div class="spacer-l"></div>
        </div>
    </div>
</section>