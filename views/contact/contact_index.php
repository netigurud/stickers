<?php if (!empty($error)): ?>
    <div class="alert alert-danger">
        <?= $error; ?>
    </div>
<?php endif; ?>

<section class="cart-cart">
    <div class="row">
        <div class="column medium-12 contactpage">
            <div class="spacer-m"></div>

            <h4 class="h-bold"><?= __('Contact') ?></h4>

            <div class="spacer-m"></div>

            <div class="white-panel">
                <h5>
                    <?= __("Can't find your answer in our") ?> <a href="faq"><?= __('FAQ') ?></a>?
                    <?= __('Just fill the form and we will get right back to you.') ?>
                </h5>

                <div class="spacer-s"></div>

                <form method="post" action="contact/send" enctype="multipart/form-data">
                    <label>
                        <span><?= __('Your name') ?></span> <em><?= __('Required') ?></em>
                        <input name="user_name"
                               value="<?= $this->auth->logged_in ? $this->auth->user_full_name : '' ?>"
                               required="required"
                               type="text">
                    </label>

                    <label>
                        <span><?= __('Your e-mail') ?></span> <em><?= __('Required') ?></em>
                        <input name="email" value="<?= $this->auth->logged_in ? $this->auth->user_email : '' ?>"
                               required="required"
                               type="text">
                    </label>

                    <label>
                        <span><?= __('Subject') ?></span> <em><?= __('Required') ?></em>
                        <input name="subject" value="<?= $subject ?>" required="required"
                               type="text">
                    </label>

                    <label>
                        <span><?= __('Message') ?></span> <em><?= __('Required') ?></em>
                        <textarea name="message" required="required"><?= $message ?></textarea>
                    </label>

                    <button type="button" class="btn file-select"><?= __('Choose file') ?></button>
                    <input style="display: none" id="files" type="file" name="files[]" class="input-group-field"
                           multiple>

                    <p><?= __('Maximum file size is') . ' ' . $upload_max_filesize ?></p>

                    <div class="uploaded-files"
                         style="display: inline-block; position: relative; overflow: hidden;"></div>

                    <div class="spacer-s"></div>
                    <div class="g-recaptcha" data-callback="enableBtn" data-sitekey="<?= CAPTCHA_SITE_KEY ?>"></div>
                    <div class="spacer-s"></div>
                    <button type="submit" id="submit" class="button success large full-page">
                        <?= __('Send') ?>
                    </button>
                </form>
            </div>

            <div class="spacer-l"></div>
        </div>
    </div>
</section>

<script>
    $(function () {
        $("#files").on("change", function () {
            // Remove previously selected files (those aren't selected anymore)
            $(".uploaded-files").html('');

            // Go through each selected file
            $($(this)[0].files).each(function (key, file) {
                // Append the file name with a nice picture/file icon
                $(".uploaded-files").append('<div><span class="glyphicon glyphicon-file"></span> ' + file.name + '</div>');
            });

        });
        $('.file-select').on('click', function () {
            $('#files').trigger('click')
        })
    });
</script>
<script>
    document.getElementById("submit").disabled = true;
    function enableBtn(){
        document.getElementById("submit").disabled = false;
    }
</script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>