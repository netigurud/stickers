<style>
    p {
        margin-bottom: 0;
    }

    .btn {
        padding: .525rem 1.075rem;
        font-size: .775rem;
    }

    .button {
        border-radius: 3px;

        margin: 0;
    }

    input[type="text"] {
        margin: 0;
    }

    input.order-row-final-width, input.order-row-final-height {
        width: 5rem;
    }

    .let-table tbody tr td:last-child {
        width: 5rem;
    }

    .let-table tbody tr td:first-child.button-row {
        padding: 15px 0 15px 0;
    }

    @media screen and (max-width: 768px) {
        .button {
            padding: 5px;
        }
    }

    table th, table td {
        text-align: center !important;
    }

    .printing-house-has-downloaded-the-file {
        background: #4dff4d;
    }
</style>
<section class="user">
    <div class="spacer-l"></div>
    <div class="row">
        <div class="columns">
            <div class="white-panel">
                <h3><?= __('All Orders') ?></h3>

                <div class="spacer-s"></div>

                <table class="let-table">
                    <thead>
                    <tr>
                        <th></th>
                        <th><?= __('ID') ?></th>
                        <th><?= __('Image') ?></th>
                        <th><?= __('Status') ?></th>
                        <th><?= __('Order total') ?></th>
                        <!--                        <th>--><? //= __('Order details') ?><!--</th>-->
                        <th><?= __('Req. changes reason') ?></th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($orders['unfinished_orders'] as $order): ?>
                        <tr <?= $order['order_status_id'] >= ORDER_STATUS_IN_PRODUCTION ? 'class="printing-house-has-downloaded-the-file"' : ''; ?>
                                data-file_id="<?= $order['order_file_id'] ?>" data-order_id="<?= $order['order_id'] ?>">
                            <td style="width: auto;" class="text-row">
                                <?php if ($order['order_file_id']): ?>
                                    <input type="checkbox" class="check-file" style="margin-left: 5px">
                                <?php endif; ?>
                            </td>
                            <td style="width: initial;" class="text-row"><p><?= $order['order_id'] ?></p></td>
                            <td class="order-image-td text-row" data-open="order-image-modal">
                                <?php if ($order['order_file_id']): ?>
                                    <img style="max-height: 50px;" class="order-image"
                                         src="data:image/jpeg;base64,<?= $order['file_preview'] ?>">
                                <?php else: ?>
                                    <?= __('No image') ?>
                                <?php endif; ?>
                            </td>
                            <td class="text-row">
                                <?= $order['order_status_name'] ?>
                                <div>


                                </div>

                            </td>
                            <td class="text-row"><?= $order['order_total_price_with_vat'] ?> €</td>

                            <td style="width: 55%" class="text-row"><?= $order['request_changes_reason'] ?></td>
                        </tr>

                        <tr <?= $order['order_status_id'] >= ORDER_STATUS_IN_PRODUCTION ? 'class="printing-house-has-downloaded-the-file"' : ''; ?>
                                data-order_id="<?= $order['order_id'] ?>">
                            <td colspan="6" class="button-row">
                                <div class="clearfix">
                                    <div class="small-12 columns">
                                        <div style="float: left">


                                            <?php if ($order['order_status_id'] >= ORDER_STATUS_WAITING_FOR_DELIVERY): ?>
                                                <a target="_blank"
                                                   href="https://www.tnt.com/express/en_us/site/tracking.html?searchType=con&cons=<?= $order['order_con_number'] ?>">
                                                    <?= $order['order_con_number'] ?>
                                                </a>
                                            <?php endif; ?>
                                        </div>

                                        <div style="float: right">
                                            <?php if ($auth->is_admin == 1): ?>
                                                <?php if ($order['order_status_id'] != ORDER_STATUS_WAITING_FOR_CUSTOMER_REVIEW): ?>
                                                    <button type="button"
                                                            class="button success small set-order-waiting-for-customer-review">
                                                        <?= __('Set order waiting for customer review') ?>
                                                    </button>
                                                <?php endif; ?>

                                                <?php if ($order['order_status_id'] == ORDER_STATUS_WAITING_FOR_DELIVERY): ?>
                                                    <a href="orders/download_shipping_documentation/<?= $order['order_id'] ?>"
                                                       class="button success small download-shipping-documentation">
                                                        <?= __('Download package label') ?>
                                                    </a>
                                                <?php endif; ?>

                                                <?php if ($order['order_status_id'] == ORDER_STATUS_IN_PRODUCTION): ?>
                                                    <a class="button success small generate-shipping-documentation"
                                                       data-open="shipping-details">
                                                        <?= __('Generate shipping documentation') ?>
                                                    </a>
                                                <?php endif; ?>

                                            <?php else: ?>

                                                <?php if ($is_designer && $order['order_status_id'] == ORDER_STATUS_WAITING_FOR_DESIGNER_REVIEW): ?>
                                                    <button type="button"
                                                            class="button success small set-order-waiting-for-customer-review">
                                                        <?= __('Set order waiting for customer review') ?>
                                                    </button>
                                                <?php endif; ?>

                                                <?php if ($is_printing_house && $order['order_status_id'] == ORDER_STATUS_IN_PRODUCTION): ?>

                                                    <button class="button success small generate-shipping-documentation"
                                                            data-open="shipping-details">
                                                        <?= __('Generate shipping documentation') ?>
                                                    </button>

                                                <?php elseif ($order['order_status_id'] == ORDER_STATUS_WAITING_FOR_DELIVERY): ?>
                                                    <a href="orders/download_shipping_documentation/<?= $order['order_id'] ?>"
                                                       class="button success small download-shipping-documentation">
                                                        <img style="display: none"
                                                             src="assets/img/svg/spinner-hidden.svg">
                                                        <!-- To center the text -->
                                                        <?= __('Download package label') ?>
                                                        <img style="display: none" src="assets/img/svg/spinner.svg">
                                                    </a>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            <a class="button success small order-details-btn"><?= __('Order details') ?></a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr style="display: none;">
                            <td colspan="6">
                                <table class="let-table">
                                    <thead>
                                    <tr>
                                        <th><?= __('Width') ?></th>
                                        <th><?= __('Height') ?></th>
                                        <th><?= __('Price') ?></th>
                                        <th><?= __('Quantity') ?></th>
                                        <th><?= __('Product type') ?></th>
                                        <th><?= __('Final width') ?></th>
                                        <th><?= __('Final height') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody data-order_id="<?= $order['order_id'] ?>"
                                           class="order-details-tbody"></tbody>
                                </table>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

                <p>* <?= __('Green row means that the printing house has downloaded the file.') ?></p>


                <?php if (!empty($orders['finished_orders'])): ?>
                    <div class="spacer-l"></div>

                    <div class="finished-orders">
                        <h3><?= __('Finished') ?></h3>

                        <table class="let-table">
                            <thead>
                            <tr>
                                <th></th>
                                <th><?= __('ID') ?></th>
                                <th><?= __('Image') ?></th>
                                <th><?= __('Status') ?></th>
                                <th><?= __('Order total') ?></th>
                                <th><?= __('Order details') ?></th>
                                <th><?= __('Tracking id') ?></th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php foreach ($orders['finished_orders'] as $order): ?>
                                <tr data-order_id="<?= $order['order_id'] ?>">
                                    <td style="width: auto">
                                        <?php if ($order['order_file_id']): ?>
                                            <input type="checkbox" class="check-file">
                                        <?php endif; ?>
                                    </td>
                                    <td style="width: initial;"><?= $order['order_id'] ?></td>
                                    <td class="order-image-td">
                                        <?php if ($order['order_file_id']): ?>
                                            <img style="max-height: 50px;" class="order-image"
                                                 src="data:image/jpeg;base64,<?= $order['file_preview'] ?>">
                                        <?php else: ?>
                                            <?= __('No image') ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <select class="form-control">
                                            <?php foreach ($order_statuses as $status): ?>
                                                <option value="<?= $status['order_status_id'] ?>"
                                                    <?= $status['order_status_id'] == $order['order_status_id'] ? 'selected' : '' ?>>
                                                    <?= $status['order_status_name'] ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td><?= $order['order_total_price_with_vat'] ?> €</td>
                                    <td style="width: auto; text-align: left;">
                                        <a class="button success small order-details-btn">
                                            <?= __('Order details') ?>
                                        </a>
                                    </td>
                                    <td style="width: auto; text-align: left;">
                                        <a class="">
                                            <?= $order['order_con_number'] ?>
                                        </a>
                                    </td>
                                </tr>

                                <tr style="display: none;">
                                    <td colspan="6">
                                        <table class="let-table">
                                            <thead>
                                            <tr>
                                                <th><?= __('Width') ?></th>
                                                <th><?= __('Height') ?></th>
                                                <th><?= __('Price') ?></th>
                                                <th><?= __('Quantity') ?></th>
                                                <th><?= __('Product type') ?></th>
                                                <th><?= __('Final width') ?></th>
                                                <th><?= __('Final height') ?></th>
                                            </tr>
                                            </thead>
                                            <tbody data-order_id="<?= $order['order_id'] ?>"
                                                   class="order-details-tbody"></tbody>
                                        </table>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>

                <a class="btn btn-primary download-checked-button" disabled download>
                    <span
                            class="glyphicon glyphicon-download-alt"></span>&nbsp;&nbsp;<?= __('Download checked files') ?>
                </a>
            </div>
        </div>
    </div>

    <div class="spacer-l"></div>
</section>


<div class="reveal large" id="shipping-details" data-reveal>
    <div class="row columns">
        <div class="spacer-s"></div>
        <h4 class="h-bold"><?= __('Set shipping details') ?></h4>
        <div class="spacer-m"></div>
    </div>
    <div class="large-6 columns">
        <div class="row">
            <label for="ship-date" class="col-sm-2"><?= __('Date') ?></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="ship-date" value="<?= $collection_time['date']; ?>">
            </div>
        </div>

        <div class="spacer-s"></div>

        <div class="row">
            <label for="collection-time-from" class="col-sm-2"><?= __('Collection time from') ?></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="collection-time-from"
                       value="<?= $collection_time['from'] ?>">
            </div>
        </div>

        <div class="spacer-s"></div>

        <div class="row">
            <label for="collection-time-to" class="col-sm-2"><?= __('Collection time to') ?></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="collection-time-to" value="<?= $collection_time['to'] ?>">
            </div>
        </div>

        <div class="spacer-s"></div>

        <div class="row">
            <label for="set-package-height" class="col-sm-2"><?= __('Height') ?>&nbsp;(m)</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="set-package-height"
                       value="<?= $settings['PACKAGE_HEIGHT'] ?>">
            </div>
        </div>

        <div class="spacer-s"></div>

        <div class="row">
            <label for="set-package-length" class="col-sm-2"><?= __('Length') ?>&nbsp;(m)</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="set-package-length"
                       value="<?= $settings['PACKAGE_LENGTH'] ?>">
            </div>
        </div>

        <div class="spacer-s"></div>

        <div class="row">
            <label for="set-package-width" class="col-sm-2"><?= __('Width') ?>&nbsp;(m)</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="set-package-width"
                       value="<?= $settings['PACKAGE_WIDTH'] ?>">
            </div>
        </div>

        <div class="spacer-s"></div>

        <div class="row">
            <label for="set-package-weight" class="col-sm-2"><?= __('Weight') ?>&nbsp;(kg)</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="set-package-weight"
                       value="<?= $settings['PACKAGE_WEIGHT'] ?>">
            </div>
        </div>
    </div>

    <div class="large-6 columns">
        <div class="row">
            <label for="country-id" class="col-sm-2"><?= __('Country') ?></label>
            <div class="col-sm-10">
                <select name="country_id" id="country-id" class="select2" style="width:100%">
                    <option disabled value=""><?= __('Please select one') ?></option>
                    <?php foreach ($countries as $country): ?>
                        <option value="<?= $country['country_id'] ?>">
                            <?= $country['country_name'] ?>
                        </option>
                    <?php endforeach ?>
                </select>
            </div>
        </div>

        <div class="spacer-s"></div>

        <div class="row">
            <label for="order-state" class="col-sm-2"><?= __('State') ?></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="order-state" autocomplete="off">

            </div>
        </div>

        <div class="spacer-s"></div>

        <div class="row">
            <label for="order-dial-code" class="col-sm-2"><?= __('Dial-code') ?></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="order-dial-code" autocomplete="off">
            </div>
        </div>

        <div class="spacer-s"></div>

        <div class="row">
            <label for="order-street" class="col-sm-2"><?= __('Street') ?></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="order-street" autocomplete="off">
            </div>
        </div>

        <div class="spacer-s"></div>

        <div class="row">
            <label for="order-city" class="col-sm-2"><?= __('City') ?></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="order-city" autocomplete="off">
            </div>
        </div>

        <div class="spacer-s"></div>

        <div class="row">
            <label for="order-phone" class="col-sm-2"><?= __('Phone') ?></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="order-phone" autocomplete="off">
            </div>
        </div>

        <div class="spacer-s"></div>

        <div class="row">
            <label for="order-postal-code" class="col-sm-2"><?= __('Postal-code') ?></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="order-postal-code" autocomplete="off">
            </div>
        </div>

        <div class="spacer-s"></div>

        <div class="row">
            <label for="order-full-name" class="col-sm-2"><?= __('Full-name') ?></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="order-full-name" autocomplete="off">
            </div>
        </div>

        <div class="spacer-s"></div>

        <div class="row">
            <label for="order-company" class="col-sm-2"><?= __('Company') ?></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="order-company" autocomplete="off">
            </div>
        </div>

        <div class="spacer-s"></div>

        <div class="row">
            <label for="order-company-reg-nr" class="col-sm-2"><?= __('Company reg nr') ?></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="order-company-reg-nr" autocomplete="off">
            </div>
        </div>

        <div class="spacer-s"></div>
    </div>

    <div class="row columns">
        <div class="column medium-12">
            <button class="button primary full-page confirm-shipping-details" type="button">
                <img src="assets/img/svg/spinner-hidden.svg"> <!-- To center the text -->
                <?= __('Confirm') ?>
                <img style="display: none" id="confirm-shipping-details-loading-wheel" src="assets/img/svg/spinner.svg">
                <img id="confirm-shipping-details-loading-wheel-hidden" src="assets/img/svg/spinner-hidden.svg">
            </button>
        </div>
    </div>
</div>

<div class="reveal login-reveal" id="order-image-modal" data-reveal>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>

    <div class="row columns">
        <div class="spacer-s"></div>
        <h4 class="h-bold"><?= __('Orders file') ?></h4>

        <div class="spacer-m"></div>
    </div>

    <div class="row columns">
        <img src="" class="order-image-in-modal">
    </div>

    <div class="spacer-s"></div>

    <div class="row columns">

        <a href="#" download class="button primary full-page download-image">
            <?= __('Download image') ?>
        </a>

        <div class="spacer-xs"></div>

        <?php if ($is_designer): ?>
            <a href="#" download class="button primary full-page download-image take-order">
                <?= __('Take order and download image') ?>
            </a>

            <div class="spacer-xs"></div>

            <button type="button" class="button primary full-page upload-new-file resumable-browse">
                <?= __('Upload new image') ?>
            </button>

        <?php endif; ?>
    </div>
</div><!-- Order image modal -->

<script src="assets/js/resumable.js"></script>
<script>

    var r;
    var order_id;
    var clicked_order_id;
    var clicked_order_file_id;
    var order_row_can_be_changed;
    var country_id = $('#country-id');
    var order_state = $('#order-state');
    var order_dial_code = $('#order-dial-code');
    var order_street = $('#order-street');
    var order_city = $('#order-city');
    var order_phone = $('#order-phone');
    var order_postal_code = $('#order-postal-code');
    var order_full_name = $('#order-full-name');
    var order_company = $('#order-company');
    var order_company_reg_nr = $('#order-company-reg-nr');


    $(function () {

        $('.check-file').on('click', function () {

            constructDownloadCheckedButtonUrlAndEnableOrDisableTheButton('images')
        });

        // Show order details
        $(".order-details-btn").on("click", function () {
            order_id = getTrData($(this), 'order_id');
            showOrderDetails(order_id);
        });

        <?php if(!empty($_SESSION['order_id'])):?>
        showOrderDetails(<?=$_SESSION['order_id']?>);
        <?php unset($_SESSION['order_id']);?>
        <?php endif;?>

        // Show order details
        function showOrderDetails(order_id) {
            ajax("orders/get", {
                order_id: order_id
            }, function (json) {
                var readonly;
                var order_data = json.data;
                var order_details_tbody = $('tbody.order-details-tbody[data-order_id="' + order_id + '"]');

                // Remove old data
                order_details_tbody.html('');

                order_row_can_be_changed = order_data['order']['order_status_id'] == <?= ORDER_STATUS_WAITING_FOR_DESIGNER_REVIEW ?>;

                if (order_row_can_be_changed) {
                    readonly = '<?= $is_designer || $this->auth->is_admin ? '' : 'readonly' ?>';
                } else {
                    readonly = 'readonly';
                }

                // Fill the table
                $.each(order_data['order_rows'], function (index, value) {
                    order_details_tbody.append(
                        '<tr data-order_row_id="' + value.order_row_id + '">' +
                        '<td style="width: initial;">' + value.width + '</td>' +
                        '<td>' + value.height + '</td>' +
                        '<td>' + value.price_with_vat + '</td>' +
                        '<td>' + value.quantity + '</td>' +
                        '<td>' + value.product_type_name + '</td>' +
                        '<td><input type="text" class="order-row-final-width" value="' + value.final_width + '" ' + readonly + '></td>' +
                        '<td><input type="text" class="order-row-final-height" value="' + value.final_height + '" ' + readonly + '></td>' +
                        '</tr>'
                    );
                });

                $('tr[data-order_id="' + order_id + '"]').next().next().fadeToggle();

                <?php if ($is_designer): ?>
                if (order_row_can_be_changed) {
                    // Let the designer set the final measurement for this order row
                    $(document).on('change', '.order-row-final-width, .order-row-final-height', function () {
                        var order_row_id = $(this).closest('tr').attr('data-order_row_id');
                        var row = $(this).closest('tr');

                        ajax("orders/set_final_measurements", {
                            order_row_id: order_row_id,
                            final_width: row.find(".order-row-final-width").val(),
                            final_height: row.find(".order-row-final-height").val()
                        });
                    });
                }
                <?php endif; ?>

            });
        }

        $(".generate-shipping-documentation").on("click", function () {
            order_id = getTrData($(this), 'order_id');
            ajax('orders/get', {order_id: order_id}, function (json) {
                country_id.val(json.data.order.order_country_id);
                order_state.val(json.data.order.order_state);
                order_dial_code.val(json.data.order.order_dial_code);
                order_street.val(json.data.order.order_street);
                order_city.val(json.data.order.order_city);
                order_phone.val(json.data.order.order_phone);
                order_postal_code.val(json.data.order.order_postal_code);
                order_full_name.val(json.data.order.order_full_name);
                order_company.val(json.data.order.order_company);
                order_company_reg_nr.val(json.data.order.order_company_reg_nr);


            })

        });

        // Download the orders connote, label, manifest and invoice
        $(".confirm-shipping-details").on("click", function () {
            ;
            $("#confirm-shipping-details-loading-wheel-hidden").hide();
            $("#confirm-shipping-details-loading-wheel").show();
            var error = false;
            var package_height = $("#set-package-height").val();
            var package_length = $("#set-package-length").val();
            var package_weight = $("#set-package-weight").val();
            var package_width = $("#set-package-width").val();

            $.each([package_height, package_length, package_weight, package_width],
                function (key, value) {
                    if (isNaN(value) || value < 0.01) {
                        error = true;
                        return false;
                    }
                }
            );

            ajax("orders/set_shipping_details", {
                dimensions: {
                    PACKAGE_HEIGHT: package_height,
                    PACKAGE_LENGTH: package_length,
                    PACKAGE_WEIGHT: package_weight,
                    PACKAGE_WIDTH: package_width
                },
                ship_date: $('#ship-date').val(),
                order_id: order_id,
                collection_time_to: $('#collection-time-to').val(),
                collection_time_from: $('#collection-time-from').val(),
                order_company: order_company.val(),
                order_company_reg_nr: order_company_reg_nr.val(),
                address: {
                    country_id: country_id.val(),
                    order_state: order_state.val(),
                    order_dial_code: order_dial_code.val(),
                    order_street: order_street.val(),
                    order_city: order_city.val(),
                    order_phone: order_phone.val(),
                    order_postal_code: order_postal_code.val(),
                    order_full_name: order_full_name.val()

                }
            }, function () {
                ajax("orders/generate_shipping_documentation", {order_id: order_id}, RELOAD, function (json) {
                    show_error_modal(json.data);
                    $("#confirm-shipping-details-loading-wheel").hide();
                    $("#confirm-shipping-details-loading-wheel-hidden").show();
                });
            });
            $(".confirm-shipping-details").prop("disabled", true);
        });


        <?php if ($is_designer): ?>
        $(".set-order-waiting-for-customer-review").on("click", function () {
            var that = $(this);

            swal({
                title: "<?= __('Are you sure you want to set this orders status to pending?') ?>",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then(function (changeStatus) {
                if (changeStatus) {
                    ajax("orders/set_order_waiting_for_customer_review", {order_id: getTrData(that, 'order_id')}, RELOAD);
                }
            });
        });

        resumable('files/upload', false, function () {
            // Show the loading modal
            $("#progress-modal").modal('show');

            $('#upload-progress').removeClass('hide').find('.progress-bar').css('width', '0%');

            // Actually start the upload
            r.upload();

        }, function () {
            location.reload()
        });

        // Update order file
        $(".upload-new-file").on("click", function () {
            r.opts.target = 'files/upload/' + clicked_order_id;

        });

        <?php endif; ?>



        $(".order-image-td").on("click", function () {

            // Remember clicked order id
            clicked_order_id = getTrData($(this), 'order_id');
            clicked_order_file_id = getTrData($(this), 'file_id');

            // Replace modal image's src to clicked image
            $(".order-image-in-modal").attr("src", $(this).find('img').attr("src"));

            // Replace download button
            $('.download-image').prop('href', 'files/' + clicked_order_file_id);
        });

        $(".order-image-in-modal, .download-image").on("click", function () {

            // Close modal
            //$('#order-image-modal').hide();
            $('.reveal-overlay').fadeOut();

            <?php if ($is_printing_house): ?>

            // Change order status
            ajax("orders/send_to_print_house", {order_id: clicked_order_id}, function () {

                // Make order row green
                setTimeout(function () {
                    location.reload()
                }, 300);

            });

            <?php endif; ?>
        });

        $(".take-order").on("click", function () {

            // Close modal
            //$('#order-image-modal').hide();
            $('.reveal-overlay').fadeOut();

            <?php if ($is_designer): ?>
            ajax("orders/take", {order_id: clicked_order_id}, function () {
            });
            <?php endif; ?>
        });

        $(".download-checked-button").on("click", function () {
            setTimeout(function () {
                location.reload()
            }, 300);
        });

        $(".download-shipping-documentation").on("click", function () {
            var that_children = $(this).children();

            // Don't allow link to work when already downloading package label, else show that it's downloading
            if ($(this).prop('disabled') === true) {
                return false;
            }
            that_children.show();
            $(".download-shipping-documentation").prop('disabled', true);

            setTimeout(function () {
                that_children.hide();
                $(".download-shipping-documentation").prop('disabled', false);
            }, 6000);
        });


    });

</script>