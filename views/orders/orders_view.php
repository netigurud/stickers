<style>
    .let-table tbody tr td:first-child {
        padding: 0;
    }

</style>
<div class="text-center">
    <div class="spacer-m"></div>


    <div class="row">
        <div class="approvebox">

            <div class="column medium-6">
                <div>
                    <h3><?= __('Designer-adjusted image') ?></h3>
                    <img src="data:image/jpeg;base64,<?= $order['order']['file_preview'] ?>">
                </div>
            </div>
            <div class="column medium-6">
                <h3>Products</h3>
                <?php $n = 0;
                foreach ($order['order_rows'] as $order_row): $n++ ?>
                    <div class="alert alert-info text-left">
                        <h3 class="h-bold">
                            <?= $n ?>) <?= $order_row['product_type_id'] == 1 ? __('Stickers') : __('Magnets') ?>
                            (<?= $order_row['quantity'] ?>pc)

                        </h3>

                        <?php if($order_row['final_width'] != $order_row['width'] || $order_row['final_height'] != $order_row['height']):?>
                        <p class="alert alert-warning">
                            <b><?= __('Originally requested size') ?></b>:
                            <?= $order_row['width'] ?> cm x <?= $order_row['height'] ?> cm<br>

                            <b><?= __('Designer-adjusted size') ?></b>:
                            <?= $order_row['final_width'] ?> cm x <?= $order_row['final_height'] ?> cm
                        </p>
                        <?php else: ?>
                        <p>
                            <b><?= __('Size') ?></b>:
                            <?= $order_row['final_width'] ?> cm x <?= $order_row['final_height'] ?> cm<br>
                        </p>
                        <?php endif?>
                    </div>


                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="spacer-m"></div>
        <div class="column medium">
            <button class="button big success approve-order">
                <?= __('Approve') ?>
            </button>

            <h6 class="h-bold">
                <?= __('or') ?>
                <a data-open="request-changes-modal">
                    <?= __('request changes') ?>
                </a>
            </h6>
        </div>
    </div>

    <div class="spacer-m"></div>
</div>

<div class="reveal login-reveal" id="request-changes-modal" data-reveal>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>

    <div class="row columns">
        <div class="spacer-s"></div>
        <h4 class="h-bold"><?= __('Request changes') ?></h4>
        <div class="spacer-m"></div>
    </div>

    <div class="row columns">
        <label><span><?= __('Reason') ?></span>
            <input type="text" class="request-changes-reason" required>
        </label>
    </div>

    <div class="spacer-s"></div>

    <div class="row columns">
        <button class="button primary full-page request-changes" type="button"><?= __('Request changes') ?></button>
    </div>
</div>

<script>
    $(function () {
        var approve_order_btn = $(".approve-order");
        var request_changes_reason = $(".request-changes-reason");

        // Change order status to "user requested changes" and add the reason
        $(".request-changes").on("click", function () {

            // Check that the reason isn't too short
            if (request_changes_reason.val().length < 3) {
                show_alert_modal("<?= __('The reason must be at least 3 characters long') ?>");
                return false;
            }

            ajax("orders/request_changes", {
                order_id: <?= $order['order']['order_id'] ?>,
                request_changes_reason: request_changes_reason.val()
            }, "my_orders");
        });

        approve_order_btn.on("click", function () {
            swal({
                title: "<?= __('Are you sure?') ?>",
                text: "<?= __('Are you sure you want to approve the order?') ?>",
                icon: 'success',
                buttons: {
                    cancel: {
                        text: "<?= __('Cancel') ?>",
                        value: false,
                        visible: true,
                        className: "",
                        closeModal: true
                    },
                    confirm: {
                        text: "<?= __('Yes, approve it') ?>",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                }
            }).then(function (result) {
                if (result) {
                    // Disable the button to indicate that we're loading the users request
                    approve_order_btn.attr("disabled", true);
                    approve_order_btn.html("Approving...");

                    ajax("orders/approve", {
                        order_id: <?= $this->params[0] ?>
                    }, "my_orders", function (json) {

                        // Re-enable Approve button
                        approve_order_btn.removeAttr("disabled");

                        // Fill error modal with data from the server
                        $("#error-modal .modal-title").html(json.data.title + "!");
                        $(".error-modal-body").html(json.data.description);
                        $("#error-modal").modal('show');
                    });
                }
            });
        });
    });
</script>