<?php namespace App;

// Init composer auto-loading
if (!@include_once("vendor/autoload.php")) {

    exit('Run composer install');

}

include 'system/functions.php';
include 'constants.php';

// Load config
if (file_exists('config.php')) {

    define('ENV_DEVELOPMENT', 0);
    define('ENV_PRODUCTION', 1);

    include 'config.php';

    // Default env is development
    if(!defined('ENV')) define('ENV', ENV_DEVELOPMENT);

} else {
    error_out('No config.php. Please make a copy of config.sample.php and name it config.php and configure it.', 500);
}

// Set the correct timezone
date_default_timezone_set("Europe/Tallinn");

// Load app
try {
    $app = new Application;
} catch (\Exception $e) {
    if (ENV == ENV_DEVELOPMENT)  {

        // Throw unhandled exception on localhost
        /** @noinspection PhpUnhandledExceptionInspection */
        throw $e;
    } else {
        $message = $e->getMessage();
        $file = $e->getFile();
        $line = $e->getLine();
        $trace = $e->getTrace();
        send_error_report("$message [$file:$line]", $trace);
        error_out('There was an error but don\'t worry, we are now aware of it and will look into it. Meanwhile you can go back and try again or contact support at the following number:' . ' ' . SUPPORT_PHONE_NUMBER . '.');
    }
}