<?php

use App\Mail;

/**
 * Display a fancy error page and quit.
 * @param $error_msg string Error message to show
 * @param int $code HTTP RESPONSE CODE. Default is 500 (Internal server error)
 */
function error_out($error_msg, $code = 500)
{

    // Return HTTP RESPONSE CODE to browser
    header($_SERVER["SERVER_PROTOCOL"] . " $code Something went wrong", true, $code);


    // Set error message
    $errors[] = $error_msg;


    // Show pretty error, too, to humans
    require __DIR__ . '/../templates/error_template.php';


    // Stop execution
    exit();
}

/**
 * Loads given controller/action, and global, translations to memory
 * @param $lang string Language to load
 * @param $controller string Controller to load translations for
 * @param $action string Action to load translations for
 */
function get_translation_strings($lang)
{
    global $translations;
    $translations_raw = get_all("SELECT phrase,translation FROM translations WHERE language='$lang'");

    foreach ($translations_raw as $item) {
        // Uncomment this line if the same phrase need to be translated differently on different pages
        //$translations[$item['controller'] . $item['action'] . $item['phrase']] = $item['translation'];
        $translations[$item['phrase']] = $item['translation'];

    }
}

/**
 * Translates the text into currently selected language
 * @param $text string The text to be translated
 * @param bool $global Set to false if you want to let the user translate this string differently on different sub-pages.
 * @return string Translated text
 */
function __($text, $global = true)
{
    global $translations;
    global $translations_used;

    $active_language = $_SESSION['language'];

    // Database phrase field length is 255 due to unique index
    $first_765_bytes_of_text = substr($text, 0, 765);

    // Load translations only the first time (per request)
    if (empty($translations) && $active_language) {
        get_translation_strings($active_language);
    }

    // Safe way to query translation
    $translation = isset($translations[$first_765_bytes_of_text]) ? $translations[$first_765_bytes_of_text] : null;

    // Insert new translation stub into DB when text wasn't empty but a matching translation didn't exist in the DB
    if ($text !== null && $translation == null) {

        // Insert new stub
        insert('translations', [
            'phrase' => $first_765_bytes_of_text,
            'translation' => '{untranslated}',
            'language' => $active_language,
        ]);

        // Set translation to input text when stub didn't exist
        $translation = $text;

    } else {
        if ($translation == '{untranslated}') {

            // Set translation to input text when stub existed but was not yet translated
            $translation = $text;
        }
    }

    // Store this translation to update its last_active at the end of the script
    $translations_used[] = $text;

    return nl2br($translation);

}

/**
 * @param $code
 * @param bool $data
 * @param bool $encode
 */
function stop($code, $data = false, $encode = false)
{
    $response['status'] = $code;

    if (ENV == ENV_DEVELOPMENT && $code != 200) {
        $response['debug'] = get_backtrace();
    }

    // Hide actual error in production in case of system errors
    if ($code == 500 && ENV == ENV_PRODUCTION) {
        $auth = empty($_SESSION['user_id']) ? null : get_all("select * from users where user_id = $_SESSION[user_id]");
        send_error_report($data);
        $data = __('There was an error');
    }

    if ($data) {
        $response['data'] = $encode ? base64_encode($data) : $data;
        $response['encoded'] = $encode ? true : false;
    }

    $encoded = json_encode($response);

    // Change HTTP status code
    http_response_code($code);

    echo $encoded;
    exit();
}

function get_backtrace()
{
    $backtrace = debug_backtrace();
    array_shift($backtrace);

    return json_encode($backtrace, JSON_PRETTY_PRINT);
}

function fieldsAreEmpty($array)
{
    foreach ($array as $item) {
        if (empty($item)) {
            return true;
        }
    }

    return false;
}


function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


function redirect($url = '')
{
    header("Location: " . BASE_URL . $url);
    exit();
}

function slugify($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '_', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}

function curlPost($post_data = [])
{

    // Curl check
    if (!function_exists("curl_init") &&
        !function_exists("curl_setopt") &&
        !function_exists("curl_exec") &&
        !function_exists("curl_close")
    ) {
        die('Install curl extension first.');
    }

    $ch = curl_init();

    if (false === $ch) {
        throw new Exception('failed to initialize');
    }

    // set url
    curl_setopt($ch, CURLOPT_HEADER, true);    // we want headers
    curl_setopt($ch, CURLOPT_URL, "https://express.tnt.com/expressconnect/shipping/ship");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);

    // $output contains the output string
    $output = curl_exec($ch);
    $information = curl_getinfo($ch);

    // Verify that we have a response
    if (curl_error($ch) || false === $output) {
        throw new \Exception(json_encode(curl_error($ch), JSON_PRETTY_PRINT));
    }

    // Get response code
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $headers = explode("\r\n", substr($output, 0, $header_size));
    // Process body
    $body = substr($output, $header_size);

    /*
        echo "Request headers:";
        echo "<pre>";
        echo $information['request_header'];
        echo "</pre>";


        echo "Post data: (shown in json format)";
        echo "<pre>";
        echo http_build_query($post_data);
        echo "</pre>";

        echo "Response:";
        echo "<pre>";
        echo htmlentities($output);
        echo "</pre>";

        echo "<hr>";
    */
    if ($information['http_code'] > 399
        || strpos($body, '<parse_error>')
        || strpos($body, '<error_reason>')
        || strpos($body, '<ERROR>')) {
        throw new \Exception($body);
    }

    return $body;
}


function fillPlaceholders($placeholders, $subject)
{
    foreach ($placeholders as $placeholder => $value) {
        $subject = str_replace($placeholder, $value, __($subject));
    }
    return $subject;
}


function showXMLasHTML($xml_received, $fix_stylesheet_encoding = false)
{
    // Load XML
    $XML = new DOMDocument();
    $XML->loadXml($xml_received);

    // Get the stylesheet link
    $xpath = new DOMXpath($XML);
    $styleLocation = $xpath->evaluate('string(//processing-instruction()[name() = "xml-stylesheet"])');
    $xslLocation = $fix_stylesheet_encoding ? BASE_URL . "assets/xsl/HTMLAddressLabelRenderer.xsl" : explode("\"",
        $styleLocation, 3)[1];

    // Start XSLT
    $xslt = new XSLTProcessor();

    // Import stylesheet
    $XSL = new DOMDocument();
    $XSL->load($xslLocation);
    $xslt->importStylesheet($XSL);

    $html = $xslt->transformToXML($XML);
    return $html;
}

function priceWithVAT($price, $user_vat)
{
    // Coefficient is 1.2 when user_vat is given
    $coefficient = empty($user_vat) ? 1.2 : 1;
    return number_format($price * 1.2, 2, '.',
        '');
}

if (!function_exists('dd')) {
    function dd(...$args)
    {
        if (function_exists('dump')) {
            dump(...$args);
        } else {
            var_dump(...$args);
        }
        try {
            throw new Exception();
        } catch (Exception $e) {
            echo "<pre>";
            echo $e->getTraceAsString();
            echo "</pre>";
        }
        die;
    }
}

function array_map_recursive(callable $func, array $arr)
{
    array_walk_recursive($arr, function (&$v) use ($func) {
        $v = $func($v);
    });
    return $arr;
}

function prettify_json($input_string)
{
    $json_object = json_decode($input_string);
    $string_is_json = (json_last_error() === JSON_ERROR_NONE);
    return $string_is_json ? json_encode($json_object, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) : $input_string;
}

function attempt_to_stringify($input)
{
    return is_array($input) ? json_encode($input, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE) : $input;
}

function send_error_report($message, $trace = null)
{
    $trace = empty($trace) ? get_backtrace() : $trace;

    $auth = empty($_SESSION['user_id']) ? null : get_all("select * from users where user_id = $_SESSION[user_id]");

    $referer = empty($_SERVER['HTTP_REFERER']) ? '' : $_SERVER['HTTP_REFERER'];
    Mail::send(
        DEVELOPER_EMAIL,
        'An error occurred in the project' . PROJECT_NAME,
        '<pre> PHP' . date('Y-m-d H:i:s ') . strtr(json_encode([
            'error_message' => $message,
            'SERVER' => [
                'REMOTE_ADDR' => $_SERVER['REMOTE_ADDR'],
                'HTTP_REFERER' => $referer,
            ],
            'POST' => $_POST,
            'SESSION' => $_SESSION,
            'COOKIE' => $_COOKIE,
            'AUTH' => $auth,
            'backtrace' => $trace
        ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_LINE_TERMINATORS), [
            ' ' => '&nbsp',
            '\r\n' => '<br>',
            '\n' => '<br>',
            '\"' => '"'
        ]) . '</pre>');
}