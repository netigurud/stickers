<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 19.02.2018
 * Time: 10:27
 */

namespace App;


class PDF
{
    static function orderManifestAndLabel($html)
    {
        $mpdf = new \Mpdf\Mpdf();

        // Label
        $mpdf->WriteHTML(urldecode(utf8_decode($html[0])));
        $mpdf->AddPage();

        // Replace � with a space
        $manifest_html = preg_replace('/\xa0/', ' ', $html[1]);

        // Fix image URLs
        $manifest_html = urldecode(utf8_decode($manifest_html));

        // Manifest
        $mpdf->WriteHTML($manifest_html);

        $mpdf->SetDisplayMode('fullwidth');

        return $mpdf;
    }

    static function userData($user_data)
    {
        ob_start();
        include 'templates/pdf_files/user_data.php';
        $html = ob_get_clean();

        $mpdf = new \Mpdf\Mpdf();

        $mpdf->WriteHTML($html);
        $mpdf->SetDisplayMode('fullwidth');

        return $mpdf;
    }

}