<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 05.02.2018
 * Time: 14:55
 */

namespace App;


class ShoppingCart
{
    static function containsOppositeProduct($width, $height, $product_type)
    {
        $opposite_product_type = $product_type == PRODUCT_TYPE_STICKER ? PRODUCT_TYPE_MAGNET : PRODUCT_TYPE_STICKER;
        foreach ($_SESSION['shopping_cart']['products'] as $product) {
            if ($product['product_width'] == $width && $product['product_height'] == $height && $product['product_type_id'] == $opposite_product_type) {
                return true;
            }
        }

        return false;
    }
}