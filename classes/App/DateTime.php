<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 07.04.2018
 * Time: 10:59
 */

namespace App;


class DateTime
{
    /**
     * Check if given date is todays date
     * @param $date
     * @return bool
     */
    static function isToday($date)
    {
        return strtotime($date) == strtotime(date('Y-m-d'));
    }

    /**
     * Checks if given date is a weekend
     * @param $date
     * @return bool
     */
    static function isWeekend($date)
    {
        return (date('N', strtotime($date)) >= 6);
    }

    /**
     * Adds given number of days to given date
     * @param $date
     * @param $days_to_add
     * @return false|string
     */
    static function addDaysToDate($date, $days_to_add)
    {
        $final_date_timestamp = strtotime("+$days_to_add day", strtotime($date));
        return date('Y-m-d', $final_date_timestamp);
    }

    /**
     * Format given date to given format
     * @param $date
     * @param $format
     * @return false|string
     */
    static function formatDate($date, $format)
    {
        return date($format, strtotime($date));
    }

    /**
     * Check if given date is a holiday in Estonia
     * @param $date
     * @return bool
     */
    public static function isHoliday($date)
    {
        return !empty(get_one("SELECT date FROM holidays WHERE (kind_id = 1 OR kind_id = 2) AND date = '$date'"));
    }

    /**
     * Check if given date is shortened in Estonia
     * @param $date
     * @return bool
     */
    public static function shortenedWorkday($date)
    {
        return !empty(get_one("SELECT date FROM holidays WHERE kind_id = 4 AND date = '$date'"));
    }

    public static function generateYearRange($begin_year, $endyear)
    {
        $years = [];
        for ($year = $begin_year; $year <= $endyear;)  $years[] = $year++;
        return $years;
    }

    public static function generateMonthRange()
    {
        $months = [];
        for ($month = 1; $month <= 12;)  $months[] = $month++;
        return $months;
    }

    public static function period($year, $month){
        return $year . '-' . str_pad($month, 2, '0', STR_PAD_LEFT);
    }

    public static function padMonth($month){
        return str_pad($month, 2, '0', STR_PAD_LEFT);
    }
}