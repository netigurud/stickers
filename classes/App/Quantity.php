<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 09.02.2018
 * Time: 15:36
 */

namespace App;


class Quantity
{

    static function get($product_type_id)
    {
        return get_all("SELECT DISTINCT quantity FROM prices WHERE product_type_id = $product_type_id");
    }

    static function listPrices($product_type_id, $m2 = '')
    {
        if (!$m2) {
            $m2 = get_first("select products.product_width*products.product_height as m2 from products");
            $m2 = $m2['m2'];
        }

        return get_all("SELECT quantity, price FROM prices 
WHERE m2=(SELECT m2 FROM prices WHERE m2>=$m2 AND product_type_id = $product_type_id ORDER BY m2 LIMIT 1) AND product_type_id = $product_type_id");
    }
}