<?php

namespace App;


class Post
{
    static function get($post_id)
    {
        return get_first("SELECT * FROM posts WHERE post_id=$post_id");
    }

    static function getAll()
    {
        return get_all("SELECT * FROM posts");
    }
}