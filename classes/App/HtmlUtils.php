<?php

namespace App;

use HTMLPurifier;

class HtmlUtils
{
    static function purify($html)
    {
        $allowed_tags = ["b", "br", "em", "hr", "i", "li", "ol", "p", "s", "span", "table", "tr", "td", "u", "ul", "img", "iframe", "a", "h1", "h2", "h3", "h4", "h5"];
        $allowed_attr = ["src", "height", "width", "alt", "style", "href"];

        $config = \HTMLPurifier_Config::createDefault();
        $config->set('Core.Encoding', 'UTF-8');
        $config->set('HTML.Doctype', 'HTML 4.01 Transitional');
        $config->set('HTML.AllowedAttributes', $allowed_attr);
        $config->set('HTML.AllowedElements', $allowed_tags);
        $config->set('AutoFormat.RemoveEmpty', true); // remove empty tag pairs
        $config->set('AutoFormat.RemoveEmpty.RemoveNbsp', true); // remove empty, even if it contains an &nbsp;
        $config->set('AutoFormat.AutoParagraph', false);
        $config->set('URI.AllowedSchemes', array('data' => true, 'https' => true, 'http' => true));
        $config->set('Core.RemoveInvalidImg',false);
        $config->set('HTML.SafeIframe', true);
        $config->set('URI.SafeIframeRegexp', '%^(https?:)?(\/\/www\.youtube(?:-nocookie)?\.com\/embed\/|\/\/player\.vimeo\.com\/)%');

        $purifier = new HTMLPurifier($config);

        return $purifier->purify($html);
    }
}