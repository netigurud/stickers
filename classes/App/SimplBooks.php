<?php namespace App;


class SimplBooks
{

    /**
     * @param $rows
     * @param $order
     * @param $simplbooks_client_id
     * @return object
     * @throws \Exception
     */
    public static function createInvoice($rows, $order, $simplbooks_client_id)
    {
        $today = date('Y-m-d');

        if (!is_numeric($simplbooks_client_id)) {
            throw new \Exception('Invalid simplbooks_client_id');
        }


        $invoice = array(
            "Invoice" => array(
                "client_id" => $simplbooks_client_id,
                "language" => 'en_GB',
                "due" => date('Y-m-d', strtotime($today . ' + 7 days')),
                "additional_info" => "Invoice for the Order with an ID of $order[order_id]",
                "sent" => $today
            )
        );


        // Generate invoice number based on the environment the code runs in (development DB gets reset a lot)
        $invoice['Invoice']['number'] = ENV == ENV_DEVELOPMENT ?
            date('YmdHis') : // current timestamp
            $order['order_id']; // order_id

        // Check existing invoice with same number
        $existing_invoice = SimplBooks::get('Invoices', ['invoice_number' => $invoice['Invoice']['number']]);
        if ($existing_invoice) {
            throw new \Exception("For some unforeseen reason, invoice number {$invoice['Invoice']['number']} already existed in Simplbooks.");
        }

        // Add rows
        foreach ($rows as $row) {
            $invoice["Tasks"][] = array(
                "Task" => $row
            );
        };

        $result = self::request(Curl::POST, "invoices/create", $invoice);
        return $result;
    }

    /**
     * @param $client_data
     * @return object
     * @throws \Exception
     */
    public static function createClient($client_data)
    {
        $client = self::request(Curl::POST, "clients/create", ['Client' => $client_data]);

        return $client->inserted_id;
    }

    /**
     * @param $client_data array [id, name, address_city, address_street, address_postal_code, address_county,
     * address_country, reg_no]. Must include id!
     * @return object
     * @throws \Exception
     */
    public static function updateClient($client_data)
    {
        return self::request(Curl::POST, "clients/update", [
            "Client" => $client_data]);
    }


    /**
     * @param $simplbooks_invoice_id
     * @return mixed
     * @throws \Exception
     */
    public static function getInvoice($simplbooks_invoice_id)
    {

        if (!is_numeric($simplbooks_invoice_id)) {
            throw new \Exception('Invalid simplbooks_invoice_id');
        }

        $request = SimplBooks::request(Curl::GET, 'Invoices/get/' . $simplbooks_invoice_id);

        if (empty($request->data)) {
            throw new \Exception('Invalid Simplbooks response. ' . file_get_contents('curl_last.txt'));
        }

        // Move invoice rows under invoice
        $request->data->Invoice->rows = $request->data->Task;

        // Remove [123] from the end of the client name
        $request->data->Invoice->client_name = preg_replace("/(.*)\[\d+\]$/", '$1', $request->data->Invoice->client_name);
        return $request->data->Invoice;
    }

    /**
     * Perform request to Simplbooks API
     * @param $endpoint string SimplBooks API url
     * @param array $post_data Array to be POSTed
     * @param string $method Curl::GET or Curl::POST
     * @return object SimplBooks JSON object converted to PHP object
     * @throws \Exception SimplBooks response(JSON, if possible) serialized to PHP array
     */
    public static function request($method, $endpoint, $post_data = array())
    {

        if (!in_array($method, [Curl::GET, Curl::POST])) {
            throw new \Exception('Invalid method');
        }

        if (!defined('SIMPLBOOKS_TOKEN') || empty(SIMPLBOOKS_TOKEN)) {
            die('SIMPLBOOKS_TOKEN is not set in config.php');
        }

        if (!defined('SIMPLBOOKS_API_URL') || empty(SIMPLBOOKS_API_URL)) {
            die('SIMPLBOOKS_API_URL is not set in config.php');
        }

        $response = Curl::request
        (
            $method,
            SIMPLBOOKS_API_URL . $endpoint,
            $post_data,
            ['X-Simplbooks-Token: ' . SIMPLBOOKS_TOKEN]);

        if (!$response = json_decode($response))
            throw new \Exception('Unable to convert Simplbooks response from JSON to PHP array: ' . json_last_error_msg() . "\n" . Curl::getLast());

        if (empty($response->status))
            throw new \Exception('Simplbooks response was in unexpected format: ' . Curl::getLast());

        if ((int)$response->status >= 300)
            throw new \Exception(Curl::getLast());

        return $response;

    }


    /**
     * Returns the id of a client whose name ends with [$user_id]
     * @param $user_id
     * @return bool simplbooks_client_id
     * @throws \Exception
     */
    public static function getClientIdByUserId($user_id)
    {

        // Get an array of clients whose name *contains* [$user_id]
        $simplbooks_clients = self::get('Clients', ['name' => "[$user_id]"], true);

        // Make sure Simplbooks doesn't return more than one client
        if (count($simplbooks_clients) > 1) {
            throw new \Exception('Simplbooks has more than one client with a name matching [$user_id] which should never happen');
        }

        // Filter out occurrences where something like [1234] was entered by the user as the name (Simplbooks matches substrings)
        foreach ($simplbooks_clients as $simplbooks_client) {
            if (preg_match("/\[(\d+)\]$/", $simplbooks_client->Client->name)) {

                // Return found client
                return $simplbooks_client->Client->id;
            }
        }

        // No client with [$user_id] in the name exists in Simplbooks
        return false;

    }

    /**
     * Gets the existing Simplbooks Client id for given user_id or, if Simplbooks didn't have one, creates a new
     * @param $order array of order data as given by Order::get(). Must include country name
     * @return bool|int simplbooks_client_id
     * @throws \Exception
     */
    public static function getOrCreateClient($order)
    {
        // Generate client name
        $client_name = self::generateClientName($order);

        $client_data = [
            'e_mail' => $order['user_email'],
            'name' => $client_name,
            'phone' => $order['user_phone'],
            'address_street' => $order['user_street'],
            'address_city' => $order['user_city'],
            'address_postal_code' => $order['user_postal_code'],
            'address_country' => $order['country_name'],
            'address_county' => $order['user_state'],
            'reg_no' => null, // Simplbook errors when reg_no is already used (happens a lot in dev when our DB is wiped),
            'vat_no' => $order['user_vat'] ? $order['user_vat'] : ''];


        // Attempt to get the id of an existing client from Simplbooks
        if ($simplbooks_client_id = self::getClientIdByUserId($order['order_made_by'])) {

            // Add id to array
            $client_data['id'] = $simplbooks_client_id;

            // Update Simplbooks client data
            SimplBooks::updateClient($client_data);

        } else {

            // Create a new client if it didn't exist in Simplbooks
            $simplbooks_client_id = SimplBooks::createClient($client_data);
        }

        return $simplbooks_client_id;

    }

    /**
     * Generates a name to use in Simplbooks for client from given order array
     * @param $order
     * @return string
     */
    public static function generateClientName($order)
    {
        return $order['order_company'] ?
            "$order[order_company] [$order[order_made_by]]" :
            "$order[order_full_name] [$order[order_made_by]]";
    }

    /**
     * Returns an array of objects of requested type, filtered by given criteria
     * @param $category
     * @param array $criteria
     * @param bool $partial Whether to return also partial matches
     * @return array
     * @throws \Exception
     */
    public static function get($category, $criteria = [], $partial = false)
    {
        $page = 1;
        $result = [];
//        $object_name = rtrim($category, 's'); // Remove s from the end
        $object_name = strtolower($category);
        $lookup = [
            'Invoices' => [
                'object_name' => 'invoices',
                'fields' => [
                    'invoice_number' => 'number',
                ]
            ],
            'Client' => [
                'object_name' => 'Client',
                'fields' => []
            ]
        ];

        $post_data = array_filter(array_merge($criteria, ["per_page" => "1000", "page" => &$page]));

        while ($objects = self::request(Curl::POST, "$category/list", $post_data)) {

            // Bail out when out of data
            if (empty($objects->data)) {
                return $result;
            }

            if (!$partial) {

                // Simplbooks does partial match search but we need exact match

                foreach ($objects->data as $object) {

                    foreach ($criteria as $search_field => $value) {

                        $search_field = !empty($lookup[$category]['fields'][$search_field]) ?
                            $lookup[$category]['fields'][$search_field] :
                            $search_field;

                        if ($object->{$lookup[$category]['object_name']}->$search_field == $value) {


                            // Add data to result array
                            $result = array_merge($result, $objects->data);
                        }

                    }

                }


            } else {
                // Add data to result array
                $result = array_merge($result, $objects->data);
            }


            // Get next page
            $page++;
        }

        return $result;
    }

    static function getClient($filter)
    {

        $clients = self::getClients($filter);


        // Simplbooks does partial match search but we need exact match

        foreach ($clients as $client) {

            foreach ($filter as $search_field => $value) {

                if ($client->Client->$search_field == $value) {

                    return $client->Client;

                }

            }

        }

        return false;

    }
}
