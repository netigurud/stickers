<?php

namespace App;


class SQL
{
    static function stringify($array)
    {
        // Escape apostrophies in every member of array with backslashes
        $array = array_map('addslashes', $array);

        // Stringify array
        return "'" . implode("','", $array) . "'";
    }

}