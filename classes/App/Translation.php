<?php

namespace App;


class Translation
{
    public static function delete($translation_ids)
    {
        $translation_ids = SQL::stringify($translation_ids);

        q("delete from translations where translation_id in ($translation_ids)");
    }
}