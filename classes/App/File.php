<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 13.02.2018
 * Time: 12:30
 */

namespace App;


class File
{

    const SHORTER_SIDE = 0;
    const LONGER_SIDE = 0;
    const WIDTH = 1;

    public static function get($file_id)
    {
        return get_first("SELECT * FROM files WHERE file_id = $file_id");
    }

    static function getUserFiles($user_id, $get_accepted_files = false, $include_session = false)
    {
        $result = [];
        $where = $get_accepted_files ? ' AND order_status_id IS NOT NULL' : '';

        $files = get_all("SELECT DISTINCT file_name, file_id, file_preview
                            FROM orders
                              LEFT JOIN order_statuses USING (order_status_id)
                              LEFT JOIN files ON (order_file_id = file_id)
                            WHERE file_uploaded_by = $user_id $where");

        // To eliminate duplicates, convert array from anonymous indices to ID-based indices
        foreach ($files as $file) {
            $result[$file['file_id']] = $file;
            $result[$file['file_id']]['selected'] = false;
        }

        // Include files uploaded in current session (to still see them after refreshing checkout_index page)
        if ($include_session) {
            $session_files = get_all("SELECT * FROM files WHERE file_session_id = '" . session_id() . "'");
            if (!empty($session_files)) foreach ($session_files as $session_file) {
                $result[$session_file['file_id']] = $session_file;
            }
        }

        return $result;
    }


    /**
     * @param $file_id
     * @throws \Exception
     */
    static function delete($file_id)
    {

        // !SQLi
        if(!is_numeric($file_id)){
            throw new \Exception('Invalid file_id');
        }

        // Delete the current file from our uploads folder
        unlink(dirname(__DIR__, 2) . "/.uploads/$file_id");

        // Delete the file from our database
        q("DELETE FROM files WHERE file_id = $file_id");
    }

    public static function createFileToTmp($file_name, $file_content)
    {
        $file_name = dirname(__DIR__, 2) . "/.tmps/$file_name";

        // Put the PDF content into the file
        file_put_contents($file_name, $file_content);

        return true;
    }

    public static function generate_preview($file_id, $delegate = '')
    {

        $magick = escapeshellarg(IMAGEMAGICK_COMMAND);
        $file_id = (int)$file_id;
        $in_file = realpath(dirname(__DIR__, 2) . '/.uploads/' . $file_id);
        $out_file = self::generate_temporary_file_name('jpg');

        // Verify that input file is a real file
        if (!file_exists($in_file)) {
            error_out("File $in_file does not exist");
        }

        // Generate thumbnail
        $source = escapeshellarg("{$in_file}[0]");
        $destination = escapeshellarg($out_file);

        $preview_command_output = self::generate_image("$magick  
                $delegate$source 
                -strip 
                -resize 700x700 
                -flatten
                -quality 60% 
                $destination");

        if ($preview_command_output === true) {
            // Copy preview to db
            $file_contents = file_get_contents($out_file);
            $image_dimensions = getimagesizefromstring($file_contents);
            $thumb_dimensions = self::scaleImageDimensions($image_dimensions, 20, self::SHORTER_SIDE);
            $preview_dimensions = self::scaleImageDimensions($image_dimensions, 200, self::WIDTH);

            update('files', [
                'file_preview' => base64_encode($file_contents),
                'file_thumb_width' => $thumb_dimensions[0],
                'file_thumb_height' => $thumb_dimensions[1],
                'file_preview_width' => $preview_dimensions[0],
                'file_preview_height' => $preview_dimensions[1]
            ], "file_id = $file_id");

            return ['code' => 200, 'data' => File::get($file_id)];
        } else {
            ob_clean();
            header($_SERVER["SERVER_PROTOCOL"] . " 500 Something went really wrong", true, 500);
            echo $preview_command_output;
            exit();
        }
    }

    private static function generate_temporary_file_name($suffix)
    {
        return sys_get_temp_dir() . DIRECTORY_SEPARATOR . "php_temp_" . mt_rand() . "." . $suffix;
    }

    static function generate_image($command)
    {
        $result = 0;
        $output = [];
        $command = preg_replace('/\s+/', ' ', $command);

        // Generate preview
        if (PHP_OS == 'WINNT') {
            exec($command . " 2>&1", $output, $result);
        } else {
            exec($command . " 2>&1 >&-", $output, $result);
        }

        // Catch errors
        if (!empty($output) || $result > 1) {
            // Debug output
            return json_encode($output, JSON_PRETTY_PRINT);
        }
        return true;
    }

    private static function scaleImageDimensions($image_dimensions, $desired_side_length, $mode = self::SHORTER_SIDE)
    {
        $scale_factor = 1;

        // Calculate factor depending on mode
        switch ($mode) {
            case self::SHORTER_SIDE:
                $scale_factor = $image_dimensions[0] / $desired_side_length < $image_dimensions[1] / $desired_side_length
                    ? $image_dimensions[1] / $desired_side_length
                    : $image_dimensions[0] / $desired_side_length;
                break;

            case self::WIDTH:
                $scale_factor = $image_dimensions[0] / $desired_side_length;
                break;

        }

        // Reduce dimensions
        $image_dimensions[0] = $image_dimensions[0] / $scale_factor;
        $image_dimensions[1] = $image_dimensions[1] / $scale_factor;

        return $image_dimensions;
    }

    public static function generateFileInSysTempDir($file_name)
    {
        return sys_get_temp_dir() . '/' . PROJECT_NAME . "_$file_name";
    }

    /**
     *
     * Delete a directory RECURSIVELY
     * @param string $dir - directory path
     * @link http://php.net/manual/en/private function.rmdir.php
     */
    public static function rrmdir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") {
                        rmdir($dir . "/" . $object);
                    } else {
                        unlink($dir . "/" . $object);
                    }
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    public static function getTmpPath()
    {
        return dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . '.tmps' . DIRECTORY_SEPARATOR;
    }

    public static function generateName($order_id, $file_type = 'invoice', $file_ext = 'pdf')
    {
        $order = 'order';
        $project_name = PROJECT_NAME;

        if (is_array($order_id)) {

            // Pluralize word 'order'
            if (count($order_id) > 1) {
                $order = $order . 's';
            }

            // Sort IDs
            sort($order_id);

            // Stringify IDs
            $order_id = implode(', ', $order_id);
        }

        return __("$project_name $file_type for $order") . ' ' . "$order_id.$file_ext";
    }

    static function generateImageName($order_id, $file_name)
    {
        $ext = pathinfo($file_name, PATHINFO_EXTENSION);
        return File::generateName($order_id, 'image', $ext);
    }

    public static function getUploadsDir()
    {
        return dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . ".uploads" . DIRECTORY_SEPARATOR;
    }

    public static function associateUploadedSessionFilesWithLoggedInUser()
    {
        update('files', ['file_uploaded_by' => $_SESSION['user_id']],
            "file_session_id = '" . session_id() . "' AND file_uploaded_by IS NULL");
    }

    /**
     * @param $file_name
     * @return bool
     */
    public static function isVideo($file_name): bool
    {
        return in_array(pathinfo($file_name, PATHINFO_EXTENSION), ['mp4']);
    }

    /**
     * @param $file_name
     * @return string
     */
    public static function getDelegate($file_name): string
    {
        return File::isVideo($file_name) ? 'mpeg:' : '';
    }

    public static function getVideos()
    {
        return  get_all("SELECT file_id, file_name, file_preview FROM files WHERE file_is_video = 1");
    }

}