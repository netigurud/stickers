<?php
/**
 * Created by PhpStorm.
 * User: henno
 * Date: 13/05/2018
 * Time: 22:45
 */

namespace App;


class Session
{

    static function login($user_id){
        $_SESSION['user_id'] = $user_id;
        File::associateUploadedSessionFilesWithLoggedInUser();
        User::updateLastVisitTime($user_id);
    }

    public static function logout()
    {
        // Clear the array
        $_SESSION = array();

        // Delete the session cookie to get a new session ID on the next page load
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(
                session_name(),
                '',
                time() - 42000,
                $params["path"],
                $params["domain"],
                $params["secure"],
                $params["httponly"]);
        }

        // Delete session file
        session_destroy();
    }
}