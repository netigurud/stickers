<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 22.01.2018
 * Time: 13:13
 */

namespace App;


class Product
{

    static function get($product_id)
    {
        return get_first("SELECT * FROM products WHERE product_id = $product_id AND product_deleted = 0");
    }

    static function getAll($product_type = false)
    {
        $product_type = $product_type ? " AND product_type_id = $product_type" : '';

        return get_all("SELECT * FROM products WHERE product_deleted = 0 $product_type");
    }

    public static function getProductTypes()
    {
        return get_all("SELECT * FROM product_types");
    }

    public static function getProductType($product_type_id)
    {
        return get_first("SELECT * FROM product_types WHERE product_type_id = $product_type_id");
    }

    public static function getPrice($m2, $quantity, $product_type_id)
    {
        return get_one("SELECT price 
                              FROM prices 
                             WHERE quantity = $quantity 
                              AND m2 >= $m2 
                              AND product_type_id = $product_type_id
                             ORDER BY m2 LIMIT 1");
    }

}