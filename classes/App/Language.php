<?php

namespace App;


class Language
{
    static function get_prefered_language($supported_languages, $http_accept_language, $default_language)
    {
        $default_language = self::get_default($supported_languages);
        $supported_languages = array_flip($supported_languages);
        $langs = array();
        preg_match_all('~([\w-]+)(?:[^,\d]+([\d.]+))?~', strtolower($http_accept_language), $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
            list($a, $b) = explode('-', $match[1]) + array('', '');
            $value = isset($match[2]) ? (float)$match[2] : 1.0;
            if (isset($supported_languages[$match[1]])) {
                $langs[$match[1]] = $value;
                continue;
            }
            if (isset($supported_languages[$a])) {
                $langs[$a] = $value - 0.1;
            }
        }
        if ($langs) {
            arsort($langs);
            return key($langs); // We don't need the whole array of choices since we have a match
        } else {
            return $default_language;
        }
    }

    static function get_default($supported_languages)
    {
        // Set default language (defaults to 'en', if no supported languages are given)
        return isset($supported_languages[0]) ? $supported_languages[0] : 'en';
    }
}