<?php namespace App;

use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;

/**
 * Created by PhpStorm.
 * User: henno
 * Date: 29/10/16
 * Time: 22:24
 */
class User
{

    static function get($user_id = null)
    {
        $user_id = $user_id ? $user_id : $_SESSION['user_id'];
        return get_first("SELECT * FROM users JOIN countries USING (country_id) WHERE user_id = $user_id");
    }

    static function getAll()
    {
        return get_all("SELECT * FROM users JOIN countries USING(country_id)");
    }

    static function getByField($fieldName, $fieldValue)
    {
        return get_all("SELECT * FROM users JOIN countries USING (country_id) WHERE $fieldName = '$fieldValue'");
    }

    static function register(
        $email,
        $full_name,
        $company_name,
        $company_reg_nr,
        $password,
        $state,
        $phone,
        $user_dial_code,
        $country_id,
        $city,
        $street,
        $postal_code,
        $vat
    )
    {
        // Hash the password
        $password = password_hash($password, PASSWORD_DEFAULT);

        // Insert user into database
        $user_id = insert('users', [
                "password" => $password,
                "user_email" => $email,
                "user_full_name" => $full_name,
                "user_company" => $company_name,
                "user_company_reg_nr" => $company_reg_nr,
                "user_phone" => $phone,
                "user_dial_code" => $user_dial_code,
                "user_state" => $state,
                "user_city" => $city,
                "country_id" => $country_id,
                "user_street" => $street,
                "user_postal_code" => $postal_code,
                "user_vat" => $vat
            ]
        );

        // Return new user's ID
        return $user_id;
    }

    static function emailExists($email)
    {
        return get_one("SELECT user_email FROM users WHERE user_email = '$email'");
    }

    public
    static function getGoogleLoginURL(
        $modal = false
    )
    {

        $modal = empty($modal) ? '' : '/modal';

        // Set some variables
        $request_params = array(
            "response_type" => "code",
            "client_id" => GOOGLE_CLIENT_ID,
            "redirect_uri" => BASE_URL . GOOGLE_REDIRECT_URI . $modal,
            "access_type" => "offline",
            "approval_prompt" => "force",
            "scope" => "openid profile email",
        );

        return "https://accounts.google.com/o/oauth2/auth?" . http_build_query($request_params);

    }

    public static function getFBLoginUrl($callback_action_name = 'callback')
    {

        try {
            $fb = new Facebook([
                'app_id' => FACEBOOK_APP_ID,
                'app_secret' => FACEBOOK_SECRET,
                'default_graph_version' => 'v2.2',
            ]);
        } catch (FacebookSDKException $e) {
            error_out($e->getMessage());
            exit();
        }


        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email']; // Optional permissions

        return htmlspecialchars($helper->getLoginUrl(BASE_URL . 'login_facebook/' . $callback_action_name,
            $permissions));
    }

    public static function generateRandomPassword()
    {
        $password = [];

        // Generate random string
        $password['password'] = generateRandomString();

        // Hash the string
        $password['hash'] = password_hash($password['password'], PASSWORD_DEFAULT);

        return $password;
    }


    public static function isLoggedIn()
    {
        return !empty($_SESSION['user_id']);
    }

    public static function isPrivileged($user = null)
    {
        // Unauthenticated user is never privileged
        if (empty($_SESSION['user_id'])) {
            return false;
        }

        $user = empty($user) ? (object)get_first("SELECT * FROM users WHERE user_id = $_SESSION[user_id]") : $user;

        if (!$user) {
            return false;
        }

        return $user->is_admin || $user->is_designer || $user->is_printing_house;
    }

    public static function ownsOrder($order)
    {
        // Unauthenticated users own nothing
        if (empty($_SESSION['user_id'])) {
            return false;
        }

        return $order['order_made_by'] == $_SESSION['user_id'];
    }

    public static function isUnprivileged($user = null)
    {
        return !self::isPrivileged($user);
    }

    public static function hasNotMade($order)
    {
        return !self::ownsOrder($order);
    }

    public static function normaliseName($user_full_name)
    {
        // Sanitize consecutive whitespace to a single space character
        $user_full_name = preg_replace('/\s+/', ' ', $user_full_name);

        // Replace UTF8 characters larger than 3 bytes for Simplbooks compatibility
        $user_full_name = preg_replace('%(?:
          \xF0[\x90-\xBF][\x80-\xBF]{2}      # planes 1-3
        | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
        | \xF4[\x80-\x8F][\x80-\xBF]{2}      # plane 16
        )%xs', '�', $user_full_name);

        // Prevent XSS
        $user_full_name = preg_replace('#<(/?script)#', '&lt;$1', $user_full_name);

        return $user_full_name;
    }

    public static function hasNotUploaded($file)
    {
        return $_SESSION['user_id'] != $file['file_uploaded_by'];
    }


    static function getDeleted()
    {
        return get_all("
            SELECT * FROM users WHERE deleted = 1 
            AND user_deletion_time < DATE_ADD(NOW(), INTERVAL -30 DAY)");
    }

    static function delete($user_id)
    {
        q("DELETE FROM users WHERE user_id = $user_id");
    }

    static function deleteFiles($user_id)
    {
        q("DELETE FROM files WHERE file_uploaded_by = $user_id");
    }

    static function deleteOrders($user_id)
    {
        // Order rows will be cascade deleted
        q("DELETE FROM orders WHERE order_made_by = $user_id");
    }

    public static function markForDeletion($user_id)
    {
        q("UPDATE users SET deleted = 1, user_deletion_time = NOW() WHERE user_id = $user_id");
    }

    public static function unmarkForDeletion($user_id)
    {
        q("UPDATE users SET deleted = 0, user_deletion_time = null WHERE user_id = $user_id");
    }

    public static function updateLastVisitTime($user_id)
    {
        q("UPDATE users SET user_last_visit = NOW() WHERE user_id = $user_id");
    }

    public static function getDesigners()
    {
        $a = get_all("
            SELECT
              users.user_id,
              user_full_name,
              COUNT(order_id)                                AS order_count,
              DATE_FORMAT(order_design_finished_at, '%Y-%m') AS period,
              if(settled_periods.period IS NULL, 0, 1)       AS settled
            FROM users
              JOIN orders ON orders.order_taken_by_designer = user_id
              LEFT JOIN settled_periods ON settled_periods.period = DATE_FORMAT(order_design_finished_at, '%Y-%m') 
              AND settled_periods.user_id = users.user_id
            WHERE DATE_FORMAT(order_design_finished_at, '%Y-%m') IS NOT NULL
            GROUP BY users.user_id, YEAR(order_design_finished_at), MONTH(order_design_finished_at)");

        $result = [];
        foreach ($a as $item) {
            $result[$item['user_id']]['user_full_name'] = $item['user_full_name'];
            $result[$item['user_id']]['periods'][$item['period']] = ['count' => $item['order_count'], 'settled' => (int)$item['settled']];
        }
        return $result;


    }

    /**
     * Validates that the name composes of at least two parts, separated by space
     * @param $full_name User provided user full name
     * @return array
     */
    public static function validateName($full_name): array
    {

        // Remove leading and trailind white-space
        $user_full_name = trim($full_name);

        // Remove excess spaces and dangerous characters
        $user_full_name = User::normaliseName($user_full_name);

        // Validate that the user has entered at least 2 words as the name
        $userNameArray = explode(" ", $user_full_name);

        if (count($userNameArray) < 2) {
            stop(400, __('Please enter your first and last name'));
        }

        $user_last_name = array_pop($userNameArray);
        $user_first_name = implode(' ', $userNameArray);
        if (!$user_first_name) {
            stop(400, __('There was a problem understanding your first name'));
        }
        if (!$user_last_name) {
            stop(400, __('There was a problem understanding your last name'));
        }

        return [$user_first_name, $user_last_name];
    }

}