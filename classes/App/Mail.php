<?php namespace App;

use PHPMailer\PHPMailer\PHPMailer;

class Mail
{
    public static function get($email_id)
    {
        return get_first("SELECT * FROM emails WHERE email_id = $email_id");
    }

    public static function getAll()
    {
        return get_all("SELECT * FROM emails");
    }

    static function send($to, $subject, $body, $attachments = null, $stringAttachments = null, $cc_to = null, $ReplyToEmail = null, $ReplyToName = '')
    {

        //SMTP needs accurate times, and the PHP time zone MUST be set
        //This should be done in your php.ini, but this is how to do it if you don't have access to that
        date_default_timezone_set('Etc/UTC');

        //Create a new PHPMailer instance
        $mail = new PHPMailer(true);

        //Tell PHPMailer to use SMTP

        if (SMTP_USE_SENDMAIL) {
            $mail->isSendmail();
        } else {
            $mail->isSMTP();
        }

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->isHTML(true);

        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug = SMTP_DEBUG;

        //Ask for HTML-friendly debug output
        if (SMTP_DEBUG) {
            $log_file = fopen(__DIR__ . "/../../.tmps/smtp_" . date('Y-m-d__H.i.s') . ".txt", "w") or die("Unable to write to file!");
            $mail->Debugoutput = function ($str, $level) use ($log_file) {
                // Write to file
                fwrite($log_file, $str);

            };
        }


        //Set the hostname of the mail server
        $mail->Host = SMTP_HOST;

        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $mail->CharSet = 'UTF-8';
        $mail->Port = SMTP_PORT;

        if (SMTP_AUTH) {
            $mail->SMTPSecure = SMTP_ENCRYPTION;

            //Whether to use SMTP authentication

            $mail->SMTPAuth = SMTP_AUTH;

            //Username to use for SMTP authentication - use full email address for gmail
            $mail->Username = SMTP_AUTH_USERNAME;


            //Password to use for SMTP authentication
            $mail->Password = SMTP_AUTH_PASSWORD;
        }

        //Set who the message is to be sent from
        try {

            $data = User::getByField('user_email', $to);
            if (!$data) {
                $name = $to;
            } else {
                $name = $data[0]['user_full_name'];
            }

            if (!empty($ReplyToEmail)) {
                $mail->AddReplyTo($ReplyToEmail, $ReplyToName);
            }

            $mail->setFrom(SMTP_FROM);


            //Set who the message is to be sent to
            $mail->addAddress($to);

            //Set the subject line
            $mail->Subject = $subject;


            // Get email template
            $email_template = file_get_contents(dirname(__DIR__, 2) . '/templates/email_template.php');

            // Replace email title and body
            $email_template = str_replace('%NAME%', $name, $email_template);
            $email_template = str_replace('%SUBJECT%', $subject, $email_template);
            $email_template = str_replace('%BODY%', $body, $email_template);
            $email_template = str_replace('%BASE_URL%', BASE_URL, $email_template);

            // Workaround for Outlook.com dkim fail: body hash did not verify error
            // @link https://github.com/PHPMailer/PHPMailer/issues/892#issuecomment-265116565
            $mail->Encoding = 'quoted-printable';
            $lines = explode("\n", $email_template);
            $body = '';
            foreach ($lines as $line) {
                $body .= trim($line) . "\n";
            }

            // Set HTML body
            $mail->MsgHTML($body);

            // Set plain text body
            $body = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $body);
            $body_with_brs_converted_to_newlines = preg_replace('#<br */*>#', "\n", $body);
            $body_converted_to_plain_text_with_links_preserved = str_replace("|a", "<a",
                strip_tags(str_replace("<a", "|a", $body_with_brs_converted_to_newlines)));
            $mail->AltBody = $body_converted_to_plain_text_with_links_preserved;


            // DKIM
            if (!empty(SMTP_DKIM_ENABLED)) {
                $mail->DKIM_domain = SMTP_DKIM_DOMAIN;
                $mail->DKIM_private = SMTP_DKIM_PRIVATE_KEY;
                $mail->DKIM_selector = SMTP_DKIM_SELECTOR;
                $mail->DKIM_passphrase = SMTP_DKIM_PASSPHRASE;
                $mail->DKIM_identity = SMTP_DKIM_IDENTITY;
            }


            // Set Carbon Copy recipient
            if ($cc_to) {
                $mail->AddCC($cc_to);
            }

            // Add attachments, if given
            if (!empty($attachments)) {
                foreach ($attachments as $attachment) {
                    $mail->addAttachment($attachment);
                }
            }

            // Add string attachments, if given
            if (!empty($stringAttachments)) {
                foreach ($stringAttachments as $key => $val) {
                    $mail->addStringAttachment($val, $key);
                }
            }

            // Send the message
            $mail->send();

        } catch (\Exception $e) {
            Request::isAjax() ? stop(500, $e->getMessage()) : error_out($e->getMessage());
        }

    }


    /**
     * Returns true if the domain of the email address exists and has an MX record set up
     * @param $email
     * @return bool
     */
    static function hasValidDomain($email)
    {
        $regex = "^[^@]+@([a-z0-9]+([._-][a-z0-9]+))+$";

        if (preg_match("/$regex/", $email, $email_parts) == 1) {

            $domain = $email_parts[1];

            if (checkdnsrr($domain, "MX")) {
                return true;
            }

        }

        return false;
    }

    /**
     * Returns true if input validates to correct email address and domain is set up to accept mail
     * @param $email
     */
    static function validateAddress($email)
    {

        if (substr($email, -12) != '@example.com') {

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                stop(400, __('There is a problem with your email address') . ': ' . $email . ' ' . __('is not a valid email address') . '. ' . __('Please correct your email address and try again') . '.');
            }

            $user_email_domain = explode("@", $email, 2)[1];

            if (!self::hasValidDomain($email)) {
                stop(400, __('There is a problem with your email address') . ': ' . $user_email_domain . ' ' . __('is not a valid domain name') . '. ' . __('Please correct your email address and try again') . '.');
            }
        }
    }

    public static function exists($email)
    {
        return get_all("SELECT * FROM users WHERE user_email = '$email'");
    }

    /**
     * @param $user_email
     * @param $order_id
     * @param $invoice
     * @throws \Mpdf\MpdfException
     */
    public static function sendInvoice($user_email, $order_id, $invoice)
    {
        Mail::send(
            $user_email,
            __('Invoice from ' . PROJECT_NAME, 1),
            __("The invoice for your order is in the attachments.", 1) . "<br><br>" .
            __("Thank you for choosing", 1) . ' ' . PROJECT_NAME . '!',
            null,
            [
                File::generateName($order_id) => Invoice::generateContent($invoice->inserted_id)
            ],
            ADMIN_EMAIL
        );
    }

    /**
     * @param $user_email
     * @param $order_id
     * @param $invoice
     * @throws \Mpdf\MpdfException
     */
    public static function notify($user_email, $order_id, $message)
    {
        Mail::send(
            $user_email,
            __('Order') . ' ' . $order_id,
            $message
        );
    }

    /**
     * @param $user
     * @param $order_id
     * @param $invoice
     * @throws \Mpdf\MpdfException
     */
    public static function notifyPrintingHouse($order_id)
    {
        $printing_house_users = User::getByField('is_printing_house', 1);
        foreach ($printing_house_users as $user) {
            Mail::notify(
                $user['user_email'],
                $order_id,
                __('Please print order') . " $order_id. <br><br>
                <a href='" . BASE_URL . "orders/'>ORDERS</a>"
            );
        };
    }

}