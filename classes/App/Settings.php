<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 06.04.2018
 * Time: 10:23
 */

namespace App;


class Settings
{
    private static $settings;

    static function get($setting_short_name)
    {
        self::prepareCache();

        return self::$settings[$setting_short_name];
    }

    static function getValues()
    {
        $settings = [];
        self::prepareCache();

        foreach (self::$settings as $setting) {
            $settings[$setting['setting_short_name']] = $setting['setting_value'];
        }

        return $settings;
    }

    static function prepareCache()
    {
        if (!isset(self::$settings)) {
            $settings = get_all("SELECT * FROM settings");

            foreach ($settings as $setting) {
                self::$settings[$setting['setting_short_name']] = $setting;
            }
        }
    }
}
