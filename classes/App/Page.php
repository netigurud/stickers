<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 14.03.2018
 * Time: 10:16
 */

namespace App;


class Page
{
    static function get($page_id)
    {
        // Check if we've given the page id or the slug
        $where = is_numeric($page_id) ? "page_id = $page_id" : "page_slug = '$page_id'";
        return get_first("SELECT * FROM pages WHERE $where");
    }

    static function getAll()
    {
        return get_all("SELECT * FROM pages");
    }
}