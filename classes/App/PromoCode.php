<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 24.01.2018
 * Time: 13:13
 */

namespace App;


class PromoCode
{

    static function get($promo_code)
    {
        return get_first("SELECT *
                               FROM promo_codes
                               WHERE promo_code = '$promo_code' AND promo_code_valid_from < NOW() AND promo_code_valid_to > NOW()");
    }
}