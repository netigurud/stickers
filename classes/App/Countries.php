<?php
/**
 * Created by PhpStorm.
 * User: henno
 * Date: 30/04/2018
 * Time: 11:12
 */

namespace App;


class Countries
{

    public static function get($criteria = null)
    {
        $function = 'get_all';
        $where = '';

        if (is_array($criteria)) {
            $where = 'WHERE ' . implode('AND', escape($criteria));
            $function = 'get_all';
        }
        if (is_numeric($criteria)) {
            $where = "WHERE country_id = $criteria";
            $function = 'get_first';
        }

        return $function("
            SELECT * 
            FROM countries JOIN continents USING (continent_id) $where ORDER BY country_name");
    }

}