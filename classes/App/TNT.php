<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 03.04.2018
 * Time: 11:46
 */

namespace App;


class TNT
{

    static function fillXML($order, $receiver)
    {
        $order_general = $order;
        $order = $order['order'];
        $order_id = $order['order_id'];
        $order_rows = $order_general['order_rows'];
        $total_items = count($order_rows);

        // Format date for TNT
        $ship_date = DateTime::formatDate($order['ship_date'], 'd/m/Y');

        $receiver_vat = $order['order_vat'];
        $receiver_city = $order['order_city'];
        $receiver_phone = $order['order_phone'];
        $receiver_email = $receiver['user_email'];
        $receiver_address = $order['order_street'];
        $receiver_name = $order['order_full_name'];
        $receiver_state = $order['order_state'];
        $receiver_postal_code = $order['order_postal_code'];
        $receiver_company_or_name = empty($order['order_company']) ? $order['order_full_name'] : $order['order_company'];
        $receiver_country_short_name = $order['country_code'];
        $receiver_dial_code = "+" . $order['country_dial_code'];
        $settings = Settings::getValues();
        $conref = "ORDER #$order_id";
        $package_total_volume = $settings['PACKAGE_HEIGHT'] * $settings['PACKAGE_WIDTH'] * $settings['PACKAGE_LENGTH'];
        $xml = "<?xml version='1.0' encoding='UTF-8'?>
                    <ESHIPPER>
                        <LOGIN>
                            <COMPANY>" . TNT_USERNAME . "</COMPANY>
                            <PASSWORD>" . TNT_PASSWORD . "</PASSWORD>
                            <APPID>EC</APPID>
                            <APPVERSION>3.0</APPVERSION>
                        </LOGIN>
                        <CONSIGNMENTBATCH>
                            <SENDER>
                                <COMPANYNAME><![CDATA[" . SITE_OWNER_COMPANY . "]]></COMPANYNAME>
                                <STREETADDRESS1><![CDATA[" . SITE_OWNER_ADDRESS_LINE . "]]></STREETADDRESS1>
                                
                                <!-- Element CITY is a key element.  To ship with TNT, we must know the exact
                                     location that you are shipping to/from, in TNT terms. The full town name must match
                                     the spelling TNT uses for validation.  Please work with your TNT representative to
                                     ensure your town names match TNT?s in case of errors. -->
                                <CITY><![CDATA[" . SITE_OWNER_CITY . "]]></CITY>
                                <POSTCODE><![CDATA[" . SITE_OWNER_POSTAL_CODE . "]]></POSTCODE>
                                <COUNTRY><![CDATA[" . SITE_OWNER_COUNTRY_CODE . "]]></COUNTRY>
                                <ACCOUNT><![CDATA[" . TNT_ACCOUNT_NUMBER . "]]></ACCOUNT>
                                <CONTACTNAME><![CDATA[" . SITE_OWNER_FULL_NAME . "]]></CONTACTNAME>
                                <CONTACTDIALCODE><![CDATA[" . ESTONIA_DIAL_CODE . "]]></CONTACTDIALCODE>
                                <CONTACTTELEPHONE><![CDATA[" . SITE_OWNER_PHONE . "]]></CONTACTTELEPHONE>
                                <CONTACTEMAIL><![CDATA[" . SITE_OWNER_EMAIL . "]]></CONTACTEMAIL>
                                <COLLECTION>
                                    <COLLECTIONADDRESS>
                                        <COMPANYNAME><![CDATA[" . SITE_OWNER_COMPANY . "]]></COMPANYNAME>
                                        <STREETADDRESS1><![CDATA[" . SITE_OWNER_ADDRESS_LINE . "]]></STREETADDRESS1>
                                        
                                        <!-- Element CITY is a key element. To ship with TNT, we must know the exact
                                             location that you are shipping to/from, in TNT terms. The full town name 
                                             must match the spelling TNT uses for validation. Please work with your TNT
                                             representative to ensure your town names match TNT's in case of errors. -->
                                        <CITY><![CDATA[" . SITE_OWNER_CITY . "]]></CITY>
                                        <POSTCODE><![CDATA[" . SITE_OWNER_POSTAL_CODE . "]]></POSTCODE>
                                        <COUNTRY><![CDATA[" . SITE_OWNER_COUNTRY_CODE . "]]></COUNTRY>
                                        <CONTACTNAME><![CDATA[" . SITE_OWNER_FULL_NAME . "]]></CONTACTNAME>
                                        <CONTACTDIALCODE><![CDATA[" . ESTONIA_DIAL_CODE . "]]></CONTACTDIALCODE>
                                        <CONTACTTELEPHONE><![CDATA[" . SITE_OWNER_PHONE . "]]></CONTACTTELEPHONE>
                                    </COLLECTIONADDRESS>
                                    
                                    <!-- Element SHIPDATE is the date which you wish the parcels to be collected, and 
                                         it must be supplied in the format DD/MM/CCYY. It is a mandatory field. -->
                                    <SHIPDATE><![CDATA[$ship_date]]></SHIPDATE>
                                   
                                    <!-- Collections is raised for every consignment, with exception of customers with
                                         scheduled collection. The earliest time the goods may be collected 
                                         (Goods ready time) should be supplied in the PREFCOLLECTTIME / FROM element, 
                                         in the format HH:MM. The latest time the goods can be collected must be 
                                         supplied in the PREFCOLLECTTIME / TO element, in the format HH:MM. If your
                                         company has a lunchtime, them please supply the morning opening times in the
                                         PREFCOLLECTTIME element and the afternoon opening times in the ALTCOLLECTTIME.
                                         These details are validated against the time the TNT truck will be within your 
                                         area, to ensure the TNT truck is within your area at a time where you are open.
                                    -->
                                    <PREFCOLLECTTIME>
                                        <FROM><![CDATA[$order[collection_time_from]]]></FROM>
                                        <TO><![CDATA[$order[collection_time_to]]]></TO>
                                    </PREFCOLLECTTIME>
                                    
                                    <!-- Please supply any special instruction which need to be passed to the collection
                                         driver in the <COLLINSTRUCTIONS> element. -->
                                    <COLLINSTRUCTIONS>
                                        <![CDATA[Please supply any special instruction which need to be passed to the collection driver]]>
                                    </COLLINSTRUCTIONS>
                                </COLLECTION>
                            </SENDER>
                            
                            <!-- It is recommended that no more than 3 CONSIGNMENT elements are supplied, unless you have set a large timeout period. -->
                            <CONSIGNMENT>
                                <!-- A CONSIGNMENT element contains a key attribute that identifies it uniquely within 
                                     the request. The response will associate Shipping data and validation errors with 
                                     their ACTIVITY through this key. The value of this key must be of type string and 
                                     is supplied in CONREF element. -->
                                <CONREF>$conref</CONREF>
                                <DETAILS>
                                    <RECEIVER>
                                        <COMPANYNAME><![CDATA[$receiver_company_or_name]]></COMPANYNAME>
                                        <STREETADDRESS1><![CDATA[$receiver_address]]></STREETADDRESS1>
                                        
                                        <!-- Element CITY is a key element. To ship with TNT, we must know the exact
                                             location that you are shipping to/from, in TNT terms. The full town name 
                                             must match the spelling TNT uses for validation. Please work with your TNT
                                             representative to ensure your town names match TNT's in case of errors. -->
                                        <CITY><![CDATA[$receiver_city]]></CITY>
                                        ".(empty($receiver_state)?'':"<PROVINCE><![CDATA[$receiver_state]]></PROVINCE>")."
                                        <POSTCODE><![CDATA[$receiver_postal_code]]></POSTCODE>
                                        <COUNTRY><![CDATA[$receiver_country_short_name]]></COUNTRY>
                                        <VAT><![CDATA[$receiver_vat]]></VAT>
                                        <CONTACTNAME><![CDATA[$receiver_name]]></CONTACTNAME>
                                        <CONTACTDIALCODE><![CDATA[$receiver_dial_code]]></CONTACTDIALCODE>
                                        <CONTACTTELEPHONE><![CDATA[$receiver_phone]]></CONTACTTELEPHONE>
                                        <CONTACTEMAIL><![CDATA[$receiver_email]]></CONTACTEMAIL>
                                    </RECEIVER>
                                    <DELIVERY>
                                        <COMPANYNAME><![CDATA[$receiver_company_or_name]]></COMPANYNAME>
                                        <STREETADDRESS1><![CDATA[$receiver_address]]></STREETADDRESS1>
                                        
                                        <!-- Element CITY is a key element. To ship with TNT, we must know the exact
                                             location that you are shipping to/from, in TNT terms. The full town name 
                                             must match the spelling TNT uses for validation. Please work with your TNT
                                             representative to ensure your town names match TNT's in case of errors. -->
                                        <CITY><![CDATA[$receiver_city]]></CITY>
                                        ".(empty($receiver_state)?'':"<PROVINCE><![CDATA[$receiver_state]]></PROVINCE>")."
                                        <POSTCODE><![CDATA[$receiver_postal_code]]></POSTCODE>
                                        <COUNTRY><![CDATA[$receiver_country_short_name]]></COUNTRY>
                                        <CONTACTNAME><![CDATA[$receiver_name]]></CONTACTNAME>
                                        <CONTACTDIALCODE><![CDATA[$receiver_dial_code]]></CONTACTDIALCODE>
                                        <CONTACTTELEPHONE><![CDATA[$receiver_phone]]></CONTACTTELEPHONE>
                                        <CONTACTEMAIL><![CDATA[$receiver_email]]></CONTACTEMAIL>
                                    </DELIVERY>
                                    
                                    <!-- Element CONNUMBER is optional, if not provided TNT will allocate the number.
                                         This element contains your consignment number, if your application generates
                                         its own TNT consignment number. 
                                    <CONNUMBER/>
                                    -->
                                    
                                    <!-- Element CUSTOMERREF is optional This element contains your consignment reference.
                                         This will be printed on the shipping documentations and can be used to track your consignment. -->
                                    <CUSTOMERREF><![CDATA[Order number $order_id]]></CUSTOMERREF>
                                    
                                    <!-- Element CONTYPE identifies whether you are shipping a 'D' Document 
                                         (paper/manuals/reports) or 'N' Non Document (packages). Document services are 
                                         not offered for a number of TNT domestic shipments. For domestic shipment
                                         within these countries please use a CONTYPE of 'N' for Non-document. -->
                                    <CONTYPE><![CDATA[N]]></CONTYPE>
                                    
                                    <!-- Element PAYMENTIND is optional. A PAYMENTIND type of 'S' represents a sender
                                         pays shipments, 'R' represents a receiver. If this element is not supplied, 
                                         then the payment type is defaulted to sender. Please see user guide for 
                                         further details on receiver pays. 
                                    -->
                                    <PAYMENTIND><![CDATA[S]]></PAYMENTIND>
                                    
                                    <!-- Element ITEMS is mandatory. This element will contain the number of items
                                         within the shipment. The ITEMS element should match the total number of items
                                         defined in the packages elements. -->
                                    <!-- TODO:  --> 
                                    <ITEMS><![CDATA[$total_items]]></ITEMS>
                                    
                                    <!-- Elements TOTALWEIGHT and TOTALVOLUME describe the total weight and volume of the consignment
                                         being shipped. The WEIGHT element contains the total weight of the shipment in Kilograms and is
                                         a mandatory element. The VOLUME element contains the total volume of the shipment in cubic metres
                                         and is a mandatory element for Non-documents, CONTYPE = 'N'. These totals should match the sum of the
                                         weights and volume supplies in the packages elements. The totals from the PACKAGE elements will
                                         be used if greater that the consignment supplied totals. -->
                                    <TOTALWEIGHT><![CDATA[$settings[PACKAGE_WEIGHT]]]></TOTALWEIGHT>
                                    <TOTALVOLUME><![CDATA[$package_total_volume]]></TOTALVOLUME>
                                    
                                    <!-- Elements CURRENCY and GOODSVALUE identifies the value of the goods being 
                                         shipped and the associated currency. The currency is represented by the 3 
                                         digit ISO 4217 Alpha - 3 currency codes. The value of goods must be supplied 
                                         if you wish to insure the shipment. -->
                                    <CURRENCY><![CDATA[EUR]]></CURRENCY>
                                    <GOODSVALUE><![CDATA[$order_general[order_total_price]]]></GOODSVALUE>
                                    
                                    <!-- Element DIVISION is optional 
                                    <DIVISION/>
                                    -->
                                    
                                    <!-- The particular service that the shipment is being moved under. The values in
                                         this section will be provided by your TNT representative. -->
                                    <SERVICE><![CDATA[15N]]></SERVICE>
                                    
                                    <!-- Element DESCRIPTION is optional. Please describe the consignment goods being 
                                         shipped. This is not used for custom purposes. For UK domestic dangerous goods 
                                         shipments, please include HZ in first 2 characters of the description. For UK 
                                         domestic shipments, please include the carton Code in first 2 characters of the
                                         description. This must be used to attract the correct rate when invoicing the
                                         consignment for Palletised goods. -->
                                    <DESCRIPTION><![CDATA[Stickers and/or magnets]]></DESCRIPTION>
                                    
                                    <!-- Element DELIVERYINST is optional. Please supply any instructions that must be 
                                         passed to the TNT delivery driver when delivering the consignment.
                                         For UK dangerous domestic shipments, the UN Number must be supplied in the 
                                         first 4 characters of the DELIVERYINST 
                                    -->
                                    <DELIVERYINST>
                                    <![CDATA[Please supply any instructions that must be passed to the TNT delivery driver when delivering the consignment.]]>
                                    </DELIVERYINST>
                                    
                                    <!-- Element UNNUMBER and PACKINGGROUP are optional, please see user guide for 
                                         further information
                                    <UNNUMBER/>
                                    <PACKINGGROUP/>
                                    -->
                                    
                                    <!-- The PACKAGE element provides details on the package line. There must be at 
                                         least one Package per Consignment	when entered in detail form. There may not 
                                         be more than 50 Packages per Consignment when entered in detail form with no 
                                         more than 99 items for a consignment. -->
                                    <PACKAGE>
                                        <ITEMS><![CDATA[$total_items]]></ITEMS>
                                        <DESCRIPTION><![CDATA[Stickers and/or magnets]]></DESCRIPTION>
                                        <LENGTH><![CDATA[$settings[PACKAGE_LENGTH]]]></LENGTH>
                                        <HEIGHT><![CDATA[$settings[PACKAGE_HEIGHT]]]></HEIGHT>
                                        <WIDTH><![CDATA[$settings[PACKAGE_WIDTH]]]></WIDTH>
                                        <WEIGHT><![CDATA[$settings[PACKAGE_WEIGHT]]]></WEIGHT>";

        foreach ($order_rows as $order_row) {
            $xml .= "
                                        <ARTICLE>
                                             <ITEMS><![CDATA[$order_row[quantity]]]></ITEMS>
                                             <DESCRIPTION><![CDATA[$order_row[product_type_name]]]></DESCRIPTION>
                                             <WEIGHT><![CDATA[$settings[PACKAGE_WEIGHT]]]></WEIGHT>
                                             <INVOICEVALUE><![CDATA[$order_row[price]]]></INVOICEVALUE>
                                             <INVOICEDESC><![CDATA[$order_row[product_type_name]]]></INVOICEDESC>
                                             
                                             <!-- For each article you need to capture the Tariff code. A tariff code is
                                                  a product-specific code as documented in the Harmonised System (HS) 
                                                  maintained by the World Customs Organisation (WCO). Tariff codes exist
                                                  for almost every product involved in global commerce.The Harmonized 
                                                  Item Description and Coding System (HS) is an international standard 
                                                  maintained by the World Customs Organization (WCO) that classifies 
                                                  traded products. Items are identified by a 6-digit harmonized number 
                                                  that is recognized by countries that have adopted the harmonized
                                                  system. The Tariff code is supplied in the <HTS> element -->
                                             <HTS><![CDATA[ABC]]></HTS>
                                             
                                             <!-- Element COUNTRY Country of article's origin is optional -->
                                             <COUNTRY><![CDATA[EE]]></COUNTRY>
                                             
                                             <!-- Element EMRN is optional
                                             <EMRN><![CDATA[Export Management]]></EMRN>
                                             -->
                                        </ARTICLE>";
        }

        $xml .= "</PACKAGE>
                                </DETAILS>
                            </CONSIGNMENT>
                        </CONSIGNMENTBATCH>
                        <ACTIVITY>
                            <CREATE>
                                <CONREF><![CDATA[$conref]]></CONREF>
                            </CREATE>
                            <BOOK ShowBookingRef='Y'>
                                <CONREF><![CDATA[$conref]]></CONREF>
                            </BOOK>
                            <SHIP>
                                <CONREF><![CDATA[$conref]]></CONREF>
                            </SHIP>
                            <PRINT>
                                <CONNOTE>
                                    <CONREF><![CDATA[$conref]]></CONREF>
                                </CONNOTE>
                                <LABEL>
                                    <CONREF><![CDATA[$conref]]></CONREF>
                                </LABEL>
                                <MANIFEST>
                                    <CONREF><![CDATA[$conref]]></CONREF>
                                </MANIFEST>
                                <INVOICE>
                                    <CONREF><![CDATA[$conref]]></CONREF>
                                </INVOICE>
                                <EMAILTO><![CDATA[test.name@tnt.com]]></EMAILTO>
                                <EMAILFROM><![CDATA[test.name@tnt.com]]></EMAILFROM>
                            </PRINT>
                        </ACTIVITY>
                    </ESHIPPER>";

        return self::encodeOrDecodeCdata($xml, 'decode');
    }

    /**
     * Adds one day to given date until it's not a weekend anymore.
     */
    static function getNextAvailableCollectionTime()
    {
        $date = date('Y-m-d');
        $collection_time = [
            'from' => '09:00',
            'to' => '16:00'
        ];

        while (DateTime::isWeekend($date)
            || DateTime::isHoliday($date)
            || (DateTime::isToday($date) && time() > (DateTime::shortenedWorkday($date) ? strtotime('12:00') : strtotime('15:00')))) {
            $date = DateTime::addDaysToDate($date, 1);
        }

        $collection_time['date'] = $date;

        if (DateTime::isToday($date) && time() > strtotime('09:00')) {
            $collection_time['from'] = date('H:i');
        }

        // Shorten the collection time to time by 3 hours
        if (DateTime::shortenedWorkday($date)) {
            $collection_time['to'] = '13:00';
        }

        return $collection_time;
    }

    /**
     * Verify that given shipping time is valid
     * @param $ship_date
     * @param $from
     * @param $to
     * @return bool
     */
    static function checkCollectionTimes($ship_date, $from, $to)
    {
        if (strtotime($from) < strtotime('09:00')) {
            $collection_times = ['from' => '09:00'];
            stop(400, "Collection time must be between 09:00 and 16:00");
        }

        if (strtotime($to) > strtotime('16:00')) {
            $collection_times = ['to' => '16:00'];
            stop(400, "Collection time must be between 09:00 and 16:00");
        }

        // Shorten the collection time to time by 3 hours
        if (DateTime::shortenedWorkday($ship_date)) {
            $collection_times['to'] = '13:00';
            stop(400, "Due to shortened work day the collection time must be between 09:00 and 13:00");
        }

        return true;
    }

    public static function encodeOrDecodeCdata($xml_string, $mode = 'encode')
    {
        return preg_replace_callback(

            "/<!\[CDATA\[([^]]+)\]\]>/",
            function ($matches) use (&$mode) {
                $encoded_city = $mode == 'encode' ? utf8_encode($matches[1]) : utf8_decode($matches[1]);
                return "<![CDATA[$encoded_city]]>";
            },
            $xml_string
        );
    }

    public static function replaceTagsWithHtmlEntities($string)
    {
        $search = array("/</", "/>/", "/ /");
        $replace = array("&lt;", "&gt;", "&nbsp;");
        return preg_replace($search, $replace, $string);
    }
}