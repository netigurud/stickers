<?php
/**
 * Created by PhpStorm.
 * User: henno
 * Date: 28/04/2018
 * Time: 20:44
 */

namespace App;


class Error
{

    static function permissionDenied(){
        error_out(__('Permission denied'), 403);
    }
}