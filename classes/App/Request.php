<?php
/**
 * Created by PhpStorm.
 * User: Kaspar
 * Date: 30.01.19
 * Time: 10:38
 */

namespace App;


class Request
{
    static function isAjax()
    {
        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }
}