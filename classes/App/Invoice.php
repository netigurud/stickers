<?php
/**
 * Created by PhpStorm.
 * User: henno
 * Date: 28/04/2018
 * Time: 13:19
 */

namespace App;

use Braintree;

class Invoice
{

    /**
     * @param $simplbooks_invoice_id
     * @return string
     * @throws \Mpdf\MpdfException
     * @throws \Exception
     */
    public static function generateContent($simplbooks_invoice_id)
    {

        // Validate input
        if (!is_numeric($simplbooks_invoice_id)) {
            throw new \Exception('Invalid simplbooks_invoice_id');
        }

        // $invoiceData is used in invoice template which is included below
        $invoice = SimplBooks::getInvoice($simplbooks_invoice_id);

        ob_start();
        include 'templates/pdf_files/simplbooks_invoice.php';
        $html = ob_get_clean();

        if (ENV === ENV_DEVELOPMENT && strpos($html, 'xdebug')) {
            throw new \Exception('An xdebug error message has been found within the invoice');
        }

        $mpdf = new \Mpdf\Mpdf();

        $mpdf->WriteHTML($html);
        $mpdf->SetDisplayMode('fullwidth');

        return $mpdf->Output('test.pdf', \Mpdf\Output\Destination::STRING_RETURN);


    }

    public static function generateBraintreeTransactionSaleLineItems($order_rows)
    {

        foreach ($order_rows as $order_row) {

            $lineItems[] =
                ['quantity' => $order_row['quantity'],
                    'name' => "$order_row[product_type_name]s ($order_row[final_width] × $order_row[final_height] cm)",
                    'description' => $order_row['product_type_name'],
                    'kind' => Braintree\TransactionLineItem::DEBIT,
                    'unitAmount' => number_format(round($order_row['price_with_vat'] / (1+VAT_PERCENT) / $order_row['quantity'], 2, PHP_ROUND_HALF_EVEN), 2),
                    'taxAmount' => number_format(round($order_row['price_with_vat'] / (1+VAT_PERCENT) * VAT_PERCENT, PHP_ROUND_HALF_EVEN), 2),
                    'totalAmount' => number_format(round($order_row['price_with_vat'], 2, PHP_ROUND_HALF_EVEN), 2),
                    'unitOfMeasure' => 'pcs'
                ];
        }

        return $lineItems;
    }

}