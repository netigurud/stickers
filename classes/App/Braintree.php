<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 27.03.2018
 * Time: 15:23
 */

namespace App;


use Braintree_Gateway;

class Braintree
{

    static function getTransactionError($error_code, $error_message)
    {
        $errors = self::errors();

        if (array_key_exists($error_code, $errors)) {
            return [
                "title" => __(key($errors[$error_code])),
                "description" => __(current($errors[$error_code]))
            ];
        } elseif ($error_code >= 2093 && $error_code <= 2999) {
            return [
                "title" => __('Processor Declined'),
                "description" => __("The customer's bank is unwilling to accept the transaction. The customer will need to contact their bank for more details regarding this generic decline.")
            ];
        } else {
            return [
                "title" => __('Braintree error code ' . $error_code),
                "description" => __($error_message)
            ];
        }
    }

    public static function errors()
    {
        $errors[2000]["Do Not Honor"] = "The customer's bank is unwilling to accept the transaction. The customer will need to contact their bank for more details regarding this generic decline.";
        $errors[2001]["Insufficient Funds"] = "The account did not have sufficient funds to cover the transaction amount at the time of the transaction – subsequent attempts at a later date may be successful.";
        $errors[2002]["Limit Exceeded"] = "The attempted transaction exceeds the withdrawal limit of the account. The customer will need to contact their bank to change the account limits or use a different payment method.";
        $errors[2003]["Cardholder's Activity Limit Exceeded"] = "The attempted transaction exceeds the activity limit of the account. The customer will need to contact their bank to change the account limits or use a different payment method.";
        $errors[2004]["Expired Card"] = "Card is expired. The customer will need to use a different payment method.";
        $errors[2005]["Invalid Credit Card Number"] = "The customer entered an invalid payment method or made a typo in their credit card information. Have the customer correct their payment information and attempt the transaction again – if the decline persists, they will need to contact their bank.";
        $errors[2006]["Invalid Expiration Date"] = "The customer entered an invalid payment method or made a typo in their card expiration date. Have the customer correct their payment information and attempt the transaction again – if the decline persists, they will need to contact their bank.";
        $errors[2007]["No Account"] = "The submitted card number is not on file with the card-issuing bank. The customer will need to contact their bank.";
        $errors[2008]["Card Account Length Error"] = "The submitted card number does not include the proper number of digits. Have the customer attempt the transaction again – if the decline persists, the customer will need to contact their bank.";
        $errors[2009]["No Such Issuer"] = "This decline code could indicate that the submitted card number does not correlate to an existing card-issuing bank or that there is a connectivity error with the issuer. Have the customer attempt the transaction again – if the decline persists, the customer will need to contact their bank.";
        $errors[2010]["Card Issuer Declined CVV"] = "The customer entered in an invalid security code or made a typo in their card information. Have the customer attempt the transaction again – if the decline persists, the customer will need to contact their bank.";
        $errors[2011]["Voice Authorization Required"] = "The customer’s bank is requesting that the merchant (you) call to obtain a special authorization code in order to complete this transaction. This can result in a lengthy process – we recommend obtaining a new payment method instead. Contact our Support team for more details.";
        $errors[2012]["Processor Declined – Possible Lost Card"] = "The card used has likely been reported as lost. The customer will need to contact their bank for more information.";
        $errors[2013]["Processor Declined – Possible Stolen Card"] = "The card used has likely been reported as stolen. The customer will need to contact their bank for more information.";
        $errors[2014]["Processor Declined – Fraud Suspected"] = "The customer’s bank suspects fraud – they will need to contact their bank for more information.";
        $errors[2015]["Transaction Not Allowed"] = "The customer's bank is declining the transaction for unspecified reasons, possibly due to an issue with the card itself. They will need to contact their bank or use an alternative payment method.";
        $errors[2016]["Duplicate Transaction"] = "The submitted transaction appears to be a duplicate of a previously submitted transaction and was declined to prevent charging the same card twice for the same service.";
        $errors[2017]["Cardholder Stopped Billing"] = "The customer requested a cancellation of a single transaction – reach out to them for more information.";
        $errors[2018]["Cardholder Stopped All Billing"] = "The customer requested the cancellation of a recurring transaction or subscription – reach out to them for more information.";
        $errors[2019]["Invalid Transaction"] = "The customer’s bank declined the transaction, typically because the card in question does not support this type of transaction – for example, the customer used an FSA debit card for a non-healthcare related purchase. They will need to contact their bank for more information.";
        $errors[2020]["Violation"] = "The customer will need to contact their bank for more information.";
        $errors[2021]["Security Violation"] = "Have the customer attempt the transaction again – if the decline persists, contact our Support team for more information.";
        $errors[2022]["Declined – Updated Cardholder Available"] = "The submitted card has expired or been reported lost and a new card has been issued. Reach out to your customer to obtain updated card information.";
        $errors[2023]["Processor Does Not Support This Feature"] = "Your account can't process transactions with the intended feature – for example, 3D Secure or Level 2/Level 3 data. If you believe your merchant account should be set up to accept this type of transaction, contact our Support team.";
        $errors[2024]["Card Type Not Enabled"] = "Your account can't process the attempted card type. If you believe your merchant account should be set up to accept this type of card, contact our Support team.";
        $errors[2025]["Set Up Error – Merchant"] = "Depending on your region, this response could indicate a connectivity or setup issue. Contact our Support team for more information regarding this error message.";
        $errors[2026]["Invalid Merchant ID"] = "Depending on your region, this response could indicate a connectivity or setup issue. Contact our Support team for more information regarding this error message.";
        $errors[2027]["Set Up Error – Amount"] = "This rare decline code indicates an issue with processing the amount of the transaction. The customer will need to contact their bank for more details.";
        $errors[2028]["Set Up Error – Hierarchy"] = "There is a setup issue with your account. Contact our Support team for more information.";
        $errors[2029]["Set Up Error – Card"] = "This response generally indicates that there is a problem with the submitted card. The customer will need to use a different payment method.";
        $errors[2030]["Set Up Error – Terminal"] = "There is a setup issue with your account. Contact our Support team for more information.";
        $errors[2031]["Encryption Error"] = "The cardholder’s bank does not support $0.00 card verifications. Enable the Retry All Failed $0 option to resolve this error. Contact our Support team with questions.";
        $errors[2032]["Surcharge Not Permitted"] = "Surcharge amount not permitted on this card. The customer will need to use a different payment method.";
        $errors[2033]["Inconsistent Data"] = "An error occurred when communicating with the processor. Attempt the transaction again.";
        $errors[2034]["No Action Taken"] = "An error occurred and the intended transaction was not completed. Attempt the transaction again.";
        $errors[2035]["Partial Approval For Amount In Group III Version"] = "Refer to the AVS response for further details.";
        $errors[2036]["Authorization could not be found to reverse"] = "An error occurred when trying to process the authorization. This response could indicate an issue with the customer’s card – contact our Support team for more information.";
        $errors[2037]["Already Reversed"] = "The indicated authorization has already been reversed. If you believe this to be false, contact our Support team for more information.";
        $errors[2038]["Processor Declined"] = "The customer's bank declined the request. The reasons for this response vary. For example the card number is invalid or the card does not have sufficient funds or the card is suspended but also when the same amount was attempted to charge in a too short time interval and other reasons. Customer will need to contact their bank for more details.";
        $errors[2039]["Invalid Authorization Code"] = "The authorization code was not found or not provided. Have the customer attempt the transaction again – if the decline persists, they will need to contact their bank.";
        $errors[2040]["Invalid Store"] = "There may be an issue with the configuration of your account. Have the customer attempt the transaction again – if the decline persists, contact our Support team for more information.";
        $errors[2041]["Declined – Call For Approval"] = "The card used for this transaction requires customer approval – they will need to contact their bank.";
        $errors[2042]["Invalid Client ID"] = "There may be an issue with the configuration of your account. Have the customer attempt the transaction again – if the decline persists, contact our Support team for more information.";
        $errors[2043]["Error – Do Not Retry, Call Issuer"] = "The card-issuing bank will not allow this transaction. The customer will need to contact their bank for more information.";
        $errors[2044]["Declined – Call Issuer"] = "The card-issuing bank has declined this transaction. Have the customer attempt the transaction again – if the decline persists, they will need to contact their bank for more information.";
        $errors[2045]["Invalid Merchant Number"] = "There is a setup issue with your account. Contact our Support team for more information.";
        $errors[2046]["Declined"] = "The customer's bank is unwilling to accept the transaction. For credit/debit card transactions, the customer will need to contact their bank for more details regarding this generic decline; if this is a PayPal transaction, the customer will need to contact PayPal.";
        $errors[2047]["Call Issuer. Pick Up Card"] = "The customer’s card has been reported as lost or stolen by the cardholder and the card-issuing bank has requested that merchants keep the card and call the number on the back to report it. As an online merchant, you don’t have the physical card and can't complete this request – obtain a different payment method from the customer.";
        $errors[2048]["Invalid Amount"] = "The authorized amount is set to zero, is unreadable, or exceeds the allowable amount. Make sure the amount is greater than zero and in a suitable format.";
        $errors[2049]["Invalid SKU Number"] = "A non-numeric value was sent with the attempted transaction. Fix errors and resubmit with the transaction with the proper SKU Number.";
        $errors[2050]["Invalid Credit Plan"] = "There may be an issue with the customer’s card or a temporary issue at the card-issuing bank. Have the customer attempt the transaction again – if the decline persists, ask for an alternative payment method.";
        $errors[2051]["Credit Card Number does not match method of payment"] = "There may be an issue with the customer’s credit card or a temporary issue at the card-issuing bank. Have the customer attempt the transaction again – if the decline persists, ask for an alternative payment method.";
        $errors[2053]["Card reported as lost or stolen"] = "The card used was reported lost or stolen. The customer will need to contact their bank for more information or use an alternative payment method.";
        $errors[2054]["Reversal amount does not match authorization amount"] = "There may be an issue with the customer’s card or a temporary issue at the card-issuing bank. Have the customer attempt the transaction again – if the decline persists, ask for an alternative payment method.";
        $errors[2055]["Invalid Transaction Division Number"] = "Contact our Support team for more information regarding this error message.";
        $errors[2056]["Transaction amount exceeds the transaction division limit"] = "Contact our Support team for more information regarding this error message.";
        $errors[2057]["Issuer or Cardholder has put a restriction on the card"] = "The customer will need to contact their issuing bank for more information.";
        $errors[2058]["Merchant not Mastercard SecureCode enabled"] = "The attempted card can't be processed without enabling 3D Secure for your account. Contact our Support team for more information regarding this feature or contact the customer for an alternative payment method.";
        $errors[2059]["Address Verification Failed"] = "PayPal was unable to verify that the transaction qualifies for Seller Protection because the address was improperly formatted. The customer should contact PayPal for more information or use an alternative payment method.";
        $errors[2060]["Address Verification and Card Security Code Failed"] = "Both the AVS and CVV checks failed for this transaction. The customer should contact PayPal for more information or use an alternative payment method.";
        $errors[2061]["Invalid Transaction Data"] = "There may be an issue with the customer’s card or a temporary issue at the card-issuing bank. Have the customer attempt the transaction again – if the decline persists, ask for an alternative payment method.";
        $errors[2062]["Invalid Tax Amount"] = "There may be an issue with the customer’s card or a temporary issue at the card-issuing bank. Have the customer attempt the transaction again – if the decline persists, ask for an alternative payment method.";
        $errors[2063]["PayPal Business Account preference resulted in the transaction failing"] = "You can't process this transaction because your account is set to block certain payment types, such as eChecks or foreign currencies. If you believe you have received this decline in error, contact our Support team.";
        $errors[2064]["Invalid Currency Code"] = "There may be an issue with the configuration of your account for the currency specified. Contact our Support team for more information.";
        $errors[2065]["Refund Time Limit Exceeded"] = "PayPal requires that refunds are issued within 180 days of the sale. This refund can't be successfully processed.";
        $errors[2066]["PayPal Business Account Restricted"] = "Contact PayPal’s Support team to resolve this issue with your account. Then, you can attempt the transaction again.";
        $errors[2067]["Authorization Expired"] = "The PayPal authorization is no longer valid.";
        $errors[2068]["PayPal Business Account Locked or Closed"] = "You'll need to contact PayPal’s Support team to resolve an issue with your account. Once resolved, you can attempt to process the transaction again.";
        $errors[2069]["PayPal Blocking Duplicate Order IDs"] = "The submitted PayPal transaction appears to be a duplicate of a previously submitted transaction. This decline code indicates an attempt to prevent charging the same PayPal account twice for the same service.";
        $errors[2070]["PayPal Buyer Revoked Future Payment Authorization"] = "The customer requested a cancellation of all future transactions on their PayPal account. Reach out to the customer for more information or an alternative payment method.";
        $errors[2071]["PayPal Payee Account Invalid Or Does Not Have a Confirmed Email"] = "Customer has not finalized setup of their PayPal account. Reach out to the customer for more information or an alternative payment method.";
        $errors[2072]["PayPal Payee Email Incorrectly Formatted"] = "Customer made a typo or is attempting to use an invalid PayPal account.";
        $errors[2073]["PayPal Validation Error"] = "PayPal can't validate this transaction. This decline code will be triggered if you attempt a transaction using the email address registered with your PayPal Business account.";
        $errors[2074]["Funding Instrument In The PayPal Account Was Declined By The Processor Or Bank, Or It Can't Be Used For This Payment"] = "The customer’s payment method associated with their PayPal account was declined. Reach out to the customer for more information or an alternative payment method.";
        $errors[2075]["Payer Account Is Locked Or Closed"] = "The customer’s PayPal account can't be used for transactions at this time. The customer will need to contact PayPal for more information or use an alternative payment method.";
        $errors[2076]["Payer Cannot Pay For This Transaction With PayPal"] = "The customer should contact PayPal for more information or use an alternative payment method. You may also receive this response if you create transactions using the email address registered with your PayPal Business account.";
        $errors[2077]["Transaction Refused Due To PayPal Risk Model"] = "PayPal has declined this transaction due to risk limitations. You'll need to contact PayPal’s Support team to resolve this issue.";
        $errors[2079]["PayPal Merchant Account Configuration Error"] = "You'll need to contact our Support team to resolve an issue with your account. Once resolved, you can attempt to process the transaction again.";
        $errors[2081]["PayPal pending payments are not supported"] = "Braintree received an unsupported Pending Verification response from PayPal. Contact Braintree’s Support team to resolve a potential issues with your account settings. If there is no issue with your account, have the customer reach out to PayPal for more information.";
        $errors[2082]["PayPal Domestic Transaction Required"] = "This transaction requires the customer to be a resident of the same country as the merchant. Reach out to the customer for an alternative payment method.";
        $errors[2083]["PayPal Phone Number Required"] = "This transaction requires the payer to provide a valid phone number. The customer should contact PayPal for more information or use an alternative payment method.";
        $errors[2084]["PayPal Tax Info Required"] = "The customer must complete their PayPal account information, including submitting their phone number and all required tax information.";
        $errors[2085]["PayPal Payee Blocked Transaction"] = "Fraud settings on your PayPal business account are blocking payments from this customer. These can be adjusted in the Block Payments section of your PayPal business account.";
        $errors[2086]["PayPal Transaction Limit Exceeded"] = "The settings on the customer's account do not allow a transaction amount this large. They will need to contact PayPal to resolve this issue.";
        $errors[2087]["PayPal reference transactions not enabled for your account"] = "The fraud settings on your PayPal business account are blocking payments from this customer. You can adjust these settings in the Block Payments section of your PayPal business account.";
        $errors[2088]["Currency not enabled for your PayPal seller account"] = "This currency is not currently supported by your PayPal account. You can accept additional currencies by updating your PayPal profile.";
        $errors[2089]["PayPal payee email permission denied for this request"] = "PayPal API permissions are not set up between your PayPal business accounts. Contact our Support team for more details.";
        $errors[2090]["PayPal account not configured to refund more than settled amount"] = "Your PayPal account is not set up to refund amounts higher than the original transaction amount. Contact PayPal's Support team for information on how to enable this.";
        $errors[2091]["Currency of this transaction must match currency of your PayPal account"] = "Your PayPal account can only process transactions in the currency of your home country. Contact PayPal's Support team for more information.";
        $errors[2092]["No Data Found - Try Another Verification Method"] = "The processor is unable to provide a definitive answer about the customer's bank account. Please try a different US bank account verification method.";
        $errors[3000]["Processor Network Unavailable – Try Again"] = "The processor is unable to provide a definitive answer about the customer's bank account. Please try a different US bank account verification method.";

        return $errors;
    }

    public static function getErrorDescription($message)
    {
        foreach (self::errors() as $error) {
            if (key($error) == $message) {
                return $error[$message];
            }
        }

        return false;
    }

    /**
     * @param $first_name
     * @param $last_name
     * @param $company
     * @param $email
     * @param $phone
     * @return \Braintree\Braintree_Result_Error|\Braintree\Braintree_Result_Successful
     */
    public static function createCustomer($first_name, $last_name, $company, $email, $phone)
    {
        // Configure the environment and API credentials
        $gateway = new Braintree_Gateway([
            'environment' => BT_ENVIRONMENT,
            'merchantId' => BT_MERCHANT_ID,
            'publicKey' => BT_PUBLIC_KEY,
            'privateKey' => BT_PRIVATE_KEY
        ]);

        // Create a customer without an associated payment method
        $result = $gateway->customer()->create([
            'firstName' => $first_name,
            'lastName' => $last_name,
            'company' => $company,
            'email' => $email,
            'phone' => $phone,
            'paymentMethodNonce' => $_POST['payment_method_nonce']
        ]);

        // Check if the customer creation was successful
        return $result;
    }

    static function checkForErrors ($braintree_action_result) {
        if (!$braintree_action_result->success) {

            // Stop if there are errors in $braintree_sale_result->errors
            if(!empty($braintree_action_result->errors)) foreach ($braintree_action_result->errors->deepAll() AS $error) {
                stop($error->code > 2999 ? 500 : 400, Braintree::getTransactionError($error->code, $error->message));
            }

            // Stop if $braintree_sale_result->message is set or $transaction->status is not a happy one
            if (!empty($braintree_action_result->message)) {

                $message = trim($braintree_action_result->message);
                $error_description = Braintree::getErrorDescription($message);

                stop(400, [
                    'title' => __('There was a problem processing your payment information:') .' ' . $message,
                    'description' => $error_description ? __("$message") . ' - ' . __("$error_description") : __($message)
                ]);
            }

        }

    }
}