<?php
/**
 * Created by PhpStorm.
 * User: henno
 * Date: 23/06/2018
 * Time: 23:59
 */

namespace App;


class Curl
{

    const GET = 0;
    const POST = 1;

    /**
     * @param int $method
     * @param $url string SimplBooks API url
     * @param array $post_fields Array to be POSTed
     * @param array $custom_headers
     * @return object SimplBooks JSON object converted to PHP object
     * @throws \Exception SimplBooks response(JSON, if possible) serialized to PHP array
     */
    public static function request($method = Curl::GET, $url, $post_fields = [], $custom_headers)
    {

        // Curl check
        if (!function_exists("curl_init")
            || !function_exists("curl_setopt")
            || !function_exists("curl_exec")
            || !function_exists("curl_close")
        ) die('Install curl extension first.');

        $curl = curl_init();

        if (FALSE === $curl)
            throw new \Exception('failed to initialize');

        $post_fields = http_build_query($post_fields);

        // Set cURL options
        curl_setopt($curl, CURLOPT_HEADER, true);    // we want headers
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, $method == Curl::POST ? true : false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $custom_headers);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_fields);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);

        // $output contains the output string
        $response = curl_exec($curl);

        // Verify that we have a response
        if (curl_error($curl) || FALSE === $response) {
            throw new \Exception(curl_error($curl));
        }

        // Extract headers and body
        $transfer = curl_getinfo($curl);
        $request_headers = $transfer['request_header'];
        $response_body = substr($response, $transfer['header_size']);
        $response_headers = substr($response, 0, $transfer['header_size']);
        $request_body = str_replace('&', '&' . "\n", urldecode($post_fields));

        self::log(
            $url,
            $request_headers,
            $request_body,
            $response_headers,
            $response_body);

        return $response_body;
    }

    public static function getLast()
    {
        return '<pre>' . file_get_contents('curl_last.txt') . '</pre>';
    }

    public static function log($url, $request_headers, $request_body, $response_headers, $response_body)
    {

        $response_body = prettify_json($response_body);

        // Open log file
        $log_file = fopen("curl_last.txt", "w") or die("Unable to write to file!");

        // Write to file
        fwrite($log_file,
            date('Y-m-d H:i:s') . " " . $url . "\n" .
            "\n------------------------- REQUEST --------------------------\n" .
            $request_headers .
            "$request_body\n" .
            "\n------------------------- RESPONSE -------------------------\n" .
            $response_headers .
            "$response_body");
    }
}

