<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 10.01.2018
 * Time: 15:33
 */

namespace App;


class Order
{

    public static function get(
        $order_id = false,
        $order_made_by_user_id = false,
        $printing_house = false,
        $is_designer = false
    )
    {
        $and_order_made_by = $order_made_by_user_id ? "AND order_made_by = $order_made_by_user_id" : '';
        $and_order_status_id = '';
        $and_order_taken_by_designer = '';

        // If the user is a designer and also the printing house then don't use printing house constraints
        if (!$is_designer) {
            // Users with print house rights can only see orders that have been confirmed by user or have been sent to print house
            if ($printing_house) {
                $sent_to_print_house = ORDER_STATUS_IN_PRODUCTION;
                $waiting_for_delivery = ORDER_STATUS_WAITING_FOR_DELIVERY;
                $confirmed_by_user = ORDER_STATUS_QUEUED_FOR_PRINTING;
                $and_order_status_id = "AND (order_status_id = $sent_to_print_house OR order_status_id = $confirmed_by_user OR order_status_id = $waiting_for_delivery)";
            }
        } else {
            $and_order_taken_by_designer = "AND order_taken_by_designer IS NULL OR order_taken_by_designer = $_SESSION[user_id]";
        }

        if ($order_id) {
            $result['order'] = get_first("SELECT orders.*, order_statuses.*, files.*, users.*, countries.*, designer.user_full_name as designer_full_name, orders.country_id as order_country_id
                                               FROM orders
                                                 LEFT JOIN users ON (order_made_by = user_id)
                                                 LEFT JOIN order_statuses USING (order_status_id)
                                                 LEFT JOIN files ON (file_id = order_file_id)
                                                 LEFT JOIN countries ON (orders.country_id = countries.country_id)
                                                 LEFT JOIN users designer ON order_taken_by_designer = designer.user_id
                                               WHERE order_id = $order_id $and_order_made_by $and_order_status_id $and_order_taken_by_designer");
            $vat_percent = $result['order']['order_vat_percent'];
            $result['order_rows'] = self::getRows($order_id, $vat_percent);
            $result['order_total_price'] = 0;
            $result['order_total_price_with_vat'] = 0;

            // Calculate order total price
            foreach ($result['order_rows'] as $order_row) {
                $result['order_total_price'] += $order_row['price'];
                $result['order_total_price_with_vat'] += $order_row['price_with_vat'];
            }

            // Round total
            $result['order_total_price_with_vat'] = number_format($result['order_total_price_with_vat'],
                2, ".", ",");


        } else {
            $result['orders'] = get_all("SELECT orders.*, order_statuses.*, files.*, users.*, countries.*, designer.user_full_name as designer_full_name
                                              FROM orders 
                                                LEFT JOIN users ON (order_made_by = user_id)
                                                LEFT JOIN order_statuses USING (order_status_id)
                                                LEFT JOIN files ON (file_id = order_file_id)
                                                LEFT JOIN countries ON (orders.country_id = countries.country_id)
                                                LEFT JOIN users designer ON order_taken_by_designer = designer.user_id
                                              WHERE 1 = 1 $and_order_made_by $and_order_status_id $and_order_taken_by_designer ORDER BY order_id DESC");
            $result['finished_orders'] = [];
            $result['unfinished_orders'] = [];

            // Add orders rows to orders
            foreach ($result['orders'] as $key => $order) {

                $vat_percent = $result['orders'][$key]['order_vat_percent'];
                $result['orders'][$key]['order_rows'] = self::getRows($order['order_id'], $vat_percent);
                $result['orders'][$key]['order_total_price'] = 0;
                $result['orders'][$key]['order_total_price_with_vat'] = 0;

                // Calculate order total price

                foreach ($result['orders'][$key]['order_rows'] as $order_row) {
                    $result['orders'][$key]['order_total_price'] += $order_row['price'];
                    $result['orders'][$key]['order_total_price_with_vat'] += $order_row['price_with_vat'];
                }

                // Round total
                $result['orders'][$key]['order_total_price_with_vat'] = number_format($result['orders'][$key]['order_total_price_with_vat'],
                    2, ".", ",");

                // Check what state the order is in
                if ($order['order_status_id'] == ORDER_STATUS_SHIPPED) {
                    $result['finished_orders'][] = $result['orders'][$key];
                } else {
                    $result['unfinished_orders'][] = $result['orders'][$key];
                }
            }
        }

        self::translateOrderStatuses($result['finished_orders']);
        self::translateOrderStatuses($result['unfinished_orders']);
        self::translateOrderStatuses($result['orders']);

        return $result;
    }

    public static function getShippedOrders()
    {
        return get_all("SELECT * FROM orders JOIN users ON (order_made_by = user_id) WHERE order_status_id = " . ORDER_STATUS_WAITING_FOR_DELIVERY);
    }

    public static function getStatuses()
    {
        $statuses = get_all("SELECT * FROM order_statuses");
        self::translateOrderStatuses($statuses);


    }

    public static function getRows($order_id, $vat_percent = 0)
    {
        $order_rows = get_all("SELECT * 
                                    FROM order_rows 
                                      LEFT JOIN orders USING (order_id)
                                      LEFT JOIN product_types USING (product_type_id) 
                                    WHERE order_id = $order_id");

        // Process rows
        foreach ($order_rows as $key => $order_row) {

            // Proper case for product name
            $order_rows[$key]['product_type_name'] = ucfirst($order_row['product_type_name']);


            // Add VAT
            $order_rows[$key]['price_with_vat'] = round((1 + $vat_percent / 100) * $order_rows[$key]['price'], 2);
        }

        return $order_rows;
    }

    public static function getRow($order_row_id)
    {
        $order = get_first("SELECT * 
                               FROM order_rows 
                                LEFT JOIN orders USING (order_id)
                                LEFT JOIN product_types USING (product_type_id)
                               WHERE order_row_id = $order_row_id");

        // Make first letter uppercase
        $order['product_type_name'] = ucfirst($order['product_type_name']);

        return $order;
    }

    public static function getSimplbooksInvoiceRows($order_id)
    {
        $invoice_rows = [];

        foreach (self::getRows($order_id) as $order_row) {
            $invoice_rows[] = [
                "name" => "$order_row[product_type_name]s $order_row[final_width] cm x $order_row[final_height] cm",
                "unit" => "pcs",
                "amount" => $order_row['quantity'],
                "price_per_unit" => $order_row['price'] / $order_row['quantity'],
                "vat" => $order_row['order_vat_percent']
            ];
        }

        return $invoice_rows;
    }

    public static function getTotalWithVAT($order_rows, $promo_code = false, $vat_percent = false)
    {
        $order_total = 0;
        $promo_code_discount_percent = 0;

        foreach ($order_rows as $order_row) {
            $order_total += $order_row['price'];
        }

        // Check if the user entered a promo code
        if ($promo_code) {
            $promo_code = PromoCode::get($promo_code);
            $promo_code_discount_percent = $promo_code ? $promo_code['promo_code_discount_percent'] : 0;
        }

        // Calculate total with promo code discount
        $order_total = $order_total * ((100 - $promo_code_discount_percent) / 100);

        // Get VAT percent
        $vat_percent = $vat_percent ? $vat_percent : 20;

        // Calculate total with VAT
        $order_total = $order_total * (1 + ($vat_percent / 100));

        return round($order_total, 2);
    }

    public static function getByCriteria($criteria, $no_escape = false)
    {
        $where = is_array($criteria) ? 'WHERE ' . implode(' AND ',
                ($no_escape ? $criteria : escape($criteria))) : (is_numeric($criteria) ? "WHERE order_id = $criteria" : '');
        return get_all("
            SELECT 
                order_id,
                order_full_name,
                d.user_full_name designer_name,
                order_made_at,
                order_made_by,
                order_status_changed_at,
                request_changes_reason,
                order_vat_percent,
                order_promo_code
            FROM orders 
            LEFT JOIN users d ON orders.order_taken_by_designer=d.user_id
            $where");
    }

    /**
     * @param $simplbooks_invoice_id
     * @return mixed
     * @throws \Exception
     */
    public static function getBySimplbooksInvoiceId($simplbooks_invoice_id)
    {

        // Validate invoice id
        if (empty($simplbooks_invoice_id) || !is_numeric($simplbooks_invoice_id)) {
            throw new \Exception('No simplbooks invoice id given or it is invalid');
        }

        // Get the order
        $order = Order::getByCriteria(['simplbooks_invoice_id' => $simplbooks_invoice_id]);

        if (!$order) {
            error_out(fillPlaceholders(['%INVOICE_ID%' => $simplbooks_invoice_id],
                __('No order is associated with invoice %INVOICE_ID%')));
        }

        if (count($order) != 1) {
            throw new \Exception("More than 1 order found matching Invoice ID $simplbooks_invoice_id");
        }

        return $order[0];
    }

    /**
     * @param $order_id
     * @param $order_status_id
     * @throws \Exception
     */
    public static function setStatus($order_id, $order_status_id)
    {
        if (!is_numeric($order_id)) {
            throw new \Exception("Order id '$order_id' was not numeric");
        }

        $data = ["order_status_id" => $order_status_id, "order_status_changed_at" => ["no_escape" => "now()"]];

        // Set order design finish time when new status is >= queued for printing, unless it's already set
        if ($order_status_id >= ORDER_STATUS_QUEUED_FOR_PRINTING) {
            $order_design_finished_at = get_one("SELECT order_design_finished_at FROM orders WHERE order_id = $order_id");
            if (!$order_design_finished_at) {
                $data['order_design_finished_at']['no_escape'] = 'NOW()';
            }
        }

        update("orders", $data,
            "order_id = $order_id");
    }

    /**
     * @param $user_id
     * @param $vat_percent
     * @param $file_id
     * @param $user_vat
     * @param $country_id
     * @param $user_state
     * @param $user_dial_code
     * @param $user_street
     * @param $user_city
     * @param $user_phone
     * @param $user_full_name
     * @param $user_company
     * @param $user_company_reg_nr
     * @param $user_postal_code
     * @return bool|int
     */
    public static function create($user_id, $vat_percent, $file_id, $user_vat, $country_id, $user_state, $user_dial_code, $user_street, $user_city, $user_phone, $user_full_name, $user_company, $user_company_reg_nr, $user_postal_code, $products)
    {
// Insert the order
        $order_id = insert("orders", [
            "order_made_by" => $user_id,
            "order_vat_percent" => $vat_percent,
            "order_promo_code" => $_POST['promo_code'] ?? null,
            "order_file_id" => $file_id,
            "order_vat" => $user_vat,
            "country_id" => $country_id,
            "order_state" => $user_state,
            "order_dial_code" => $user_dial_code,
            "order_street" => $user_street,
            "order_city" => $user_city,
            "order_phone" => $user_phone,
            "order_full_name" => $user_full_name,
            "order_company" => $user_company,
            "order_company_reg_nr" => $user_company_reg_nr,
            "order_postal_code" => $user_postal_code
        ]);

        // Insert the order rows
        foreach ($products as $product) {
            insert("order_rows", [
                'order_id' => $order_id,
                'width' => $product['product_width'],
                'height' => $product['product_height'],
                'final_width' => $product['product_width'],
                'final_height' => $product['product_height'],
                'price' => $product['product_price'],
                'quantity' => $product['quantity'],
                'product_type_id' => $product['product_type_id']
            ]);
        }
        return $order_id;
    }

    /**
     * @param $array
     */
    public static function translateOrderStatuses(&$array)
    {
        // Translate statuses
        if(!empty($array)) foreach ($array as $key => $member) {
            $array[$key]['order_status_name'] = __($member['order_status_name']);
        }
    }


}