<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 28.03.2018
 * Time: 11:44
 */

namespace App;
class VATException extends \Exception
{
}

class VAT
{


    /**
     * Returns true in case of valid VAT number, false otherwise
     * @param $vat_id
     * @return bool
     * @throws VATException
     */
    static function isValid($vat_id)
    {

        /*
         * Usage:
         *      The countryCode input parameter must follow the pattern [A-Z]{2}
         *      The vatNumber input parameter must follow the pattern [0-9A-Za-z\+\*\.]{2,12}
         *
         *
         * In case of problems, the returned FaultString can take the following specific values:
         *      INVALID_INPUT: The provided CountryCode is invalid or the VAT number is empty;
         *
         *      GLOBAL_MAX_CONCURRENT_REQ: Your Request for VAT validation has not been processed; the maximum number of
         *      concurrent requests has been reached. Please re-submit your request later or contact
         *      TAXUD-VIESWEB@ec.europa.eu for further information":
         *      Your request cannot be processed due to high traffic on the web application. Please try again later;
         *
         *      MS_MAX_CONCURRENT_REQ: Your Request for VAT validation has not been processed; the maximum number of
         *      concurrent requests for this Member State has been reached. Please re-submit your request later or contact
         *      TAXUD-VIESWEB@ec.europa.eu for further information": Your request cannot be processed due to high traffic
         *      towards the Member State you are trying to reach. Please try again later.
         *
         *      SERVICE_UNAVAILABLE: an error was encountered either at the network level or the Web application level, try again later;
         *
         *      MS_UNAVAILABLE: The application at the Member State is not replying or not available. Please refer to
         *      the Technical Information page to check the status of the requested Member State, try again later;
         *      Information page to check the status of the requested Member State, try again later;
         *
         *      TIMEOUT: The application did not receive a reply within the allocated time period, try again later.
         * */

        $vat_id = str_replace(array(' ', '.', '-', ',', ', '), '', trim($vat_id));
        $cc = substr($vat_id, 0, 2);
        $vat_number = substr($vat_id, 2);
        $client = new \SoapClient("http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl");

        if ($client) {
            $params = array('countryCode' => $cc, 'vatNumber' => $vat_number);
            try {
                $r = $client->checkVat($params);
                if ($r->valid == true) {
                    return true;
                } else {
                    return false;
                }
            } catch (\SoapFault $e) {
                if ($e->faultstring == 'MS_UNAVAILABLE') {
                    $error = fillPlaceholders(
                        ["%COUNTRY_CODE%" => $cc],
                        "Member State (%COUNTRY_CODE%) VAT number validation service unavailable. Please re-submit your request later."
                    );

                    stop(500, $error);
                } elseif ($e->faultstring == 'INVALID_INPUT') {
                    $error = fillPlaceholders(
                        ["%VAT_ID%" => $vat_id],
                        "'%VAT_ID%' is not a valid VAT number."
                    );

                    stop(400, $error);
                }

                throw new VATException($e->faultstring);
            }

        } else {
            throw new VATException('europa.eu VAT validation service is not reachable');
        }
    }
}