<?php

/**
 * Application's constants
 */

/* Projects general constants
====================================================================================================*/
define('PROJECT_NAME', 'Letstickers');
define('PROJECT_SESSION_ID', 'SESSID_STICKERS'); // For separating sessions of multiple Halo projects running on same server
define('DEFAULT_CONTROLLER', 'welcome');
define('WEBSITE_LANGUAGES', 'en|et|fi'); // Languages this website supports. The first one is the default
define('ESTONIA_DIAL_CODE', '+372');
define('VAT_PERCENT', 0.2); // 20%

// Custom sizes
define('CUSTOM_SIZE_PRICE', 2);
define('CUSTOM_SIZE_MIN_WIDTH', 2.54);
define('CUSTOM_SIZE_MIN_HEIGHT', 2.54);
define('CUSTOM_SIZE_MAX_WIDTH', 29);
define('CUSTOM_SIZE_MAX_HEIGHT', 29);

// Files
define('MAX_UPLOAD_FILE_SIZE', 50000000); // 50MB

// Product types
define('PRODUCT_TYPE_STICKER', 1);
define('PRODUCT_TYPE_MAGNET', 2);

// Order statuses
define('ORDER_STATUS_CANCELLED', 1);
define('ORDER_STATUS_WAITING_FOR_DESIGNER_REVIEW', 2);
define('ORDER_STATUS_WAITING_FOR_CUSTOMER_REVIEW', 3);
define('ORDER_STATUS_QUEUED_FOR_PRINTING', 4);
define('ORDER_STATUS_IN_PRODUCTION', 5);
define('ORDER_STATUS_WAITING_FOR_DELIVERY', 6);
define('ORDER_STATUS_SHIPPED', 7);

/* Site owner information
====================================================================================================*/
define('SITE_OWNER_CITY', 'Tallinn');
define('SITE_OWNER_PHONE', '56607075');
define('SITE_OWNER_EMAIL', 'info@netguru.ee'); // Where to send site owner stuff
define('SITE_OWNER_COMPANY', 'NetGuru OÜ');
define('SITE_OWNER_FULL_NAME', 'Filipp Kungur');
define('SITE_OWNER_POSTAL_CODE', '11620');
define('SITE_OWNER_COUNTRY_CODE', 'EE');
define('SITE_OWNER_ADDRESS_LINE', 'Hiiu 20-2');
define('SUPPORT_PHONE_NUMBER', '+372 5660 7075');


/* Google login
====================================================================================================*/
define('GOOGLE_CLIENT_ID', '115140560643-gj6mbi51vn9f528uh9drids0g4gk79ji.apps.googleusercontent.com');
define('GOOGLE_CLIENT_SECRET', 'jUGYgVGhzovGq9_Kr7TQZzXq');
define('GOOGLE_REDIRECT_URI', 'login_google/callback');