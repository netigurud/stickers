<?php
/**
 * Created by PhpStorm.
 * User: IBM
 * Date: 16/07/2018
 * Time: 17:52
 */

namespace App;


class profile extends Controller
{
    public $requires_auth = true;

    function index()
    {
        $user_id = $this->auth->user_id;
        $this->user = User::get($user_id);
        $this->orders = Order::get(false, $user_id);
        $this->countries = Countries::get(['country_active' => 1]);
    }
}