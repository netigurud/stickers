<?php namespace App;

use Braintree_Gateway;

class checkout extends Controller
{


    function index()
    {
        // Redirect to home page if no products are selected
        if (empty($_SESSION['shopping_cart']['products'])) {
            redirect();
        }

        // Get the user if logged in
        $user_id = $this->auth->user_id ?? 0;
        $this->user_files = File::getUserFiles($user_id, true, true);
        $this->products = $_SESSION['shopping_cart']['products'];

        if ($user_id) {
            $this->user = User::get();
        }


        // Configure the environment and API credentials
        $gateway = new Braintree_Gateway([
            'environment' => BT_ENVIRONMENT,
            'merchantId' => BT_MERCHANT_ID,
            'publicKey' => BT_PUBLIC_KEY,
            'privateKey' => BT_PRIVATE_KEY
        ]);

        $this->client_token = $gateway->clientToken()->generate();
        $this->countries = Countries::get(['country_active' => 1]);
    }

    /**
     * @throws \Exception
     */
    function ajax_place_order()
    {
        if (fieldsAreEmpty([
            $_POST["user_email"],
            $_POST["user_full_name"],
            $_POST["country_id"],
            $_POST["user_city"],
            $_POST["user_street"],
            $_POST["user_dial_code"],
            $_POST["user_phone"],
            $_POST["user_postal_code"]
        ])) {
            stop(400, __('Please fill in all the required fields for the shipping info section.'));
        }

        // Check if company name was somehow emptied
        if (!empty($_POST["user_vat"]) && empty($_POST["user_company"])) {
            stop(400, __('Please fill in the name of your company.'));
        };

        // Check if somehow the file wasn't chosen
        if (!isset($_SESSION['shopping_cart']['file']['file_id'])) {
            stop(400, __('Please choose a file'));
        }

        // validate two names on server side as well
        list($user_first_name, $user_last_name) = User::validateName($_POST["user_full_name"]);

        $file_id = $_SESSION['shopping_cart']['file']['file_id'];

        // Trim everything of whitespace
        $user_email = trim($_POST["user_email"]);
        $user_state = trim($_POST["user_state"]);
        $country_id = trim($_POST["country_id"]);
        $user_city = trim($_POST["user_city"]);
        $user_street = trim($_POST["user_street"]);
        $user_postal_code = trim($_POST["user_postal_code"]);
        $user_dial_code = trim($_POST["user_dial_code"]);
        $user_phone = trim($_POST["user_phone"]);

        // Optional inputs
        $user_company = isset($_POST["user_company"]) ? trim($_POST["user_company"]) : null;
        $user_company_reg_nr = isset($_POST["user_company_reg_nr"]) ? trim($_POST["user_company_reg_nr"]) : null;
        $user_vat = isset($_POST["user_vat"]) ? trim($_POST["user_vat"]) : null;


        // Estonian companies get 20% VAT even when they have provided their VAT number
        $vat_percent = !empty($user_vat) ? (substr($user_vat, 0, 2)== 'EE' ? 20 : 0) : 20;

        $session_id = session_id();

        // Verify email address
        Mail::validateAddress($user_email);

        // Create a new account for the user if they're not logged in
        if ($this->auth->logged_in) {
            $user_id = $this->auth->user_id ? $this->auth->user_id : $_SESSION['user_id'];

            // Update the user
            update("users", [
                "user_email" => $user_email,
                "user_state" => $user_state,
                "user_full_name" => "$user_first_name $user_last_name",
                "user_company" => $user_company,
                "user_company_reg_nr" => $user_company_reg_nr,
                "user_phone" => $user_phone,
                "user_dial_code" => $user_dial_code,
                "country_id" => $country_id,
                "user_city" => $user_city,
                "user_street" => $user_street,
                "user_postal_code" => $user_postal_code,
                "user_vat" => $user_vat
            ], "user_id = $user_id");


        } else {


            // Check if user with give email exists
            if (User::emailExists($_POST['user_email'])) {
                stop(400, __('User with given email address already exists. Please login before placing the order.'));
            }


            // Register the user
            $user_id = User::register(
                $user_email,
                "$user_first_name $user_last_name",
                $user_company,
                $user_company_reg_nr,
                generateRandomString(),
                $user_state,
                $user_phone,
                $user_dial_code,
                $country_id,
                $user_city,
                $user_street,
                $user_postal_code,
                $user_vat
            );


            // Log the user in
            $_SESSION['user_id'] = $user_id;

            // Associate uploaded files during this session with this new user
            File::associateUploadedSessionFilesWithLoggedInUser();

        }

        $braintree_customer_create_result = Braintree::createCustomer(
            $user_first_name,
            $user_last_name,
            $user_company,
            $user_email,
            $user_phone);

        // Stop if there are any Braintree errors
        Braintree::checkForErrors($braintree_customer_create_result);

        $order_id = Order::create(
            $user_id,
            $vat_percent,
            $file_id,
            $user_vat,
            $country_id,
            $user_state,
            $user_dial_code,
            $user_street,
            $user_city,
            $user_phone,
            "$user_first_name $user_last_name",
            $user_company,
            $user_company_reg_nr,
            $user_postal_code,
            $_SESSION['shopping_cart']['products']);

        // Set BrainTree customer ID to order
        update("orders", ["order_braintree_id" => $braintree_customer_create_result->customer->id], "order_id = $order_id");


        // Find the file that we want to send
        $file = File::get($file_id);

        // Send the orders file to given emails
        foreach (Mail::getAll() as $email) {
            $url = BASE_URL . 'orders';
            $body = str_replace("?", $order_id, __('New order with the ID of ? has been made. You can see orders here: LINK'));
            $body = str_replace("LINK", "<a href=\"$url\">$url</a>", $body);

            Mail::send(
                $email['email'],
                __('New order has been made at ' . PROJECT_NAME),
                $body
            );
        }

        $order_link = BASE_URL . "admin/orders";

        // Notify the administrator
        Mail::send(
            ADMIN_EMAIL,
            __('New order') . " $order_id",
            __('New order has been made:') .
            ' <a href="' . $order_link . '">' . __('Click here to go to orders') . '</a> ' .
            __('or paste this URL in your browser:') . ' ' . $order_link
        );

        // Unset shopping cart data
        unset($_SESSION['shopping_cart']);

        // Delete unused session uploads
        $uploader_constraint = $this->auth->logged_in ? "OR file_uploaded_by = $_SESSION[user_id]" : '';
        q("DELETE files, orders 
                FROM files 
                LEFT JOIN orders ON (orders.order_file_id=files.file_id)
                WHERE (file_session_id = '$session_id' $uploader_constraint) AND orders.order_id IS NULL");

        stop(200, ["user_id" => $user_id]);
    }

    /**
     * @throws VATException
     */
    function ajax_check_vat()
    {
        if (VAT::isValid($_POST['vat'])) {
            stop(200);
        } else {
            stop(400, __('Invalid VAT'));
        }
    }
}
