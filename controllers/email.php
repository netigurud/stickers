<?php
/**
 * Created by PhpStorm.
 * User: henno
 * Date: 08/12/2017
 * Time: 22:46
 */

namespace App;


class email extends Controller
{

    function ajax_get()
    {
        stop(200, Mail::get($_POST['email_id']));
    }

    function ajax_add()
    {
        insert("emails", ['email' => $_POST['email']]);
        stop(200);
    }

    function ajax_edit()
    {
        update("emails", ["email" => $_POST['email']], "email_id = $_POST[email_id]");
        stop(200);
    }

    function ajax_delete()
    {
        q("DELETE FROM emails WHERE email_id = $_POST[email_id]");
        stop(200);
    }

    function ajax_available()
    {
        if (Mail::exists($_POST['email'])) {
            stop(400, 'An user with given email already exists');
        }

        stop(200);
    }

    function ajax_check_existence()
    {
        if ($this->auth->logged_in && $this->auth->user_email == $_POST['email']) {
            // User entered the same email that they initially registered with, don't go to the next if
            stop(200);
        } else if ($this->auth->logged_in && !empty(User::emailExists($_POST['email']))) {
            stop(400, __('Another user with given email address already exists. Please use another email.'));
        } else if (!empty(User::emailExists($_POST['email']))) {
            stop(400, __('User with given email address already exists. Please login.'));
        } else {
            stop(200);
        }
    }

}