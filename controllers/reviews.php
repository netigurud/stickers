<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 27/02/2018
 * Time: 12:47
 */

namespace App;


class reviews extends Controller
{
    function ajax_get()
    {
        stop(200, get_first("SELECT * FROM reviews WHERE review_id = $_POST[review_id]"));
    }

    function ajax_add()
    {
        insert("reviews", $_POST);
        stop(200);
    }

    function ajax_edit()
    {
        update("reviews", $_POST, "review_id = $_POST[review_id]");
        stop(200);
    }

    function ajax_delete()
    {
        q("DELETE FROM reviews WHERE review_id = $_POST[review_id]");
        stop(200);
    }

}