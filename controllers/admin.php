<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 22.01.2018
 * Time: 12:30
 */

namespace App;


class admin extends Controller
{
    public $requires_admin = true;
    public $template = 'admin';

    function products()
    {
        $this->product_types = Product::getProductTypes();
        $this->magnets = Product::getAll(PRODUCT_TYPE_MAGNET);
        $this->stickers = Product::getAll(PRODUCT_TYPE_STICKER);
    }

    function users()
    {
        $this->users = User::getAll();
    }

    function designers()
    {
        $this->designers = User::getDesigners();
        $this->years = DateTime::generateYearRange(2018, date('Y'));
        $this->months = DateTime::generateMonthRange();
        $this->begin_month = empty($_GET['month']) ? 1 : $_GET['month'];
        $this->end_month = empty($_GET['month']) ? 12 : $_GET['month'];;
        $this->selected_month = empty($_GET['month']) ? null : $_GET['month'];;
        $this->selected_year = empty($_GET['year']) ? date('Y') : $_GET['year'];;
    }

    function orders()
    {
        $this->orders = Order::get();
        $this->order_statuses = Order::getStatuses();
    }

    function posts()
    {
        $this->posts = Post::getAll();
    }

    function emails()
    {
        $this->emails = Mail::getAll();
    }

    function reviews()
    {
        $this->reviews = get_all("SELECT * FROM reviews");
    }

    function pages()
    {
        $this->pages = get_all("SELECT * FROM pages");
        $this->videos = File::getVideos();
    }

    function prices()
    {
        $this->prices = get_all("SELECT * FROM prices ORDER BY m2");
        $this->product_types = Product::getProductTypes();
    }

    function ajax_delete_price()
    {
        q("DELETE FROM prices WHERE price_id = $_POST[price_id]");
        stop(200);
    }

    function ajax_edit_price()
    {
        update("prices", [$_POST['field'] => $_POST['value']], "price_id = $_POST[price_id]");
        stop(200);
    }

    function ajax_add_new_price()
    {
        insert("prices", $_POST);
        stop(200);
    }

    function ajax_get_product()
    {
        stop(200, Product::get($_POST['product_id']));
    }

    function ajax_edit_product()
    {
        if (fieldsAreEmpty([
            $_POST['product_id'],
            $_POST['product_height'],
            $_POST['product_width']
        ])) {
            stop(400, __("Please fill in all the fields"));
        }

        update("products", $_POST, "product_id = $_POST[product_id]");
        stop(200);
    }

    function ajax_delete_product()
    {
        update("products", ["product_deleted" => 1], "product_id = $_POST[product_id]");
        stop(200);
    }

    function ajax_get_order_row()
    {
        if (fieldsAreEmpty([$_POST['order_row_id']])) {
            stop(400, __('Missing order row id'));
        };

        stop(200, Order::getRow($_POST['order_row_id']));
    }

    function ajax_save_order_row()
    {
        if (fieldsAreEmpty([
            $_POST['data']['width'],
            $_POST['data']['height'],
            $_POST['data']['price'],
            $_POST['data']['quantity'],
            $_POST['data']['product_type_id']
        ])) {
            stop(400, __('Please fill in all the fields'));
        }

        $order_row_id = $_POST['order_row_id'];

        update("order_rows", $_POST['data'], "order_row_id = $order_row_id");

        stop(200);
    }

    function ajax_delete_user()
    {

        $user_id = $_POST['user_id'];
        $user = User::get($user_id);
        User::markForDeletion($user_id);

        // Email the user that their account has been deleted
        Mail::send(
            $user['user_email'],
            PROJECT_NAME . ': ' . __('Your account has been deleted'),
            __('Your account has been deleted.') . ' ' .
            __('Your account can be restored within 30 days, after which restoring it will be impossible.')
            . ' ' .
            __('Please contact the site owner if you believe this was a mistake and would like your account restored: ')
            . SITE_OWNER_EMAIL
        );

        stop(200);
    }

    function ajax_restore_user()
    {
        $user_id = $_POST['user_id'];
        User::unmarkForDeletion($user_id);
        stop(200);
    }

    function ajax_send_user_data()
    {
        $user_data = [];
        $attachments = [];
        $user_id = $_POST['user_id'];
        $user = User::get($user_id);

        // Users data
        $user_data['user'] = $user;

        // Orders data
        foreach (Order::get(false, $user_id)['orders'] as $order) {
            $user_data['orders'][$order['order_id']] = $order;
        }

        // Generate a filename pointing to a system's /tmp folder
        $file_name = File::generateFileInSysTempDir('users_data.pdf');

        // Create the PDF file
        $user_data_pdf = PDF::userData($user_data);

        // Write the PDF to that temporary file
        $user_data_pdf->Output($file_name, 'F');

        // Add the PDF file to attachments
        $attachments[] = $file_name;

        // Add uploaded files if there are any
        if ($user_uploaded_files = File::getUserFiles($user_id)) {
            $zip = new \ZipArchive();
            $zip_file_path = dirname(__DIR__, 1) . '/.tmps/user-uploaded-files.zip';

            // Create the archive
            $zip->open($zip_file_path, \ZipArchive::CREATE);

            // Add files to archive
            foreach ($user_uploaded_files as $file) {
                $file_path = dirname(__DIR__, 1) . "/.uploads/$file[file_id]";
                if (file_exists($file_path)) {
                    $zip->addFile($file_path, $file['file_name']);
                }
            }

            // Close the archive
            $zip->close();

            // Add the archive to attachments
            $attachments[] = $zip_file_path;
        }

        // Send the user their data
        Mail::send(
            $user['user_email'],
            __('Your data'),
            __('Please see the attachments for your data.'),
            $attachments
        );

        // Delete the ZIP file if it was created
        if ($zip_file_path) {
            unlink($zip_file_path);
        }

        stop(200);
    }

    function ajax_add_new_product()
    {
        if (fieldsAreEmpty([
            $_POST['product_width'],
            $_POST['product_height'],
            $_POST['product_type_id']
        ])) {
            stop(400, __('Missing width, height, price or product type.'));
        }

        insert("products", $_POST);

        stop(200);
    }

    function ajax_add_user()
    {
        if (fieldsAreEmpty([
            $_POST['user_full_name'],
            $_POST['user_email'],
            $_POST['user_city'],
            $_POST['user_street'],
            $_POST['user_postal_code']
        ])) {
            stop(400, __('Missing name, email, city, street or postal code'));
        }

        // Hash the password
        $_POST['password'] = password_hash($_POST['password'], PASSWORD_DEFAULT);

        // Insert the user
        insert("users", $_POST);

        stop(200);
    }

    function ajax_edit_user()
    {
        if (fieldsAreEmpty([
            $_POST['user_full_name'],
            $_POST['user_email'],
            $_POST['user_city'],
            $_POST['user_street'],
            $_POST['user_postal_code']
        ])) {
            stop(400, __('Missing name, email, city, street or postal code'));
        }

        // Update the user
        update("users", $_POST, "user_id = $_POST[user_id]");

        stop(200);
    }

    /**
     * @throws \Exception
     */
    function ajax_generate_invoice_for_order()
    {
        $order_id = $_POST['order_id'];

        // Get the order we want to create the invoice for
        $order = Order::get($order_id);
        $order = $order['order'];
        try {
            $simplbooks_client_id = SimplBooks::getOrCreateClient($order);

            // Create the invoice
            $invoice = SimplBooks::createInvoice(
                Order::getSimplbooksInvoiceRows($order_id),
                $order,
                $simplbooks_client_id
            );

            // Check if the invoice was created
            if ($invoice->status != 200) {
                stop(400, $invoice->errors);
            }

            // Set the invoice ID for this order
            update("orders", ["simplbooks_invoice_id" => $invoice->inserted_id], "order_id = $order_id");

        } catch (\Exception $e) {
            stop(500, $e->getMessage());
        }

        stop(200);
    }

    function ajax_add_page()
    {
        if (fieldsAreEmpty([
            $_POST['page_title'],
            $_POST['page_content_html'],
        ])) {
            stop(400, __('Missing page title or content.'));
        }

        // Generate slug
        $_POST['page_slug'] = slugify($_POST['page_title']);

        insert("pages", $_POST);

        stop(200);
    }

    function ajax_edit_page()
    {
        if (fieldsAreEmpty([
            $_POST['page_title'],
            $_POST['page_slug'],
            $_POST['page_content_html'],
        ])) {
            stop(400, __('Missing title, slug or content'));
        }


        // Make file_id always NULL if it's a falsey value
        $_POST['file_id'] = $_POST['file_id'] ?: null;


        // Generate slug
        $_POST['page_slug'] = slugify($_POST['page_slug']);

        update("pages", $_POST, "page_id = $_POST[page_id]");
        stop(200);
    }

    function ajax_delete_page()
    {
        $page_id = (int)$_POST['page_id'];
        q("DELETE FROM pages WHERE page_id = $page_id");
        stop(200);
    }

    function ajax_add_post()
    {
        if (fieldsAreEmpty([
            $_POST['post_title'],
            $_POST['post_content_html'],
        ])) {
            stop(400, __('Missing post title or content.'));
        }

        $data = $_POST;

        $maxFileSize = 3000000; // 3MB
        if (strlen($data['post_content_html']) > $maxFileSize) {
            stop(400, __('Uploaded data exeeds database maximum size.'));
        }

        $data['post_author'] = $this->auth->user_id;

        insert("posts", $data);

        stop(200);
    }

    function ajax_edit_post()
    {
        if (fieldsAreEmpty([
            $_POST['post_title'],
            $_POST['post_content_html'],
        ])) {
            stop(400, __('Missing title or content'));
        }

        $maxFileSize = 3000000; // 3MB
        if (strlen($_POST['post_content_html']) > $maxFileSize) {
            stop(400, __('Uploaded data exeeds database maximum size.'));
        }

        update("posts", $_POST, "post_id = $_POST[post_id]");
        stop(200);
    }

    function ajax_delete_post()
    {
        $post_id = (int)$_POST['post_id'];
        q("DELETE FROM posts WHERE post_id = $post_id");
        stop(200);
    }


    function ajax_change_order_status()
    {
        if (fieldsAreEmpty([$_POST['order_status_id'], $_POST['order_id']])) {
            stop(400, "Missing required data");
        }

        $order_id = $_POST['order_id'];

        // Update the orders status
        Order::setStatus($order_id, $_POST['order_status_id']);

        // Get the updated order
        $order = Order::get($order_id)['order'];

        // Only notify the user if the status was one of these
        if ($order['order_status_id'] == ORDER_STATUS_CANCELLED || $order['order_status_id'] == ORDER_STATUS_WAITING_FOR_CUSTOMER_REVIEW) {
            $order_link = " <a href='" . BASE_URL . "orders/$order_id" . "'>" . 'HERE' . '<a>';

            if ($order['order_status_id'] == ORDER_STATUS_CANCELLED) {
                $subject = PROJECT_NAME . ": Your order has been canceled.";
                $body = __("Your order") . " $order_id " . __("has been canceled!");

            } else {
                $subject = PROJECT_NAME . ": Your order is waiting for you to review it.";
                $body =
                    __("Hello!") . '<br><br>' .
                    __("Your order Nr") . " $order_id " . __("is ready.") . '<br>' .
                    __("Confirm the order or request changes") . ' ' . $order_link . '.<br>' .
                    __("After the order has been confirmed, we can print and send it.");
            }

            Mail::send(
                $order['user_email'],
                __($subject),
                $body
            );
        }

        stop(200);
    }

    function settings()
    {
        $this->settings = get_all("SELECT * FROM settings");
        $this->countries = Countries::get();
        $this->continents = get_all("SELECT * FROM continents");
    }

    function ajax_get_setting()
    {
        stop(200, get_first("SELECT * FROM settings WHERE setting_short_name = '$_POST[setting_short_name]'"));
    }

    function ajax_edit_setting()
    {
        update("settings", $_POST, "setting_short_name = '$_POST[setting_short_name]'");
        stop(200);
    }


    function ajax_get_country()
    {
        $country_id = (int)$_POST['country_id'];
        stop(200, get_first("SELECT * FROM countries WHERE country_id = '$country_id'"));
    }

    function ajax_edit_country()
    {
        $country_id = (int)$_POST['country_id'];

        if ($country_id == -1) {

            // Validate new country data
            $country_name = addslashes($_POST['country_name']);
            $country_code = addslashes($_POST['country_code']);
            $existing_countries_with_same_properties = get_one("SELECT GROUP_CONCAT(country_name SEPARATOR ', ') FROM countries WHERE country_code = '$country_code' OR country_name = '$country_name'");
            if (count($existing_countries_with_same_properties)) {
                stop(400, fillPlaceholders(['%countries%' => $existing_countries_with_same_properties], __("Unable to add country because some countries (%countries%) have same name or code")));
            }

            // Insert new country to database
            insert("countries", $_POST);

        } else {

            update("countries", $_POST, "country_id = '$country_id'");

        }

        stop(200);

    }

    function ajax_delete_country()
    {
        $country_id = (int)$_POST['country_id'];
        $country_users = get_one("SELECT GROUP_CONCAT(user_full_name SEPARATOR ', ') FROM users WHERE country_id = '$country_id'");
        if (count($country_users)) {
            stop(400, fillPlaceholders(['%users%' => $country_users], __("Unable to delete country because some users (%users%) have this country set as their country")));
        }
        stop(200, q("DELETE FROM countries WHERE country_id = '$country_id'"));
    }

    function translations()
    {
        global $supported_languages;

        // Redirect to admin/translations/estonian when no language is specified
        if (empty($this->params[0]) || !in_array($this->params[0], $supported_languages)) {
            header('Location: ' . BASE_URL . 'admin/translations/et/untranslated');
            exit();
        }


        // Redirect to admin/translations/LANGUAGE/untranslated when no translatedness is specified
        if (empty($this->params[1]) || !in_array($this->params[1], ['untranslated', 'translated', 'all'])) {
            header('Location: ' . BASE_URL . 'admin/translations/' . $this->params[0] . '/untranslated');
            exit();
        }


        $language = $this->params[0];
        if ($this->params[1] === 'translated') {
            $translatedness = "AND translation != '{untranslated}'";
        } elseif ($this->params[1] === 'untranslated') {
            $translatedness = "AND translation = '{untranslated}'";
        } else {
            $translatedness = "";
        }


        $this->translations = get_all("SELECT * FROM translations WHERE language='$language' $translatedness ORDER BY appeared DESC, phrase");

        foreach ($this->translations as $index => &$translation) {
            $translation['phrase'] = strip_tags($translation['phrase']);
        }
    }

    function ajax_edit_translation()
    {
        $translation_id = (int)$_POST['translation_id'];
        $key = $_POST['key'];
        $value = HtmlUtils::purify($_POST['value']);

        if (!empty($value)) {
            exit(update('translations', [$key => $value], 'translation_id = ' . $translation_id) ? 'Ok' : 'Fail');
        }
    }

    function ajax_delete_translations()
    {
        $translation_ids = $_POST['translation_ids'];

        Translation::delete($translation_ids);
        stop(200);
    }

    function ajax_delete_file()
    {
        $file_id = $_POST['file_id'];

        File::delete($file_id);
        stop(200);
    }

    function change_mark()
    {
        if (fieldsAreEmpty([
            $_POST["user_id"],
            $_POST["period"],
            $_POST["action"]
        ])) {
            stop(400, 'Invalid input');
        }
        if ($_POST["action"] == 'mark') {
            insert('settled_periods', ['user_id' => $_POST["user_id"], 'period' => $_POST["period"]]);
        } else {
            q("DELETE from settled_periods where period = '$_POST[period]' and user_id = '$_POST[user_id]'");
        }
        stop(200);
    }
}