<?php namespace App;

class files extends Controller
{

    function view()
    {
        $file_id = (int)$this->params[0];

        // Get file metadata from database
        $file = get_first("SELECT * FROM files LEFT JOIN orders ON order_file_id=file_id WHERE file_id = $file_id");

        if (!$file) {
            error_out(__('File not found'), 404);
        }

        // Security
        if (User::isUnprivileged() && User::hasNotMade($file) && User::hasNotUploaded($file) && !File::isVideo($file['file_name'])) {
            Error::permissionDenied();
        }

        // Get file path
        $file_path = File::getUploadsDir() . $file['file_id'];

        // Get file extension
        $file_name = File::generateImageName(
            $file['order_id'],
            $file['file_name']);

        // Check that the file exists
        if (!$file_path) {
            header("HTTP/1.0 404 Not Found");
            exit(__("File doesn't exist"));
        }

        $content_type = mime_content_type($file_path);

        // Add headers to make browser 'download'
        header('Content-Type:' . $content_type);
        //header("Content-Disposition: attachment; filename=\"$file_name\"");

        // Output file
        if ($content_type != 'video/mp4') {
            readfile($file_path);
            exit();
        }


        // Video streaming support (resume download for pause/play and seek). Safari requires this for videos.
        // @ref: https://github.com/videojs/video.js/issues/3728
        header('Content-type: video/mp4');
        header('Content-Disposition: inline; filename="' . $file_name . '"');

        $fp = @fopen($file_path, 'rb');
        $file_size = filesize($file_path); // File size
        $length = $file_size; // Content length
        $start = 0; // Start byte
        $end = $file_size - 1; // End byte

        header("Accept-Ranges: 0-$length");

        if (isset($_SERVER['HTTP_RANGE'])) {
            $c_end = $end;

            // Extract the range string
            list(, $range) = explode('=', $_SERVER['HTTP_RANGE'], 2);

            // Make sure the client hasn't sent us a multibyte range (which we don't support)
            if (strpos($range, ',') !== false) {
                header('HTTP/1.1 416 Requested Range Not Satisfiable');
                header("Content-Range: bytes $start-$end/$file_size");
                exit();
            }

            // If the range starts with an '-', we start from the beginning
            if ($range{0} == '-') {


                $c_start = $file_size - substr($range, 1);

            } else {

                // Split range to array
                $range = explode('-', $range);

                // Get start
                $c_start = $range[0];

                // Get end
                $c_end = (isset($range[1]) && is_numeric($range[1])) ? $range[1] : $file_size;
            }

            /* Check the range and make sure it's treated according to the specs.
             * http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
             */

            // End bytes cannot be larger than $end.
            $c_end = ($c_end > $end) ? $end : $c_end;

            // Validate the requested range and return an error if it's not correct.
            if ($c_start > $c_end || $c_start > $file_size - 1 || $c_end >= $file_size) {
                header('HTTP/1.1 416 Requested Range Not Satisfiable');
                header("Content-Range: bytes $start-$end/$file_size");
                exit();
            }

            // Calculate new content length
            $start = $c_start;
            $end = $c_end;
            $length = $end - $start + 1;

            fseek($fp, $start);

            header('HTTP/1.1 206 Partial Content');
        }

        // Notify the client the byte range we'll be outputting
        header("Content-Range: bytes $start-$end/$file_size");
        header("Content-Length: $length");

        // Start buffered download
        $buffer = 1024 * 8;

        while (!feof($fp) && ($p = ftell($fp)) <= $end) {

            // Make sure we don't read past the end
            if ($p + $buffer > $end) {
                $buffer = $end - $p + 1;
            }

            // Reset time limit for big files
            set_time_limit(0);

            // Read a chunck and output it
            echo fread($fp, $buffer);

            // Prevent tripping PHP memory limit
            flush();

        }

        fclose($fp);

        // Stop loading view
        exit();
    }

    /**
     * @throws \Exception
     */
    function POST_upload()
    {


        // Unset the previous file if the user had chosen one earlier
        if (isset($_SESSION['shopping_cart']['file']['file_id'])) {
            unset($_SESSION['shopping_cart']['file']['file_id']);
        }

        ini_set('memory_limit', '128M');

        // Loop through files and move the chunks to a temporarily created directory
        if (!empty($_FILES)) {
            foreach ($_FILES as $file) {

                // check the error status
                if ($file['error'] != 0) {
                    echo 'error ' . $file['error'] . ' in file ' . $_POST['resumableFilename'];
                    continue;
                }

                if ($file['size'] > MAX_UPLOAD_FILE_SIZE) {
                    error_out('File too big');
                }

                // Init the destination file (format <filename.ext>.part<#chunk>
                // the file is stored in a temporary directory
                $temp_dir = '.tmps/' . $_POST['resumableIdentifier'];
                $dest_file = $temp_dir . '/' . $_POST['resumableFilename'] . '.part' . $_POST['resumableChunkNumber'];

                // Create the temporary directory
                if (!is_dir($temp_dir)) {
                    mkdir($temp_dir, 0777, true);
                }

                // Move the temporary file
                if (!move_uploaded_file($file['tmp_name'], $dest_file)) {

                    throw new \Exception('Error saving (move_uploaded_file) chunk ' . $_POST['resumableChunkNumber'] . ' for file ' . $_POST['resumableFilename'], 500);

                } else {
                    // Check if all the parts present, and create the final destination file
                    $this->createFileFromChunks(
                        $temp_dir,
                        $_POST['resumableFilename'],
                        $_POST['resumableTotalSize']
                    );
                }
                exit();
            }
        }
    }

    /**
     *
     * Check if all the parts exist, and
     * gather all the parts of the file together
     * @param string $temp_dir - the temporary directory holding all the parts of the file
     * @param string $file_name - the original file name
     * @param string $file_original_size - original file size (in bytes)
     * @return bool
     */
    private function createFileFromChunks($temp_dir, $file_name, $file_original_size)
    {

        $destination_dir = '.uploads/';
        $folder_id = null;
        $size_of_total_files_on_server = 0;
        $temp_total = 0;
        $count_of_files = 0;

        // Calculate total size of part-files on the server
        foreach (scandir($temp_dir) as $file) {
            // Skip . and ..
            if ($file == '.' || $file == '..') {
                continue;
            }

            // Add
            $count_of_files++;
            $temp_total = $size_of_total_files_on_server;
            $temp_filesize = filesize($temp_dir . '/' . $file);
            $size_of_total_files_on_server = $temp_total + $temp_filesize;
        }

        // Check that all the parts are present
        if ($size_of_total_files_on_server >= $file_original_size) {


            // Create a new file record in database
            $file_id = insert('files',
                [
                    'file_name' => $file_name,
                    'file_session_id' => session_id(),
                    'file_uploaded_by' => isset($this->auth->user_id) ? $this->auth->user_id : null,
                    'file_is_video' => empty($_GET['file_is_video']) ? 0 : 1
                ]
            );

            // Check if path exists
            if (!is_dir($destination_dir)) {
                mkdir($destination_dir, 0777, true);
            }

            // Compose full file from chunks, into $file_content
            // Create the final destination file (name it by file_id)
            if (($fp = fopen($destination_dir . $file_id, 'w')) !== false) {

                for ($i = 1; $i <= $count_of_files; $i++) {
                    fwrite($fp, file_get_contents(__DIR__ . '/../' . $temp_dir . '/' . $file_name . '.part' . $i));
                }

                fclose($fp);

            } else {
                return false;
            }

            // Mark file as uploaded
            update('files', ['file_upload_completed' => 1], "file_id = $file_id");

            // Rename the temporary directory (to avoid access from other
            // concurrent chunks uploads) and then delete it
            if (rename("$temp_dir", "$temp_dir" . '_UNUSED')) {
                File::rrmdir($temp_dir . '_UNUSED');
            } else {
                File::rrmdir($temp_dir);
            }

            $preview_response = File::generate_preview(
                $file_id,
                File::getDelegate($file_name));

            if ($preview_response['code'] == 500) {
                error_out($preview_response['data']);
            }

            if (isset($this->params[0]) && is_numeric($this->params[0])) {
                update('orders', [
                    'order_file_id' => $preview_response['data']['file_id']
                ], "order_id = {$this->params[0]}");
                $_SESSION['order_id'] = $this->params[0];
            } else {

                $_SESSION['shopping_cart']['file'] = $preview_response['data'];
            }

        }
        exit();
    }


    public function ajax_get_file_meta_data_from_session()
    {
        if (empty($_SESSION['shopping_cart']['file'])) {
            stop(404);
        }
        stop(200, $_SESSION['shopping_cart']['file']);
    }


    /**
     * @throws \Exception
     */
    public function download_zip()
    {
        $zip = new \ZipArchive();
        $generated_pdf_files = [];
        $tmp_path = File::getTmpPath();
        $user_is_unprivileged = User::isUnprivileged(); // Cache SQL result to variable
        $order_ids = explode(',', $_GET['i']);
        $file_content_type = isset($_GET['type']) && $_GET['type'] == 'images' ? 'images' : 'invoices';
        $zip_file_name = File::generateName($order_ids, $file_content_type, 'zip');
        $full_path_to_zip_file = $tmp_path . $zip_file_name;
        $uploads_folder = dirname(__DIR__) . "/.uploads/";

        // Create a new zip file
        $zip->open($full_path_to_zip_file, \ZipArchive::CREATE);

        try {

            foreach ($order_ids as $order_id) {

                // Check that order id is a number
                if (!is_numeric($order_id)) {
                    throw new \Exception("Order ID $order_id is not numeric", 500);
                }

                // Get the order
                $order = get_first("SELECT * FROM orders LEFT JOIN files ON order_file_id = file_id WHERE order_id = $order_id");

                // Make sure the order exists
                if (empty($order)) {
                    throw new \Exception("An order with an ID of $order_id could not be found", 404);
                }

                // Security check
                if ($user_is_unprivileged && User::hasNotMade($order)) {

                    // Instead of Error::permissionDenied(), we throw an exception here to clean up files
                    throw new \Exception(__('Permission denied'), 403);
                }

                switch ($file_content_type) {

                    case 'images':

                        // Extract extension from file name
                        $file_ext = pathinfo($order['file_name'], PATHINFO_EXTENSION);

                        // Compose full path to file
                        $full_path_to_uploaded_file = $uploads_folder . $order['file_id'];

                        // Interrupt the process and alarm developers when an order with non-existing file is found
                        if (!file_exists($full_path_to_uploaded_file)) {
                            throw new \Exception("An order ($order_id) with non existing file!", 500);
                        }

                        // Add file to ZIP
                        $zip->addFile(
                            $full_path_to_uploaded_file,
                            File::generateName($order_id, 'image', $file_ext));

                        break;


                    case 'invoices':

                        // Check if invoice exists
                        if (empty($order['simplbooks_invoice_id'])) {
                            throw new \Exception("Order $order_id does not yet have an invoice", 404);
                        }

                        // Compose file name from Invoice ID
                        $file_name = File::generateName($order_id, 'invoice');

                        // Create invoice PDF to .tmps folder
                        File::createFileToTmp($file_name, Invoice::generateContent($order['simplbooks_invoice_id']));

                        // Add the invoice PDF to the ZIP
                        $zip->addFile($tmp_path . $file_name, $file_name);

                        // Add the pdf file to the pdf files array to be removed after we close the ZIP
                        $generated_pdf_files[] = $tmp_path . $file_name;

                        break;
                }
            }
        } catch (\Exception $e) {

            // Clean up temp files
            $zip->close();
            unlink($full_path_to_zip_file);
            if (!empty($generated_pdf_files)) {
                foreach ($generated_pdf_files as $full_path_to_generated_pdf_file) {
                    unlink($full_path_to_generated_pdf_file);
                }
            }

            if ($e->getCode() >= 400 && $e->getCode() <= 499) {

                // Display user error to user
                error_out($e->getMessage(), $e->getCode());

            } else {

                // Rethrow system errors upwards (will be emailed to developer)
                throw $e;
            }
        }

        // Close the archive
        $zip->close();

        // Remove the generated PDF files if there were any
        if (!empty($generated_pdf_files)) {
            foreach ($generated_pdf_files as $full_path_to_generated_pdf_file) {
                unlink($full_path_to_generated_pdf_file);
            }
        }

        // Turn off all output buffers
        while (ob_get_level() > 0) {
            ob_end_flush();
        }

        // Add headers to force browser to 'download' the file
        header("Content-type: application/download");
        header("Content-Disposition: attachment; filename=\"$zip_file_name\"");
        header("Content-length: " . filesize($full_path_to_zip_file));
        header("Pragma: no-cache");
        header("Expires: 0");

        // Output file
        readfile($full_path_to_zip_file);

        // Delete the file
        unlink($full_path_to_zip_file);


        // Upgrade order status when printing house downloads images
        if ($this->auth->is_printing_house && $file_content_type == 'images') {
            update('orders', [
                'order_status_id' => ORDER_STATUS_IN_PRODUCTION
            ], 'order_status_id = ' . ORDER_STATUS_QUEUED_FOR_PRINTING . ' AND order_id IN(' . implode(',', $order_ids) . ')');
        }


        // Stop loading a view
        exit();
    }


    /**
     * @used-by views/users/users_view.php
     * @throws \Exception
     */
    public function download_invoice()
    {


        $simplbooks_invoice_id = $this->getId();

        $order = Order::getBySimplbooksInvoiceId($simplbooks_invoice_id);

        // Security
        if (User::isUnprivileged() && User::hasNotMade($order)) {
            Error::permissionDenied();
        }

        $filename = File::generateName($order['order_id']);
        $content = Invoice::generateContent($simplbooks_invoice_id);

        ob_clean();

        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Description: File Transfer");
        header("Content-Length: " . strlen($content));

        echo $content;
        exit();
    }


}