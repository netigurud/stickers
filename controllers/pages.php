<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 14.03.2018
 * Time: 10:16
 */

namespace App;


class pages extends Controller
{
    function view()
    {
        $this->page = Page::get($this->params[0]);
    }

    function ajax_get()
    {
        stop(200, Page::get($_POST['page_id']));
    }
}