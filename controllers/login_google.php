<?php namespace App;

class login_google extends Controller
{
    public $requires_auth = false;

    function index()
    {
        $url = $this->getGoogleLoginURL();
        // Redirect user to Google login page
        header("Location: " . $url);
        exit();
    }


    function modal()
    {
        $url = $this->getGoogleLoginURL(true);
        // Redirect user to Google login page
        header("Location: " . $url);
        exit();
    }


    function callback()
    {
        $modal = empty($this->params[0]) ? false : true;

        $case = isset($_GET['error']) ? 'error' : 'sign_in';
        switch ($case) {

            case 'error':

                if (isset($_SESSION['redirect'])) {
                    header('Location: ' . BASE_URL . $_SESSION['redirect']);
                    unset($_SESSION['redirect']);
                } else {
                    header('Location: ' . BASE_URL);
                }

                exit();
                break;

            case 'sign_in':

                // Process google output into object
                $authObj = $this->get_authObj($modal);

                // If google returned an object which contains error property show it and quit
                if (isset($authObj->error)) die('Error: ' . $authObj->error . ' ' . $authObj->error_description);

                // Extract user data from google object
                $google_user = $this->get_user_data($authObj);

                // Check for errors
                if(!empty($google_user->error)){
                    throw new \Exception(json_encode($google_user));
                }

                // Get email
                $email = empty($google_user->hd) ?
                    $google_user->email . '@gmail.com' : $google_user->email . '@' . $google_user->hd;

                // Check if user with this email already exists
                $user = get_first("SELECT * FROM users
                                     WHERE user_email = '$email'
                                     AND deleted = 0");

                $now = date('Y-m-d H:i:s');

                // User doesn't exist
                if (empty($user['user_id'])) {

                    // Generate random password
                    $password = User::generateRandomPassword();

                    // Insert the user
                    $user = array(
                        'user_full_name' => $google_user->name,
                        'user_email' => $email,
                        'password' => $password['hash'],
                        'user_created_at' => $now,
                        'user_last_visit' => $now);

                    $user['user_id'] = insert('users', $user);

                    $body =
                        __('Thank you for choosing Letsticker.com.') . '<br><br>' .
                        __('Your email is:') . ' ' . $email . ' ' .
                        __('and your password is:') . ' ' . $password['password'] . '<br>' .
                        __('You can change your password by clicking Forgot password? in the login window');

                    Mail::send($email, __('Letsticker.com registration'), $body);

                } else {
                    // User existed, update user's last visit time
                    update("users", ['user_last_visit' => $now], "user_email = '$email' and deleted = 0");
                }

                // Log the user in
                Session::login($user['user_id']);

                // Close this tab, if user logged in from popup
                if ($modal) {
                    ob_clean();
                    require 'templates/partials/modal_callback.php';
                    exit();
                }

                // Load user's data and return
                $this->auth->load_user_data($user);

                header('Location: ' . BASE_URL);
                exit();

        }
    }

    private function get_user_data($authObj)
    {

        // Get user data
        $dataurl = "https://www.googleapis.com/userinfo/v2/me?access_token={$authObj->access_token}";
        $curl = curl_init($dataurl);

        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Bearer {$authObj->access_token}"));
        $json_response = curl_exec($curl);
        curl_close($curl);

        // Decode Google JSON response into PHP object
        $user = json_decode($json_response);

        if(!empty($user->error)){
            throw new \Exception($json_response);
        }

        // Add email into object
        $user->email = substr($user->email, 0, strpos($user->email, '@'));

        // Return user object
        return $user;
    }


    private function get_authObj($modal = false)
    {

        $modal = empty($modal) ? '' : '/modal';

        // Get refresh_token and access_token from Google
        $params = array(
            "code" => $_GET['code'],
            "client_id" => GOOGLE_CLIENT_ID,
            "client_secret" => GOOGLE_CLIENT_SECRET,
            "redirect_uri" => BASE_URL . GOOGLE_REDIRECT_URI . $modal,
            "grant_type" => "authorization_code"
        );
        $curl = curl_init('https://accounts.google.com/o/oauth2/token');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $json_response = curl_exec($curl);
        curl_close($curl);
        $authObj = json_decode($json_response);
        return $authObj;
    }

    private function getGoogleLoginURL($modal = false)
    {

        $modal = empty($modal) ? '' : '/modal';

        // Set some variables
        $request_params = array(
            "response_type" => "code",
            "client_id" => GOOGLE_CLIENT_ID,
            "redirect_uri" => BASE_URL . GOOGLE_REDIRECT_URI . $modal,
            "access_type" => "offline",
            "approval_prompt" => "force",
            "scope" => "openid profile email",
        );

        return "https://accounts.google.com/o/oauth2/auth?" . http_build_query($request_params);

    }


}