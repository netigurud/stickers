<?php namespace App;


class welcome extends Controller
{

    /**
     * This is a normal action which will be called when user visits /welcome/index URL. Since index is the default
     * action name, it may be omitted (URL can be /welcome). Since welcome is by default the default controller, it may
     * be omitted (URL can be /). After calling the action, a view from views/controller-name/controller-name_action-name.php
     * is loaded (it must exist, unless the function ends with stop() call.
     */
    function index()
    {
        $this->prices = json_encode(get_all("SELECT * FROM prices"));
        $this->posts = Post::getAll();
        $this->shipping = Page::get(2);
    }

    function ajax_send_new_password()
    {
        $email = addslashes($_POST['email']);

        if (Mail::hasValidDomain($email)) {

            if (!User::emailExists($email)) {
                stop(404, __('Unknown email address'));
            }

            // Generate a new password and update database
            $password = generateRandomString();
            update('users', [
                'password' => password_hash($password, PASSWORD_DEFAULT)
            ], "user_email = '$email'");

            // Send the new password
            $url = BASE_URL;
            $body = nl2br(__("Hello!") . "\n\n" .
                __("You recently wanted us to send you a new password. Your new password is") . " " . $password . "\n\n" .
                __("Take good care of it!") . "\n\n" . "<a href='$url'>$url</a>");

            Mail::send($email, __('Your new password'), $body);

            stop(200, __("Your new password has been sent to you"));
        }

        stop(400, __("Invalid email address"));
    }

}