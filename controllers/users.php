<?php namespace App;

class users extends Controller
{

public $requires_auth = true;



    function ajax_get()
    {
        stop(200, User::get($_POST['user_id']));
    }

    function ajax_edit()
    {
        $user = User::get();
        $requestedUserPassword = $_POST['requested_user_password'];

        // Check that the requested password matches the user's current password
        if (!password_verify($requestedUserPassword, $user['password'])) {
            stop(400, __("Wrong password"));
        }

        // Check for valid data
        if (fieldsAreEmpty([$_POST['user_full_name']])) {
            stop(400, __("Invalid name"));
        }

        // Removes password from the end of the array
        array_pop($_POST);

        // Update the user
        update("users", $_POST, "user_id = $_SESSION[user_id]");

        // Everything is OK
        stop(200);
    }

    function ajax_edit_password()
    {
        // Check for valid data
        if (fieldsAreEmpty([$_POST['current_password'], $_POST['new_password'], $_POST['new_password_retyped']])) {
            stop(400, __("Missing required data"));
        }

        $user = User::get();
        $currentPassword = $_POST['current_password'];
        $newPassword = $_POST['new_password'];
        $newPasswordRetyped = $_POST['new_password_retyped'];

        // Check that current passwords match
        if (!password_verify($currentPassword, $user['password'])) {
            stop(400, __("Current password doesn't match"));
        }

        // Check that new passwords match
        if ($newPassword !== $newPasswordRetyped) {
            stop(400, __("New passwords don't match"));
        }

        // Hash the password
        $hashedPassword = password_hash($newPassword, PASSWORD_DEFAULT);

        // Update the user
        update("users", ["password" => $hashedPassword], "user_id = $_SESSION[user_id]");

        // Everything is OK
        stop(200);
    }

    function ajax_delete()
    {
        $user_id = $this->auth->user_id;
        User::markForDeletion($user_id);
        stop(200);
    }
}