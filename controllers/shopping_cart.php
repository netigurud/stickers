<?php namespace App;

class shopping_cart extends Controller
{

    function ajax_update()
    {
        if (fieldsAreEmpty([
            $_POST['product_height'],
            $_POST['product_width'],
            $_POST['quantity'],
            $_POST['product_type']
        ])) {
            stop(400, __("Missing height, width or quantity"));
        }

        $width = $_POST['product_height'];
        $height = $_POST['product_width'];
        $quantity = $_POST['quantity'];
        $product_type_id = $_POST['product_type'] == 'stickers' ? PRODUCT_TYPE_STICKER : PRODUCT_TYPE_MAGNET;

        $_SESSION['shopping_cart']['products'][] = [
            'product_height' => $height,
            'product_width' => $width,
            'product_price' => Product::getPrice($width * $height, $quantity, $product_type_id),
            'quantity' => $quantity,
            'product_type_id' => $product_type_id
        ];

        // Everything is OK
        stop(200);
    }

    function ajax_update_quantity()
    {
        if (fieldsAreEmpty([
            $_POST['quantity']
        ])) {
            stop(400, __("Incorrect quantity"));
        }

        $price = Product::getPrice($_SESSION['shopping_cart']['products'][$_POST['key']]['product_width'] * $_SESSION['shopping_cart']['products'][$_POST['key']]['product_height'], $_POST['quantity'], $_SESSION['shopping_cart']['products'][$_POST['key']]['product_type_id']);

        $_SESSION['shopping_cart']['products'][$_POST['key']]['product_price'] = $price;
        $_SESSION['shopping_cart']['products'][$_POST['key']]['quantity'] = $_POST['quantity'];
        stop(200);
    }


    function ajax_listPrices()
    {
        $product_type = $_POST['product_type'] == 'magnets' ? 2 : 1;
        $tax = $_POST['tax'];
        $list = Quantity::listPrices($product_type, $_POST['m2']);
        $response = [];
        foreach ($list as $i => $checkbox) {
            $price = $checkbox['price'];
            if (!empty($tax)) {
                $price = number_format($checkbox['price'] * ($tax / 100 + 1), 2, ".", ",");
            }
            $response[] = '<div class="mate-checkbox" data-type="quantity" data-quantity_sum="' . $checkbox['quantity'] . '"><input type="radio" id="' . $_POST['product_type'] . '-quantity-option-' . $i . '" name="' . $_POST['product_type'] . '-quantity-option" class="' . $_POST['product_type'] . '-quantity-option" value="' . $checkbox['quantity'] . '"><label for="' . $_POST['product_type'] . '-quantity-option-' . $i . '">' .
                '<span class="quantity-sum">' . $checkbox['quantity'] . '</span> <span class="quantity-price">' . $price . '</span> </label><div class="check"></div></div>';
        }
        stop(200, $response);

    }

    function ajax_remove_row()
    {
        unset($_SESSION['shopping_cart']['products'][$_POST['key']]);

        // Remove the file if the only row in shopping cart is removed
        if (empty($_SESSION['shopping_cart']['products'])) {
            unset($_SESSION['shopping_cart']['file']);
        }
        stop(200);
    }

    function ajax_check_promo_code()
    {
        if ($promo_code = PromoCode::get($_POST['promo_code'])) {
            stop(200, $promo_code);
        } else {
            stop(400, __("Promo code doesn't exist or has expired"));
        }
    }

    function ajax_update_file()
    {
        $_SESSION['shopping_cart']['file'] = File::get($_POST['file_id']);
        stop(200);
    }
}