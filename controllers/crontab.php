<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 06.02.2018
 * Time: 11:54
 */

namespace App;


class crontab extends Controller
{
    function index()
    {
        $this->update_holidays();
    }

    function update_holidays()
    {
        $url = "https://xn--riigiphad-v9a.ee/?output=json";
        $holidays = json_decode(file_get_contents($url), true);
        if (is_array($holidays) AND !empty($holidays)) {
            q("DELETE FROM holidays");
            foreach ($holidays as $holiday) {
                insert("holidays", $holiday);
            }
        }

        exit();
    }

    /**
     * @throws \Exception
     */
    function set_order_status_to_shipped()
    {
        foreach (Order::getShippedOrders() as $order) {
            if (strtotime($order['ship_date']) <= strtotime(date('Y-m-d'))) {
                Order::setStatus($order['order_id'], ORDER_STATUS_SHIPPED);
                $body = str_replace("?", $order['order_id'], __("Your products for order #? have been shipped. You can track the package HERE"));
                $body = str_replace("HERE", '<a href="https://www.tnt.com/express/en_us/site/tracking.html?searchType=con&cons='.$order['order_con_number'].'">HERE</a>.', $body);
                Mail::send(
                    $order['user_email'],
                    PROJECT_NAME . ": " . __("Your order has been shipped."),
                    __("Hello!") . '<br><br>' .
                $body
                );
            }
        }

        exit();
    }

    function notify_admin_if_designer_forgot_to_approve_order()
    {

        // Get a list of orders with status "Waiting for designer review" and last status change was more than 65 h ago
        $orders = Order::getByCriteria([
            "order_status_id = " . ORDER_STATUS_WAITING_FOR_DESIGNER_REVIEW,
            "order_status_changed_at < " . date("'Y-m-d H:i:s'", strtotime('65 hours ago'))], true);
        // Send notification email to admin
        Mail::send(SITE_OWNER_EMAIL,
            __('Notification about absent-minded designer'),
            __('The following orders have been waiting for designer review for more than 65 h')
            . ':<pre>' . json_encode($orders, JSON_PRETTY_PRINT) . '</pre>');
        exit();
    }

    function permanently_delete_deleted_users()
    {

        // Iterate through all users who have been deleted more than 30 days ago (deleted=1)
        foreach (User::getDeleted() as $deleted_user) {

            // Delete all files of this user
            User::deleteFiles($deleted_user['user_id']);

            // Delete the order
            User::deleteOrders($deleted_user['user_id']);

            // Delete the user
            User::delete($deleted_user['user_id']);
        }

        exit();
    }
}