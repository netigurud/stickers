<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 10.02.2018
 * Time: 12:25
 */

namespace App;


use Braintree_Gateway;
use Braintree_Transaction;
use Exception;

class orders extends Controller
{
    public $requires_auth = true;


    function index()
    {
        // Allow the user on this page if they have admin, printing house or designer rights
        if (User::isUnprivileged()) {
            Error::permissionDenied();
        }

        $this->settings = Settings::getValues();
        $this->collection_time = TNT::getNextAvailableCollectionTime();
        $this->is_printing_house = (int)$this->auth->is_printing_house;
        $this->is_designer = (int)$this->auth->is_designer || $this->auth->is_admin;
        $this->order_statuses = Order::getStatuses();

        // Get corresponding orders depending on the users rights
        $this->orders = Order::get(
            false,
            false,
            $this->is_printing_house,
            $this->auth->is_designer
        );
        $this->countries = Countries::get(['country_active' => 1]);
    }

    function view()
    {
        $this->order = Order::get($this->params[0]);

        if (!$this->auth->is_admin) {
            if ($this->order['order']['order_status_id'] != ORDER_STATUS_WAITING_FOR_CUSTOMER_REVIEW && $this->order['order']['order_status_id'] != ORDER_STATUS_WAITING_FOR_DESIGNER_REVIEW) {
                error_out(__("You can't access this page unless the orders status is waiting for your review or you've requested changes."));
            }

            if ($this->auth->user_id != $this->order['order']['order_made_by']) {
                error_out(__("This isn't your order."));
            }
        }
    }

    function ajax_get()
    {
        stop(200, Order::get($_POST['order_id']));
    }

    function ajax_get_reorder_modal()
    {
        $order = Order::get($_POST['order_id']);
        $quantities[PRODUCT_TYPE_STICKER] = Quantity::get(PRODUCT_TYPE_STICKER);
        $quantities[PRODUCT_TYPE_MAGNET] = Quantity::get(PRODUCT_TYPE_MAGNET);

        ob_start();
        require 'templates/partials/reorder_modal.php';

        stop(200, ['html' => ob_get_clean(), 'order_rows' => $order['order_rows']]);
    }


    function ajax_set_order_waiting_for_customer_review()
    {
        if (User::isUnprivileged()) {
            Error::permissionDenied();
        }

        if (fieldsAreEmpty([$_POST['order_id']])) {
            stop(400, __("Can't find selected order. Contact the admin."));
        }

        $order_id = $_POST['order_id'];

        // Update the order's status
        update("orders", [
            "order_taken_by_designer" => $_SESSION['user_id']
        ], "order_id = $order_id");
        Order::setStatus($order_id, ORDER_STATUS_WAITING_FOR_CUSTOMER_REVIEW);

        // Get the updated order
        $order = Order::get($order_id);

        $order_a_href = " <a href='" . BASE_URL . "orders/$order_id" . "'>" . __("HERE") . "<a>";

        Mail::send(
            $order['order']['user_email'],
            PROJECT_NAME . ": " . __("Your order is waiting for you to review it."),
            __("Hello!") . '<br><br>' .
            __('Your order nr') . " $order_id " . __("is ready.") . '<br>' .
            __('Confirm the order or request changes') . $order_a_href . '.<br>' .
            __('After the order has been confirmed, we can print and send it.')
        );

        stop(200);
    }

    function generate_shipping_documentation()
    {
        if (fieldsAreEmpty([$_POST['order_id']])) {
            stop(400, __("Can't find selected order. Contact the admin."));
        }

        $order_id = $_POST['order_id'];
        $order = Order::get($order_id);

        try {
            $xml_in = TNT::fillXML($order, User::get($order['order']['order_made_by']));

            // The initial POSTing of XML to EC server (contains xml request)
            $result_tab = curlPost(['xml_in' => $xml_in]);

            // Ask TNT to check that there weren't any errors in the previous query
            $result_tab2 = curlPost(['xml_in' => "GET_RESULT:" . preg_replace("/[^0-9]/", "", $result_tab)]);

            // Get documentation (connote, label, manifest and invoice)
            $tabs['connote'] = curlPost(['xml_in' => "GET_CONNOTE:" . preg_replace("/[^0-9]/", "", $result_tab)]);
            $tabs['label'] = curlPost(['xml_in' => "GET_LABEL:" . preg_replace("/[^0-9]/", "", $result_tab)]);
            $tabs['manifest'] = curlPost(['xml_in' => "GET_MANIFEST:" . preg_replace("/[^0-9]/", "", $result_tab)]);
            $tabs['invoice'] = curlPost(['xml_in' => "GET_INVOICE:" . preg_replace("/[^0-9]/", "", $result_tab)]);


            // Encode <CITY> to UTF-8
            foreach ($tabs as $k => $v) {
                $tabs[$k] = TNT::encodeOrDecodeCdata($v, $mode = 'encode');
            }

            preg_match('#<CONNUMBER>([A-Za-z0-9]+)</CONNUMBER>#', $tabs['label'], $matches);

            // Update the orders status
            update("orders", [
                "order_status_id" => ORDER_STATUS_WAITING_FOR_DELIVERY,
                "order_connote_xml" => $tabs['connote'],
                "order_label_xml" => $tabs['label'],
                "order_manifest_xml" => $tabs['manifest'],
                "order_invoice_xml" => $tabs['invoice'],
                "order_con_number" => $matches[1],
            ], "order_id = $order_id");

            Mail::send(
                $order['order']['user_email'],
                PROJECT_NAME . ": " . __("Your order is waiting to be shipped."),
                __("Hello!") . '<br><br>' .
                str_replace("?", $order_id, __("Your products for order #? have been created and are waiting to be shipped."))
            );

            stop(200);

        } catch (\Exception $e) {
            if (substr($e->getMessage(), 0, 5) == '<?xml') {
                // Beautify XML
                $xml = new \DOMDocument();
                $xml->preserveWhiteSpace = false;
                $xml->formatOutput = true;
                $xml->loadXML($e->getMessage());
                $message = $xml->saveXML();

            } else {
                $message = $e->getMessage();
            }

            $message = $message . "\n---\nxml_in=" . $xml_in;
            $message = TNT::replaceTagsWithHtmlEntities($message);
            //$message = htmlentities($message);
            $message = str_replace(' ', '&nbsp;', $message);
            $message = str_replace("\n---\n", '<hr>', $message);
            $message = nl2br($message);
            stop(400, $message, true);
        }
    }

    function download_shipping_documentation()
    {
        $order = Order::get($this->getId())['order'];
        $pdf = PDF::orderManifestAndLabel([
            showXMLasHTML($order['order_label_xml']),
            showXMLasHTML($order['order_manifest_xml'], true)
        ]);

        exit($pdf->Output('pdf.pdf', 'D'));
    }

    /**
     * @throws Exception
     */
    function ajax_approve()
    {
        if (fieldsAreEmpty([$_POST['order_id']])) {
            stop(400, "Missing required data");
        }

        $order_data = Order::get($_POST['order_id']);
        $order = $order_data['order'];
        $order_id = $order['order_id'];
        $order_rows = $order_data['order_rows'];
        $user = User::get($order['order_made_by']);

        if (empty($order['order_braintree_id'])) {
            throw new \Exception('Missing braintree customer ID');
        }

        // Get the orders sum with given promo code and VAT percent applied
        $order_sum = Order::getTotalWithVAT(
            $order_rows,
            $order['order_promo_code'],
            $order['order_vat_percent']
        );

        // Configure the environment and API credentials
        $braintree_gateway = new Braintree_Gateway([
            'environment' => BT_ENVIRONMENT,
            'merchantId' => BT_MERCHANT_ID,
            'publicKey' => BT_PUBLIC_KEY,
            'privateKey' => BT_PRIVATE_KEY
        ]);

        $braintree_order_id = $order_data['order']['order_made_at'] . '_'. $order['order_id']; // order_id


        // Create a transaction using a payment method stored in the Vault by passing customerId
        $braintree_sale_result = $braintree_gateway->transaction()->sale([
            'customerId' => $order['order_braintree_id'],
            'amount' => $order_sum,
            'orderId' => $braintree_order_id,
            'options' => [
                'submitForSettlement' => True
            ],
            'lineItems' => Invoice::generateBraintreeTransactionSaleLineItems($order_rows)

        ]);


        // Stop if there are any Braintree errors
        Braintree::checkForErrors($braintree_sale_result);


        try {
            $simplbooks_client_id = SimplBooks::getOrCreateClient($order);

            // Create the invoice
            $invoice = SimplBooks::createInvoice(
                Order::getSimplbooksInvoiceRows($order_id),
                $order,
                $simplbooks_client_id
            );

            // Something went wrong while creating the SimplBooks invoice, exit and show the error
            if ($invoice->status != 200) {
                stop(400, $invoice->errors . file_get_contents('curl_last.txt'));
            }

            // Set the invoice ID for this order
            update("orders", [
                "simplbooks_invoice_id" => $invoice->inserted_id
            ], "order_id = $order_id");

            Order::setStatus($order_id, ORDER_STATUS_QUEUED_FOR_PRINTING);

            // Send the invoice to the client
            Mail::sendInvoice($user['user_email'], $order_id, $invoice);

            // Let the printing house know to start printing
            Mail::notifyPrintingHouse($order_id);

        } catch (\Exception $e) {
            stop(500, $e->getMessage());
        }

        stop(200);


    }

    /**
     * When the user with printing house rights downloads the image, set the orders status to "ORDER_STATUS_IN_PRODUCTION"
     * @throws Exception
     */
    function ajax_send_to_print_house()
    {
        if (!is_numeric($_POST['order_id'])) {
            throw new \Exception("Order ID ($_POST[order_id]) is not numeric!");
        }

        update("orders", ["order_status_id" => ORDER_STATUS_IN_PRODUCTION], "order_id = $_POST[order_id] and order_status_id < " . ORDER_STATUS_IN_PRODUCTION);
        stop(200);
    }

    /**
     * Let the designer set the final width and height
     */
    function ajax_set_final_measurements()
    {
        update("order_rows", [
            'final_width' => $_POST['final_width'],
            'final_height' => $_POST['final_height']
        ], "order_row_id = $_POST[order_row_id]");

        stop(200);
    }

    /**
     * Let the user request changes for their order
     * @throws Exception
     */
    function ajax_request_changes()
    {
        if (fieldsAreEmpty([
            $_POST['order_id'],
            $_POST['request_changes_reason']
        ])) {
            stop(400, __('Missing order id or the reason why the user requested changes'));
        };

        $order_id = (int)$_POST['order_id'];

        // Update the orders status
        update("orders", [
            'request_changes_reason' => $_POST['request_changes_reason']
        ], "order_id = $order_id");
        Order::setStatus($order_id, ORDER_STATUS_WAITING_FOR_DESIGNER_REVIEW);


        // Get the designer who's connected to this order
        $designer_id = Order::get($order_id)['order']['order_taken_by_designer'];
        $designer = User::get($designer_id);

        $orders_url = BASE_URL . "orders";

        // Notify the designer that the user requested changes
        Mail::send(
            $designer['user_email'],
            PROJECT_NAME . ": " . __("User requested changes."),
            __("Hello!") . '<br><br>' .

            __("User requested changes to order ID") . " $order_id: " . '"' . $_POST['request_changes_reason'] . '".<br>' .
            __("Go to orders by clicking") . " <a href='" . $orders_url . "'>" . __('HERE') . '<a>.',
            false,
            false,
            ADMIN_EMAIL
        );

        stop(200);
    }

    function ajax_reorder()
    {
        if (empty($_POST['order_id']) || !is_numeric($_POST['order_id'])) {
            stop(400, __('Missing order_id'));
        }

        $previous_order = Order::get($_POST['order_id']);

        if ($previous_order['order']['user_id'] != $this->auth->user_id) {
            stop(403, __('Forbidden'));
        }

        foreach ($previous_order['order_rows'] as $order_row) {
            $_SESSION['shopping_cart']['products'][] = [
                'product_height' => $order_row['height'],
                'product_width' => $order_row['width'],
                'product_price' => $order_row['price'],
                'quantity' => $order_row['quantity'],
                'product_type_id' => $order_row['product_type_id']
            ];
        }

        $_SESSION['shopping_cart']['file'] = File::get($previous_order['order']['order_file_id']);

        stop(200);
    }


    function ajax_set_shipping_details()
    {
        if (fieldsAreEmpty($_POST['address'])) {
            stop(400, "At least one address field is empty");
        }

        TNT::checkCollectionTimes($_POST['ship_date'], $_POST['collection_time_from'], $_POST['collection_time_to']);

        update("orders", [
            "ship_date" => $_POST['ship_date'],
            "collection_time_from" => $_POST['collection_time_from'],
            "collection_time_to" => $_POST['collection_time_to'],
            "country_id" => $_POST['address']['country_id'],
            "order_state" => $_POST['address']['order_state'],
            "order_dial_code" => $_POST['address']['order_dial_code'],
            "order_street" => $_POST['address']['order_street'],
            "order_city" => $_POST['address']['order_city'],
            "order_phone" => $_POST['address']['order_phone'],
            "order_postal_code" => $_POST['address']['order_postal_code'],
            "order_full_name" => $_POST['address']['order_full_name'],
            "order_company" => $_POST['order_company'],
            "order_company_reg_nr" => $_POST['order_company_reg_nr'],
        ], "order_id = $_POST[order_id]");

        // Save settings as defaults for next time
        foreach ($_POST['dimensions'] as $name => $value) {
            update("settings", ['setting_value' => $value], "setting_short_name = '$name'");
        }

        stop(200);
    }

    function ajax_take()
    {
        $order_id = $_POST['order_id'];

        // Set order taken by designer
        update("orders", [
            "order_taken_by_designer" => $_SESSION['user_id']
        ], "order_id = $order_id");
        stop(200);
    }
}