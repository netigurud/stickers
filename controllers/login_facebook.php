<?php namespace App;

use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;

class login_facebook extends Controller
{
    public $requires_auth = false;

    function callback()
    {
        $this->_callback();
        header('Location: ' . BASE_URL);
        exit();
    }

    function modal_callback()
    {
        $this->_callback();

        // Execute a JS function from within a parent window which hides this modal and submits the post
        require 'templates/partials/modal_callback.php';

        exit();
    }

    function _callback()
    {

        try {
            $fb = new Facebook([
                'app_id' => FACEBOOK_APP_ID,
                'app_secret' => FACEBOOK_SECRET,
                'default_graph_version' => 'v2.5',
            ]);

            $helper = $fb->getRedirectLoginHelper();

            $accessToken = $helper->getAccessToken();

            // When Graph returns an error
            if (!isset($accessToken)) {
                if ($helper->getError()) {
                    // If user clicked "Cancel" then redirect to home page
                    if ($helper->getErrorCode() == 200) {
                        redirect();
                    }

                    $error_message = "Error: " . $helper->getError() . "<br>";
                    $error_message .= "Error Code: " . $helper->getErrorCode() . "<br>";
                    $error_message .= "Error Reason: " . $helper->getErrorReason() . "<br>";
                    $error_message .= "Error Description: " . $helper->getErrorDescription();
                    error_out($error_message, 401);
                } else {
                    header('HTTP/1.0 400 Bad Request');
                    echo 'Bad request';
                    var_dump($_GET);
                }
                exit;
            }


            // The OAuth 2.0 client handler helps us manage access tokens
            $oAuth2Client = $fb->getOAuth2Client();

            // Get the access token metadata from /debug_token
            $tokenMetadata = $oAuth2Client->debugToken($accessToken);

            // Validation (these will throw FacebookSDKException's when they fail)
            $tokenMetadata->validateAppId(FACEBOOK_APP_ID);
            // If you know the user ID this access token belongs to, you can validate it here
            //$tokenMetadata->validateUserId('123');
            $tokenMetadata->validateExpiration();

            if (!$accessToken->isLongLived()) {
                // Exchanges a short-lived access token for a long-lived one
                try {
                    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
                    exit;
                }
            }

            $_SESSION['fb_access_token'] = (string)$accessToken;

            // User is logged in with a long-lived access token.
            // You can redirect them to a members-only page.
            //header('Location: https://example.com/members.php');

            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get('/me?fields=first_name,last_name,email', $accessToken);

            $facebook_user = $response->getGraphUser();

        } catch (FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (!isset($facebook_user['email'])) {
            $errors[] = __('Login with facebook account failed because the user does not have an email address', 1);
            $errors[] = '<a href="https://www.facebook.com/help/162801153783275">' . __('How to add a facebook email',
                    1) . '</a>';
            require 'templates/error_template.php';
            exit();
        }

        $email = addslashes($facebook_user['email']);
        $user = get_first("SELECT * FROM users WHERE user_email = '$email'");
        $now = date('Y-m-d H:i:s');

        // User doesn't exist
        if (empty($user['user_id'])) {

            // Generate random password
            $password = User::generateRandomPassword();

            // Insert the user
            $user = array(
                'user_full_name' => $facebook_user['first_name'] . ' ' . $facebook_user['last_name'],
                'user_email' => $facebook_user['email'],
                'password' => $password['hash'],
                'user_created_at' => $now,
                'user_last_visit' => $now
            );

            $user['user_id'] = insert('users', $user);

            $body =
                __('Thank you for choosing Letsticker.com.') . '<br><br>' .
                __('Your email is:') . ' ' . $email . ' ' .
                __('and your password is:') . ' ' . $password['password'] . '<br>' .
                __('You can change your password by clicking Forgot password? in the login window');

            Mail::send($email, __('Letsticker.com registration'), $body);

        } else {
            // User existed, update user's last visit time
            update("users", ['user_last_visit' => $now], "user_email = '$email'");
        }



        // Log the user in
        Session::login($user['user_id']);

        //Load user data and return
        $this->auth->load_user_data($user);

    }


}