<?php
/**
 * Created by PhpStorm.
 * User: rasmu
 * Date: 06.02.2018
 * Time: 11:54
 */

namespace App;


class contact extends Controller
{
    function index()
    {
        $this->error = '';
        $this->subject = '';
        $this->message = '';
        if (!empty($_SESSION['error'])) {
            $this->error = $_SESSION['error']['error'];
            $this->subject = $_SESSION['error']['subject'];
            $this->message = $_SESSION['error']['message'];
            unset($_SESSION['error']);
        }
        $this->upload_max_filesize = ini_get('upload_max_filesize');
    }

    function send()
    {
        $attachments = [];

        // Upload the files if there are any
        if (!empty($_FILES['files']['name'])) for ($i = 0; $i < count($_FILES['files']['name']); $i++) {
            $temp_file_path = $_FILES['files']['tmp_name'][$i];
            $file_name = $_FILES['files']['name'][$i];

            // Make sure we have a file path
            if ($temp_file_path != "") {

                // Setup our new file path
                $new_file_path = dirname(__DIR__, 1) . "/.uploads/contact/$file_name";

                // Move the file to .uploads
                if (!move_uploaded_file($temp_file_path, $new_file_path)) {
                    $errors[] = __('File upload failed', 1);
                    require 'templates/error_template.php';
                    exit();
                }

                $attachments[] = $new_file_path;
            }
        }

        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) || !Mail::hasValidDomain($_POST['email'])) {
            $_SESSION['error']['error'] = __('Invalid email address');
            $_SESSION['error']['subject'] = $_POST['subject'];
            $_SESSION['error']['message'] = $_POST['message'];
            redirect('contact');
        }

        // Send the email
        Mail::send(SITE_OWNER_EMAIL, $_POST['subject'], $_POST['message'], $attachments, null, null, $_POST['email'], $_POST['user_name']);

        // Delete the sent files from our system
        foreach ($attachments as $attachment) {
            unlink($attachment);
        }

        // Redirect to homepage
        redirect('');
    }
}